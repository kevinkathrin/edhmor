import subprocess
import sys
import fileinput
import time
import os

def start_process(path='vrep.exe'):
	os.startfile(path)
	
def update_port(pathToFile='remoteApiConnections.txt', port = 19997, prev = 19997):
	with fileinput.FileInput(pathToFile, inplace=True, backup='.bak') as file:
		for line in file:
			print(line.replace('= ' + str(prev), '= ' + str(port)), end = '')
		
	
if __name__ == '__main__':
	execPath = sys.argv[1]
	txtPath = sys.argv[1]
	startport = int(sys.argv[3])
	threads = int(sys.argv[4])
	cPort = startport
	first = True
	for i in range(threads):
		cPort = i + startport
		print(cPort)
		update_port(port = cPort, prev=startport) if first else update_port(port = cPort, prev =(cPort-1))
		time.sleep(4)
		start_process()
		
	update_port(port=startport, prev=cPort)
	
	
	
	
	