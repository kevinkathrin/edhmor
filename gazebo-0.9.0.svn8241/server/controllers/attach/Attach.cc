/*
 *  Gazebo - Outdoor Multi-Robot Simulator
 *  Copyright (C) 2003
 *     Nate Koenig & Andrew Howard
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
/*
 * Desc: Stubbed out controller
 * Author: Nathan Koenig
 * Date: 01 Feb 2007
 * SVN info: $Id: ControllerStub.cc 4436 2008-03-24 17:42:45Z robotos $
 */

#include "Global.hh"
#include "XMLConfig.hh"
#include "Model.hh"
#include "World.hh"
#include "gazebo.h"
#include "GazeboError.hh"
#include "ControllerFactory.hh"
#include "Body.hh"
#include "SliderJoint.hh"
#include "Attach.hh"


using namespace gazebo;

GZ_REGISTER_STATIC_CONTROLLER("attach", Attach);

////////////////////////////////////////////////////////////////////////////////
// Constructor
Attach::Attach(Entity *parent )
    : Controller(parent)
{
  this->myParent = dynamic_cast<Model*>(this->parent);

  if (!this->myParent)
    gzthrow("Attach controller requires a Model as its parent");
}

////////////////////////////////////////////////////////////////////////////////
// Destructor
Attach::~Attach()
{
}

////////////////////////////////////////////////////////////////////////////////
// Load the controller
void Attach::LoadChild(XMLConfigNode *node)
{
  this->myIface = dynamic_cast<AttachIface*>(this->GetIface("attach"));

  if (!this->myIface)
    gzthrow("Attach controller requires a Attach Iface");

  this->body = (this->myParent->GetCanonicalBody()); 
  if (!this->body)
    gzthrow("couldn't get body body");

  joint = this->myParent->CreateJoint(Joint::SLIDER);
  joint->SetModel(this->myParent);
  joint->Detach();
}

////////////////////////////////////////////////////////////////////////////////
// Initialize the controller
void Attach::InitChild()
{
}

////////////////////////////////////////////////////////////////////////////////
// Update the controller
void Attach::UpdateChild()
{
  this->myIface->Lock(1);

  double cmd = this->myIface->data->cmd_attach;

  Vector3 axis(0.0, 0.0, 1.0);

  if(cmd>0){
    joint->Attach(0, body);
    dynamic_cast<SliderJoint*>(joint)->SetAxis(axis);
    //joint->SetFixed();
  }else{
    joint->Detach();
  }
  //std::cout << "Fai2: Force: UpdateChild: body="<< this->myParent->GetName() <<"\n";
  //std::cout << "Fai2: Force: UpdateChild: body force="<< body->GetForce() <<"\n";
  this->myIface->Unlock();

}

////////////////////////////////////////////////////////////////////////////////
// Finalize the controller
void Attach::FiniChild()
{
}
