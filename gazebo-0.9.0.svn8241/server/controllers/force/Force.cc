/*
 *  Gazebo - Outdoor Multi-Robot Simulator
 *  Copyright (C) 2003
 *     Nate Koenig & Andrew Howard
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
/*
 * Desc: Stubbed out controller
 * Author: Nathan Koenig
 * Date: 01 Feb 2007
 * SVN info: $Id: ControllerStub.cc 4436 2008-03-24 17:42:45Z robotos $
 */

#include "Global.hh"
#include "XMLConfig.hh"
#include "Model.hh"
#include "World.hh"
#include "gazebo.h"
#include "GazeboError.hh"
#include "ControllerFactory.hh"
#include "Body.hh"
#include "Force.hh"


using namespace gazebo;

GZ_REGISTER_STATIC_CONTROLLER("force", Force);

////////////////////////////////////////////////////////////////////////////////
// Constructor
Force::Force(Entity *parent )
    : Controller(parent)
{
  this->myParent = dynamic_cast<Model*>(this->parent);

  if (!this->myParent)
    gzthrow("Force controller requires a Model as its parent");
}

////////////////////////////////////////////////////////////////////////////////
// Destructor
Force::~Force()
{
}

////////////////////////////////////////////////////////////////////////////////
// Load the controller
void Force::LoadChild(XMLConfigNode *node)
{
  this->myIface = dynamic_cast<ForceIface*>(this->GetIface("force"));

  if (!this->myIface)
    gzthrow("Force controller requires a force Iface");

  this->body = (this->myParent->GetCanonicalBody()); 
  if (!this->body)
    gzthrow("couldn't get body body");

}

////////////////////////////////////////////////////////////////////////////////
// Initialize the controller
void Force::InitChild()
{
}

////////////////////////////////////////////////////////////////////////////////
// Update the controller
void Force::UpdateChild()
{
  this->myIface->Lock(1);

  double force_x = this->myIface->data->cmd_force_x;
  double force_y = this->myIface->data->cmd_force_y;
  double force_z = this->myIface->data->cmd_force_z;

  double torque_x = this->myIface->data->cmd_torque_x;
  double torque_y = this->myIface->data->cmd_torque_y;
  double torque_z = this->myIface->data->cmd_torque_z;
  
  Vector3 force(force_x, force_y, force_z);
  Vector3 torque(torque_x, torque_y, torque_z);
  body->SetForce(force);
  body->SetTorque(torque);
  
  this->myIface->Unlock();

}

////////////////////////////////////////////////////////////////////////////////
// Finalize the controller
void Force::FiniChild()
{
}
