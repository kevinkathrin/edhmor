/*
 *  Gazebo - Outdoor Multi-Robot Simulator
 *  Copyright (C) 2003
 *     Nate Koenig & Andrew Howard
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
/*
 * Desc: Stubbed out controller
 * Author: Nathan Koenig
 * Date: 01 Feb 2007
 * SVN info: $Id: ControllerStub.cc 4436 2008-03-24 17:42:45Z robotos $
 */

#include "Global.hh"
#include "XMLConfig.hh"
#include "Model.hh"
#include "World.hh"
#include "gazebo.h"
#include "GazeboError.hh"
#include "ControllerFactory.hh"
#include "Body.hh"
#include "Simulator.hh"
#include "Spring.hh"


using namespace gazebo;

GZ_REGISTER_STATIC_CONTROLLER("spring", Spring);

////////////////////////////////////////////////////////////////////////////////
// Constructor
Spring::Spring(Entity *parent )
    : Controller(parent)
{
  this->myParent = dynamic_cast<Model*>(this->parent);

  if (!this->myParent)
    gzthrow("Spring controller requires a Model as its parent");
}

////////////////////////////////////////////////////////////////////////////////
// Destructor
Spring::~Spring()
{
}

////////////////////////////////////////////////////////////////////////////////
// Load the controller
void Spring::LoadChild(XMLConfigNode *node)
{
  this->myIface = dynamic_cast<SpringIface*>(this->GetIface("spring"));

  if (!this->myIface)
    gzthrow("Spring controller requires a Spring Iface");


  /*
  this->body = (this->myParent->GetCanonicalBody()); 
  if (!this->body)
    gzthrow("couldn't get body body");
  */
}

////////////////////////////////////////////////////////////////////////////////
// Initialize the controller
void Spring::InitChild()
{
  std::string name1, name2;
  name1="hinge_actuador_body_0";
  name2="hinge_actuador_body_6";

  models = gazebo::World::Instance()->GetModels();

  std::vector< Model* >::iterator miter;
  
  for (miter = this->models.begin(); miter != this->models.end(); miter++){
    if (*miter){
//      std::cout<<"modelo valido, size: "<< models.size() << ", time: " << Simulator::Instance()->GetSimTime()<<"\n";
      if(body2==NULL){
        body2 = (*miter)->GetBody(name2);
      }
      if(body1==NULL){
        body1 = (*miter)->GetBody(name1);
      } 
    }
//    std::cout<<"modelo INvalido \n";
    if(body1!=NULL && body2!=NULL)
      break;
  }
  if(body1==NULL)
    gzthrow("Spring controller requires a valid body1 name");
  if(body2==NULL)
    gzthrow("Spring controller requires a valid body2 name");

  AddResort();
}

////////////////////////////////////////////////////////////////////////////////
// Update the controller
void Spring::UpdateChild()
{
  AddResort();

}

////////////////////////////////////////////////////////////////////////////////
// Finalize the controller
void Spring::FiniChild()
{
}

////////////////////////////////////////////////////////////////////////////////
// Calcula la fuerza y la aplica sobre los cuerpos
void Spring::AddResort(){

  this->myIface->Lock(1);

  double kMuelle = this->myIface->data->kMuelle;
  double lDamp = this->myIface->data->lDamp;
  double kMuelleAng = this->myIface->data->kMuelleAng;
  double lDampAng = this->myIface->data->lDampAng;

  Vector3 relPos1(this->myIface->data->face1_x ,this->myIface->data->face1_y ,this->myIface->data->face1_z );
  Vector3 relPos2(this->myIface->data->face2_x ,this->myIface->data->face2_y ,this->myIface->data->face2_z );
  
  Vector3 nRelPos1(this->myIface->data->nFace1_x, this->myIface->data->nFace1_y, this->myIface->data->nFace1_z);
  Vector3 nRelPos2(this->myIface->data->nFace2_x, this->myIface->data->nFace2_y, this->myIface->data->nFace1_z);

  Vector3 posBody1(0,0,0), posBody2(0,0,0), dist(0,0,0);
  Vector3 nPos1(0,0,0), nPos2(0,0,0), velBody1(0,0,0), velBody2(0,0,0), vel(0,0,0);

  posBody1=body1->GetRelPointPos(relPos1);
  posBody2=body2->GetRelPointPos(relPos2);
  velBody1=body1->GetRelPointVel(relPos1);
  velBody2=body2->GetRelPointVel(relPos2);
  nPos1=body1->GetBodyVectorToWorld(nRelPos1);
  nPos2=body2->GetBodyVectorToWorld(nRelPos2);


  Vector3 position1, position2;
  position1 = body1->GetPosition();
  position2 = body2->GetPosition();

//  std::cout<<"Posicion body 1: "<< position1 <<"\nPosicion body2: "<< position2 << "\n";
//  std::cout<<"Posicion cara body 1: "<< posBody1 <<"\nPosicion cara body2: "<< posBody2 << "\n";
//  std::cout<<"UpdateRate: "<< this->updatePeriod << ", time: " << Simulator::Instance()->GetSimTime()<<"\n";

  dist.x=posBody1.x-posBody2.x;
  dist.y=posBody1.y-posBody2.y;
  dist.z=posBody1.z-posBody2.z;
  vel.x=velBody1.x-velBody2.x;
  vel.y=velBody1.y-velBody2.y;
  vel.z=velBody1.z-velBody2.z;

  ///Calculamos los angulos de cada vector normal
  //nPos2.x=(-1)*nPos2.x; nPos2.y=(-1)*nPos2.y; nPos2.z=(-1)*nPos2.z; 
  double a1, a2, b1, b2, c1, c2, a, b, c;
  double n1 =sqrt(pow(nPos1.x,2) + pow(nPos1.y,2) + pow(nPos1.z,2));
  double n2 =sqrt(pow(nPos2.x,2) + pow(nPos2.y,2) + pow(nPos2.z,2));

  double proyeccion=sqrt(pow(nPos1.y,2) + pow(nPos1.z,2));
  if(proyeccion>0){
    a1=acos(nPos1.y/proyeccion);
    if(nPos1.z<0)
      a1=2*M_PI-a1;
  }else{
    a1=0;
  }
  a1=proyeccion/n1*a1;

  proyeccion=sqrt(pow(nPos2.y,2) + pow(nPos2.z,2));
  if(proyeccion>0){
    a2=acos(nPos2.y/proyeccion);
    if(nPos2.z<0)
      a2=2*M_PI-a2;
  }else{
    a2=0;
  }
  a2=proyeccion/n2*a2;  

  //Calculo de b
  proyeccion=sqrt(pow(nPos1.x,2) + pow(nPos1.z,2));
  if(proyeccion>0){
    b1=acos(nPos1.x/proyeccion);
    if(nPos1.z<0)
      b1=2*M_PI-b1;
  }else{
    b1=0;
  }
  b1=proyeccion/n1*b1;

  proyeccion=sqrt(pow(nPos2.x,2) + pow(nPos2.z,2));
  if(proyeccion>0){
    b2=acos(nPos2.x/proyeccion);
    if(nPos2.z<0)
      b2=2*M_PI-b2;
  }else{
    b2=0;
  }
  b2=proyeccion/n2*b2;

  //Calculo de c
  proyeccion=sqrt(pow(nPos1.x,2) + pow(nPos1.y,2));
  if(proyeccion>0){
    c1=acos(nPos1.x/proyeccion);
    if(nPos1.y<0)
      c1=2*M_PI-c1;
  }else{
    c1=0;
  }
  c1=proyeccion/n1*c1;

  proyeccion=sqrt(pow(nPos2.x,2) + pow(nPos2.y,2));
  if(proyeccion>0){
    c2=acos(nPos2.x/proyeccion);
    if(nPos2.y<0)
      c2=2*M_PI-c2;
  }else{
    c2=0;
  }
  c2=proyeccion/n2*c2;

  a=a1-a2; b=b1-b2; c=c1-c2;
  //Los transformamos de -pi a pi
  if(a>M_PI)
    a-=2*M_PI;
  if(a<(-1)*M_PI)
    a+=2*M_PI;
  if(b>M_PI)
    b-=2*M_PI;
  if(b<(-1)*M_PI)
    b+=2*M_PI;
  if(c>M_PI)
    c-=2*M_PI;
  if(c<(-1)*M_PI)
    c+=2*M_PI;

  //std::cout<<"Time: " << Simulator::Instance()->GetSimTime()<<"\n";
  //std::cout<<"angulos1:"<< a1 << ", " << b1 << ", " << c1<<"\n";  
  //std::cout<<"angulos2:"<< a2 << ", " << b2 << ", " << c2<<"\n";
  //std::cout<<"angulos:"<< a << ", " << b << ", " << c<<"\n";


  Vector3 torqueResort(kMuelleAng*a, kMuelleAng*b, kMuelleAng*c);
  Vector3 torqueDamp(0, 0, 0);

  Vector3 torque1((-1)*torqueResort.x + (-1)*torqueDamp.x, (-1)*torqueResort.y + (-1)*torqueDamp.y, (-1)*torqueResort.z + (-1)*torqueDamp.z);
  Vector3 torque2(torqueResort.x + torqueDamp.x, torqueResort.y + torqueDamp.y, torqueResort.z + torqueDamp.z);

  double distancia=sqrt(pow(dist.x,2) + pow(dist.y,2) + pow(dist.z,2));
  double velocidad=sqrt(pow(vel.x,2) + pow(vel.y,2) + pow(vel.z,2));


//  std::cout<<"vel body 1: "<< velBody1 <<", vel body2: "<< velBody2 << ", vel : "<< vel <<"\n";
//  std::cout<<"Fuerza X: "<< (-1)*kMuelle*dist.x <<", : "<< (-1)*lDamp*vel.x << "\n";
  //std::cout<<"Fuerza Y: "<< (-1)*kMuelle*dist.y <<", : "<< (-1)*lDamp*vel.y << "\n";
  //std::cout<<"Fuerza Z: "<< (-1)*kMuelle*dist.z <<", : "<< (-1)*lDamp*vel.z << "\n";
  Vector3 forceMuelle(kMuelle*dist.x, kMuelle*dist.y, kMuelle*dist.z);
  Vector3 forceDamp(lDamp*vel.x, lDamp*vel.y, lDamp*vel.z); 

  Vector3 force1((-1)*forceMuelle.x + (-1)*forceDamp.x, (-1)*forceMuelle.y + (-1)*forceDamp.y, (-1)*forceMuelle.z+ (-1)*forceDamp.z);

  Vector3 force2(forceMuelle.x + forceDamp.x, forceMuelle.y + forceDamp.y, forceMuelle.z + forceDamp.z);


  //std::cout<<"Fuerzas: "<< force1 <<",  "<< force2 << "\n";
  body1->AddForceAtRelPos(force1, relPos1);
  body2->AddForceAtRelPos(force2, relPos2);
  
  if(distancia<0.25){
    body1->AddTorque(torque1);
    body2->AddTorque(torque2);
  }


  ///Devolvemos la informacion por la interfaz
  this->myIface->data->resortF_x=forceMuelle.x;
  this->myIface->data->resortF_y=forceMuelle.y;
  this->myIface->data->resortF_z=forceMuelle.z;

  this->myIface->data->dampF_x=forceDamp.x;
  this->myIface->data->dampF_y=forceDamp.y;
  this->myIface->data->dampF_z=forceDamp.z;

  this->myIface->data->resortT_x=torqueResort.x;
  this->myIface->data->resortT_y=torqueResort.y;
  this->myIface->data->resortT_z=torqueResort.z;

  this->myIface->data->dampT_x=torqueDamp.x;
  this->myIface->data->dampT_y=torqueDamp.y;
  this->myIface->data->dampT_z=torqueDamp.z;

  this->myIface->data->a=a;  this->myIface->data->b=b;  this->myIface->data->c=c;
  this->myIface->data->a1=a1;  this->myIface->data->b1=b1;  this->myIface->data->c1=c1;
  this->myIface->data->a2=a2;  this->myIface->data->b2=b2;  this->myIface->data->c2=c2;

  this->myIface->data->distancia=distancia;
  this->myIface->data->velocidad=velocidad;



  this->myIface->Unlock();

}
