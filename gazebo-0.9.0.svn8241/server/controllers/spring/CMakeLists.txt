include (${gazebo_cmake_dir}/GazeboUtils.cmake)

SET (sources Spring.cc)
SET (headers Spring.hh)

APPEND_TO_SERVER_SOURCES(${sources})
APPEND_TO_SERVER_HEADERS(${headers})

