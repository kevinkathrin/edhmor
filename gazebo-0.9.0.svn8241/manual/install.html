
/**
@page install Installation (Linux)

Gazebo relies on a number of third-party libraries that make
installation a little bit tricky.  If things go wrong, please check
the @ref install_problems section and the archives of the Gazebo <a
href="http://sourceforge.net/mailarchive/forum.php?forum_id=33909">mailing
list</a>.  Please read the instructions below @b carefully before
reporting posting to the mailing list.

<b> These are the Linux installation instructions, OS X users should
go @ref install_osx "here". </b>


@section install_prepare Preparing your system

The the GUI component of Gazebo requires two third-party packages that
may not be installed on your system; if you plan to use the GUI,
install these packages before proceeding:

- <a href="http://www.swig.org">SWIG</a> (Simplified Wrapper and
Interface Generator).

- <a href="http://www.wxpython.org">wxPython</a> (Python bindings for
<a href="http://www.wxwidgets.org">wxWidgets</a>).

The default install path for many source packages (Gazebo included) is:
@verbatim
/usr/local
/usr/local/bin
/usr/local/include
/usr/local/lib
@endverbatim

In some Linux distributions, however, these paths are not searched by
default, leading to problems when compiling and linking some packages.
We therefore recommend that you configure your system with some
additional paths (added to your .bashrc script, for example):

@verbatim
export PATH=/usr/local/bin:$PATH
export CPATH=/usr/local/include:$CPATH
export LIBRARY_PATH=/usr/local/lib:$LIBRARY_PATH
@endverbatim

The first line sets the executable path; the second sets the path for
C and C++ header files; the third line sets the library search path.
You will also need to set two more paths:

@verbatim
export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig:$PKG_CONFIG_PATH
export PYTHONPATH=/usr/local/lib/python2.3/site-packages:$PYTHONPATH
@endverbatim 

The first line sets the pkg-config path (a neat utility for mananging
source dependencies); the second line is for Python extensions (the
version number must match the version of Python you have installed;
type "python -V" if in doubt).


@section install_choice Selecting an installation method

First-time users should read the section on @ref install_minimal "minimal installations" 
to get a basic version of Gazebo up and
running.  More advanced users may wish to consult one of the
following:

- @ref install_minimal
- @ref install_full
- @ref install_local



@section install_minimal Minimal installation 

The minimal Gazebo build requires the <a
href="http://ode.org">OpenDynamicsEngine</a> (ODE), which is very
probably @b not installed on your system (or not installed correctly).

- Install ODE 0.5:
@verbatim
$ tar xvzf ode-0.5.tgz
$ cd ode-0.5
$ make configure
$ make ode-lib
$ su
$ cp -r include/ode /usr/local/include/
$ cp lib/libode.a /usr/local/lib/
$ exit
@endverbatim
Note that the @c ranlib step is only necessary for OS X installations.
If in doubt, you should also read the INSTALL file that comes with the
ODE distribution (but don't necessarily believe everything it tells
you).

- Install Gazebo:
@verbatim
$ tar xvzf gazebo-<version>.tar.gz
$ cd gazebo-<version>
$ ./configure
@endverbatim
Note the final output of the configure script: it will tell you
what will be built, and where it will be installed.
Once you are satisfied, build and install the package:
@verbatim
$ make
$ su
$ make install
$ exit
@endverbatim

Gazebo is now ready to run; try:
@verbatim
$ gazebo /usr/local/share/gazebo/worlds/example1.world
@endverbatim

This will start the server in console mode (no GUI).  Assuming you
have all the GUI dependencies installed, you can run in graphical mode
using:

@verbatim
$ wxgazebo /usr/local/share/gazebo/worlds/example1.world
@endverbatim





@section install_full Full installation (with OPCODE support)

Some models (e.g., terrains and roads) require the OPCODE collision
detection library (now part of the ODE distribution).  The following
steps are required to build ODE with OPCODE support.

- Install (or re-install) ODE with OPCODE enabled; this
requires some editing of the ODE configuration files:
@verbatim
$ tar xvzf ode-0.5.tgz
$ cd ode-0.5
@endverbatim
Open the file <tt>config/user-settings</tt>, and change
the default <tt>OPCODE_DIRECTORY</tt>:
@verbatim
OPCODE_DIRECTORY=[srcdir]/OPCODE/
@endverbatim
where <tt>[srcdir]</tt> is the fully qualified path to the
ODE source distribution.  I.e., if the original ODE tarball
is in <tt>/home/[username]/tmp</tt>, this should read
<tt>/home/[username]/tmp/ode-0.5/OPCODE/</tt>.
Note that the comments for this section of the settings file are @b
bogus; ignore them.  Finish installing ODE:
@verbatim
$ make configure
$ make ode-lib
$ su
$ cp -r include/ode /usr/local/include/
$ cp lib/libode.a /usr/local/lib/
$ exit
@endverbatim

- Install (or re-install) Gazebo
@verbatim
$ tar xvzf gazebo-src-<version>.tar.gz
$ cd gazebo-src-<version>
$ ./configure
@endverbatim
Pay close attention output of the configure script and make sure the
Terrain and Road models will be built. Once you are satisfied,
finish the installation:
@verbatim
$ make
$ su
$ make install
$ exit
@endverbatim

To test the terrain model, try:
@verbatim
$ gazebo /usr/local/share/gazebo/worlds/terrain.world
@endverbatim



@section install_gdal GDAL installation (Terrain Builder)

The builder "terrain builder utility" included in the Gazebo
package uses the <a
href="http://www.remotesensing.org/gdal/">Geospatial Data Abstraction
Library</a> (GDAL) for loading images, digital elevation maps, and so
on.  If you plan to use this utility, you will need to install GDAL
first.

The GDAL package is available in many Linux distros (including
Gentoo), and can be installed in the standard fashion.  If the package
is not available, it must be built from source, as follows.

- Download the <a href="http://www.remotesensing.org/gdal/">Geospatial
  Data Abstraction Library</a> (GDAL):
@verbatim
$ tar xvzg gdal-1.2.1.tar.gz
$ cd gdal-1.2.1
$ ./configure --without-python
$ make
$ su
$ make install
$ exit
@endverbatim
The GDAL installer may not set up require links for dynamic libraries, so
you may have to do this yourself.  For example, if GDAL-1.1 is installed in 
/usr/local/lib:
@verbatim
$ su
$ ln -s /usr/local/lib/libgdal.1.1.so /usr/local/lib/libgdal.so
$ ldconfig
$ exit
@endverbatim
Depending on your system configuration, you may also need to adjust
your library path so that the linker can find the newly installed files.
@verbatim
$ su
$ echo /usr/local/lib >> /etc/ld.so.conf
$ ldconfig
$ exit
@endverbatim


@section install_local Local installation (does not require root access)

Some developers prefer to install Gazebo in our home directory (e.g.,
/home/[username]/local) rather than in a system directory.  This is
useful if you are working on shared machines and/or lack root access.
Naturally, local installs can make it a bit tricky for Gazebo (and
other packages) to find the right headers, libs and so on.  Here,
then, is the recommended way to do it:
 
- Pick a spot for "local" installs; for me it is
 "/home/[username]/local".  The install scripts will create relevant
 subdirs under this, such as:
@verbatim
/home/[username]/local/bin
/home/[username]/local/include
/home/[username]/local/lib
@endverbatim
 
- Set up the necessary compiler paths in your .bashrc (or whatever)
  script; e.g.:
@verbatim
export PATH=~/local/bin:$PATH
export CPATH=~/local/include:$CPATH
export LIBRARY_PATH=~/local/lib:$LIBRARY_PATH
@endverbatim
The first line sets the executable path; the second sets the path for C
and C++ header files; the third line sets the library search path.
 
- Set up some additional paths in your .bashrc (or whatever):
@verbatim
export PKG_CONFIG_PATH=~/local/lib/pkgconfig:$PKG_CONFIG_PATH
export PYTHONPATH=~/local/lib/python2.3/site-packages:$PYTHONPATH
@endverbatim 
The first line sets the pkg-config path (for applications using
pkg-config, which will be everything in the Player/Stage/Gazebo
project pretty soon); the second line is for Python extensions (the
version number should match the version of Python you have installed;
type "python -V" if in doubt).

- (Optional) Install ODE locally:
@verbatim
$ tar xvzf ode-0.5.tgz
$ cd ode-0.5
$ make configure
$ make ode-lib
$ cp -r include/ode /home/[username]/local/include/
$ cp lib/libode.a /home/[username]/local/lib/
@endverbatim

- Build Gazebo using the "--prefix" argument:
@verbatim
$ ./configure --prefix=/home/[username]/local
@endverbatim
Note the final output of the configure script: it should tell
you that Gazebo will be installed in your home directory.

- Build and install the package:
@verbatim
$ make
$ make install
@endverbatim
 
Everything should now work seamlessly, and your locally installed
packages will be used in preference to any system-wide defaults.


@section install_problems Problem-solving

@subsection install_gdal Cannot find gdal

If you are getting a linker error "cannot find -lgdal", check you
ld.so.conf settings as described above.

@subsection install_missing Missing dependencies

Occasionally, you may find that certain third-party libraries are not
installed on you system; the <tt>configure</tt> script will let you
know what libraries are missing, from which you can figure out what
additional packages need to be installed.

All of the packages below should be available with your distribution
(this includes OS X, either from Apple, or via <a
ref="http://fink.sourceforge.net">Fink</a>).

- GL, GLU, GLUT
- libxml2 
- zlib

*/
