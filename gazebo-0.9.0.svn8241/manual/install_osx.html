
/**
@page install_osx Installation (OS X)

Gazebo relies on a number of third-party libraries that make
installation a little bit tricky.  If things go wrong, please check
the install_os_problems section and the archives of the Gazebo <a
href="http://sourceforge.net/mailarchive/forum.php?forum_id=33909">mailing
list</a>.  Please read the instructions below @b carefully before
posting to the mailing lists.

<b> These are the OS X installation instructions, Linux users should
go @ref install "here". </b>


@section install_osx_prepare Preparing your system

The OS X installation is a little tricker than the Linux installation,
partly because of the large number of dependencies that need to be
installed, and partly because of the quirky file system layout.  As a
first step, you must prepare your system:

- Install Apple's developer tools and X11 developer packages; these
come standard with the OS X installation disks.

- Install <a href="http://fink.sourceforge.net/">Fink</a>.

- Use Fink to install additional packages, such as:
  - libxml2
  - glut

- The GUI component of Gazebo requires two packages:
   - <a href="http://www.swig.org">SWIG</a> (Simplified Wrapper and
     Interface Generator).
   - <a href="http://www.wxpython.org">wxPython</a> (Python bindings
     for <a href="http://www.wxwidgets.org">wxWidgets</a>).

- The terrain builder utility requires the <a
href="http://www.remotesensing.org/gdal/">Geospatial Data Abstraction
Library</a> (GDAL)

To save some time, there is a shell script that will @c apt-get
pre-compiled versions on the required packages (thanks to Lars
Cremean).  Look on the packages page:

- http://playerstage.sourceforge.net/doc/packages/

Fink installs everything in a rather unusual location (@c /sw) and you
must your paths approppriately; if you are using the bash shell, add
the following lines to your .bashrc script:

@verbatim
export CPATH=/usr/X11R6/include:/sw/include:$CPATH
export LIBRARY_PATH=/usr/X11R6/lib:/sw/lib:$LIBRARY_PATH
export PKG_CONFIG_PATH=/sw/lib:$PKG_CONFIG_PATH
export PYTHONPATH=/sw/lib/python2.3/site-packages:$PYTHONPATH
@endverbatim

The last line sets the path for Python extensions (the version number
should match the version of Python you have installed; type "python
-V" if in doubt).

You should also set the following compiler flag:

@verbatim
export CPPFLAGS=-no-cpp-precomp 
@endverbatim

This prevents some common bugs with Apple's "smart" pre-compiler.


@section install_osx_choice Selecting an installation method

irst-time users should read the section on @ref install_osx_minimal "minimal installations" 
to get a basic version of Gazebo up and running.  More advanced users may 
wish to consult one of the following:

- @ref install_osx_minimal
- @ref install_osx_full
- @ref install_osx_local


@section install_osx_minimal Minimal installation 

The minimal Gazebo build requires the <a
href="http://ode.org">OpenDynamicsEngine</a> (ODE), which is very
probably @b not installed on your system (or not installed correctly).

- Install ODE 0.5:
@verbatim
$ tar xvzf ode-0.5.tgz
$ cd ode-0.5
$ make configure
$ make ode-lib
$ sudo cp -r include/ode /sw/include/
$ sudo cp lib/libode.a /sw/lib/
$ sudo ranlib /sw/lib/libode.a
@endverbatim
If in doubt, you should also read the INSTALL file that comes with the
ODE distribution (but don't necessarily believe everything it tells
you).

- Install Gazebo:
@verbatim
$ tar xvzf gazebo-<version>.tar.gz
$ cd gazebo-<version>
$ ./configure --prefix=/sw
@endverbatim
Note the final output of the configure script: it will tell you
what will be built, and where it will be installed.
Once you are satisfied, build and install the package:
@verbatim
$ make
$ sudo make install
@endverbatim

Gazebo is now ready to run; try:
@verbatim
$ gazebo /sw/share/gazebo/worlds/example1.world
@endverbatim

This will start the server in console mode (no GUI).  Assuming you
have all the GUI dependencies installed, you can run in graphical mode
using:

@verbatim
$ wxgazebo /usr/local/share/gazebo/worlds/example1.world
@endverbatim


@section install_osx_full Full installation (with OPCODE support)

Some models (e.g., terrains and roads) require the OPCODE collision
detection library (now part of the ODE distribution).  The following
steps are required to build ODE with OPCODE support.

- Install (or re-install) ODE with OPCODE enabled; this
requires some editing of the ODE configuration files:
@verbatim
$ tar xvzf ode-0.5.tgz
$ cd ode-0.5
@endverbatim
Open the file <tt>config/user-settings</tt>, and change
the default <tt>OPCODE_DIRECTORY</tt>:
@verbatim
OPCODE_DIRECTORY=[srcdir]/OPCODE/
@endverbatim
where <tt>[srcdir]</tt> is the fully qualified path to the
ODE source distribution.  I.e., if the original ODE tarball
is in <tt>/home/[username]/tmp</tt>, this should read
<tt>/home/[username]/tmp/ode-0.5/OPCODE/</tt>.
Note that the comments for this section of the settings file are @b
bogus; ignore them.  Finish installing ODE:
@verbatim
$ make configure
$ make ode-lib
$ sudo cp -r include/ode /sw/include/
$ sudo cp lib/libode.a /sw/lib/
$ sudo ranlib /sw/lib/libode.a
@endverbatim

- Install (or re-install) Gazebo
@verbatim
$ tar xvzf gazebo-src-<version>.tar.gz
$ cd gazebo-src-<version>
$ ./configure --prefix=/sw
@endverbatim
Pay close attention output of the configure script and make sure the
Terrain and Road models will be built. Once you are satisfied,
finish the installation:
@verbatim
$ make
$ sudo make install
$ exit
@endverbatim

To test the terrain model, try:
@verbatim
$ gazebo /sw/share/gazebo/worlds/terrain.world
@endverbatim


@section install_osx_local Local installation (does not require root access)

Some developers prefer to install Gazebo in our home directory (e.g.,
/home/[username]/local) rather than in a system directory.  This is
useful if you are working on shared machines and/or lack root access.
Naturally, local installs can make it a bit tricky for Gazebo (and
other packages) to find the right headers, libs and so on.  Here,
then, is the recommended way to do it:
 
- Pick a spot for "local" installs; for me it is
 "/home/[username]/local".  The install scripts will create relevant
 subdirs under this, such as:
@verbatim
/home/[username]/local/bin
/home/[username]/local/include
/home/[username]/local/lib
@endverbatim
 
- Set up the necessary compiler paths in your .bashrc (or whatever)
  script; e.g.:
@verbatim
export PATH=~/local/bin:$PATH
export CPATH=~/local/include:$CPATH
export LIBRARY_PATH=~/local/lib:$LIBRARY_PATH
@endverbatim
The first line sets the executable path; the second sets the path for C
and C++ header files; the third line sets the library search path.
 
- Set up some additional paths in your .bashrc (or whatever):
@verbatim
export PKG_CONFIG_PATH=~/local/lib/pkgconfig:$PKG_CONFIG_PATH
export PYTHONPATH=~/local/lib/python2.3/site-packages:$PYTHONPATH
@endverbatim 
The first line sets the pkg-config path (for applications using
pkg-config, which will be everything in the Player/Stage/Gazebo
project pretty soon); the second line is for Python extensions (the
version number should match the version of Python you have installed;
type "python -V" if in doubt).

- (Optional) Install ODE locally:
@verbatim
$ tar xvzf ode-0.5.tgz
$ cd ode-0.5
$ make configure
$ make ode-lib
$ cp -r include/ode /home/[username]/local/include/
$ cp lib/libode.a /home/[username]/local/lib/
$ ranlib /home/[username]/local/lib/libode.a
@endverbatim

- Build Gazebo using the "--prefix" argument:
@verbatim
$ ./configure --prefix=/home/[username]/local
@endverbatim
Note the final output of the configure script: it should tell
you that Gazebo will be installed in your home directory.

- Build and install the package :
@verbatim
$ make
$ make install
@endverbatim
 
Everything should now work seamlessly, and your locally installed
packages will be used in preference to any system-wide defaults.



@section install_osx_problems Problem-solving

TODO

*/
