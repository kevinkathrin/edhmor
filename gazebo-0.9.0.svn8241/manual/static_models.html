
/** @page static_models HOWTO: Creating a Static Model

The are two distinct type of models in Gazebo:

- <i>Static</i> models have their code in the main Gazebo
distribution, and are statically linked into the server.  Generally
speaking, such models will be added by the lead developers.

- <i>Plugin</i> models are shared objects that are loaded at runtime
(like loadable modules in the Linux kernel). They are the recommended
method for all new, experimental or third party models.  Instructions
for building plugin modules can be found @ref plugin_models "here".

This page describes the basic steps for creating a static model; it
describes how the source files should be laid out, how to register
models with the server and how to work with GNU Autotools.

<b>N.B. These instructions assume you are working from CVS, not a
source snap-shot or distribution.</b>

For details of how to write the @e contents of the model, read @ref
plugin_models.  Developers are also advised to read @ref standards
before adding their model to the respository (lest I come wading
through the source tree with my big red pen).


@section static_models_files Model Source Files

Source code for models is located under <tt>server/models/</tt>, with
a separate directory for each model.  Model directory names should
match model class names, i.e., the <tt>ExampleModel</tt> model is found
the <tt> server/models/ExampleModel/</tt> directory. Layout of files
within the model directory is at the developers discretion; by
convention, however, models are comprised of a single header file
containing the model class declarations, and one or more source files
containing the class definitions.  Thus, the <tt>ExampleModel</tt> model
is comprised of <tt>ExampleModel.hh</tt> and <tt>ExampleModel.cc</tt>.

The recommended way to create a new model is to copy an existing model
with similar functionality, and perform some judicious
search-and-replacing.



@section static_models_register Registering the Model

Models must be registered with the server.  Registration is handled by
the <tt>ModelFactory</tt> class, which can be found in
<tt>server/ModelFactory.cc</tt>.  

Static model registration is handled by a macro defined in
ModelFactory.hh:

@verbatim
GZ_REGISTER_STATIC("ExampleModel", ExampleModel)
@endverbatim

The first argument gives the model name as it appears in the world
file; the second argument gives the class name.

Note that static and dynamic registration macros can co-exist in the
same source file, but only one of them will be used.


@section static_models_gnu Working with GNU Autotools

Gazebo uses GNU Autotools to managing the build process; while
Autotools can be daunting for newcomers, the rewards are well worth
the effort.  When using Autotools, there are two key notions to bear
in mind:

- Project-level configuration is controlled by <tt>configure.in</tt>
found in the project top-level directory.

- Module-level configuration is controlled by <tt>Makefile.am</tt>
found in every sub-directory.

These configuration files are used to <i>generate</i> the <tt>
Makefile</tt>'s that will ultimately control the build process
(developers should never manipulate <tt>Makefile</tt>'s directly).

The basic process for adding a new model to the Autotools setup is as
follows.

- Create <tt>Makefile.am</tt> for the new model:
  - Copy <tt>Makefile.am</tt> from another model into the new model
  directory.
  - Modify all entries describing the model name.  For example, if
  you copied <tt>Makefile.am</tt> from the <tt>Pioneer2AT</tt> model, replace
  all references to <tt>pioneer2at</tt> with <tt>examplemodel</tt>.
  - Modify the <tt>SOURCES</tt> line to list the new model's source files.
  Be sure to include header files in this list (so they will included in the
  final distribution).
  
- Modify <tt>Makefile.am</tt> in the <tt>server/models</tt> directory.
  - Add the new model directory to the <tt>SUBDIRS</tt> line.
  
- Modify <tt>configure.in</tt> in the top-level directory.  
  - In the ``model tests'' section, add a <tt>GAZEBO_ADD_MODEL</tt>
    entry for the new model.  The arguments are: model name (camel
    caps), model path, whether or not the model should be included in
    the default build, a list of libraries to check for and a list of
    headers to check for.  The model will not be built unless all of
    the library and header dependencies are satisfied.
  - In the ``create makefiles'' section, add the path of the <tt>
    Makefile</tt> that needs to be created.
  
- Re-generate the <tt>Makefile</tt>'s:
  - From the top-level directory, run:
  @verbatim
  $ autoreconf
  $ ./configure
  @endverbatim
  with whatever arguments you would normally pass to <tt>configure</tt>.

Run <tt>make</tt> to build and link the new driver.

*/