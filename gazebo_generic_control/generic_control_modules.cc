

/*Ejemplo de prueba para mover el actuador hinge axial
 *
 * Compilacion:
 * g++ -Wall -g3 `pkg-config --cflags libgazeboServer` -c -o generic_control.o generic_control.cc
 * g++ generic_control.o -o generic_control `pkg-config --libs libgazeboServer`
 *
 */


#include <gazebo/gazebo.h>
#include <gazebo/GazeboError.hh>
#include "generic_control.hh"
#include <math.h>
#include <string>


#define N_MODULOS_MAX 16
#define PERIODO 2
#define AMPLITUD_SLIDER 0.3
#define AMPLITUD_TELESCOPE 0.4
#define AMPLITUD_HINGE_AXIAL 3.1415*0.5
#define AMPLITUD_HINGE 3.1415*0.5
#define AMPLITUD_SNAKE 3.1415*3/6

int n_base=0, n_slider=0, n_telescope=0, n_hinge_axial=0, n_hinge=0, n_snake=0;

//Funciones auxiliares
int connectGazeboServer(gazebo::Client *client, int serverId);
int connectSimIface(gazebo::Client *client, gazebo::SimulationIface *simIface);
void inicializacionInterfaces(gazebo::ActarrayIface *base_actIface[],
        gazebo::ActarrayIface *slider_actIface[],
        gazebo::ActarrayIface *telescope_actIface[],
        gazebo::ActarrayIface *hinge_axial_actIface[],
        gazebo::ActarrayIface *hinge_actIface[],
        gazebo::ActarrayIface *snake_actIface[]);
int openIfaces(gazebo::Client *client,
        gazebo::ActarrayIface *base_actIface[],
        gazebo::ActarrayIface *slider_actIface[],
        gazebo::ActarrayIface *telescope_actIface[],
        gazebo::ActarrayIface *hinge_axial_actIface[],
        gazebo::ActarrayIface *hinge_actIface[],
        gazebo::ActarrayIface *snake_actIface[]);
int closeIfaces(gazebo::ActarrayIface *base_actIface[],
        gazebo::ActarrayIface *slider_actIface[],
        gazebo::ActarrayIface *telescope_actIface[],
        gazebo::ActarrayIface *hinge_axial_actIface[],
        gazebo::ActarrayIface *hinge_actIface[],
        gazebo::ActarrayIface *snake_actIface[]);
int closeSimIface(gazebo::SimulationIface *simIface);
int disconnectClient(gazebo::Client *client);



/**********     Analisis de los modulos     **********/
void analisisTipoModulos(int n_modulos, int *tipo_m){
    n_base=0, n_slider=0, n_telescope=0, n_hinge_axial=0, n_hinge=0;
    for(int i=0; i<n_modulos; i++){
        if(tipo_m[i]==1)
            n_slider++;
        if(tipo_m[i]==2)
            n_telescope++;
        if(tipo_m[i]==3)
            n_hinge_axial++;
        if(tipo_m[i]==4)
            n_hinge++;
        if(tipo_m[i]==5)
            n_snake++;
    }
    std::cout << "N_Modulos: " << n_modulos << "\n";
    std::cout << "N_Slider: " << n_slider << "\n";
    std::cout << "N_Telescope: " << n_telescope << "\n";
    std::cout << "N_Hinge_Axial: " << n_hinge_axial << "\n";
    std::cout << "N_Hinge: " << n_hinge << "\n";
    
}



/**********     Espera a tiempo de iniciar la sim     **********/
int waitToStartTime(gazebo::SimulationIface *simIface, int time_start_sim){
    double time=simIface->data->simTime;
    int count=0;
    while(time<time_start_sim){
        //std::cout<< "Esperando a iniciar: "<< simIface->data->simTime << "\n";
        if((time_start_sim-time)>1){
            usleep(50);
        }else{
            if((time_start_sim-time)>0.5){
                usleep(10);
            }else{
                usleep(5);
            }
        }
        if(time==simIface->data->simTime){
            count++;
            //usleep(10000);
        }else{
            count=0;
        }
        if(count>150000){
            std::cout<< "Gazebo a dejado de actualizar datos durante la espera a llegar al time_start_sim, time:"<< time << " count: "<< count<< "\n";
            return -1;
        }
        
        time=simIface->data->simTime;
    }
    return 0;
}

/**********    Set Posicion Actuadores     **********/
void setPosicionActuadores(gazebo::ActarrayIface *base_actIface[],
        gazebo::ActarrayIface *slider_actIface[],
        gazebo::ActarrayIface *telescope_actIface[],
        gazebo::ActarrayIface *hinge_axial_actIface[],
        gazebo::ActarrayIface *hinge_actIface[],
        gazebo::ActarrayIface *snake_actIface[],
        double param[],
        double time){
    int j=0;
    
    for(int i=0; i<n_slider; i++)
        slider_actIface[i]->Lock(1);
    for(int i=0; i<n_telescope; i++)
        telescope_actIface[i]->Lock(1);
    for(int i=0; i<n_hinge_axial; i++)
        hinge_axial_actIface[i]->Lock(1);
    for(int i=0; i<n_hinge; i++)
        hinge_actIface[i]->Lock(1);
    for(int i=0; i<n_snake; i++)
        snake_actIface[i]->Lock(1);
    
    for(int i=0; i<n_slider; i++){
        slider_actIface[i]->data->cmd_pos[0]=AMPLITUD_SLIDER * sin(PERIODO * time + param[j++]);
    }
    
    for(int i=0; i<n_telescope; i++){
        telescope_actIface[i]->data->cmd_pos[0]=fabs(AMPLITUD_TELESCOPE * sin(0.5*PERIODO * time + param[j++]));
    }
    
    for(int i=0; i<n_hinge_axial; i++){
        hinge_axial_actIface[i]->data->cmd_pos[0]= AMPLITUD_HINGE_AXIAL * sin(PERIODO * time + param[j++]);
    }
    
    for(int i=0; i<n_hinge; i++){
        hinge_actIface[i]->data->cmd_pos[0] = AMPLITUD_HINGE * sin(PERIODO * time + param[j++]);
    }
    for(int i=0; i<n_snake; i++){
        snake_actIface[i]->data->cmd_pos[0] = AMPLITUD_SNAKE * sin(PERIODO * time + param[j++]);
    }
    
    
    for(int i=0; i<n_slider; i++)
        slider_actIface[i]->Unlock();
    for(int i=0; i<n_telescope; i++)
        telescope_actIface[i]->Unlock();
    for(int i=0; i<n_hinge_axial; i++)
        hinge_axial_actIface[i]->Unlock();
    for(int i=0; i<n_hinge; i++)
        hinge_actIface[i]->Unlock();
    for(int i=0; i<n_snake; i++)
        snake_actIface[i]->Unlock();
}

/******************************************************************************/
/*************************     Estabiliza el robot    *************************/
/******************************************************************************/
int estabilizaRobot(gazebo::SimulationIface *simIface,
        gazebo::SimulationRequestData *responses,
        gazebo::ActarrayIface *base_actIface[],
        gazebo::ActarrayIface *slider_actIface[],
        gazebo::ActarrayIface *telescope_actIface[],
        gazebo::ActarrayIface *hinge_axial_actIface[],
        gazebo::ActarrayIface *hinge_actIface[],
        gazebo::ActarrayIface *snake_actIface[],
        double param[],
        double stepTime){
    double error=100, errorMax, pose_x, pose_y, pose_z, pose_x0, pose_y0, pose_z0, timeStart;
    double roll_0, pitch_0, yaw_0, roll, pitch, yaw;
    
    pose_x0=100;
    pose_y0=100;
    pose_z0=100;
    roll_0=100;
    pitch_0=100;
    yaw_0=0;
    double time=0;
    int count = 0;
    timeStart=simIface->data->simTime;
    simIface->data->controlTime+=stepTime;
    errorMax=0.000001;
    
    std::cout<< "Inicio del bucle de estabilizacion, timeStart: "<< timeStart<<"\n";
    std::cout<< "Param: "<< param[0] << param[1] << param[2] << param[3] << param[4] << param[5] << param[6] <<  "\n";
    
    while(error>errorMax){
        simIface->Lock(1);
        pose_x=responses->modelPose.pos.x;
        pose_y=responses->modelPose.pos.y;
        pose_z=responses->modelPose.pos.z;
        roll=responses->modelPose.roll;
        pitch=responses->modelPose.pitch;
        yaw=responses->modelPose.yaw;
        simIface->Unlock();
        if(time>0.000001){
            std::cout <<"Estabilizando, simTime: " << simIface->data->simTime << "\n";
            std::cout << "Posicion anterior: "<< pose_x0 << " " << pose_y0 << " " << pose_z0 << "\n";
            std::cout << "Posicion actual: "<< pose_x << " " << pose_y << " " << pose_z << "\n";
            std::cout << "Posicion (rot) anterior: "<< roll_0 << " " << pitch_0 << " " << yaw_0 << "\n";
            std::cout << "Posicion (rot) actual: "<< roll << " " << pitch << " " << yaw << "\n";
            std::cout<< "Estabilizando el sistema: error = "<<error<<"\n";
            
            std::cout<< "Estabilizando el sistema: error = "<<error<<", Tiempo Real: "<< simIface->data->simTime << "\n\n\n";
            
            //Si no ha aumentado el tiempo no actualizo
            error= pow(pose_x0-pose_x, 2)+pow(pose_y0-pose_y, 2)+pow(pose_z0-pose_z, 2)
                    +pow(roll_0-roll, 2)+pow(pitch_0-pitch, 2)+pow(yaw_0-yaw, 2);
            simIface->Lock(1);
            pose_x0=responses->modelPose.pos.x;
            pose_y0=responses->modelPose.pos.y;
            pose_z0=responses->modelPose.pos.z;
            roll_0=responses->modelPose.roll;
            pitch_0=responses->modelPose.pitch;
            yaw_0=responses->modelPose.yaw;
            simIface->Unlock();
            
            setPosicionActuadores(base_actIface,
                    slider_actIface,
                    telescope_actIface,
                    hinge_axial_actIface,
                    hinge_actIface,
                    snake_actIface,
                    param,
                    0);
            
            
            //std::cout<< "Estabilizando el sistema, Tiempo = "<<time<< " Tiempo total: " << timeStart << "\n\n";
            simIface->data->controlTime+=stepTime;
            count=0;
        }else{/*
         * if(count>150000){
         * //El gazebo ha dejado de actualizar datos y damos la evaluacion por terminada
         * std::cerr << "Gazebo error: Gazebo ha dejado de actualizar los datos en la estabilizacion, datos sin actualizar " << count << " iteraciones\n";
         * std::cerr << "Gazebo error: Tiempo de simulacion:" << simIface->data->simTime << " \n";
         * return -1;
         * }*/
            count++;
        }
        
        
        
        if(time>10){
            //Alta probabilidad de que el sistema sea inestable
            if(time>15){
                std::cout << "time >15\n";
                errorMax=0.0001;
            }else{
                std::cout << "time >10 y <15\n";
                errorMax=0.00001;
            }
        }
        
        simIface->Lock(1);
        simIface->data->requestCount=1;
        simIface->Unlock();
        
        usleep(100);
        simIface->Lock(1);
        time=(simIface->data->simTime-timeStart);
        timeStart=simIface->data->simTime;
        simIface->Unlock();
    }///Fin del bucle de la estabilizacion
    
    while(simIface->data->simTime < simIface->data->controlTime){
        std::cout << "Saliendo del bucle de estabilizacion"<<"\n";
        usleep(100);
    }
    
    std::cout << "Sistema estable, empezamos simulacion: error = " << error <<" time= "<< time<<" tiempo de simulacion: " << simIface->data->simTime << "\n";
    return 0;
}
/******************************************************************************/
/*******************************     EVALUA     *******************************/
/******************************************************************************/
int evalua(int serverId, int n_ciclos_sim, double stepTime, int n_modulos, int * tipo_m, int * control_m, double &calidad){
    
    gazebo::Client *client = new gazebo::Client();
    gazebo::SimulationIface *simIface = new gazebo::SimulationIface();
    gazebo::ActarrayIface *base_actIface[N_MODULOS_MAX];
    gazebo::ActarrayIface *slider_actIface[N_MODULOS_MAX];
    gazebo::ActarrayIface *telescope_actIface[N_MODULOS_MAX];
    gazebo::ActarrayIface *hinge_axial_actIface[N_MODULOS_MAX];
    gazebo::ActarrayIface *hinge_actIface[N_MODULOS_MAX];
    gazebo::ActarrayIface *snake_actIface[N_MODULOS_MAX];
    
    analisisTipoModulos(n_modulos, tipo_m);
    
    std::cout <<  "\n\n";
    
    ///Inicializacion de las interfaces
    inicializacionInterfaces(base_actIface,
            slider_actIface,
            telescope_actIface,
            hinge_axial_actIface,
            hinge_actIface,
            snake_actIface);
    
    /// Connect to the libgazebo server
    if (connectGazeboServer(client, serverId)<0)
        return -1;
    
    /// Connect to the simIface
    if (connectSimIface(client, simIface)<0)
        return -1;
    
    
    int count=0;
    char modelName[512];
    
    if(tipo_m[0]==0)
        memcpy(modelName, "base0", 512);
    if(tipo_m[0]==1)
        memcpy(modelName, "slider0", 512);
    if(tipo_m[0]==2)
        memcpy(modelName, "telescope0", 512);
    if(tipo_m[0]==3)
        memcpy(modelName, "hinge_axial0", 512);
    if(tipo_m[0]==4)
        memcpy(modelName, "hinge0", 512);
    if(tipo_m[0]==5)
        memcpy(modelName, "snake0", 512);
    std::cout<<"TipoModel: "<<tipo_m[0]<<"  name: "<< modelName << "\n";
    
    //Pedimos la posicion inicial del modulo 0
    simIface->Lock(1);
    gazebo::SimulationRequestData *requestPos = &(simIface->data->requests[simIface->data->requestCount++]);
    gazebo::SimulationRequestData *responses = &(simIface->data->responses[0]);
    simIface->data->requestCount=1;
    requestPos->type = gazebo::SimulationRequestData::GET_POSE3D;
    memcpy(requestPos->modelName, modelName, 512);
    simIface->data->controlTime+=stepTime;
    simIface->Unlock();
    
    while(true){
        simIface->Lock(1);
        if(simIface->data->simTime==stepTime){
            simIface->Unlock();
            break;
        }else{
            simIface->Unlock();
            usleep(10);
        }
    }
    
    
    /* Open the ActArrays interfaces */
    if(openIfaces(client,
            base_actIface,
            slider_actIface,
            telescope_actIface,
            hinge_axial_actIface,
            hinge_actIface,
            snake_actIface)<0)
        return -1;
    
    /* Inicializacion de variables */
    double distRecorrida=0;
    double param[16];
    
    for(int i=0; i<n_modulos; i++){
        param[i]=(double) control_m[i]/180*3.1415;
    }
    
    
//      std::cout<< "Conexiones abiertas: "<< simIface->GetOpenCount() << "\n";
//      std::cout<< "Espera del programa, time:"<< simIface->data->simTime << "\n";
    
    
    
    std::cout<< "Empezamos el control, tiempo de simulacion: "<< simIface->data->simTime << "\n";
    std::cout << "Pos Inicial: x=" << responses->modelPose.pos.x << ", y=" << responses->modelPose.pos.y<< ", z=" << responses->modelPose.pos.z<< "\n";
    
    
    /*Lo situamos en la posicion inicial de sus actuadores*/
    setPosicionActuadores(base_actIface,
            slider_actIface,
            telescope_actIface,
            hinge_axial_actIface,
            hinge_actIface,
            snake_actIface,
            param,
            0);
    
    
    
//   while(1){
//         simIface->Lock(1);
//         std::cout <<  "Inicio: \n";
//         std::cout << "resquestCount: "<< simIface->data->requestCount << "\n";
//         std::cout << "responseCount: " << simIface->data->responseCount << "\n";
//
//         gazebo::SimulationRequestData *responses1 = &(simIface->data->responses[1]);
//         std::cout << responses1->modelPose.pos.x << "\n";
//         std::cout <<  "\n\n\n";
//         simIface->Unlock();
//         usleep(1000);
//     }
    
    
    
    
    /*
     * simIface->Lock(1);
     * simIface->data->requestCount=1;
     * requestPos->type = gazebo::SimulationRequestData::GET_POSE3D;
     * memcpy(requestPos->modelName, modelName, 512);
     * simIface->data->controlTime+=stepTime;
     * simIface->Unlock();
     */
    
    
    /* Estabilizacion */
//    if(estabilizaRobot(simIface, responses, base_actIface,
//            slider_actIface, telescope_actIface,
//            hinge_axial_actIface, hinge_actIface, param, stepTime)<0)
//        return -1;
    
    simIface->Lock(1);
    double pose_x0=responses->modelPose.pos.x;
    double pose_y0=responses->modelPose.pos.y;
    double pose_z0=responses->modelPose.pos.z;
    simIface->Unlock();
    simIface->data->controlTime+=stepTime;
    
    std::cout << "Pos Estabilizado: x=" << pose_x0 << ", y=" << pose_y0 << ", z=" << pose_z0 << "\n";
    
    count=0;
    double timeStart=simIface->data->simTime;
    double time=0, timeOld=0, timeControl=0;
    
    
    /**** Bucle Principal de la simulacion ***/
    while (time<n_ciclos_sim*2*3.1415) {
        
        if(time>(timeOld+timeControl)){
            std::cout << time << timeStart << "\n";
            
            setPosicionActuadores(base_actIface,
                    slider_actIface,
                    telescope_actIface,
                    hinge_axial_actIface,
                    hinge_actIface,
                    snake_actIface,
                    param,
                    time);
            
            
//       std::cout << "timeStart: " << timeStart << "   simTime: " << time<< "\n";
//       std::cout << "Modulo 0: " << responses->modelPose.pos.x << "  " << responses->modelPose.pos.y <<
//              "  " << responses->modelPose.pos.z << "\n";
            
            simIface->Lock(1);
            simIface->data->requestCount=1;
//            request->type = gazebo::SimulationRequestData::GET_POSE3D;
//            memcpy(request->modelName, modelName, 512);
            simIface->Unlock();
            
            count=0;
            timeOld=time;
            simIface->data->controlTime+=stepTime;
            
        }else{
            if(count>150000){
                //El gazebo ha dejado de actualizar datos y damos la evaluacion por terminada
                //std::cerr << "Gazebo error: Gazebo ha dejado de actualizar los datos en plena simulacion, datos sin actualizar en" << count << " iteraciones\n";
                //return -1;
            }
            count++;
        }
        
        
        //usleep(100);
        simIface->Lock(1);
        time=(simIface->data->simTime-timeStart);
        simIface->Unlock();
    }/* Fin Bucle principal */
    
    std::cout << "simTime "<< simIface->data->simTime << "controlTime "<< simIface->data->controlTime <<"\n";
//    while(simIface->data->simTime < simIface->data->controlTime){
//        std::cout << "Bucle "<<"\n";
//        usleep(100);
//    }
    
    while(true){
        simIface->Lock(1);
        if(simIface->data->simTime < simIface->data->controlTime){
            simIface->Unlock();
            usleep(10);
        }else{
            simIface->Unlock();
            break;
        }
    }
    
    double pose_x=responses->modelPose.pos.x;
    double pose_y=responses->modelPose.pos.y;
    double pose_z=responses->modelPose.pos.z;
    
    
    std::cout << "Pos Final: x=" << pose_x << ", y=" << pose_y << ", z=" << pose_z << "\n";
    distRecorrida=pose_x-pose_x0;
    std::cout << "distRecorrida: " << distRecorrida << "\n";
    calidad=distRecorrida;
    
    /* Close the ActArrays interfaces */
    if(closeIfaces(base_actIface,
            slider_actIface,
            telescope_actIface,
            hinge_axial_actIface,
            hinge_actIface,
            snake_actIface)<0)
        return -1;
    
    /* Vamos a cerrar el gazebo */
    std::cout << "señal de cerrar el gazebo" << "\n";
    simIface->data->quit=1;
    simIface->data->controlTime+=stepTime;
    usleep(10);
    
    /* Close the Simulation Interface */
    if(closeSimIface(simIface)<0)
        return -1;
    
    /* Disconnect to the libgazebo server */
    if(disconnectClient(client)<0)
        return -1;
    
    
    
    return 0;
}






int main(int argc, char *argv[]) {
    int tipo_modulos[N_MODULOS_MAX];
    int param[N_MODULOS_MAX];
    
    int n_modulos=(int)(argc-1)/2;
    double calidad=0;
    
    
    for(int i=0; i<n_modulos; i++){
        tipo_modulos[i]=atoi(argv[i+1]);
        param[i]=atoi(argv[n_modulos+1+i]);
    }
    
    std::cout << "N_modulos (main): " << n_modulos<< "\n";
    //std::cout << "Param (main): " << param[0] <<" "<<param[1]<<" "<<param[2]<<" "<<param[3]<< "\n";
    
    int st=evalua(0, 3, 4, n_modulos, tipo_modulos, param, calidad);
    
    std::cout << "Calidad: " << calidad<< "\n\n";
    
    return st;
    
}

















/********** Conectarse al simulador     **********/
int connectGazeboServer(gazebo::Client *client, int serverId){
    try {
        client->Connect(serverId);
    }
    catch (std::string e) {
        std::cout << "Gazebo error: Unable to connect\n" << e << "\n";
        return -1;
    }
    return 0;
}

/**********    Abre la interfaz de simulacion     **********/
int connectSimIface(gazebo::Client *client, gazebo::SimulationIface *simIface){
    /// Open the Simulation Interface
    try {
        simIface->Open(client, "default");
    }
    catch (std::string e) {
        std::cout << "Gazebo error: Unable to connect to the sim interface\n" << e << "\n";
        //Desconect Gazebo Server
        return -1;
    }
    return 0;
}

/**********     Inicialización de las interfaces     **********/
void inicializacionInterfaces(gazebo::ActarrayIface *base_actIface[],
        gazebo::ActarrayIface *slider_actIface[],
        gazebo::ActarrayIface *telescope_actIface[],
        gazebo::ActarrayIface *hinge_axial_actIface[],
        gazebo::ActarrayIface *hinge_actIface[],
        gazebo::ActarrayIface *snake_actIface[]){
    
    ///Inicializacion de las interfaces
    for(int i=0; i<n_base; i++){
        base_actIface[i*4]= new gazebo::ActarrayIface();
        base_actIface[i*4+1]= new gazebo::ActarrayIface();
        base_actIface[i*4+2]= new gazebo::ActarrayIface();
        base_actIface[i*4+3]= new gazebo::ActarrayIface();
    }
    for(int i=0; i<n_slider; i++){
        slider_actIface[i]= new gazebo::ActarrayIface();
    }
    for(int i=0; i<n_telescope; i++){
        telescope_actIface[i]= new gazebo::ActarrayIface();
    }
    for(int i=0; i<n_hinge_axial; i++){
        hinge_axial_actIface[i]= new gazebo::ActarrayIface();
    }
    for(int i=0; i<n_hinge; i++){
        hinge_actIface[i]= new gazebo::ActarrayIface();
    }
    for(int i=0; i<n_snake; i++){
        snake_actIface[i]= new gazebo::ActarrayIface();
    }
}

/**********     Open the ActArrays interfaces     **********/
int openIfaces(gazebo::Client *client,
        gazebo::ActarrayIface *base_actIface[],
        gazebo::ActarrayIface *slider_actIface[],
        gazebo::ActarrayIface *telescope_actIface[],
        gazebo::ActarrayIface *hinge_axial_actIface[],
        gazebo::ActarrayIface *hinge_actIface[],
        gazebo::ActarrayIface *snake_actIface[]){
    try {
        for(int i=0; i<n_slider; i++){
            char name[50];
            sprintf(name, "slider_actarray_iface_%d", i);
            //std::cout << name << "\n";
            slider_actIface[i]->Open(client, name);
        }
    }
    catch (std::string e) {
        std::cout << "Gazebo error: Unable to connect to the slider actarray interface\n"
        << e << "\n";
        return -1;
    }
    try {
        for(int i=0; i<n_telescope; i++){
            char name[50];
            sprintf(name, "telescope_actarray_iface_%d", i);
            telescope_actIface[i]->Open(client, name);
        }
    }
    catch (std::string e) {
        std::cout << "Gazebo error: Unable to connect to the telescope actarray interface\n"
        << e << "\n";
        return -1;
    }
    try {
        for(int i=0; i<n_hinge_axial; i++){
            char name[50];
            sprintf(name, "hinge_axial_actarray_iface_%d", i);
            hinge_axial_actIface[i]->Open(client, name);
        }
    }
    catch (std::string e) {
        std::cout << "Gazebo error: Unable to connect to the hinge_axial actarray interface\n"
        << e << "\n";
        return -1;
    }
    try {
        for(int i=0; i<n_hinge; i++){
            char name[50];
            sprintf(name, "hinge_actarray_iface_%d", i);
            hinge_actIface[i]->Open(client, name);
        }
    }
    catch (std::string e) {
        std::cout << "Gazebo error: Unable to connect to the hinge actarray interface\n"
        << e << "\n";
        return -1;
    }
    try {
        for(int i=0; i<n_snake; i++){
            char name[50];
            sprintf(name, "snake_actarray_iface_%d", i);
            snake_actIface[i]->Open(client, name);
        }
    }
    catch (std::string e) {
        std::cout << "Gazebo error: Unable to connect to the hinge actarray interface\n"
        << e << "\n";
        return -1;
    }
    
    /*
    try {
        for(int i=0; i<n_metamodulo; i++){
            char name[50];
            sprintf(name, "metamodule_slider0_actarray_iface_%d", i);
            slider_actIface[i]->Open(client, name);
            sprintf(name, "metamodule_slider1_actarray_iface_%d", i);
            slider_actIface[i]->Open(client, name);
            sprintf(name, "metamodule_slider2_actarray_iface_%d", i);
            slider_actIface[i]->Open(client, name);
            sprintf(name, "metamodule_slider3_actarray_iface_%d", i);
            slider_actIface[i]->Open(client, name);
        }
    }
    catch (std::string e) {
        std::cout << "Gazebo error: Unable to connect to the actarray interface\n"
        << e << "\n";
        return -1;
    }*/
    
    return 0;
}

/**********     Close the ActArrays interfaces     **********/
int closeIfaces(gazebo::ActarrayIface *base_actIface[],
        gazebo::ActarrayIface *slider_actIface[],
        gazebo::ActarrayIface *telescope_actIface[],
        gazebo::ActarrayIface *hinge_axial_actIface[],
        gazebo::ActarrayIface *hinge_actIface[],
        gazebo::ActarrayIface *snake_actIface[]){
    try {
        for(int i=0; i<n_slider; i++){
            char name[50];
            sprintf(name, "slider_actarray_iface_%d", i);
            //std::cout << name << "\n";
            slider_actIface[i]->Close();
        }
        for(int i=0; i<n_telescope; i++){
            char name[50];
            sprintf(name, "telescope_actarray_iface_%d", i);
            telescope_actIface[i]->Close();
        }
        for(int i=0; i<n_hinge_axial; i++){
            char name[50];
            sprintf(name, "hinge_axial_actarray_iface_%d", i);
            hinge_axial_actIface[i]->Close();
        }
        for(int i=0; i<n_hinge; i++){
            char name[50];
            sprintf(name, "hinge_actarray_iface_%d", i);
            hinge_actIface[i]->Close();
        }
        for(int i=0; i<n_snake; i++){
            char name[50];
            sprintf(name, "snake_actarray_iface_%d", i);
            snake_actIface[i]->Close();
        }
    }
    catch (std::string e) {
        std::cerr << "Gazebo error: Unable to close the actarray interface\n"
        << e << "\n";
        return -1;
    }
    return 0;
}

/**********     Cierra la interface de simulacion     **********/
int closeSimIface(gazebo::SimulationIface *simIface){
    try {
        simIface->Close();
        return 0;
    }
    catch (gazebo::GazeboError e) {
        std::cerr << "Gazebo error: Unable to close the sim interface\n" << e << "\n";
        return -1;
    }
}

/**********     Se desconencta del simulador     **********/
int disconnectClient(gazebo::Client *client){
    try {
        client->Disconnect();
        return 0;
    }
    catch (gazebo::GazeboError e) {
        std::cerr << "Gazebo error: Unable to disconnect\n" << e << "\n";
        return -1;
    }
}
