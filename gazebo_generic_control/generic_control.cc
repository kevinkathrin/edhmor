

/*Ejemplo de prueba para mover el actuador hinge axial
 *
 * Compilacion:
 * g++ -Wall -g3 `pkg-config --cflags libgazeboServer` -c -o generic_control.o generic_control.cc
 * g++ generic_control.o -o generic_control `pkg-config --libs libgazeboServer`
 *
 */


#include <gazebo/gazebo.h>
#include <gazebo/GazeboError.hh>
#include "generic_control.hh"
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>


#define N_MODULOS_MAX 16
#define FREQANG 2.0
#define AMPLITUD_SLIDER 0.3
#define AMPLITUD_TELESCOPE 0.4
#define AMPLITUD_HINGE_AXIAL 3.1415*0.5
#define AMPLITUD_HINGE 3.1415*1/3
#define AMPLITUD_SNAKE 3.1415*3/6
#define GAZEBO_ACTARRAY_ACTSTATE_IDLE     1
#define GAZEBO_ACTARRAY_ACTSTATE_MOVING   2

#define FREQANGMAX_WITHFREQMOD 2.0
#define FREQANGMIN_WITHFREQMOD 0.0
#define FREQANGMAX FREQANG

#define DIMEN_PARED_Y 1
#define DIMEN_PARED_Z 1
#define ALTURA_START_PARED 2.0
#define DIMEN_SUELO_X 4
#define DIMEN_SUELO_Y 4
#define RADIO_LIMPIEZA 0.1
#define RADIO_NO_LIMPIEZA 0.4

#define FIRST_ITER 3.1415*2
int n_base = 0, n_slider = 0, n_telescope = 0, n_hinge_axial = 0, n_hinge = 0, n_effector = 0, n_snake = 0;

char modelName[512], cargaName[512];
char effectorName[N_MODULOS_MAX][512];

gazebo::Pose pose, cargaPose;
gazebo::Pose effectorPose[N_MODULOS_MAX];

//Funciones auxiliares
int connectGazeboServer(gazebo::Client *client, int serverId);
int connectSimIface(gazebo::Client *client, gazebo::SimulationIface *simIface);
int connectGraphicsIface(gazebo::Client *client, gazebo::Graphics3dIface *g3dIface);
void pintaPared(gazebo::Graphics3dIface *g3dIface, double fitnessParameter);
void pintaSuelo(gazebo::Graphics3dIface *g3dIface);
void pintaZona(gazebo::Graphics3dIface *g3dIface, int indiceHorizontal,
        int indiceAltura, double pintaWall[DIMEN_PARED_Y * 10][DIMEN_PARED_Z * 10], double fitnessParameter);
void pintaZonaSuelo(gazebo::Graphics3dIface *g3dIface, bool showCleanFloor, double pintaSuelo[DIMEN_SUELO_X * 10][DIMEN_SUELO_Y * 10], gazebo::Pose pose);
void inicializacionInterfaces(gazebo::ActarrayIface *base_actIface[],
        gazebo::ActarrayIface *slider_actIface[],
        gazebo::ActarrayIface *telescope_actIface[],
        gazebo::ActarrayIface *hinge_axial_actIface[],
        gazebo::ActarrayIface *hinge_actIface[],
        gazebo::ActarrayIface *snake_actIface[]);
int openIfaces(gazebo::Client *client,
        gazebo::ActarrayIface *base_actIface[],
        gazebo::ActarrayIface *slider_actIface[],
        gazebo::ActarrayIface *telescope_actIface[],
        gazebo::ActarrayIface *hinge_axial_actIface[],
        gazebo::ActarrayIface *hinge_actIface[],
        gazebo::ActarrayIface *snake_actIface[]);
int closeIfaces(gazebo::ActarrayIface *base_actIface[],
        gazebo::ActarrayIface *slider_actIface[],
        gazebo::ActarrayIface *telescope_actIface[],
        gazebo::ActarrayIface *hinge_axial_actIface[],
        gazebo::ActarrayIface *hinge_actIface[],
        gazebo::ActarrayIface *snake_actIface[]);
int closeSimIface(gazebo::SimulationIface *simIface);
int disconnectClient(gazebo::Client *client);
int closeAll(gazebo::ActarrayIface *base_actIface[],
        gazebo::ActarrayIface *slider_actIface[],
        gazebo::ActarrayIface *telescope_actIface[],
        gazebo::ActarrayIface *hinge_axial_actIface[],
        gazebo::ActarrayIface *hinge_actIface[],
        gazebo::ActarrayIface *snake_actIface[],
        gazebo::SimulationIface *simIface,
        gazebo::Client *client,
        double stp);

/**********     Analisis de los modulos     **********/
void analisisTipoModulos(int n_modulos, int *tipo_m) {
    n_base = 0, n_slider = 0, n_telescope = 0, n_hinge_axial = 0, n_hinge = 0, n_effector = 0, n_snake = 0;
    for (int i = 0; i < n_modulos; i++) {
        if (tipo_m[i] == 0)
            n_base++;
        if (tipo_m[i] == 1)
            n_slider++;
        if (tipo_m[i] == 2)
            n_telescope++;
        if (tipo_m[i] == 3)
            n_hinge_axial++;
        if (tipo_m[i] == 4)
            n_hinge++;
        if (tipo_m[i] == 5)
            n_effector++;
        if (tipo_m[i] == 6)
            n_snake++;
    }
    /*
    std::cout << "N_Modulos: " << n_modulos << "\n";
    std::cout << "N_Slider: " << n_slider << "\n";
    std::cout << "N_Telescope: " << n_telescope << "\n";
    std::cout << "N_Hinge_Axial: " << n_hinge_axial << "\n";
    std::cout << "N_Hinge: " << n_hinge << "\n";
     */

    memcpy(cargaName, "carga", 512);

    if (tipo_m[0] == 0)
        memcpy(modelName, "base0", 512);
    if (tipo_m[0] == 1)
        memcpy(modelName, "slider0", 512);
    if (tipo_m[0] == 2)
        memcpy(modelName, "telescope0", 512);
    if (tipo_m[0] == 3)
        memcpy(modelName, "hinge_axial0", 512);
    if (tipo_m[0] == 4)
        memcpy(modelName, "hinge_x0", 512);
    if (tipo_m[0] == 5)
        memcpy(modelName, "snake0", 512);
    //std::cout << "SERVER_GENERIC_CONTROL: " << serverId << "TipoModel: " << tipo_m[0] << "  name: " << modelName << "\n";

    for (int i = 0; i < n_effector; i++) {
        sprintf(effectorName[i], "effector%d", i);
    }
}

/**********    Set State Actuadores     **********/
void setEstadoActuadores(gazebo::ActarrayIface *base_actIface[],
        gazebo::ActarrayIface *slider_actIface[],
        gazebo::ActarrayIface *telescope_actIface[],
        gazebo::ActarrayIface *hinge_axial_actIface[],
        gazebo::ActarrayIface *hinge_actIface[],
        gazebo::ActarrayIface *snake_actIface[],
        int cmd_state) {


    for (int i = 0; i < n_slider; i++)
        slider_actIface[i]->Lock(1);
    for (int i = 0; i < n_telescope; i++)
        telescope_actIface[i]->Lock(1);
    for (int i = 0; i < n_hinge_axial; i++)
        hinge_axial_actIface[i]->Lock(1);
    for (int i = 0; i < n_hinge; i++)
        hinge_actIface[i]->Lock(1);
    for (int i = 0; i < n_snake; i++)
        snake_actIface[i]->Lock(1);

    for (int i = 0; i < n_slider; i++) {
        slider_actIface[i]->data->actuators[0].state = cmd_state;
    }

    for (int i = 0; i < n_telescope; i++) {
        telescope_actIface[i]->data->actuators[0].state = cmd_state;
    }

    for (int i = 0; i < n_hinge_axial; i++) {
        hinge_axial_actIface[i]->data->actuators[0].state = cmd_state;
    }

    for (int i = 0; i < n_hinge; i++) {
        hinge_actIface[i]->data->actuators[0].state = cmd_state;
    }
    for (int i = 0; i < n_snake; i++) {
        snake_actIface[i]->data->actuators[0].state = cmd_state;
    }


    for (int i = 0; i < n_slider; i++)
        slider_actIface[i]->Unlock();
    for (int i = 0; i < n_telescope; i++)
        telescope_actIface[i]->Unlock();
    for (int i = 0; i < n_hinge_axial; i++)
        hinge_axial_actIface[i]->Unlock();
    for (int i = 0; i < n_hinge; i++)
        hinge_actIface[i]->Unlock();
    for (int i = 0; i < n_snake; i++)
        snake_actIface[i]->Unlock();
}

void updatePoses(gazebo::SimulationIface *simIface, bool useCarga, bool usePoseEffectors) {

    simIface->GetPose3d(modelName, pose);
    if (useCarga)
        simIface->GetPose3d(cargaName, cargaPose);
    if (usePoseEffectors) {
        for (int i = 0; i < n_effector; i++) {
            simIface->GetPose3d(effectorName[i], effectorPose[i]);
        }
    }
}

double checkAmplitud(double amplitudModulo, double amplitudMax) {
    if (amplitudModulo > amplitudMax) {
        return amplitudMax;
    } else {
        if (amplitudModulo < 0) {
            return 0;
        } else {
            return amplitudModulo;
        }
    }
}

double checkFreqAngular(double freqModulo, bool useFreqMod) {
    if (useFreqMod) {
        if (freqModulo > FREQANGMAX_WITHFREQMOD)
            freqModulo = FREQANGMAX_WITHFREQMOD;
        else if (freqModulo < FREQANGMIN_WITHFREQMOD)
            freqModulo = FREQANGMIN_WITHFREQMOD;
    } else {
        if (freqModulo > FREQANGMAX)
            freqModulo = FREQANGMAX;
        else if (freqModulo < 0)
            freqModulo = 0;
    }
    return freqModulo;
}

void ajustaParametrosControl(int n_modulos, double* ampControl, double* angFreqControl,
        int* phaseControlDeg, double* phaseControlRad, double* ampMod, double* frequencyMod,
        bool useAmpControl, bool useAngFreqControl, bool usePhaseControl, bool useAmpModulation, bool useFreqModulation) {

    //Si no hay modulacion de la ampitud ponemos a uno esta variable
    if (!useAmpControl) {
        for (int i = 0; i < n_modulos; i++)
            ampControl[i] = 1.0;
    }

    //Si no hay modulacion de la ampitud ponemos a uno esta variable
    if (!useAngFreqControl) {
        for (int i = 0; i < n_modulos; i++)
            angFreqControl[i] = 1.0;
    }

    //Si no hay modulacion de la ampitud ponemos a cero esta variable
    if (!usePhaseControl) {
        for (int i = 0; i < n_modulos; i++)
            phaseControlDeg[i] = 0;
    }//.. y pasamos a radianes la phase de control
    for (int i = 0; i < n_modulos; i++) {
        phaseControlRad[i] = (double) phaseControlDeg[i] / 180 * 3.1415;
    }

    //Si no hay modulacion de la ampitud ponemos a cero esta variable
    if (!useAmpModulation) {
        for (int i = 0; i < n_modulos; i++)
            ampMod[i] = 0.0;
    }

    //Si no hay modulacion de la frecuencia ponemos a cero esta variable
    if (!useFreqModulation) {
        for (int i = 0; i < n_modulos; i++)
            frequencyMod[i] = 0.0;
    }
}

/**********    Set Posicion Actuadores     **********/
void setPosicionActuadores(gazebo::ActarrayIface *base_actIface[],
        gazebo::ActarrayIface *slider_actIface[],
        gazebo::ActarrayIface *telescope_actIface[],
        gazebo::ActarrayIface *hinge_axial_actIface[],
        gazebo::ActarrayIface *hinge_actIface[],
        gazebo::ActarrayIface *snake_actIface[],
        double amplitud[], double freqAngular[], double phase[],
        double ampMod[], double freqMod[],
        bool useAmpCtrl, bool useAngFreqCtrl, bool usePhaseCtrl, bool useAmpMod, bool useFrqMod,
        double sensor,
        double time) {
    int j = 0;
    //    if(n_base>0){
    //        j=1;
    //    }

    double amplitudModulo = 0;
    double freqAngularModulo = 0;

    //std::cout << "Set Posicion actauadores: sensor= " << sensor << ";\n";

    for (int i = 0; i < n_slider; i++)
        slider_actIface[i]->Lock(1);
    for (int i = 0; i < n_telescope; i++)
        telescope_actIface[i]->Lock(1);
    for (int i = 0; i < n_hinge_axial; i++)
        hinge_axial_actIface[i]->Lock(1);
    for (int i = 0; i < n_hinge; i++)
        hinge_actIface[i]->Lock(1);
    for (int i = 0; i < n_snake; i++)
        snake_actIface[i]->Lock(1);
    //    std::cout<< "Posicion actuadores: Slider: ";

    for (int i = 0; i < n_slider; i++) {
        amplitudModulo = AMPLITUD_SLIDER * amplitud[j] + ampMod[j] * sensor * AMPLITUD_SLIDER;
        amplitudModulo = checkAmplitud(amplitudModulo, AMPLITUD_SLIDER);
        freqAngularModulo = FREQANG * freqAngular[j] + freqMod[j] * sensor * FREQANG;
        freqAngularModulo = checkFreqAngular(freqAngularModulo, useFrqMod);
        //std::cout << "Slider" << i << ", j=" << j << ", amplitudCtrl: " << (double) amplitud[j] << ", freqCtrl: " << (double) freqAngular[j] << "; \n";
        slider_actIface[i]->data->cmd_pos[0] = amplitudModulo * sin(freqAngularModulo * time + phase[j++]);
        //        std::cout<< slider_actIface[i]->data->cmd_pos[0]<< ", "<< slider_actIface[i]->data->actuators[0].position<< "; ";
        //std::cout << "Slider" << i << ": freqMod= " << freqMod[j] << ", periodo=  " << periodo << "\n";
    }
    //    std::cout<< "\nPosicion actuadores: Telescope: ";
    for (int i = 0; i < n_telescope; i++) {
        amplitudModulo = AMPLITUD_TELESCOPE * amplitud[j] + ampMod[j] * sensor * AMPLITUD_TELESCOPE;
        amplitudModulo = checkAmplitud(amplitudModulo, AMPLITUD_TELESCOPE);
        freqAngularModulo = FREQANG * freqAngular[j] + freqMod[j] * sensor * FREQANG;
        freqAngularModulo = checkFreqAngular(freqAngularModulo, useFrqMod);

        telescope_actIface[i]->data->cmd_pos[0] = fabs(amplitudModulo * sin(0.5 * freqAngularModulo * time + 0.5 * phase[j++]));
        //        std::cout<< telescope_actIface[i]->data->cmd_pos[0]<< ", "<< telescope_actIface[i]->data->actuators[0].position<< "; ";
        //std::cout << "Telescope" << i << ": freqMod= " << freqMod[j] << ", periodo=  " << periodo << "\n";
    }
    //    std::cout<< "\nPosicion actuadores: Hinge Axial: ";
    for (int i = 0; i < n_hinge_axial; i++) {
        amplitudModulo = AMPLITUD_HINGE_AXIAL * amplitud[j] + ampMod[j] * sensor * AMPLITUD_HINGE_AXIAL;
        amplitudModulo = checkAmplitud(amplitudModulo, AMPLITUD_HINGE_AXIAL);
        freqAngularModulo = FREQANG * freqAngular[j] + freqMod[j] * sensor * FREQANG;
        freqAngularModulo = checkFreqAngular(freqAngularModulo, useFrqMod);
        //std::cout << "Rotacional" << i << ", j=" << j << ", amplitudCtrl: " << (double) amplitud[j] << ", freqCtrl: " << (double) freqAngular[j] << "; \n\n";
        hinge_axial_actIface[i]->data->cmd_pos[0] = amplitudModulo * sin(freqAngularModulo * time + phase[j++]);
        //std::cout << hinge_axial_actIface[i]->data->cmd_pos[0] << ", " << hinge_axial_actIface[i]->data->actuators[0].position << "; \n\n";

    }
    //    std::cout<< "\nPosicion actuadores: Hinge: ";
    for (int i = 0; i < n_hinge; i++) {
        amplitudModulo = AMPLITUD_HINGE * amplitud[j] + ampMod[j] * sensor * AMPLITUD_HINGE;
        amplitudModulo = checkAmplitud(amplitudModulo, AMPLITUD_HINGE);
        freqAngularModulo = FREQANG * freqAngular[j] + freqMod[j] * sensor * FREQANG;
        freqAngularModulo = checkFreqAngular(freqAngularModulo, useFrqMod);

        hinge_actIface[i]->data->cmd_pos[0] = amplitudModulo * sin(freqAngularModulo * time + phase[j++]);
        //        std::cout<< hinge_actIface[i]->data->cmd_pos[0]<< ", "<< hinge_actIface[i]->data->actuators[0].position<< "; ";
        //std::cout << "Hinge" << i << ": freqMod= " << freqMod[j] << ", periodo=  " << periodo << "\n";
    }

    //    std::cout<< "\n";
    //Por ahora nos saltamos los parametros de control de los efectores
    for (int i = 0; i < n_effector; i++) {
        j++;
    }

    //    std::cout<< "\n";
    for (int i = 0; i < n_snake; i++) {
        amplitudModulo = AMPLITUD_SNAKE * amplitud[j] + ampMod[j] * sensor * AMPLITUD_SNAKE;
        amplitudModulo = checkAmplitud(amplitudModulo, AMPLITUD_SNAKE);
        freqAngularModulo = FREQANG * freqAngular[j] + freqMod[j] * sensor * FREQANG;
        freqAngularModulo = checkFreqAngular(freqAngularModulo, useFrqMod);

        snake_actIface[i]->data->cmd_pos[0] = amplitudModulo * sin(freqAngularModulo * time + phase[j++]);
    }


    for (int i = 0; i < n_slider; i++)
        slider_actIface[i]->Unlock();
    for (int i = 0; i < n_telescope; i++)
        telescope_actIface[i]->Unlock();
    for (int i = 0; i < n_hinge_axial; i++)
        hinge_axial_actIface[i]->Unlock();
    for (int i = 0; i < n_hinge; i++)
        hinge_actIface[i]->Unlock();
    for (int i = 0; i < n_snake; i++)
        snake_actIface[i]->Unlock();
}


/******************************************************************************/
/*******************************     EVALUA     *******************************/

/******************************************************************************/
int evalua(int serverId, int n_ciclos_sim, double stepTimeDouble, double poseUpdateRate, int n_modulos, int* tipo_m,
        double* ampControl, double* angFreqControl, int* phaseControlDeg,
        double* ampMod, double* frequencyMod, int fitnessFunction, double fitnessParameter,
        bool useAmpControl, bool useAngFreqControl, bool usePhaseControl,
        bool useAmpModulation, bool useFreqModulation, bool guiOn, double &calidad) {


    std::cout << "SERVERiD: " << serverId << ", n_ciclos_sim: " << n_ciclos_sim << "\n";
    std::cout << "stepTimeDouble: " << stepTimeDouble << ", poseUpdateRate: " << poseUpdateRate << "\n";
    std::cout << "n_modulos: " << n_modulos << ", fitnessFunction: " << fitnessFunction << "\n";
    std::cout << "fitnessParameter: " << fitnessParameter << ", useAmpControl: " << useAmpControl << "\n";
    std::cout << "useAngFreqControl: " << useAngFreqControl << "\n";
    std::cout << "usePhaseControl: " << usePhaseControl << ", useAmpModulation: " << useAmpModulation << "\n";
    std::cout << "useFreqModulation: " << useFreqModulation << "\n";


    //Estos del gazebo me marean (usar una de estas dos lineas segun version del gazebo)
    //float stepTime= (float) stepTimeDouble;
    double stepTime = stepTimeDouble;

    gazebo::Client *client = new gazebo::Client();
    gazebo::SimulationIface *simIface = new gazebo::SimulationIface();
    gazebo::ActarrayIface * base_actIface[N_MODULOS_MAX];
    gazebo::ActarrayIface * slider_actIface[N_MODULOS_MAX];
    gazebo::ActarrayIface * telescope_actIface[N_MODULOS_MAX];
    gazebo::ActarrayIface * hinge_axial_actIface[N_MODULOS_MAX];
    gazebo::ActarrayIface * hinge_actIface[N_MODULOS_MAX];
    gazebo::ActarrayIface * snake_actIface[N_MODULOS_MAX];

    gazebo::Graphics3dIface *g3dIface = new gazebo::Graphics3dIface();
    double phaseControlRad[16];


    //Variable para calcular cuanto ha pintado
    double paintWall[DIMEN_PARED_Y * 10][DIMEN_PARED_Z * 10];
    for (int i = 0; i < DIMEN_PARED_Y * 10; i++)
        for (int j = 0; j < DIMEN_PARED_Z * 10; j++)
            paintWall[i][j] = 0.0;

    //Variable para calcular cuanto ha limpiado
    double paintSuelo[DIMEN_SUELO_X * 10][DIMEN_SUELO_Y * 10];
    for (int i = 0; i < DIMEN_SUELO_X * 10; i++)
        for (int j = 0; j < DIMEN_SUELO_Y * 10; j++)
            paintSuelo[i][j] = 0.0;


    //variable que informa si hay errores en la simulacion
    //0 no hay errores
    //-1 error con las librerias del gazebo
    //-20 error con la funcion de fitness
    //10 Nos hemos caido de las escaleras o salido del camino
    //20 Se ha caido la carga
    int status = 0;

    struct timeval tv;
    double time1, time2, time3, time4, time5;


    //Si no usamos el control de frequencia eliminamos los parametros de control
    ajustaParametrosControl(n_modulos, ampControl, angFreqControl, phaseControlDeg, phaseControlRad, ampMod, frequencyMod,
            useAmpControl, useAngFreqControl, usePhaseControl, useAmpModulation, useFreqModulation);

    //Definimos segun la funcion de fitness si se mueven los actuadores,
    //si se para si se cae la carga y
    //si se para si nos caemos por las escaleras o nos salimos del camino
    bool useStaticShapes = false, useCarga = false, useWithoutFallStairs = false,
            usePoseEffectors = false, showPaintedWall = false;
    bool useSensorPosY = true, useSensorFitnessParameter = false;


    if (fitnessFunction != 11)
        useStaticShapes = true;
    if (fitnessFunction == 60 || fitnessFunction == 70 || fitnessFunction == 31)
        useCarga = true;
    if (fitnessFunction == 50 || fitnessFunction == 70)
        useWithoutFallStairs = true;
    if (fitnessFunction == 150) {
        usePoseEffectors = true;
        useSensorFitnessParameter = true;
        useSensorPosY = false;
        if (guiOn)
            showPaintedWall = true;
    }
    if (fitnessFunction == 200) {
        if (guiOn)
            showPaintedWall = true;
    }


    analisisTipoModulos(n_modulos, tipo_m);

    //std::cout << "\n\n";
    //std::cout << "SERVER_GENERIC_CONTROL: " << serverId << ", PID: " << getpid() << "\n";

    ///Inicializacion de las interfaces
    inicializacionInterfaces(base_actIface,
            slider_actIface,
            telescope_actIface,
            hinge_axial_actIface,
            hinge_actIface,
            snake_actIface);

    gettimeofday(&tv, NULL);
    time1 = tv.tv_sec + tv.tv_usec * 1e-6;
    /// Connect to the libgazebo server
    if (connectGazeboServer(client, serverId) < 0)
        return -1;
    gettimeofday(&tv, NULL);
    time2 = tv.tv_sec + tv.tv_usec * 1e-6;
    /// Connect to the simIface
    if (connectSimIface(client, simIface) < 0)
        return -1;

    //usleep(10000000);

    gettimeofday(&tv, NULL);
    time3 = tv.tv_sec + tv.tv_usec * 1e-6;

    int count = 0;





    //Pedimos la posicion de los modulos que nos interesan
    updatePoses(simIface, useCarga, usePoseEffectors);


    simIface->Lock(1);
    //gazebo::SimulationRequestData *requestPos = &(simIface->data->requests[simIface->data->requestCount++]);
    //gazebo::SimulationRequestData *responses = &(simIface->data->responses[0]);
    //simIface->data->requestCount=1;
    //requestPos->type = gazebo::SimulationRequestData::GET_POSE3D;
    //memcpy(requestPos->modelName, modelName, 512);

    simIface->data->controlTime += stepTime;
    simIface->Unlock();
    int maxRetries = 0;
    while (true) {
        //simIface->Lock(1);
        double simT = simIface->data->simTime;
        //simIface->Unlock();
        if (simT >= stepTime) {
            //std::cout << "SERVER_GENERIC_CONTROL: " << serverId << "simIface->data->simTime==stepTime; SALIMOS\n";
            break;
        } else {
            //std::cout<<"simIface->data->simTime!=stepTime; stepTime="<<stepTime<<", simIface->data->simTime="<<simT<<"\n";
            usleep(1000);
            maxRetries++;
            if (maxRetries > 1000)
                return -1;
        }
    }
    gettimeofday(&tv, NULL);
    time4 = tv.tv_sec + tv.tv_usec * 1e-6;

    /* Open the ActArrays interfaces */
    if (openIfaces(client,
            base_actIface,
            slider_actIface,
            telescope_actIface,
            hinge_axial_actIface,
            hinge_actIface,
            snake_actIface) < 0)
        return -1;
    gettimeofday(&tv, NULL);
    time5 = tv.tv_sec + tv.tv_usec * 1e-6;

    //    std::cout << time2 - time1 << time3 - time2 << time5 - time4 << "\n";
    /* Inicializacion de variables */
    double distRecorrida = 0;

    //Si hace galta nos conectamos a la interfaz grafica
    if (showPaintedWall) {
        if (connectGraphicsIface(client, g3dIface) < 0)
            return -1;
        if (fitnessFunction == 200) {
            pintaSuelo(g3dIface);
        } else {
            pintaPared(g3dIface, fitnessParameter);
        }
    }

    //std::cout << "SERVER_GENERIC_CONTROL: " << serverId << "Empezamos el control, tiempo de simulacion: " << simIface->data->simTime << "\n";
    //std::cout << "SERVER_GENERIC_CONTROL: " << serverId << "Pos Inicial: x=" << pose.pos.x << ", y=" << pose.pos.y << ", z=" << pose.pos.z << "\n";

    /*Ponemos todos los actuadores con motor*/
    setEstadoActuadores(base_actIface,
            slider_actIface,
            telescope_actIface,
            hinge_axial_actIface,
            hinge_actIface,
            snake_actIface,
            GAZEBO_ACTARRAY_ACTSTATE_MOVING);

    /*Lo situamos en la posicion inicial de sus actuadores*/
    setPosicionActuadores(base_actIface,
            slider_actIface,
            telescope_actIface,
            hinge_axial_actIface,
            hinge_actIface,
            snake_actIface,
            ampControl, angFreqControl, phaseControlRad,
            ampMod, frequencyMod,
            useAmpControl, useAngFreqControl, usePhaseControl,
            useAmpModulation, useFreqModulation,
            0, //desviacion de y
            0);



    //   while(1){
    //         simIface->Lock(1);
    //         std::cout <<  "Inicio: \n";
    //         std::cout << "resquestCount: "<< simIface->data->requestCount << "\n";
    //         std::cout << "responseCount: " << simIface->data->responseCount << "\n";
    //
    //         gazebo::SimulationRequestData *responses1 = &(simIface->data->responses[1]);
    //         std::cout << responses1->modelPose.pos.x << "\n";
    //         std::cout <<  "\n\n\n";
    //         simIface->Unlock();
    //         usleep(1000);
    //     }




    /*
     * simIface->Lock(1);
     * simIface->data->requestCount=1;
     * requestPos->type = gazebo::SimulationRequestData::GET_POSE3D;
     * memcpy(requestPos->modelName, modelName, 512);
     * simIface->data->controlTime+=stepTime;
     * simIface->Unlock();
     */


    /* Estabilizacion */
    //    if(estabilizaRobot(simIface, responses, base_actIface,
    //            slider_actIface, telescope_actIface,
    //            hinge_axial_actIface, hinge_actIface, param, stepTime)<0)
    //        return -1;

    simIface->Lock(1);
    double pose_x0 = pose.pos.x, pose_y0 = pose.pos.y, pose_z0 = pose.pos.z;
    double maxPoseX = -1 * 1E30, maxPoseY = -1 * 1E30, maxPoseZ = -1 * 1E30;
    double minPoseX = 1E30, minPoseY = 1E30, minPoseZ = 1E30;
    double maxPitch = -1 * 1E30, maxRoll = -1 * 1E30, maxYaw = -1 * 1E30;
    double minPitch = 1E30, minRoll = 1E30, minYaw = 1E30;
    double pitch = pose.pitch, roll = pose.roll, yaw = pose.yaw;
    double pitch_x0 = pose.pitch, roll_x0 = pose.roll, yaw_x0 = pose.yaw;
    double pitchOld = pose.pitch, rollOld = pose.roll, yawOld = pose.yaw;
    double pitchSum = 0.0, rollSum = 0.0, yawSum = 0.0;
    double pitchRest = 0.0, rollRest = 0.0, yawRest = 0.0;
    double errorPathZ = 0.0, posezToObtain = 0.0;
    int vueltas = 0;
    simIface->Unlock();


    //std::cout << "SERVER_GENERIC_CONTROL: " << serverId << "Pos Estabilizado: x=" << pose_x0 << ", y=" << pose_y0 << ", z=" << pose_z0 << "\n";

    count = 0;
    double timeStart = simIface->data->simTime;
    double time = 0, timeOld = 0, timeControl = 0;

    simIface->Lock(1);
    simIface->data->controlTime += stepTime;
    simIface->Unlock();

    /**** Bucle Principal de la simulacion ***/
    while (time < n_ciclos_sim * 2 * 3.1415) {

        if (time > (timeOld + timeControl)) {

            //std::cout << time << timeStart << "\n";
            double sensor;
            if (useSensorPosY) {
                sensor = pose.pos.y - pose_y0;
            } else {
                if (useSensorFitnessParameter) {
                    sensor = fitnessParameter - 1.1;
                } else {
                    //Cerramos la simulación
                    status = -99;
                    if (closeAll(base_actIface,
                            slider_actIface,
                            telescope_actIface,
                            hinge_axial_actIface,
                            hinge_actIface,
                            snake_actIface,
                            simIface,
                            client,
                            stepTime) < 0)
                        return -1;
                    return status;
                }
            }
            if (useStaticShapes) {
                //Se mueve con señales senoidales
                //std::cout << "\n\n\nActualizamos los actuadores:" << time << ", error" << pose.pos.y - pose_y0 << "\n";
                setPosicionActuadores(base_actIface,
                        slider_actIface,
                        telescope_actIface,
                        hinge_axial_actIface,
                        hinge_actIface,
                        snake_actIface,
                        ampControl, angFreqControl, phaseControlRad,
                        ampMod, frequencyMod,
                        useAmpControl, useAngFreqControl, usePhaseControl,
                        useAmpModulation, useFreqModulation,
                        sensor,
                        time);
            } else {
                //Permanece quieto en la misma posición
                setPosicionActuadores(base_actIface,
                        slider_actIface,
                        telescope_actIface,
                        hinge_axial_actIface,
                        hinge_actIface,
                        snake_actIface,
                        ampControl, angFreqControl, phaseControlRad,
                        ampMod, frequencyMod,
                        useAmpControl, useAngFreqControl, usePhaseControl,
                        useAmpModulation, useFreqModulation,
                        sensor,
                        0);

            }

            //      std::cout << "timeStart: " << timeStart << ", time: " << time << ", simTime: " << simIface->data->simTime << ", controlTime: " << simIface->data->controlTime << "\n";
            //      std::cout << "Modulo 0: " << responses->modelPose.pos.x << "  " << responses->modelPose.pos.y <<
            //             "  " << responses->modelPose.pos.z << "\n";
            //std::cout << "Time: " << time << ", fmod(time,2): " << fmod(time+0.000000001,2) << "\n";
            if (fmod(time + 0.000000001, poseUpdateRate) < 0.00001) {
                //Pedimos la posicion de los modulos que nos interesan
                updatePoses(simIface, useCarga, usePoseEffectors);

                //llamamos a calcular la zona que hemos limpiado
                if (fitnessFunction == 200) {
                    pintaZonaSuelo(g3dIface, showPaintedWall, paintSuelo, pose);
                }

                //std::cout << "Pose Base: x: " << pose.pos.x << " y: " << pose.pos.y << " z: " << pose.pos.z << "\n";
                //std::cout << "Pose Carga: x: " << cargaPose.pos.x << " y: " << cargaPose.pos.y << " z: " << cargaPose.pos.z << "\n";
            }
            //simIface->data->requestCount=1;
            //            request->type = gazebo::SimulationRequestData::GET_POSE3D;
            //            memcpy(request->modelName, modelName, 512);


            if (useWithoutFallStairs) {
                //useX-YWithoutFallStairs
                if (fabs(pose.pos.y) > fitnessParameter) {
                    //Nos hemos caido de las escaleras, se acabó
                    status = 10;
                    //std::cout << "Nos hemos caido de las escaleras: pose.pos.x: " << pose.pos.x  << "pose.pos.y: " << pose.pos.y << "pose.pos.z: " << pose.pos.z << "\n";
                    calidad = pose.pos.x - pose_x0 - fabs(pose.pos.y - pose_y0);
                    calidad /= (n_ciclos_sim - 1);
                    if (closeAll(base_actIface,
                            slider_actIface,
                            telescope_actIface,
                            hinge_axial_actIface,
                            hinge_actIface,
                            snake_actIface,
                            simIface,
                            client,
                            stepTime) < 0)
                        return -1;
                    return status;
                }
            }
            //si usamos clean Floor
            if (fitnessFunction == 200) {
                //y nos alejamos mucho de la zona a limpiar
                if (pose.pos.x < -2 || pose.pos.x > 6 || pose.pos.y < -4 || pose.pos.y > 4 ) {
                    // se acabó, cerramos
                    status = 15;
                    calidad = 0.0;
                    for (int i = 0; i < DIMEN_SUELO_X * 10; i++)
                        for (int j = 0; j < DIMEN_SUELO_Y * 10; j++)
                            calidad += paintSuelo[i][j];
                    
                    if (closeAll(base_actIface,
                            slider_actIface,
                            telescope_actIface,
                            hinge_axial_actIface,
                            hinge_actIface,
                            snake_actIface,
                            simIface,
                            client,
                            stepTime) < 0)
                        return -1;
                    return status;

                }

            }
            if (useCarga) {
                //useCargaWithoutFall
                double distBaseCarga = pow(pose.pos.x - cargaPose.pos.x, 2) + pow(pose.pos.y - cargaPose.pos.y, 2);
                distBaseCarga = sqrt(distBaseCarga);
                if (distBaseCarga > 1 || (cargaPose.pos.z - pose.pos.z) < 0.01) {
                    //Se ha caido la carga, se acabó
                    status = 20;
                    //std::cout << "Hemos perdido la carga de las escaleras: pose.pos.x: " << pose.pos.x  << " pose.pos.y: " << pose.pos.y << " pose.pos.z: " << pose.pos.z << "\n";
                    std::cout << "CargaPose.pos.x: " << cargaPose.pos.x << " cargaPose.pos.y: " << cargaPose.pos.y << " cargaPose.pos.z: " << cargaPose.pos.z << "\n";
                    if (fitnessFunction != 31) {
                        calidad = pose.pos.x - pose_x0 - fabs(pose.pos.y - pose_y0);
                    } else {
                        //maxTurnConCarga
                        calidad = 0;
                        if (maxRoll < 1 && maxPitch < 1)
                            if (minRoll > -1 && minPitch > -1)
                                calidad = fabs((yaw - yaw_x0 + vueltas * 2 * 3.14)*180 / 3.14);
                    }
                    calidad /= (n_ciclos_sim - 1);
                    if (time < FIRST_ITER)
                        calidad = -0.3;
                    if (closeAll(base_actIface,
                            slider_actIface,
                            telescope_actIface,
                            hinge_axial_actIface,
                            hinge_actIface,
                            snake_actIface,
                            simIface,
                            client,
                            stepTime) < 0)
                        return -1;
                    return status;
                }
            }
            if (usePoseEffectors) {
                double anchoEfectivo = 0.1;
                for (int effector = 0; effector < n_effector; effector++) {
                    //std::cout << "Efector" << effector << ": x: " << effectorPose[effector].pos.x << " y: " << effectorPose[effector].pos.y << " z: " << effectorPose[effector].pos.z << "\n";
                    double calidad = 0.0;
                    double distancia = fabs(fitnessParameter - effectorPose[effector].pos.x);
                    if (distancia < fitnessParameter) {

                        if (distancia < anchoEfectivo * 1.5) {
                            if (distancia < anchoEfectivo * 0.5) {
                                calidad = 1;
                            } else {
                                distancia -= anchoEfectivo * 0.5;
                                calidad = 1 - distancia / (anchoEfectivo) + 0.01;
                            }

                        } else {
                            calidad = (0.01 * 1.5 * anchoEfectivo) / (fitnessParameter - 1.5 * anchoEfectivo) + 0.01 - 0.01 * (distancia / (fitnessParameter - anchoEfectivo * 1.5));
                            //std::cout << "Calidad mala: " << calidad <<"\n";
                        }
                        //Por debajo de z=ALTURA_START_PARED y por arriba de z=ALTURA_START_PARED+DIMEN_PARED_Z no hay pared que pintar
                        if (effectorPose[effector].pos.z > ALTURA_START_PARED && effectorPose[effector].pos.z < (ALTURA_START_PARED + DIMEN_PARED_Z)) {
                            int indiceAltura = (int) floor((effectorPose[effector].pos.z - ALTURA_START_PARED) / 0.1);

                            //La pared tiene DIMEN_PARED_Y metros y esta centrada en el 0
                            if (effectorPose[effector].pos.y > (-1 * (float) DIMEN_PARED_Y / 2) && effectorPose[effector].pos.y < ((float) DIMEN_PARED_Y / 2)) {
                                int indiceHorizontal = (int) floor(fabs(effectorPose[effector].pos.y) / 0.1);
                                if (effectorPose[effector].pos.y > 0) {
                                    indiceHorizontal += DIMEN_PARED_Y * 10 / 2;
                                } else {
                                    indiceHorizontal = DIMEN_PARED_Y * 10 / 2 - indiceHorizontal;
                                }
                                paintWall[indiceHorizontal][indiceAltura] += calidad;
                                if (paintWall[indiceHorizontal][indiceAltura] > 1)
                                    paintWall[indiceHorizontal][indiceAltura] = 1;
                                //std::cout << "Pintamos pared: iH: " << indiceHorizontal << ", iA: " << indiceAltura << "x: " << effectorPose[effector].pos.x << ", y: " << effectorPose[effector].pos.y << "\n";
                                //Pintamos la pared
                                if (showPaintedWall && paintWall[indiceHorizontal][indiceAltura] > 0.1) {
                                    pintaZona(g3dIface, indiceHorizontal, indiceAltura, paintWall, fitnessParameter);
                                }

                            }
                        }
                    }
                }
            }

            if (time < FIRST_ITER) {
                pose_x0 = pose.pos.x;
                pose_y0 = pose.pos.y;
                pose_z0 = pose.pos.z;
                pitch = pose.pitch;
                roll = pose.roll;
                yaw = pose.yaw;
                pitch_x0 = pose.pitch;
                roll_x0 = pose.roll;
                yaw_x0 = pose.yaw;
                pitchOld = pitch;
                rollOld = roll;
                yawOld = yaw;
                //std::cout << "PRIMERA_ITER: yaw: " << yaw << "pitch: " << pitch << "roll: " << roll << "\n";

            } else {
                if (maxPoseX < pose.pos.x) maxPoseX = pose.pos.x;
                if (maxPoseY < pose.pos.y) maxPoseY = pose.pos.y;
                if (maxPoseZ < pose.pos.z) maxPoseZ = pose.pos.z;
                if (minPoseX > pose.pos.x) minPoseX = pose.pos.x;
                if (minPoseY > pose.pos.y) minPoseY = pose.pos.y;
                if (minPoseZ > pose.pos.z) minPoseZ = pose.pos.z;

                if (maxPitch < pose.pitch) maxPitch = pose.pitch;
                if (maxRoll < pose.roll) maxRoll = pose.roll;
                if (maxYaw < pose.yaw) maxYaw = pose.yaw;
                if (minPitch > pose.pitch) minPitch = pose.pitch;
                if (minRoll > pose.roll) minRoll = pose.roll;
                if (minYaw > pose.yaw) minYaw = pose.yaw;

                pitch = pose.pitch;
                roll = pose.roll;
                yaw = pose.yaw;

                posezToObtain = 0.35 + fabs(AMPLITUD_TELESCOPE * sin(0.5 * FREQANG * time + 0.5 * 0));
                errorPathZ += pow(pose.pos.z - posezToObtain, 2);
                //std::cout << "poseZToObtain: " << posezToObtain << "  poseZ: " << pose.pos.z << "\n";


                if (yaw - yawOld > 3.14) {
                    vueltas--;
                    //std::cout << "\n\n VUELTA menos!!!!!: yaw:" << yaw << "  yawOld: " << yawOld << "\n";
                }
                if (yaw - yawOld < -3.14) {
                    vueltas++;
                    //std::cout << "\n\n VUELTA mas!!!!!: yaw:" << yaw << "  yawOld: " << yawOld << "\n";
                }
                /*
                if (pitch > pitchOld)
                    pitchSum += pitch - pitchOld;
                else
                    pitchRest += pitchOld - pitch;
                if(roll > rollOld)
                    rollSum += roll - rollOld ;
                else
                    rollRest += rollOld - roll;
                if(yaw > yawOld)
                    yawSum += yaw - yawOld;
                else
                    yawRest += yawOld - yaw;
                 */

                //std::cout << "yaw: " << pose.yaw << "pitch: " << pose.pitch << "roll: " << pose.roll << "\n";
                pitchOld = pitch;
                rollOld = roll;
                yawOld = yaw;

            }


            count = 0;
            timeOld = time;
            simIface->Lock(1);
            simIface->data->controlTime += stepTime;
            simIface->Unlock();
        } else {
            if (count > 150000) {
                //El gazebo ha dejado de actualizar datos y damos la evaluacion por terminada
                std::cerr << "SERVER_GENERIC_CONTROL: " << serverId << "Gazebo error: Gazebo ha dejado de actualizar los datos en plena simulacion, datos sin actualizar en" << count << " iteraciones\n";
                std::cerr << "SERVER_GENERIC_CONTROL: " << serverId << "Gazebo error: time: " << time << ", simTime" << simIface->data->simTime << ", controlTime: " << simIface->data->controlTime << "\n";
                closeAll(base_actIface,
                        slider_actIface,
                        telescope_actIface,
                        hinge_axial_actIface,
                        hinge_actIface,
                        snake_actIface,
                        simIface,
                        client,
                        stepTime);
                return -1;
            }
            if (count > 140000)
                usleep(100);
            count++;
        }


        usleep(1);
        simIface->Lock(1);
        time = (simIface->data->simTime - timeStart);
        simIface->Unlock();
    }/* Fin Bucle principal */

    //std::cout << "SERVER_GENERIC_CONTROL: " << serverId << "simTime " << simIface->data->simTime << "controlTime " << simIface->data->controlTime << "\n";
    //    while(simIface->data->simTime < simIface->data->controlTime){
    //        std::cout << "Bucle "<<"\n";
    //        usleep(100);
    //    }

    /*//Ya no hace falta porque GetPose es bloqueante
    while (true) {
        simIface->Lock(1);
        if (simIface->data->simTime < simIface->data->controlTime) {
            simIface->Unlock();
            usleep(10);
        } else {
            simIface->Unlock();
            break;
        }
    }
     */

    simIface->GetPose3d(modelName, pose);
    if (useCarga)
        simIface->GetPose3d(cargaName, cargaPose);

    double pose_x = pose.pos.x;
    double pose_y = pose.pos.y;
    double pose_z = pose.pos.z;

    //    std::cout << "SERVER_GENERIC_CONTROL: " << serverId << "Pos Inicial: x=" << pose_x0 << ", y=" << pose_y0 << ", z=" << pose_z0 << "\n";
    //    std::cout << "SERVER_GENERIC_CONTROL: " << serverId << "Pos Final: x=" << pose_x << ", y=" << pose_y << ", z=" << pose_z << "\n";

    double x = 0, y = 0;
    x = pose_x - pose_x0;
    y = pose_y - pose_y0;
    //    std::cout << "SERVER_GENERIC_CONTROL: " << serverId << "x=" << x << ", y=" << y << "\n";
    double xp = 0, yp = 0;
    //    xp = sqrt(2)/2*x - sqrt(2)/2*y;
    //    yp = sqrt(2)/2*x + sqrt(2)/2*y;

    distRecorrida = x - fabs(y);

    if (fitnessFunction == 0) {
        //useWalkDistance
        calidad = sqrt(pow(pose_x - pose_x0, 2) + pow(pose_y - pose_y0, 2));
        calidad /= (n_ciclos_sim - 1);
    } else if (fitnessFunction == 1) {
        //useWalkDistance45Degrees
        calidad = sqrt(2) / 2 * x - sqrt(2) / 2 * y;
        calidad /= (n_ciclos_sim - 1);
    } else if (fitnessFunction == 2) {
        //useWalkDistanceXMinusAbsY
        calidad = x - fabs(y);
        calidad /= (n_ciclos_sim - 1);
    } else if (fitnessFunction == 3) {
        //useWalkDistanceX
        calidad = x;
        calidad /= (n_ciclos_sim - 1);
    } else if (fitnessFunction == 10) {
        //useMaxHeight
        calidad = pose_z;
    } else if (fitnessFunction == 11) {
        //useMaxHeightWithLowTurns
        calidad = -10;
        if (maxRoll < 1 && maxPitch < 1)
            if (minRoll > -1 && minPitch > -1)
                calidad = pose_z;
        //double indiceEstabilidad = (maxPitch - minPitch) + (maxRoll - minRoll) + (maxYaw - minYaw);
        //calidad = pose_z - indiceEstabilidad * 0.8;
        //std::cout << "SERVER_GENERIC_CONTROL: " << serverId << "Maximos Pitch, Roll y Yaw: " << maxPitch << "  "<< maxRoll << "  " << maxYaw << "\n";
        //std::cout << "SERVER_GENERIC_CONTROL: " << serverId << "Minimos Pitch, Roll y Yaw: " << minPitch << "  "<< minRoll << "  " << minYaw << "\n";
    } else if (fitnessFunction == 20) {
        //useMaxZMovement
        calidad = maxPoseZ - minPoseZ;
    } else if (fitnessFunction == 30 || fitnessFunction == 31) {
        //useMaxTurn
        calidad = 0;
        if (maxRoll < 1 && maxPitch < 1)
            if (minRoll > -1 && minPitch > -1)
                calidad = fabs((yaw - yaw_x0 + vueltas * 2 * 3.14)*180 / 3.14);
        calidad /= (n_ciclos_sim - 1);
        //std::cout << "\n\n FIN!!!!!: maxYaw:" << maxYaw << "  maxPitch: " << maxPitch << " maxRoll: " << maxRoll << "\n";
        //std::cout << "\n FIN!!!!!: minYaw:" << minYaw << "  minPitch: " << minPitch << " minRoll: " << minRoll << "\n";
        //std::cout << "\n vueltas:" << vueltas << "\n\n\n";
    } else if (fitnessFunction == 40) {
        calidad = 30 - errorPathZ / (n_ciclos_sim - 1);
    } else if (fitnessFunction == 50) {
        //useX-YWithoutFallStairs
        calidad = x - fabs(y);
        calidad /= (n_ciclos_sim - 1);
    } else if (fitnessFunction == 60) {
        //useCargaWithoutFall
        calidad = x - fabs(y);
        calidad /= (n_ciclos_sim - 1);
    } else if (fitnessFunction == 70) {
        //useCargaWithoutFallAndWithoutFallStairs
        calidad = x - fabs(y);
        calidad /= (n_ciclos_sim - 1);
    } else if (fitnessFunction == 150) {
        //useManipulator
        calidad = 0.0;
        for (int i = 0; i < DIMEN_PARED_Y * 10; i++)
            for (int j = 0; j < DIMEN_PARED_Z * 10; j++)
                calidad += paintWall[i][j];
        //calidad /= (n_ciclos_sim);
    } else if (fitnessFunction == 200) {
        //useCleanFloor
        calidad = 0.0;
        for (int i = 0; i < DIMEN_SUELO_X * 10; i++)
            for (int j = 0; j < DIMEN_SUELO_Y * 10; j++)
                calidad += paintSuelo[i][j];
        //calidad /= (n_ciclos_sim);
    } else {
        status = -20;
    }


    //distRecorrida = sqrt(pow(pose_x - pose_x0, 2) + pow(pose_y - pose_y0, 2));
    //std::cout << "SERVER_GENERIC_CONTROL: " << serverId << "distRecorrida: " << distRecorrida << "\n";

    //calidad = distRecorrida;
    //calidad = pose_z;

    if (closeAll(base_actIface,
            slider_actIface,
            telescope_actIface,
            hinge_axial_actIface,
            hinge_actIface,
            snake_actIface,
            simIface,
            client,
            stepTime) < 0)
        return -1;
    return status;
}

int main(int argc, char *argv[]) {
    int tipo_modulos[N_MODULOS_MAX];
    double ampCtrl[N_MODULOS_MAX];
    double angFreqCtrl[N_MODULOS_MAX];
    int phaseControl[N_MODULOS_MAX];
    double freqModulator[N_MODULOS_MAX];
    double ampModulator[N_MODULOS_MAX];

    int fitnessFunction = 0;
    int serverID = atoi(argv[1]);
    int nCiclos = atoi(argv[2]);
    double stepTime = atof(argv[3]);
    int n_modulos = (int) (argc - 5) / 2;
    if (n_modulos != atoi(argv[4])) {
        std::cout << "Error con el numero de modulos: " << n_modulos << ", " << atoi(argv[4]) << "\n";
    }

    double calidad = 0;


    for (int i = 0; i < n_modulos; i++) {
        tipo_modulos[i] = atoi(argv[i + 5]);
        phaseControl[i] = atoi(argv[n_modulos + 5 + i]);
        freqModulator[i] = atof(argv[2 * n_modulos + 5 + i]);
    }
    std::cout << "Servidor ID (main): " << serverID << "\n";
    std::cout << "Numero de ciclos (main): " << nCiclos << "\n";
    std::cout << "StepTime (main): " << stepTime << "\n";
    std::cout << "N_modulos (main): " << n_modulos << "\n";
    std::cout << "Param (main): " << phaseControl[0] << " " << phaseControl[1] << " " << phaseControl[2] << " " << phaseControl[3] << "\n";
    std::cout << "Frequency Param (main): " << freqModulator[0] << " " << freqModulator[1] << " " << freqModulator[2] << " " << freqModulator[3] << "\n";

    int st = evalua(serverID, nCiclos, stepTime, 2.00, n_modulos, tipo_modulos,
            ampCtrl, angFreqCtrl, phaseControl,
            ampModulator, freqModulator, fitnessFunction, 2.5,
            false, false, true, false, false, true, calidad);

    std::cout << "Calidad: " << calidad << "\n\n";

    return st;

}

/********** Conectarse al simulador     **********/
int connectGazeboServer(gazebo::Client *client, int serverId) {
    try {
        client->Connect(serverId);
    } catch (std::string e) {
        std::cout << "Gazebo error: Unable to connect\n" << e << "\n";
        return -1;
    }
    return 0;
}

/**********    Abre la interfaz de simulacion     **********/
int connectSimIface(gazebo::Client *client, gazebo::SimulationIface *simIface) {
    /// Open the Simulation Interface
    try {
        simIface->Open(client, "default");
    } catch (std::string e) {
        std::cout << "Gazebo error: Unable to connect to the sim interface\n" << e << "\n";
        //Desconect Gazebo Server
        return -1;
    }
    return 0;
}

int connectGraphicsIface(gazebo::Client *client, gazebo::Graphics3dIface *g3dIface) {
    try {
        g3dIface->Open(client, "default");
    } catch (std::string e) {
        std::cerr << "Gazebo error: Unable to connect to an  interface\n"
                << e << "\n";
        return -1;
    }
    return 0;
}

void pintaSuelo(gazebo::Graphics3dIface *g3dIface) {
    //Pinta la pared pintada:
    gazebo::Vec3 blockSize;
    gazebo::Color clr;
    std::ostringstream blockName;
    blockName << "path_block:";

    blockSize.x = DIMEN_SUELO_X;
    blockSize.y = DIMEN_SUELO_Y;
    blockSize.z = 0.02;

    clr.r = 1.0;
    clr.g = 1.0;
    clr.b = 0.0;
    clr.a = 0.5;

    gazebo::Vec3 tmpvec;
    tmpvec.x = (double) DIMEN_SUELO_X / 2;
    tmpvec.y = 0.0;
    tmpvec.z = 0.01;

    g3dIface->DrawShape(blockName.str().c_str(),
            gazebo::Graphics3dDrawData::CUBE, tmpvec,
            blockSize, clr);

}

void pintaPared(gazebo::Graphics3dIface *g3dIface, double fitnessParameter) {
    //Pinta la pared pintada:
    gazebo::Vec3 blockSize;
    gazebo::Color clr;
    std::ostringstream blockName;
    blockName << "path_block:";

    blockSize.x = 0.25;
    blockSize.y = DIMEN_PARED_Y;
    blockSize.z = DIMEN_PARED_Z;

    clr.r = 1.0;
    clr.g = 1.0;
    clr.b = 0.0;
    clr.a = 0.5;

    gazebo::Vec3 tmpvec;
    tmpvec.x = fitnessParameter + 0.125 + 0.025;
    tmpvec.y = 0.0;
    tmpvec.z = (float) DIMEN_PARED_Z / 2 + ALTURA_START_PARED;

    g3dIface->DrawShape(blockName.str().c_str(),
            gazebo::Graphics3dDrawData::CUBE, tmpvec,
            blockSize, clr);

}

void pintaZonaSuelo(gazebo::Graphics3dIface *g3dIface, bool paint, double paintSuelo[DIMEN_SUELO_X * 10][DIMEN_SUELO_Y * 10], gazebo::Pose pose) {


    double anguloMax = 0.10, alturaMaxima = 0.07;

    if (pose.pitch < anguloMax && pose.pitch > (-1 * anguloMax) && pose.roll > (-1 * anguloMax) && pose.roll < anguloMax && pose.pos.z < alturaMaxima) {
        for (int i = 0; i < DIMEN_SUELO_X * 10; i++) {
            for (int j = 0; j < DIMEN_SUELO_Y * 10; j++) {
                double posX = 0.05 + i * 0.1;
                double posY = 0.05 - (double) DIMEN_SUELO_Y / 2 + j * 0.1;
                //std::cout << "Recorremos la matriz suelo: " << ": i: " << i << " j: " << j << " posX: " << posX << " posY: " << posY<< " \n ";
                double dist = sqrt((posX - pose.pos.x)*(posX - pose.pos.x) + (posY - pose.pos.y)*(posY - pose.pos.y));
                if (dist < RADIO_NO_LIMPIEZA) {
                    double calidad = 0.25;
                    if (dist > RADIO_LIMPIEZA)
                        calidad = 0.25 - 0.25 * (dist - RADIO_LIMPIEZA) / (RADIO_NO_LIMPIEZA - RADIO_LIMPIEZA);
                    paintSuelo[i][j] += calidad;
                    if (paintSuelo[i][j] > 1)
                        paintSuelo[i][j] = 1;

                    //Pintamos
                    if (paint && paintSuelo[i][j] > 0.1) {
                        gazebo::Vec3 blockSizePaint;
                        gazebo::Color clrPaint;
                        std::ostringstream blockNamePaint;
                        blockNamePaint << "path_block:" << i << " " << j;


                        blockSizePaint.x = 0.1;
                        blockSizePaint.y = 0.1;
                        blockSizePaint.z = 0.01;

                        clrPaint.r = paintSuelo[i][j];
                        clrPaint.g = paintSuelo[i][j];
                        clrPaint.b = paintSuelo[i][j];
                        clrPaint.a = 1;

                        gazebo::Vec3 auxvec;
                        auxvec.x = posX;
                        auxvec.y = posY;
                        auxvec.z = 0.02 + 0.005;

                        //std::cout << "Vamos a pintar: " << ": x: " << auxvec.x << " y: " << auxvec.y << " z: " << auxvec.z << " \n ";
                        //std::cout << "blockNamePaint: " << blockNamePaint.str() << " posX: " << posX << ", posY: " << posY << " \n ";

                        g3dIface->DrawShape(blockNamePaint.str().c_str(),
                                gazebo::Graphics3dDrawData::CUBE, auxvec,
                                blockSizePaint, clrPaint);
                    }
                }
            }
        }
    }
}

void pintaZona(gazebo::Graphics3dIface *g3dIface, int indiceHorizontal, int indiceAltura, double paintWall[DIMEN_PARED_Y * 10][DIMEN_PARED_Z * 10], double fitnessParameter) {
    gazebo::Vec3 blockSizePaint;
    gazebo::Color clrPaint;
    std::ostringstream blockNamePaint;
    blockNamePaint << "path_block:" << indiceHorizontal << indiceAltura;
    blockSizePaint.x = 0.05;
    blockSizePaint.y = 0.1;
    blockSizePaint.z = 0.1;

    clrPaint.r = paintWall[indiceHorizontal][indiceAltura];
    clrPaint.g = paintWall[indiceHorizontal][indiceAltura];
    clrPaint.b = paintWall[indiceHorizontal][indiceAltura];
    clrPaint.a = 1;

    gazebo::Vec3 auxvec;
    auxvec.x = fitnessParameter;
    auxvec.y = (-1 * (float) DIMEN_PARED_Y / 2) + 0.05 + 0.1 * indiceHorizontal;
    auxvec.z = ALTURA_START_PARED + 0.05 + 0.1 * indiceAltura;
    //std::cout << "Vamos a pintar: " << ": x: " << auxvec.x << " y: " << auxvec.y << " z: " << auxvec.z;
    //std::cout << "blockNamePaint: " << blockNamePaint.str() << " indiceHorizontal: " << indiceHorizontal << ", indiceAltura: " << indiceAltura;
    g3dIface->DrawShape(blockNamePaint.str().c_str(),
            gazebo::Graphics3dDrawData::CUBE, auxvec,
            blockSizePaint, clrPaint);

}

/**********     Inicialización de las interfaces     **********/
void inicializacionInterfaces(gazebo::ActarrayIface *base_actIface[],
        gazebo::ActarrayIface *slider_actIface[],
        gazebo::ActarrayIface *telescope_actIface[],
        gazebo::ActarrayIface *hinge_axial_actIface[],
        gazebo::ActarrayIface *hinge_actIface[],
        gazebo::ActarrayIface *snake_actIface[]) {

    ///Inicializacion de las interfaces
    //    for (int i = 0; i < n_base; i++) {
    //        base_actIface[i * 4] = new gazebo::ActarrayIface();
    //        base_actIface[i * 4 + 1] = new gazebo::ActarrayIface();
    //        base_actIface[i * 4 + 2] = new gazebo::ActarrayIface();
    //        base_actIface[i * 4 + 3] = new gazebo::ActarrayIface();
    //    }
    for (int i = 0; i < n_slider; i++) {
        slider_actIface[i] = new gazebo::ActarrayIface();
    }
    for (int i = 0; i < n_telescope; i++) {
        telescope_actIface[i] = new gazebo::ActarrayIface();
    }
    for (int i = 0; i < n_hinge_axial; i++) {
        hinge_axial_actIface[i] = new gazebo::ActarrayIface();
    }
    for (int i = 0; i < n_hinge; i++) {
        hinge_actIface[i] = new gazebo::ActarrayIface();
    }
    for (int i = 0; i < n_snake; i++) {
        snake_actIface[i] = new gazebo::ActarrayIface();
    }
}

/**********     Open the ActArrays interfaces     **********/
int openIfaces(gazebo::Client *client,
        gazebo::ActarrayIface *base_actIface[],
        gazebo::ActarrayIface *slider_actIface[],
        gazebo::ActarrayIface *telescope_actIface[],
        gazebo::ActarrayIface *hinge_axial_actIface[],
        gazebo::ActarrayIface *hinge_actIface[],
        gazebo::ActarrayIface *snake_actIface[]) {
    try {
        for (int i = 0; i < n_slider; i++) {
            char name[50];
            sprintf(name, "slider_actarray_iface_%d", i);
            //std::cout << name << "\n";
            slider_actIface[i]->Open(client, name);
        }
    } catch (std::string e) {
        std::cout << "Gazebo error: Unable to connect to the slider actarray interface\n"
                << e << "\n";
        return -1;
    }
    try {
        for (int i = 0; i < n_telescope; i++) {
            char name[50];
            sprintf(name, "telescope_actarray_iface_%d", i);
            telescope_actIface[i]->Open(client, name);
        }
    } catch (std::string e) {
        std::cout << "Gazebo error: Unable to connect to the telescope actarray interface\n"
                << e << "\n";
        return -1;
    }
    try {
        for (int i = 0; i < n_hinge_axial; i++) {
            char name[50];
            sprintf(name, "hinge_axial_actarray_iface_%d", i);
            hinge_axial_actIface[i]->Open(client, name);
        }
    } catch (std::string e) {
        std::cout << "Gazebo error: Unable to connect to the hinge_axial actarray interface\n"
                << e << "\n";
        return -1;
    }
    try {
        for (int i = 0; i < n_hinge; i++) {
            char name[50];
            sprintf(name, "hinge_actarray_iface_%d", i);
            hinge_actIface[i]->Open(client, name);
        }
    } catch (std::string e) {
        std::cout << "Gazebo error: Unable to connect to the hinge actarray interface\n"
                << e << "\n";
        return -1;
    }
    try {
        for (int i = 0; i < n_snake; i++) {
            char name[50];
            sprintf(name, "snake_actarray_iface_%d", i);
            snake_actIface[i]->Open(client, name);
        }
    } catch (std::string e) {
        std::cout << "Gazebo error: Unable to connect to the hinge actarray interface\n"
                << e << "\n";
        return -1;
    }

    /*
    try {
        for(int i=0; i<n_metamodulo; i++){
            char name[50];
            sprintf(name, "metamodule_slider0_actarray_iface_%d", i);
            slider_actIface[i]->Open(client, name);
            sprintf(name, "metamodule_slider1_actarray_iface_%d", i);
            slider_actIface[i]->Open(client, name);
            sprintf(name, "metamodule_slider2_actarray_iface_%d", i);
            slider_actIface[i]->Open(client, name);
            sprintf(name, "metamodule_slider3_actarray_iface_%d", i);
            slider_actIface[i]->Open(client, name);
        }
    }
    catch (std::string e) {
        std::cout << "Gazebo error: Unable to connect to the actarray interface\n"
        << e << "\n";
        return -1;
    }*/

    return 0;
}

/**********     Close the ActArrays interfaces     **********/
int closeIfaces(gazebo::ActarrayIface *base_actIface[],
        gazebo::ActarrayIface *slider_actIface[],
        gazebo::ActarrayIface *telescope_actIface[],
        gazebo::ActarrayIface *hinge_axial_actIface[],
        gazebo::ActarrayIface *hinge_actIface[],
        gazebo::ActarrayIface *snake_actIface[]) {
    try {
        for (int i = 0; i < n_slider; i++) {
            char name[50];
            sprintf(name, "slider_actarray_iface_%d", i);
            //std::cout << name << "\n";
            slider_actIface[i]->Close();
            delete slider_actIface[i];
        }
        for (int i = 0; i < n_telescope; i++) {
            char name[50];
            sprintf(name, "telescope_actarray_iface_%d", i);
            telescope_actIface[i]->Close();
            delete telescope_actIface[i];
        }
        for (int i = 0; i < n_hinge_axial; i++) {
            char name[50];
            sprintf(name, "hinge_axial_actarray_iface_%d", i);
            hinge_axial_actIface[i]->Close();
            delete hinge_axial_actIface[i];
        }
        for (int i = 0; i < n_hinge; i++) {
            char name[50];
            sprintf(name, "hinge_actarray_iface_%d", i);
            hinge_actIface[i]->Close();
            delete hinge_actIface[i];
        }
        for (int i = 0; i < n_snake; i++) {
            char name[50];
            sprintf(name, "snake_actarray_iface_%d", i);
            snake_actIface[i]->Close();
            delete snake_actIface[i];
        }
    } catch (std::string e) {
        std::cerr << "Gazebo error: Unable to close the actarray interface\n"
                << e << "\n";
        return -1;
    }
    return 0;
}

/**********     Cierra la interface de simulacion     **********/
int closeSimIface(gazebo::SimulationIface *simIface) {
    try {
        simIface->Close();
        delete simIface;
        return 0;
    } catch (gazebo::GazeboError e) {
        std::cerr << "Gazebo error: Unable to close the sim interface\n" << e << "\n";
        return -1;
    }
}

/**********     Se desconencta del simulador     **********/
int disconnectClient(gazebo::Client *client) {
    try {
        client->Disconnect();
        delete client;
        return 0;
    } catch (gazebo::GazeboError e) {
        std::cerr << "Gazebo error: Unable to disconnect\n" << e << "\n";
        return -1;
    }
}

/**********     Close the ActArrays interfaces     **********/
int closeAll(gazebo::ActarrayIface *base_actIface[],
        gazebo::ActarrayIface *slider_actIface[],
        gazebo::ActarrayIface *telescope_actIface[],
        gazebo::ActarrayIface *hinge_axial_actIface[],
        gazebo::ActarrayIface *hinge_actIface[],
        gazebo::ActarrayIface *snake_actIface[],
        gazebo::SimulationIface *simIface,
        gazebo::Client *client,
        double stp) {

    int error = 0;

    /* Close the ActArrays interfaces */
    if (closeIfaces(base_actIface,
            slider_actIface,
            telescope_actIface,
            hinge_axial_actIface,
            hinge_actIface,
            snake_actIface) < 0)
        error--;

    /* Vamos a cerrar el gazebo */
    //std::cout << "SERVER_GENERIC_CONTROL: "<< serverId << "señal de cerrar el gazebo" << "\n";
    simIface->Lock(1);
    simIface->data->quit = 1;
    simIface->data->controlTime += stp;
    simIface->Unlock();
    usleep(10);

    /* Close the Simulation Interface */
    if (closeSimIface(simIface) < 0)
        error--;

    /* Disconnect to the libgazebo server */
    if (disconnectClient(client) < 0)
        error--;

    return error;
}




/////**********************************************************************/////
/********************    Funciones depreciadas     ****************************/
/////**********************************************************************/////


/**********     Espera a tiempo de iniciar la sim     **********/
/*
int waitToStartTime(gazebo::SimulationIface *simIface, int time_start_sim) {
    double time = simIface->data->simTime;
    int count = 0;
    while (time < time_start_sim) {
        //std::cout<< "Esperando a iniciar: "<< simIface->data->simTime << "\n";
        if ((time_start_sim - time) > 1) {
            usleep(50);
        } else {
            if ((time_start_sim - time) > 0.5) {
                usleep(10);
            } else {
                usleep(5);
            }
        }
        if (time == simIface->data->simTime) {
            count++;
            //usleep(10000);
        } else {
            count = 0;
        }
        if (count > 150000) {
            std::cout << "Gazebo a dejado de actualizar datos durante la espera a llegar al time_start_sim, time:" << time << " count: " << count << "\n";
            return -1;
        }

        time = simIface->data->simTime;
    }
    return 0;
}*/
