
#undef RENDER_H
#include "JNIControlModulos.h"
#include "generic_control.hh"
#include <iostream>


/*-------------------------------------------------------------------------
Function evalua

-------------------------------------------------------------------------*/
JNIEXPORT jint JNICALL 
Java_modules_evol_morfologia_JNIControlModulos_evalua
  (JNIEnv *env, jobject obj, jint server_id, jint n_ciclos_sim, jdouble step_time, jdouble poseUpdateRate, jint nModulos, jintArray tipoModulo, 
   jdoubleArray paramAmpCtrl, jdoubleArray paramAngFreqCtrl, jintArray phaseControl, 
   jdoubleArray amplitudeModulator, jdoubleArray frequencyModulator, jint fitnessFunction, jdouble fitnessParameter,
   jboolean useAmpControl, jboolean useAngFreqControl, jboolean usePhaseControl, 
   jboolean useAmplitudeModulator, jboolean useFrequencyModulator, jboolean guiOn, jdoubleArray calidad) {
	

     if (tipoModulo == NULL) return -1;
     if (paramAmpCtrl == NULL) return -1;
     if (paramAngFreqCtrl == NULL) return -1;
     if (phaseControl == NULL) return -1;
     if (amplitudeModulator == NULL) return -1;
     if (frequencyModulator == NULL) return -1;
     if (calidad == NULL) return -1;

     jint *tipoModulo2 = env->GetIntArrayElements(tipoModulo, 0);
	
     jdouble *paramAmpCtrl2 = env->GetDoubleArrayElements(paramAmpCtrl, 0);
     jdouble *paramAngFreqCtrl2 = env->GetDoubleArrayElements(paramAngFreqCtrl, 0);
     jint *phaseControl2 = env->GetIntArrayElements(phaseControl, 0);

     jdouble *amplitudeModulator2 = env->GetDoubleArrayElements(amplitudeModulator, 0);
     jdouble *frequencyModulator2 = env->GetDoubleArrayElements(frequencyModulator, 0);
     
     jdouble *calidad2 = env->GetDoubleArrayElements(calidad, 0);

     double calidad3=-1;
     jint status =evalua(server_id, n_ciclos_sim, step_time, poseUpdateRate, nModulos, tipoModulo2, 
		     paramAmpCtrl2, paramAngFreqCtrl2, phaseControl2, 
		     amplitudeModulator2, frequencyModulator2, fitnessFunction, fitnessParameter, 
		     useAmpControl, useAngFreqControl, usePhaseControl, 
		     useAmplitudeModulator, useFrequencyModulator, guiOn, calidad3);

     calidad2[0]=calidad3;

     env->ReleaseIntArrayElements(tipoModulo, tipoModulo2, 0);

     env->ReleaseDoubleArrayElements(paramAmpCtrl, paramAmpCtrl2, 0);
     env->ReleaseDoubleArrayElements(paramAngFreqCtrl, paramAngFreqCtrl2, 0);
     env->ReleaseIntArrayElements(phaseControl, phaseControl2, 0);

     env->ReleaseDoubleArrayElements(amplitudeModulator, amplitudeModulator2, 0);
     env->ReleaseDoubleArrayElements(frequencyModulator, frequencyModulator2, 0);
     
     env->ReleaseDoubleArrayElements(calidad, calidad2, 0);
	
	return status;
}  



