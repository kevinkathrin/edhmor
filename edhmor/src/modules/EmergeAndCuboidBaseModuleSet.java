/*
 * EDHMOR - Evolutionary designer of heterogeneous modular robots
 * <https://bitbucket.org/afaina/edhmor>
 * Copyright (C) 2016 GII (UDC) and REAL (ITU)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package modules;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

/**
 * EmergeAndCuboidBaseModuleSet.java
 * Created on 21/10/2016
 * @author Andres Faiña <anfv  at itu.dk>
 */
public class EmergeAndCuboidBaseModuleSet extends RodrigoModuleSet{
    
    public EmergeAndCuboidBaseModuleSet(){
        //two types of module with six connection faces and 2 orientations
        super(2, 5, 2); 
        
        //The paren class sets all the properties. Here, we have only to update 
        //the first module (instead a Emerge module) we use a cuboid base
        
        
        //The name of the different modules (they have to match with the name of
        //the file to load in the simulator)
        moduleName[0] = "cuboidBase";
        modulesFacesNumber[0] = 5; //Number of faces for base module
        //Number of faces in the base body for each type of module; the other faces are in the actuator body
        modulesBaseFacesNumber[0] = 6; //Number of faces in the base for the base module (all)
        //number of posible orientations when a module is joined to other module
        moduleOrientations[0] = 0; //Number of orientations of the module base
        connectionFaceForEachOrientation[1][0] = 0;        //connected in face 0
        rotationAboutTheNormalForEachOrientation[1][0] = 0;          //0 degrees
        //mass of each module in Kg
        modulesMass[0] = 0.1;  //Mass of the base module
        
        //max amplitude
        modulesMaxAmplitude[0] = 0 ; //Max amplitude of the base 
        
        //max angular frequency
        modulesMaxAngularFrequency[0] = 0;  //Max angular frequency of the base
        modulesMaxAngularFrequency[1] = 2.5;   //Max angular frequency of the Emerge module
        
        /**********************************************************************/
        /************************** originFaceVector **************************/
        /**********************************************************************/
        // This are the coordinates of the vector from the origon of the module 
        // to the face of the module for each face and module type
        //Base module:
        
        originFaceVector[0][0] = new Vector3D(-0.0275,0,0);         //Face 1
        originFaceVector[0][1] = new Vector3D(0.0275, 0, 0);        //Face 2
        originFaceVector[0][2] = new Vector3D(0, -0.0275, 0);       //Face 3
        originFaceVector[0][3] = new Vector3D(0,0.0275,0);          //Face 4
        originFaceVector[0][4] = new Vector3D(0, 0, 0.0275);       //Face 5

        
        
        /**********************************************************************/
        /************************** normalFaceVector **************************/
        /**********************************************************************/
        // This are the coordinates of an unitary vector normal to the face 
        // for each face and module type
        //Base module:
        normalFaceVector[0][0] = new Vector3D(-1,0,0);               //Face 1
        normalFaceVector[0][1] = new Vector3D(1, 0, 0);              //Face 2
        normalFaceVector[0][2] = new Vector3D(0, -1, 0);             //Face 3
        normalFaceVector[0][3] = new Vector3D(0,1,0);                //Face 4
        normalFaceVector[0][4] = new Vector3D(0, 0, 1);             //Face 5
        
        /**********************************************************************/
        /************************** boundingBox **************************/
        /**********************************************************************/
        // This are the size of boundingBox for each module type, it is 
        //represented as (width,height,length); (Y, Z, X) in Vrep. Units: meter
        //Base module: 
        boundingBox[0] = new Vector3D(0.055,0.055,0.055);
        
        /**********************************************************************/
        /*************************** symmetricFace  ***************************/
        /**********************************************************************/
        
        //TODO:
        symmetricFace[0][0] = 1;
        symmetricFace[0][1] = 0;
        symmetricFace[0][2] = 3;
        symmetricFace[0][3] = 2;
        symmetricFace[0][4] = -1;

    }
    
}
