package modules.grid;

import es.udc.gii.common.eaf.util.EAFRandom;
import modules.grid.modulegrid.GridPosition;
import modules.grid.modulegrid.GridPositionProperties;
import modules.grid.modulegrid.ModuleGrid;
import modules.ModuleSet;
import modules.ModuleSetFactory;
import modules.evaluation.VrepEvaluator;
import modules.evaluation.overlapping.CollisionDetector;
import modules.evaluation.overlapping.CollisionDetectorFactory;
import modules.individual.Connection;
import modules.individual.Node;
import modules.individual.TreeIndividual;
import modules.util.SimulationConfiguration;

import java.util.*;

/**
 * Created by Kevin on 17-Mar-17.
 */
public class ModuleSetCreator {
    private static Random r = new Random();
    private static TreeIndividual tree;
    private static ModuleSet moduleSet;
    private static Node rootNode;
    private static CollisionDetector collisionDetector;
    private static Queue<Node> nodesAdded = new LinkedList<>();

    private static ModuleGrid moduleGrid;

    //Test values for now
    private static final double AMP = 1;
    private static final double FREQ = 0.5;
    private static final double PHASE = 0;
    private static final int MAXMODULES = 10;
    private static final int DEFUALT_SIZE = 5;
    



    public static void main(String[] args){
        //Setup moduleSet used for testing
        EAFRandom.init();
        //SimulationConfiguration.setMaxModules(32);
        SimulationConfiguration.setModuleSet("EmergeAndCuboidBaseModules");
        ModuleSetFactory.reloadModuleSet();
        moduleSet = ModuleSetFactory.getModulesSet();
        collisionDetector = CollisionDetectorFactory.getCollisionDetector();

        //Create position grid for modules/nodes
        moduleGrid = new ModuleGrid(DEFUALT_SIZE, DEFUALT_SIZE, DEFUALT_SIZE, MAXMODULES);

        //Create the root node and add it to the position grid
        rootNode = new Node(0, 0, 0, 0, 0, 0, 0, null);
        moduleGrid.insertRootNode(rootNode);

        //Initialize Individual with rootNode.
        tree = new TreeIndividual();
        tree.init(141);     //NB: 141 = Maximum of 16 nodes
        tree.setRootNode(rootNode);

        nodesAdded.offer(rootNode);

        int orientation = 1;

        //Keep expanding until the tree becomes too big.
        while(moduleGrid.numberOfNodes() < MAXMODULES && !nodesAdded.isEmpty()) {
            Node parent = nodesAdded.poll();

            GridPositionProperties posProps = moduleGrid.getPositionProperties(parent);
            int[] faceIndices = posProps.getFreeFaces();
            for(int i = 0; i < posProps.getNumberOfFreeFace(); i++){
                GridPosition facePos = posProps.getFacePosition(faceIndices[i]);

                if(NodeExist(facePos) && moduleGrid.insideBounds(facePos)){
                    orientation = 1-orientation;
                    Node newNode = createNode(AMP,PHASE,FREQ);
                    newNode.setDad(parent);
                    Connection conn = new Connection(parent,newNode,faceIndices[i],orientation);
                    parent.addChildren(newNode,conn);
                    moduleGrid.insertNode(facePos,newNode, parent);
                    nodesAdded.offer(newNode);
                }

            }
           // expandCube(current, moduleGrid.getPosition(current), moduleGrid.getGridProperties());
        }

        //Update chromosome
        tree.modifyChromosome();

        //DrawStructure.run(moduleGrid.getGridPositionMap().values(),moduleGrid.getPosition(rootNode),"REAL_TEST_ONE");
        //Run simulation.
        VrepEvaluator evaluator = new VrepEvaluator(tree.getChromosomeAt(0), "", 0);
        evaluator.getVrepPing();
        evaluator.getVrepPing();

        double fitness = evaluator.evaluate();
        //System.out.println("Fitness: " + fitness);

    }



    private static Node createNode(double _amp, double _phase, double _frequency) {

        //Node(int type, double weighing, double amplitudeControl, double angularFreqControl, double controlOffset,
        // double amplitudeModulator, double frequencyModulator, Node dad)
        return new Node(1, 0, _amp, _frequency, _phase, 0, 0, null);
    }



    private static boolean NodeExist(GridPosition faceP) {
        return true; //50% atm

    }

/*

    private static GridPosition[] findFreeGridPositions(Node parent, GridPosition gp) throws InconsistentDataException {

        //TODO take rotation into account.
        //Gets the rotated OriginToFaceVector of the parent
        Vector3D rotationVector = nodeRotation.get(parent);

        //Returns an array of the positions of each free face from the node
        int freeFaces = parent.getFreeFaces();
        GridPosition[] gridPositions = new GridPosition[freeFaces];
        //Remember to check if face 0 is attached to a parent.
        int face = (parent.getDad() == null) ? 0 : 1;

        for(int i = 0; i < freeFaces; i++){
            //Gets the original non-rotated OriginToFaceVector of the parent
            Vector3D originFaceV = moduleSet.getOriginFaceVector(parent.getType(),face+i);
            if(rotationVector == null)
                gridPositions[i] = childGridPosition(gp,originFaceV);
            else{
                Vector3D originalVector = moduleSet.getOriginFaceVector(parent.getType(),parent.getAttachFaceToDad());
                double angle = Vector3D.angle(originalVector,rotationVector);
                gridPositions[i] = childGridPosition(gp,originFaceV);

            }
        }
        return gridPositions;
    }

    private static GridPosition childGridPosition(GridPosition p, Vector3D v){
        //do switches based on vector directions?
        GridPosition gp = new GridPosition(p.getX()-1,p.getY(),p.getZ());
        return gp;
    }




    private static void calculateChildPosition(Node parent, int faceNumber, Node child) {
        GridPosition parentPos, childPos;
        Vector3D parentVectorToFace,childFaceToVector;
        try {
            //TODO take rotation of child/parent into account.
            parentPos = gridPosition.get(parent);

            //Get vector from parent origin to connection face
            parentVectorToFace = moduleSet.getOriginFaceVector(parent.getType(), faceNumber);
            //Get vector from connection face to child origin
            childFaceToVector = moduleSet.getOriginFaceVector(child.getType(), child.getAttachFaceToDad()).negate();

            nodeRotation.put(child,calculateConnectionVector(parentVectorToFace, childFaceToVector));
            GridPosition[] childPositions = getRootChildPositions(parentPos);
            childPos = childPositions[0];
            gridPosition.put(child,childPos);
            //nodeToPos.put(child,childPos);

        } catch (InconsistentDataException e) {
            e.printStackTrace();
        }
    }

    private static double vectorLength(Vector3D pV){
        double[] values = pV.toArray();
        return Math.sqrt(Math.pow(values[0],2) + Math.pow(values[1],2) + Math.pow(values[2],2));
    }


    private static Vector3D calculateConnectionVector(Vector3D pV, Vector3D cV) {
        if(Math.abs(Vector3D.dotProduct(pV, cV)/(vectorLength(pV) * vectorLength(cV))) < 0.1){
            return null;
        }else{
            //TODO Take into account that some vectors are not completely straight
            return pV.scalarMultiply(vectorLength(cV)/ vectorLength(pV));
        }
    }
*/
}
