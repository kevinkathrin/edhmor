package modules.grid.GridTests;

import es.udc.gii.common.eaf.util.EAFRandom;
import modules.grid.modulegrid.DrawStructure;
import modules.grid.modulegrid.GridPosition;
import modules.grid.modulegrid.GridPositionProperties;
import modules.grid.modulegrid.ModuleGrid;
import modules.ModuleSetFactory;
import modules.individual.Connection;
import modules.individual.Node;
import modules.individual.TreeIndividual;
import modules.util.SimulationConfiguration;

/**
 * Created by i5-4670K on 06-04-2017.
 */
public class ModulePositionsTest {


    private static Node setUpNewNode(int type, Node parent, int face, int orientation){
        Node n1 = new Node(type,0,0,0,0,0,0,parent);
        Connection con = new Connection(parent,n1,face,orientation);
        parent.addChildren(n1,con);
        return n1;
    }

    public static void main(String[] args){
        EAFRandom.init();
        SimulationConfiguration.setModuleSet("EmergeAndCuboidBaseModules");
        ModuleSetFactory.reloadModuleSet();

        //TestZRotationInVRep();
        //TestYRotationInVRep();
        //TestOrientationShift();
       // edgeCase();
        //edgeCase_4();
        edgeCase_3();
        //edgeCase_2();
        //TestOrientationAndRotationOnYAxis();

    }


    public static void TestZRotationInVRep(){

        //SimulationConfiguration.setVrepStartingPort(portNumber);
        int orientation = 0;
        ModuleGrid grid = new ModuleGrid();

        Node rootNode = new Node(0, 0, 0, 0, 0, 0, 0, null);


        //Create to nodes, node1 attached to root on face 0 and node2 to node1 on face 2.
        Node node1 = setUpNewNode(1,rootNode,0,orientation);
        grid.insertRootNode(rootNode);
        grid.insertNode(0,node1,rootNode);
        Node node2 = setUpNewNode(1,node1,2,orientation);
        grid.insertNode(2,node2,node1);


        TreeIndividual tree = new TreeIndividual();
        tree.init(141);
        tree.setRootNode(rootNode);
        tree.modifyChromosome();

/*
        VrepEvaluator evaluator = new VrepEvaluator(tree.getChromosomeAt(0), "", 0);
        evaluator.getVrepPing();
        evaluator.getVrepPing();*/
        DrawStructure.run(grid.getGridPositionMap().values(), grid.getPosition(rootNode), "TestZRotation");
        //evaluator.evaluate();

    }

    public static void TestYRotationInVRep(){

        //SimulationConfiguration.setVrepStartingPort(portNumber);
        int orientation = 0;
        ModuleGrid grid = new ModuleGrid();

        Node rootNode = new Node(0, 0, 0, 0, 0, 0, 0, null);


        //Create two nodes, node1 attached to root on face 4 and node2 to node1 on face 3.
        Node node1 = setUpNewNode(1,rootNode,4,orientation);
        grid.insertRootNode(rootNode);
        grid.insertNode(4,node1,rootNode);
        Node node2 = setUpNewNode(1,node1,3,orientation);
        grid.insertNode(3,node2,node1);


        TreeIndividual tree = new TreeIndividual();
        tree.init(141);
        tree.setRootNode(rootNode);
        tree.modifyChromosome();

        DrawStructure.run(grid.getGridPositionMap().values(), grid.getPosition(rootNode), "TestYRotation");
       /* VrepEvaluator evaluator = new VrepEvaluator(tree.getChromosomeAt(0), "", 0);
        evaluator.getVrepPing();
        evaluator.getVrepPing();*/
       // evaluator.evaluate();
    }


    public static void TestOrientationShift(){

        int orientation = 0;
        ModuleGrid grid = new ModuleGrid();

        Node rootNode = new Node(0, 0, 0, 0, 0, 0, 0, null);


        //Create two nodes, node1 with orientation 1 attached to root on face 1 and node2 to node1 on face 2.
        Node node1 = setUpNewNode(1,rootNode,1,1);
        grid.insertRootNode(rootNode);
        grid.insertNode(1,node1,rootNode);
        Node node2 = setUpNewNode(1,node1,2,0);
        grid.insertNode(2,node2,node1);


        TreeIndividual tree = new TreeIndividual();
        tree.init(141);
        tree.setRootNode(rootNode);
        tree.modifyChromosome();

        DrawStructure.run(grid.getGridPositionMap().values(),grid.getPosition(rootNode),"TestOrientationAndRotation");
       /* VrepEvaluator evaluator = new VrepEvaluator(tree.getChromosomeAt(0), "", 0);
        evaluator.getVrepPing();
        evaluator.getVrepPing();*/
        // evaluator.evaluate();
    }


    public static void TestOrientationAndRotationOnYAxis(){


        int orientation = 0;
        ModuleGrid grid = new ModuleGrid();

        Node rootNode = new Node(0, 0, 0, 0, 0, 0, 0, null);


        //Create two nodes, node1 with orientation 1 attached to root on face 4 and node2 to node1 on face 2.
        Node node1 = setUpNewNode(1,rootNode,4,1);
        grid.insertRootNode(rootNode);
        grid.insertNode(4,node1,rootNode);
        Node node2 = setUpNewNode(1,node1,2,0);
        grid.insertNode(2,node2,node1);


        TreeIndividual tree = new TreeIndividual();
        tree.init(141);
        tree.setRootNode(rootNode);
        tree.modifyChromosome();

        DrawStructure.run(grid.getGridPositionMap().values(),grid.getPosition(rootNode),"TestOrientationAndRotation");
      /*  VrepEvaluator evaluator = new VrepEvaluator(tree.getChromosomeAt(0), "", 0);
        evaluator.getVrepPing();
        evaluator.getVrepPing();*/

        // evaluator.evaluate();

    }

    public static void edgeCase(){
        //Root node (Base module)
        Node rootN = new Node(0,0,0,0,0,0,0,null);
        ModuleGrid grid = new ModuleGrid(10,10,10,10);
        //Insertion
        grid.insertRootNode(rootN);
        GridPositionProperties rootProp = grid.getPositionProperties(rootN);

        Node n1 = setUpNewNode(1, rootN, 1, 1);
        grid.insertNode(rootProp.getFacePosition(1),n1,rootN);
        GridPositionProperties n1Prop = grid.getPositionProperties(n1);

        Node n2 = setUpNewNode(1, n1, 2, 1);
        grid.insertNode(n1Prop.getFacePosition(2), n2, n1);
        GridPositionProperties n2Prop = grid.getPositionProperties(n2);

        Node n3 = setUpNewNode(1, n2, 3, 1);
        grid.insertNode(n2Prop.getFacePosition(3), n3, n2);
        GridPositionProperties n3Prop = grid.getPositionProperties(n3);

        Node n4 = setUpNewNode(1, n3, 2, 0);
        grid.insertNode(n3Prop.getFacePosition(2), n4, n3);
        GridPositionProperties n4Prop = grid.getPositionProperties(n4);

        Node n5 = setUpNewNode(1, n4, 1, 0);
        grid.insertNode(n4Prop.getFacePosition(1), n5, n4);
        GridPositionProperties n5Prop = grid.getPositionProperties(n5);

        Node n6 = setUpNewNode(1, n5, 3, 0);
        grid.insertNode(n5Prop.getFacePosition(3), n6, n5);
        GridPositionProperties n6Prop = grid.getPositionProperties(n6);

        Node n7 = setUpNewNode(1, n6, 3, 1);
        grid.insertNode(n6Prop.getFacePosition(3), n7, n6);
        GridPositionProperties n7Prop = grid.getPositionProperties(n7);

        Node n8 = setUpNewNode(1, n7, 1, 1);
        grid.insertNode(n7Prop.getFacePosition(1), n8, n7);
        GridPositionProperties n8Prop = grid.getPositionProperties(n8);

       /* Node n9 = setUpNewNode(1, n8, 2 ,1);
        grid.insertNode(n8Prop.getFacePosition(2), n9, n8);
        GridPositionProperties n9Prop = grid.getPositionProperties(n9);
*/

        TreeIndividual tree = new TreeIndividual();
        tree.init(141);
        tree.setRootNode(rootN);
        tree.modifyChromosome();
        System.out.println(tree.toString());


        DrawStructure.run(grid.getGridPositionMap().values(), rootProp.getPosition(), "TEST 10");



    }


    public static void edgeCase_2(){


        Node rootN = new Node(0,0,0,0,0,0,0,null);
        ModuleGrid grid = new ModuleGrid(10,10,10,10);
        //Insertion
        grid.insertRootNode(rootN);
        GridPositionProperties rootProp = grid.getPositionProperties(rootN);

        Node n1 = setUpNewNode(1, rootN, 1, 1);
        grid.insertNode(rootProp.getFacePosition(1),n1,rootN);
        GridPositionProperties n1Prop = grid.getPositionProperties(n1);

        Node n2 = setUpNewNode(1, n1, 1, 0);
        grid.insertNode(n1Prop.getFacePosition(1), n2, n1);
        GridPositionProperties n2Prop = grid.getPositionProperties(n2);

        DrawStructure.run(grid.getGridPositionMap().values(),rootProp.getPosition(),"FUCK");

        TreeIndividual tree = new TreeIndividual();
        tree.init(141);
        tree.setRootNode(rootN);
        tree.modifyChromosome();
        System.out.println(tree.toString());
        /*
        Node n3 = setUpNewNode(1, n2, 3, 1);
        grid.insertNode(n2Prop.getFacePosition(3), n3, n2);
        GridPositionProperties n3Prop = grid.getPositionProperties(n3);*/



    }

    public static void edgeCase_3(){

        Node rootN = new Node(0,0,0,0,0,0,0,null);
        ModuleGrid grid = new ModuleGrid(10,10,10,10);
        //Insertion
        grid.insertRootNode(rootN);
        GridPositionProperties rootProp = grid.getPositionProperties(rootN);

        Node n1 = setUpNewNode(1, rootN, 0, 0);
        grid.insertNode(rootProp.getFacePosition(0),n1,rootN);
        GridPositionProperties n1Prop = grid.getPositionProperties(n1);

        Node n2 = setUpNewNode(1, rootN, 2, 0);
        grid.insertNode(rootProp.getFacePosition(2), n2,rootN);
        GridPositionProperties n2Prop = grid.getPositionProperties(n2);

        Node n3 = setUpNewNode(1, n1, 1, 0);
        grid.insertNode(n1Prop.getFacePosition(1), n3, n1);
        GridPositionProperties n3Prop = grid.getPositionProperties(n3);

        Node n4 = setUpNewNode(1, n1, 2, 0);
        grid.insertNode(n1Prop.getFacePosition(2), n4, n1);
        GridPositionProperties n4Prop = grid.getPositionProperties(n4);

        Node n5 = setUpNewNode(1, n4, 2, 0);
        grid.insertNode(n4Prop.getFacePosition(2), n5, n4);
        GridPositionProperties n5Prop = grid.getPositionProperties(n5);

        Node n6 = setUpNewNode(1, n5, 3, 0);
        grid.insertNode(n5Prop.getFacePosition(3), n6, n5);
        GridPositionProperties n6Prop = grid.getPositionProperties(n6);

        Node n7 = setUpNewNode(1, n6, 3, 0);
        grid.insertNode(n6Prop.getFacePosition(3), n7, n6);
        GridPositionProperties n7Prop = grid.getPositionProperties(n7);

        Node n8 = setUpNewNode(1, n7, 2, 0);
        grid.insertNode(n7Prop.getFacePosition(2), n8, n7);
        GridPositionProperties n8Prop = grid.getPositionProperties(n8);

        Node n9 = setUpNewNode(1, n8, 2 ,1);
        grid.insertNode(n8Prop.getFacePosition(2), n9, n8);
        GridPositionProperties n9Prop = grid.getPositionProperties(n9);


        TreeIndividual tree = new TreeIndividual();
        tree.init(141);
        tree.setRootNode(rootN);
        tree.modifyChromosome();
        System.out.println(tree.toString());


        DrawStructure.run(grid.getGridPositionMap().values(), rootProp.getPosition(), "TEST 10");




    }

    public static void edgeCase_4(){

        //Root node (Base module)
        Node rootN = new Node(0,0,0,0,0,0,0,null);
        ModuleGrid grid = new ModuleGrid(10,10,10,10);
        //Insertion
        grid.insertRootNode(rootN);
        GridPositionProperties rootProp = grid.getPositionProperties(rootN);


        Node n1 = setUpNewNode(1, rootN, 0, 1);

        grid.insertNode(rootProp.getFacePosition(0),n1,rootN);
        GridPositionProperties n1Prop = grid.getPositionProperties(n1);
        GridPosition n1Pos = n1Prop.getPosition();

        Node n2 = setUpNewNode(1,n1, 2, 0);
        grid.insertNode(n1Prop.getFacePosition(2), n2, n1);

        DrawStructure.run(grid.getGridPositionMap().values(),rootProp.getPosition(),"FUCK");

        TreeIndividual tree = new TreeIndividual();
        tree.init(141);
        tree.setRootNode(rootN);
        tree.modifyChromosome();
        System.out.println(tree.toString());
    }


}
