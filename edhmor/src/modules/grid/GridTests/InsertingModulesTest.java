package modules.grid.GridTests;

import modules.grid.modulegrid.DrawStructure;
import modules.grid.modulegrid.GridPosition;
import modules.grid.modulegrid.GridPositionProperties;
import modules.grid.modulegrid.ModuleGrid;
import modules.individual.Connection;
import modules.individual.Node;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

/**
 * Created by i5-4670K on 06-04-2017.
 */
public class InsertingModulesTest {
    GridPositionProperties rootProp;
    ModuleGrid grid;
    Node rootN;

    @Before
    public void init(){
        //Initial grid of 5x5x5 with max modules allowed of 5
        grid = new ModuleGrid(5,5,5,5);
    //Root node (Base module)
    rootN = new Node(0,0,0,0,0,0,0,null);

    //Insertion
    grid.insertRootNode(rootN);
    rootProp = grid.getPositionProperties(rootN);
}

    private Node newNode(int type, Node parent, int face, int orientation){
        Node n1 = new Node(type,0,0,0,0,0,0,parent);
        Connection con = new Connection(parent,n1,face,orientation);
        parent.addChildren(n1,con);
        return n1;
    }



    @Test
    public void TestModulePositionsInGrid(){

        //insert child node to face 1 of rootNode
        //Node(int type, double weighing, double amplitudeControl, double angularFreqControl, double controlOffset,
        // double amplitudeModulator, double frequencyModulator, Node dad)

        GridPosition rootFace1 = rootProp.getFacePosition(1);
        Node n1 = newNode(1, rootN, 1, 0);
        grid.insertNode(rootFace1,n1,rootN);
        GridPositionProperties n1Prop = grid.getPositionProperties(n1);


        //insert n2 to face 2 of n1
        GridPosition n1Face2 = n1Prop.getFacePosition(2);
        Node n2 = newNode(1, n1, 2, 0);

        grid.insertNode(n1Face2, n2, n1);


        Map<Node,GridPosition> positionMap = grid.getGridPositionMap();

        GridPosition actualPositionRoot = positionMap.get(rootN);
        GridPosition actualPositionN1 = positionMap.get(n1);
        GridPosition actualPositionN2 = positionMap.get(n2);

        GridPosition expectedRoot = new GridPosition(2,2,2);
        GridPosition expectedn1 = new GridPosition(3,2,2);
        GridPosition expectedn2 = new GridPosition(3,2,3);


        Assert.assertEquals(expectedRoot, actualPositionRoot);
        Assert.assertEquals(expectedn1, actualPositionN1);
        Assert.assertEquals(expectedn2, actualPositionN2);

    }

    @Test
    public void TestUpdatedFacePositionsZ_AxisRotation(){
        //Attach n1 to root on face 0 with orientation 0

        Node n1 = newNode(1,rootN,0,0);
        grid.insertNode(rootProp.getFacePosition(0),n1,rootN);
        GridPositionProperties n1Prop = grid.getPositionProperties(n1);
        GridPosition n1Pos = n1Prop.getPosition();



        GridPosition aface1 = n1Prop.getFacePosition(1);
        GridPosition aface2 = n1Prop.getFacePosition(2);
        GridPosition aface3 = n1Prop.getFacePosition(3);

        GridPosition eface1 = n1Pos.translate(-1,0,0);
        GridPosition eface2 = n1Pos.translate(0,0,1);
        GridPosition eface3 = n1Pos.translate(0,0,-1);

        Assert.assertEquals(eface1,aface1);
        Assert.assertEquals(eface2,aface2);
        Assert.assertEquals(eface3,aface3);

    }

    @Test
    public void TestUpdatedFacePositionsY_AxisRotation(){
        //Attach node to root on face 4 with orientation 0

        Node n1 = newNode(1,rootN,4,0);

        grid.insertNode(rootProp.getFacePosition(4),n1,rootN);
        GridPositionProperties n1Prop = grid.getPositionProperties(n1);
        GridPosition n1Pos = n1Prop.getPosition();

        GridPosition aface1 = n1Prop.getFacePosition(1);
        GridPosition aface2 = n1Prop.getFacePosition(2);
        GridPosition aface3 = n1Prop.getFacePosition(3);

        GridPosition eface1 = n1Pos.translate(0,0,1);
        GridPosition eface2 = n1Pos.translate(-1,0,0);
        GridPosition eface3 = n1Pos.translate(1,0,0);

        Assert.assertEquals(eface1,aface1);
        Assert.assertEquals(eface2,aface2);
        Assert.assertEquals(eface3,aface3);

    }


    @Test
    public void TestUpdatedFacePositionsFace2(){
        //Attach node to root on face 2 with orientation 0


        Node n1 = newNode(1,rootN,2,0);

        grid.insertNode(rootProp.getFacePosition(2),n1,rootN);
        GridPositionProperties n1Prop = grid.getPositionProperties(n1);
        GridPosition n1Pos = n1Prop.getPosition();

        GridPosition aface1 = n1Prop.getFacePosition(1);
        GridPosition aface2 = n1Prop.getFacePosition(2);
        GridPosition aface3 = n1Prop.getFacePosition(3);

        GridPosition eface1 = n1Pos.translate(0,1,0);
        GridPosition eface2 = n1Pos.translate(0,0,1);
        GridPosition eface3 = n1Pos.translate(0,0,-1);

        Assert.assertEquals(eface1,aface1);
        Assert.assertEquals(eface2,aface2);
        Assert.assertEquals(eface3,aface3);

    }


    @Test
    public void TestOrientationShiftedFace1(){
        //Attach n1 to root on face 1 with orientation of 1

        Node n1 = newNode(1,rootN,1,1);

        grid.insertNode(rootProp.getFacePosition(1),n1,rootN);
        GridPositionProperties n1Prop = grid.getPositionProperties(n1);
        GridPosition n1Pos = n1Prop.getPosition();

        GridPosition aface1 = n1Prop.getFacePosition(1);
        GridPosition aface2 = n1Prop.getFacePosition(2);
        GridPosition aface3 = n1Prop.getFacePosition(3);

        GridPosition eface1 = n1Pos.translate(1,0,0);
        GridPosition eface2 = n1Pos.translate(0,1,0);
        GridPosition eface3 = n1Pos.translate(0,-1,0);

        Assert.assertEquals(eface1,aface1);
        Assert.assertEquals(eface2,aface2);
        Assert.assertEquals(eface3,aface3);

    }

    @Test
    public void rotationAndOrientationShiftedFace0(){
        //Attach node to root on face 0 with orientation 1

        Node n1 = newNode(1,rootN,0,1);

        grid.insertNode(rootProp.getFacePosition(0),n1,rootN);
        GridPositionProperties n1Prop = grid.getPositionProperties(n1);
        GridPosition n1Pos = n1Prop.getPosition();

        GridPosition aface1 = n1Prop.getFacePosition(1);
        GridPosition aface2 = n1Prop.getFacePosition(2);
        GridPosition aface3 = n1Prop.getFacePosition(3);

        GridPosition eface1 = n1Pos.translate(-1,0,0);
        GridPosition eface2 = n1Pos.translate(0,1,0);
        GridPosition eface3 = n1Pos.translate(0,-1,0);


        Assert.assertEquals(eface1,aface1);
        Assert.assertEquals(eface2,aface2);
        Assert.assertEquals(eface3,aface3);





    }


    @Test
    public void TestOrientation1ForMultipleModulesStructure(){
        grid = new ModuleGrid(10,10,10,10);
        grid.insertRootNode(rootN);
        rootProp = grid.getPositionProperties(rootN);

        Node n1 = newNode(1,rootN,1,1);
        grid.insertNode(rootProp.getFacePosition(1),n1,rootN);
        GridPositionProperties n1Prop = grid.getPositionProperties(n1);

        Node n2 = newNode(1,n1,2,1);
        grid.insertNode(n1Prop.getFacePosition(2),n2,n1);
        GridPositionProperties n2Prop = grid.getPositionProperties(n2);

        Node n3 = newNode(1,n2,3,1);
        grid.insertNode(n2Prop.getFacePosition(3), n3, n2);
        GridPositionProperties n3Prop = grid.getPositionProperties(n3);

        Node n4 = newNode(1,n3,2,0);
        grid.insertNode(n3Prop.getFacePosition(2), n4, n3);
        GridPositionProperties n4Prop = grid.getPositionProperties(n4);

        Node n5 = newNode(1,n4,1,0);
        grid.insertNode(n4Prop.getFacePosition(1), n5, n4);
        GridPositionProperties n5Prop = grid.getPositionProperties(n5);

        Node n6 = newNode(1, n5, 3, 0);
        grid.insertNode(n5Prop.getFacePosition(3), n6, n5);
        GridPositionProperties n6Prop = grid.getPositionProperties(n6);

        Node n7 = newNode(1, n6, 3, 1);
        grid.insertNode(n6Prop.getFacePosition(3), n7, n6);
        GridPositionProperties n7Prop = grid.getPositionProperties(n7);

        Node n8 = newNode(1, n7, 1, 1);
        grid.insertNode(n7Prop.getFacePosition(1), n8, n7);
        GridPositionProperties n8Prop = grid.getPositionProperties(n8);

        DrawStructure.run(grid.getGridPositionMap().values(), rootProp.getPosition(), "TEST 10");

        boolean isAllowed = grid.isAllowed(n8Prop.getFacePosition(2));

        Assert.assertTrue(!isAllowed);



    }

    @Test
    public void edgeCase2(){

        Node rootN = new Node(0,0,0,0,0,0,0,null);
        ModuleGrid grid = new ModuleGrid(10,10,10,10);
        //Insertion
        grid.insertRootNode(rootN);
        GridPositionProperties rootProp = grid.getPositionProperties(rootN);

        Node n1 = newNode(1, rootN, 0, 0);
        grid.insertNode(rootProp.getFacePosition(0),n1,rootN);
        GridPositionProperties n1Prop = grid.getPositionProperties(n1);

        Node n2 = newNode(1, rootN, 2, 0);
        grid.insertNode(rootProp.getFacePosition(2), n2,rootN);
        GridPositionProperties n2Prop = grid.getPositionProperties(n2);

        Node n3 = newNode(1, n1, 1, 0);
        grid.insertNode(n1Prop.getFacePosition(1), n3, n1);
        GridPositionProperties n3Prop = grid.getPositionProperties(n3);

        Node n4 = newNode(1, n1, 2, 0);
        grid.insertNode(n1Prop.getFacePosition(2), n4, n1);
        GridPositionProperties n4Prop = grid.getPositionProperties(n4);

        Node n5 = newNode(1, n4, 2, 0);
        grid.insertNode(n4Prop.getFacePosition(2), n5, n4);
        GridPositionProperties n5Prop = grid.getPositionProperties(n5);

        Node n6 = newNode(1, n5, 3, 0);
        grid.insertNode(n5Prop.getFacePosition(3), n6, n5);
        GridPositionProperties n6Prop = grid.getPositionProperties(n6);

        Node n7 = newNode(1, n6, 3, 0);
        grid.insertNode(n6Prop.getFacePosition(3), n7, n6);
        GridPositionProperties n7Prop = grid.getPositionProperties(n7);

        Node n8 = newNode(1, n7, 2, 0);
        grid.insertNode(n7Prop.getFacePosition(2), n8, n7);
        GridPositionProperties n8Prop = grid.getPositionProperties(n8);

        Node n9 = newNode(1, n8, 2, 1);
        boolean allowed = grid.insertNode(n8Prop.getFacePosition(2), n9, n8);

        Assert.assertTrue(!allowed);

        //GridPositionProperties n9Prop = grid.getPositionProperties(n9);




    }






}
