package modules.grid.GridTests;

import modules.grid.modulegrid.GridPosition;
import modules.grid.modulegrid.GridPositionProperties;
import modules.grid.modulegrid.ModuleGrid;
import modules.grid.ShapeFitnessCalculator;
import modules.individual.Connection;
import modules.individual.Node;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Kevin on 20-Apr-17.
 */
public class LineValidatorTest {

    private GridPositionProperties rootProp;
    private ModuleGrid grid;
    private Node rootN;
    private int findLongestLine(int[][][] g, int axis){
        int l = g.length;
        int longestLine = 0, currentLine = 0;

        for(int i = 0; i < l; i++) {
            for (int j = 0; j < l; j++) {

                if (currentLine > longestLine) longestLine = currentLine;
                currentLine = 0;

                for (int k = 0; k < l; k++) {
                    if (currentLine > longestLine) {
                        longestLine = currentLine;
                    }

                    if (axis == 1) {
                        if (g[k][j][i] != 0)
                            currentLine++;
                        else currentLine = 0;

                    } else if (axis == 2) {
                        if (g[i][k][j] != 0)
                            currentLine++;
                        else currentLine = 0;

                    } else {
                        if (g[i][j][k] != 0)
                            currentLine++;
                        else currentLine = 0;

                    }

                }
            }
        }

        return longestLine;

    }



    @Test
    public void TestX_AxisLine() {

        int[][][] testG = new int[10][10][10];

        testG[1][2][3] = 1;
        testG[2][2][3] = 1;
        testG[3][2][3] = 1;
        testG[4][2][3] = 1;
        testG[5][3][3] = 1;
        testG[9][1][1] = 1;
        testG[0][2][1] = 1;
        testG[3][6][3] = 1;
        testG[2][4][5] = 1;
        testG[3][6][2] = 1;

        int l = findLongestLine(testG,1);

        Assert.assertEquals(4, l);
    }


    @Before
    public void init(){
        //Initial grid of 5x5x5 with max modules allowed of 5
        grid = new ModuleGrid(10,10,10,15);
        //Root node (Base module)
        rootN = new Node(0,0,0,0,0,0,0,null);

        //Insertion
        grid.insertRootNode(rootN);
        rootProp = grid.getPositionProperties(rootN);
    }

    private Node newNode(int type, Node parent, int face, int orientation){
        Node n1 = new Node(type,0,0,0,0,0,0,parent);
        Connection con = new Connection(parent,n1,face,orientation);
        parent.addChildren(n1,con);
        return n1;
    }

    @Test
    public void TestShapeValidatorLinePerfect(){

        GridPosition rootFace1 = rootProp.getFacePosition(1);
        Node n1 = newNode(1, rootN, 1, 0);
        grid.insertNode(rootFace1,n1,rootN);
        GridPositionProperties n1Prop = grid.getPositionProperties(n1);

        GridPosition f = n1Prop.getFacePosition(1);
        Node n2 = newNode(1, n1, 1, 0);
        grid.insertNode(f,n2,n1);
        GridPositionProperties n2Prop = grid.getPositionProperties(n2);

        GridPosition f2 = n2Prop.getFacePosition(1);
        Node n3 = newNode(1,n2,1,0);
        grid.insertNode(f2,n3,n2);
        GridPositionProperties n3Prop = grid.getPositionProperties(n3);

        GridPosition f3 = n3Prop.getFacePosition(1);
        Node n4 = newNode(1,n3,1,0);
        grid.insertNode(f3,n4,n3);
        GridPositionProperties n4Prop = grid.getPositionProperties(n4);
/*
        GridPosition f4 = n4Prop.getFacePosition(2);
        Node n5 = newNode(1,n4,1,0);
        grid.insertNode(f4,n5,n4);
*/
        int[] args = new int[]{5,1};
        double fitness = ShapeFitnessCalculator.evaluate(grid,"line",args);

        Assert.assertEquals(fitness, 1.0,0.001);

    }

    @Test
    public void randomRoundTest(){

        double x = 0.488;

        long expected =  Math.round(x);
        System.out.println(expected);
        Assert.assertEquals(expected,0);


    }


}
