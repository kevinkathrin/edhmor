package modules.grid.modulegrid;

import modules.evaluation.overlapping.CollisionDetector;
import modules.evaluation.overlapping.CollisionDetectorFactory;
import modules.individual.Node;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ModuleGrid {

    private HashMap<Node,GridPosition> gridPosition = new HashMap<>();    //Maps a node to its position in the grid
    private HashMap<Node,Vector3D> nodeRotation = new HashMap<>();   //Maps a node to its rotation around itself
    private Node rootNode;
    private GridPositionProperties[][][] grid;
    private int maxModules, xL,yL,zL;
    private int nodesInGrid;
    private CollisionDetector cd;





    public ModuleGrid() {
        this(10,10,10);
    }

    public ModuleGrid(int x, int y, int z) {
        this(x, y, z, 16);
    }

    public ModuleGrid(int x, int y, int z, int max){
        this.grid = new GridPositionProperties[x][y][z];
        this.maxModules = max;
        xL = x; yL = y; zL = z;
        nodesInGrid = 0;
        cd = CollisionDetectorFactory.getCollisionDetector();
    }

    public Iterable<Node> getAllNodes(){
        return gridPosition.keySet();
    }

    public GridPositionProperties[][][] getGridProperties() {
        return grid;
    }

    public void setGridProperties(GridPositionProperties[][][] grid) {
        this.grid = grid;
    }

    public Node[][][] getNodeGrid(){
        Node[][][] nodes = new Node[xL][yL][zL];

        for(int i = 0; i < xL; i++)
            for(int j = 0; j < yL; j++)
                for(int k = 0; k < zL; k++)
                    nodes[i][j][k] = (grid[i][j][k] == null) ? null : grid[i][j][k].getNode();
        return nodes;
    }


    public boolean insertRootNode(Node rn){
        rootNode = rn;
        int x_rn = xL/2;
        int y_rn = yL/2;
        int z_rn = zL/2;
        GridPosition gridPos = new GridPosition(x_rn,y_rn,z_rn);
        GridPositionProperties rootProp = new GridPositionProperties(gridPos, rn);
        rootProp.setFacePositions(getRootFacePositions(gridPos));
        gridPosition.put(rn,gridPos);
        grid[x_rn][y_rn][z_rn] = rootProp;
        nodesInGrid++;
        return true;
    }

    public boolean insertNode(GridPosition position, Node child, Node parent){

        if(insideBounds(position) && !nodeExists(position)){
            int face = parent.getConnection(child).getDadFace();
            grid[position.x][position.y][position.z] = new GridPositionProperties(position,child,getPositionProperties(parent),face);
            gridPosition.put(child,position);
            nodesInGrid++;
            return true;
        }else return false;
    }

    public boolean insertNode(int parentFace, Node child, Node parent){

        GridPositionProperties pProp = getPositionProperties(parent);
        GridPosition faceP = pProp.getFacePosition(parentFace);

        return insertNode(faceP,child,parent);

    }

    public Map<Node,GridPosition> getGridPositionMap(){

        return gridPosition;
    }


    public boolean nodeExists(GridPosition gp){
        return grid[gp.x][gp.y][gp.z] != null ;
    }

    public Node getRootNode(){
        return rootNode;
    }

    public boolean isAllowed(GridPosition gp) {return (insideBounds(gp) && !nodeExists(gp));}

    public int numberOfNodes(){
        return nodesInGrid;
    }

    public Node getNode(GridPosition gp){
        return getPositionProperties(gp).getNode();
    }

    public GridPosition getPosition(Node n){
        return getPositionProperties(n).getPosition();
    }


    public GridPositionProperties getPositionProperties(Node n){
        GridPosition pos = gridPosition.get(n);
        return getPositionProperties(pos);
    }

    public GridPositionProperties getPositionProperties(GridPosition gp){
        return grid[gp.x][gp.y][gp.z];
    }

    public boolean insideBounds(GridPosition gp){
        if(gp.x >= 0 && gp.y >= 0 && gp.z >= 0)
            return (gp.x < xL && gp.y < yL && gp.z < zL);
        return false;
    }


    public Node[] getNeighbours(Node n){
        GridPosition nPos = getPosition(n);
        Node[] neighbours = new Node[6];
        GridPosition[] neighboursPosition = new GridPosition[6];

        neighboursPosition[0] = nPos.translate(1, 0, 0);
        neighboursPosition[1] = nPos.translate(-1,0,0);
        neighboursPosition[2] = nPos.translate(0,1,0);
        neighboursPosition[3] = nPos.translate(0,-1,0);
        neighboursPosition[4] = nPos.translate(0,0,1);
        neighboursPosition[5] = nPos.translate(0,0,-1);

        for(int i = 0; i < neighboursPosition.length; i++)
            neighbours[i] = getNode(neighboursPosition[i]);

        return neighbours;
    }

    private GridPosition[] getRootFacePositions(GridPosition gp){
        GridPosition[] cps = new GridPosition[5];
        cps[0] = new GridPosition(gp.getX()-1,gp.getY(),gp.getZ());
        cps[1] = new GridPosition(gp.getX()+1,gp.getY(),gp.getZ());
        cps[2] = new GridPosition(gp.getX(),gp.getY()-1,gp.getZ());
        cps[3] = new GridPosition(gp.getX(),gp.getY()+1,gp.getZ());
        cps[4] = new GridPosition(gp.getX(),gp.getY(),gp.getZ()+1);
        return cps;
    }
    public int getXLength() {
        return xL;
    }

    public int getYLength() {
        return yL;
    }

    public int getZLength() {
        return zL;
    }

}


