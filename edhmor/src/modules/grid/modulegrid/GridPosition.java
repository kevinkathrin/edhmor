package modules.grid.modulegrid;

import com.sun.istack.internal.Nullable;
import modules.grid.modulegrid.GridPositionProperties.Direction;
/**
 * Created by KathrinJonsson on 30-03-2017.
 */
public class GridPosition {

    int x;
    int y;
    int z;



    public GridPosition(int x, int y, int z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    public GridPosition translate(GridPosition tp){
        return translate(tp.x,tp.y,tp.z);
    }

    public GridPosition translate( int tx, int ty, int tz){
        return new GridPosition(x+tx,y+ty,z+tz);

    }

    @Override
    public String toString() {
        return "GridPosition{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }

    @Nullable
    public static GridPositionProperties.Direction getDirection(GridPosition from, GridPosition to) {

        if(from.x != to.x)
            return ((to.x - from.x) < 0) ? Direction.X_MINUS : Direction.X_PLUS;
        else if(from.y != to.y)
            return ((to.y - from.y) < 0) ? Direction.Y_MINUS : Direction.Y_PLUS;
        else if(from.z != to.z)
            return ((to.z - from.z) < 0) ? Direction.Z_PLUS : Direction.Z_MINUS;
        else return null;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GridPosition that = (GridPosition) o;

        if (x != that.x) return false;
        if (y != that.y) return false;
        return z == that.z;

    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        result = 31 * result + z;
        return result;
    }
}
