package modules.grid.modulegrid;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class DrawStructure extends JPanel {
    private static final int D_W = 300;
    private static final int D_H = 300;
    private static final int OFFSET_H = 100, OFFSET_W = 100;
    private String view;
    private BufferedImage paintImage = new BufferedImage(300,300,BufferedImage.TYPE_3BYTE_BGR);

    private enum CameraPosition {
        TOP_DOWN, FROM_SIDE
    }

    List<Rectangle> squares;

    public DrawStructure(Collection<GridPosition> cubes, GridPosition rp, CameraPosition cameraPosition) {
        squares = new ArrayList<>();
        int rx = rp.x, ry = rp.y, rz = rp.z;

        for (GridPosition p : cubes) {
            Rectangle s;
            int x = OFFSET_W + p.x * 10, size = 10, y = 0;

            switch (cameraPosition) {
                case TOP_DOWN:
                    y += ry + (ry - p.y);
                    y *= size;
                    s = new Rectangle(x, y + OFFSET_H, size, size);
                    break;
                case FROM_SIDE:
                    y += rz + (rz - p.z);
                    y *= size;
                    s = new Rectangle(x, y + OFFSET_H, size, size);
                    break;
                default:
                    s = null;
                    break;
            }
            squares.add(s);
        }
        view = (cameraPosition == CameraPosition.FROM_SIDE) ? "From Side" : "Top Down";

        Graphics2D g = paintImage.createGraphics();

        g.setColor(Color.WHITE);
        g.fillRect(0,0, paintImage.getWidth(), paintImage.getHeight());
        g.setColor(Color.BLACK);
        for (Shape s : squares)
            g.draw(s);

        g.drawString(view, 10, 10);

        g.dispose();

    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(paintImage,0,0,null);
    }

    public BufferedImage getImage(){
        return paintImage;
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(D_W, D_H);
    }

    public static void run(Collection<GridPosition> positions, GridPosition rootposition, String name) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                JFrame frame = new JFrame();
                frame.setTitle(name);
                JPanel panel = new JPanel();
                frame.getContentPane().add(panel);
                panel.add(new DrawStructure(positions, rootposition, CameraPosition.FROM_SIDE));
                panel.add(new DrawStructure(positions, rootposition, CameraPosition.TOP_DOWN));
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.pack();
                frame.setLocationRelativeTo(null);
                frame.setVisible(true);
            }
        });
    }


    public static BufferedImage getImages(Collection<GridPosition> positions, GridPosition rootposition){
        DrawStructure dSide = new DrawStructure(positions, rootposition, CameraPosition.FROM_SIDE);
        DrawStructure dTop = new DrawStructure(positions,rootposition,CameraPosition.TOP_DOWN);
        return joinBufferedImage(dSide.getImage(),dTop.getImage());
    }


    /**
     * join two BufferedImage
     * you can add a orientation parameter to control direction
     * you can use a array to join more BufferedImage
     */

    private static BufferedImage joinBufferedImage(BufferedImage img1,BufferedImage img2) {

        //do some calculate first
        int offset  = 5;
        int wid = img1.getWidth()+img2.getWidth()+offset;
        int height = Math.max(img1.getHeight(),img2.getHeight())+offset;
        //create a new buffer and draw two image into the new image
        BufferedImage newImage = new BufferedImage(wid,height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = newImage.createGraphics();
        Color oldColor = g2.getColor();
        //fill background
        g2.setPaint(Color.BLACK);
        g2.fillRect(0, 0, wid, height);
        //draw image
        g2.setColor(Color.WHITE);
        g2.drawImage(img1, null, 0, 0);
        g2.drawImage(img2, null, img1.getWidth()+offset, 0);
        g2.dispose();
        return newImage;
    }
}