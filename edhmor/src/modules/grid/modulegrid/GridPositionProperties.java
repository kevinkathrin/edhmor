package modules.grid.modulegrid;

import modules.ModuleSet;
import modules.ModuleSetFactory;
import modules.individual.Connection;
import modules.individual.Node;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import java.util.Arrays;

/**
 * Created by Kevin on 30-Mar-17.
 */
public class GridPositionProperties {
    private Node node,parentNode;
    private int connectionFace;
    private GridPositionProperties parentProp;
    private GridPosition position, parentPosition;
    private GridPosition[] normalVectors;
    private GridPosition[] freeFacePositions;   //Array of free faces on module, the position is the that of the new attached nodes would be position in the grid.
    private GridPosition[][] faceNormalVectors;


    public enum Direction{
        X_MINUS, X_PLUS, Z_PLUS, Z_MINUS, Y_PLUS, Y_MINUS
    }

    public GridPositionProperties(int x, int y, int z, Node n){
        this(new GridPosition(x,y,z), n);
    }

    public GridPositionProperties(GridPosition gp, Node n){
        this(gp, n, null, 0);
    }

    public GridPositionProperties(GridPosition position, Node child, GridPositionProperties parent, int face) {
        this.position = position;
        parentProp = parent;
        this.node = child;
        connectionFace = face;

        if(parent != null){
            this.parentNode = parent.getNode();
            this.parentPosition = parent.getPosition();
        }
        setProperties();
    }

    public int getNumberOfFreeFace(){
        return node.getFreeFaces();
    }

    public int[] getFreeFaces(){
        int freeFacesFound = 0,faceIndex = 0, numFreeFaces = node.getFreeFaces();
        int[] faces = new int[numFreeFaces];

        while(freeFacesFound < numFreeFaces){
            if(node.isFaceFree(faceIndex))
                faces[freeFacesFound++] = faceIndex++;
            else faceIndex++;
        }
        return faces;
    }


    public GridPosition getFacePosition(int face){
        return freeFacePositions[face];
    }

    void setFacePositions(GridPosition[] positions){
        freeFacePositions = positions;
    }

    public Node getNode() {
        return node;
    }

    public GridPosition getPosition() {
        return position;
    }

    public Node getParent(){return parentNode;}


    private void setProperties() {
        ModuleSet set = ModuleSetFactory.getModulesSet();
        freeFacePositions = new GridPosition[5];
        freeFacePositions[0] = null; //Face 0 is base face.
        normalVectors = new GridPosition[5];
        faceNormalVectors = new GridPosition[5][];

        //Initiate normal vector sets
        for(int i = 0; i < faceNormalVectors.length; i++){
            faceNormalVectors[i] = new GridPosition[5];
        }

        //Set up own normal vectors to each face
        if(node.getType() != 0) {
            normalVectors = parentProp.faceNormalVectors[connectionFace];
            if(parentNode.getConnection(node).getChildrenOrientation() == 1){
                normalVectors = handleOrientation(normalVectors);
            }
        }
        else{   //Edge case for base cube
            int[] indexFreeFaces = getFreeFaces();
            for(int i : indexFreeFaces){
                int type = node.getType();
                Vector3D temp = set.getNormalFaceVector(type, i);
                GridPosition initialFacePosition = new GridPosition((int)temp.getX(),(int)temp.getY(),(int)temp.getZ());
                normalVectors[i] = initialFacePosition;
            }
        }

        //Translate to find position of each module
        for(int i = 0; i < freeFacePositions.length; i++){
            GridPosition gp = normalVectors[i];
            if(gp != null){
                freeFacePositions[i] = position.translate(gp);
            }
        }

        //Calculate the normal vectors for each its children.
        if(node.getType() == 1)
            faceNormalVectors = setFaceNormalVectors(normalVectors, freeFacePositions);
        else faceNormalVectors = setBaseCubeFaceNormalVectors();


    }

    private GridPosition[][] setBaseCubeFaceNormalVectors() {

        //Manually set up for baseCube...
        faceNormalVectors[0][1] = new GridPosition(-1,0,0);
        faceNormalVectors[0][2] = new GridPosition(0,0,1);
        faceNormalVectors[0][3] = new GridPosition(0,0,-1);


        faceNormalVectors[1][1] = new GridPosition(1,0,0);
        faceNormalVectors[1][2] = new GridPosition(0,0,1);
        faceNormalVectors[1][3] = new GridPosition(0,0,-1);


        faceNormalVectors[2][1] = new GridPosition(0,1,0);
        faceNormalVectors[2][2] = new GridPosition(0,0,1);
        faceNormalVectors[2][3] = new GridPosition(0,0,-1);


        faceNormalVectors[3][1] = new GridPosition(0,1,0);
        faceNormalVectors[3][2] = new GridPosition(0,0,1);
        faceNormalVectors[3][3] = new GridPosition(0,0,-1);


        faceNormalVectors[4][1] = new GridPosition(0,0,1);
        faceNormalVectors[4][2] = new GridPosition(-1,0,0);
        faceNormalVectors[4][3] = new GridPosition(1,0,0);

        return  faceNormalVectors;


    }

    private GridPosition[][] setFaceNormalVectors(GridPosition[] normalVectors, GridPosition[] freeFacePositions) {
        //Switch on each case setting the values correctly.
        for(int i = 0; i < freeFacePositions.length ; i++) {
            GridPosition facePos = freeFacePositions[i];
            if(facePos == null) continue;
            Direction d = GridPosition.getDirection(position, facePos);
            try {
                switch (d) {
                    case X_MINUS:
                        faceNormalVectors[i][1] = new GridPosition(-1,0,0);
                        break;
                    case X_PLUS:
                        faceNormalVectors[i][1] = new GridPosition(1,0,0);
                        break;
                    case Y_PLUS:
                        faceNormalVectors[i][1] = new GridPosition(0,1,0);
                        break;
                    case Y_MINUS:
                        faceNormalVectors[i][1] = new GridPosition(0,-1,0);
                        break;
                    case Z_PLUS:
                        faceNormalVectors[i][1] = new GridPosition(0,0,1);
                        break;
                    case Z_MINUS:
                        faceNormalVectors[i][1] = new GridPosition(0,0,-1);
                        break;
                }
            } catch (NullPointerException e) {
                System.err.println("Parent and child position is the same position ");
                System.err.print("Parent Position: " + parentPosition.toString() + ", Child Position: " + position.toString());
            }
            switch (i){
                case 1:
                    faceNormalVectors[i][2] = normalVectors[2];
                    faceNormalVectors[i][3] = normalVectors[3];
                    break;
                case 2:
                    if( d == Direction.Z_PLUS)
                    {
                        faceNormalVectors[i][3] = normalVectors[1];
                        faceNormalVectors[i][2] = negateVector(normalVectors[1]);
                        continue;
                    }

                case 3:

                    if(d == Direction.X_MINUS || d == Direction.Z_MINUS || d == Direction.Y_MINUS ){
                        faceNormalVectors[i][2] = negateVector(normalVectors[1]);
                        faceNormalVectors[i][3] = normalVectors[1];

                    }else {
                        faceNormalVectors[i][2] = normalVectors[1];
                        faceNormalVectors[i][3] = negateVector(normalVectors[1]);
                    }
                    break;

            }
        }

        return faceNormalVectors;
    }

    private GridPosition negateVector(GridPosition normalVector) {
        return new GridPosition(normalVector.getX() * -1, normalVector.getY() * -1, normalVector.getZ() * -1);

    }

    private GridPosition[] handleOrientation(GridPosition[] normalVectorsToFace) {
        //find axis to rotate around for orientation
        int dx = normalVectorsToFace[1].getX() + normalVectorsToFace[2].getX() + normalVectorsToFace[3].getX();
        int dy = normalVectorsToFace[1].getY() + normalVectorsToFace[2].getY() + normalVectorsToFace[3].getY();
       // int dz = normalVectorsToFace[1].getZ() + normalVectorsToFace[2].getZ() + normalVectorsToFace[3].getZ();
        String axis;
        if(dx != 0){
            axis = "X";

        }else if(dy != 0){
            axis = "Y";
        }else{
            axis = "Z";
        }


        normalVectorsToFace = rotateFaceArray(normalVectorsToFace, -90, axis);

        return normalVectorsToFace;
    }



    private GridPosition[] rotateFaceArray(GridPosition[] faces,int degrees,String axis ){

        GridPosition[] rotatedFaces = new GridPosition[faces.length];

        for(int i = 0; i < faces.length; i++) {
            GridPosition gp = faces[i];
            if(gp != null) {
                rotatedFaces[i] = rotateOnAxis(axis, degrees,gp);
            }
        }
        return rotatedFaces;

    }

    public GridPosition rotateOnAxis(String axis, int degrees, GridPosition p){

        /*
        Use these transformation matrices for rotating around an axis.
            around the Z-axis would be
            |cos ?   ?sin ?   0| |x|   |x cos ? - y sin ?|   |x'|
            |sin ?    cos ?   0| |y| = |x sin ? + y cos ?| = |y'|
            |  0       0      1| |z|   |        z        |   |z'|

            around the Y-axis would be
            | cos ?    0   sin ?| |x|   | x cos d + z sin d|   |x'|
            |   0      1       0| |y| = |         y        | = |y'|
            |?sin ?    0   cos ?| |z|   |-x sin d + z cos d|   |z'|

            around the X-axis would be
            |1     0           0| |x|   |        x        |   |x'|
           |0   cos ?    ?sin ?| |y| = |y cos d - z sin d| = |y'|
           |0   sin ?     cos ?| |z|   |y sin d + z cos d|   |z'|
         */
        double radians = (degrees * Math.PI) / 180.0;
        double s = Math.sin(radians);
        double c = Math.cos(radians);

        int x,y,z;
        switch (axis){
            case "X":
                x = p.x;
                y = (int)(p.y * c - p.z * s);
                z = (int)(p.y * s + p.z * c);
                break;
            case "Y":
                x = (int)(p.x * c + p.z * s);
                y = p.y;
                z = (int)(-p.x * s + p.z * c);
                break;
            case "Z":
                x = (int)(p.x * c - p.y * s);
                y = (int)(p.x * s + p.y * c);
                z = p.z;
                break;
            default:
                x = p.x;
                y = p.y;
                z = p.z;
                System.out.println("TRIED ROTATING ON A NON EXISTING AXIS: " + axis);
        }

        return new GridPosition((int)x,(int)y,(int)z);


    }

    @Override
    public String toString() {
        return "GridPositionProperties{" +
                "node=" + node +
                ", parentNode=" + parentNode +
                ", position=" + position +
                ", parentPosition=" + parentPosition +
                ", freeFacePositions=" + Arrays.toString(freeFacePositions) +
                '}';
    }
}
