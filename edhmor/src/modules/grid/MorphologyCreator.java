package modules.grid;

import com.anji.integration.Activator;
import modules.evaluation.overlapping.CollisionDetector;
import modules.evaluation.overlapping.SimpleCollisionDetector;
import modules.grid.modulegrid.GridPosition;
import modules.grid.modulegrid.GridPositionProperties;
import modules.grid.modulegrid.ModuleGrid;
import modules.ModuleSet;
import modules.ModuleSetFactory;
import modules.individual.Connection;
import modules.individual.Node;
import modules.individual.TreeIndividual;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;


public class MorphologyCreator {
    private static double middleResponse;
    private static ModuleSet moduleSet;
    private MorphologyCreator(Node root, int maxNodes){}
    private static Random rnd = new Random();



    public static void getModuleSet(){
        moduleSet = ModuleSetFactory.getModulesSet();
    }

    public static ModuleGrid constructShapeNEAT(Activator substrate, int maxNodes, int[] gridSizes){
        //System.out.println("-----------------NEW ROBOT-----------------");
        setMiddleResponse(substrate);
        Queue<Node> nodeQueue = new LinkedList<>();

        //Create position grid for modules/nodes
        ModuleGrid moduleGrid = new ModuleGrid(gridSizes[0], gridSizes[1], gridSizes[2], maxNodes);

        //Create the root node and add it to the position grid
        Node rootNode = new Node(0, 0, 0, 0, 0, 0, 0, null);
        moduleGrid.insertRootNode(rootNode);
        GridPosition rootPosition = moduleGrid.getPosition(rootNode);
        nodeQueue.offer(rootNode);

        while(moduleGrid.numberOfNodes() < maxNodes && !nodeQueue.isEmpty()) {
            Node parent = nodeQueue.poll();

            GridPositionProperties posProps = moduleGrid.getPositionProperties(parent);
            int[] faceIndices = posProps.getFreeFaces();
            for(int i = 0; i < posProps.getNumberOfFreeFace(); i++){
                if(moduleGrid.numberOfNodes() >= maxNodes) break;
                int faceIndex = faceIndices[i];

                GridPosition facePos = posProps.getFacePosition(faceIndex);
                double[] stimuli = new double[]{
                        (double)facePos.getX()/gridSizes[0],
                        (double)facePos.getY()/gridSizes[0],
                        (double)facePos.getZ()/gridSizes[0],
                        distance(rootPosition, facePos)};


                double[] response = substrate.next(stimuli);

                if(response[0] > middleResponse && moduleGrid.isAllowed(facePos)){
                    int orientation = (response[1] > middleResponse) ? 0 : 1;
                   // Node newNode = createNode(parent,response,faceIndex);
                    Node newNode = createNodeWithController(parent,orientation,faceIndex);
                    moduleGrid.insertNode(facePos,newNode, parent);
                    nodeQueue.offer(newNode);

                }
            }
            // expandCube(current, moduleGrid.getPosition(current), moduleGrid.getGridProperties());
        }

        return moduleGrid;

    }

    private static Node createNode(Node current, double[] response, int face) {
        double amplitude = 0,phase = 0,frequency = 0;
        int orientation = (response[1] > middleResponse) ? 0 : 1;

        if(response.length == 5) {
            if(middleResponse < 0.1){
                amplitude = ((response[2] + 1.0) / 2.0) * moduleSet.getModulesMaxAmplitude(current.getType());
                phase = response[3];
                frequency = ((response[4] + 1.0) / 2.0) * moduleSet.getModulesMaxAngularFrequency(current.getType());
            }else
            {
                amplitude = response[2] * moduleSet.getModulesMaxAmplitude(current.getType()) ;
                phase = response[3];
                frequency = response[4] * moduleSet.getModulesMaxAngularFrequency(current.getType());
            }


        }else if(response.length == 2){
            amplitude = rnd.nextDouble() * moduleSet.getModulesMaxAmplitude(current.getType());
            phase = rnd.nextInt(3);
            frequency = rnd.nextDouble() * moduleSet.getModulesMaxAngularFrequency(current.getType());
        }

        //System.out.println("amplitude: " + amplitude + " Phase: " + phase + " frequency: " + frequency + " orientation: " + orientation);
        Node node1 = new Node(1, 0, amplitude, frequency, phase, 0, 0, current);
        Connection conn = new Connection(current, node1, face, orientation);
        current.addChildren(node1, conn);
        return node1;
    }

    private static double distance(GridPosition p1, GridPosition p2){
        double x2 = Math.pow(p1.getX() - p2.getX(), 2.0);
        double y2 = Math.pow(p1.getY() - p2.getY(), 2.0);
        double z2 = Math.pow(p1.getZ() - p2.getZ(), 2.0);

        return Math.sqrt(x2 + y2 + z2);

    }

    public static ModuleGrid constructShapeHyperNEAT(Activator substrate, int maxNodes, int[] gridvalues) {
        setMiddleResponse(substrate);

        return constructWithPredefined(substrate, maxNodes, gridvalues);

    }

    private static void setMiddleResponse(Activator substrate){

        middleResponse = (substrate.getMinResponse() + substrate.getMaxResponse()) / 2.0;
    }

    private static ModuleGrid constructWithPredefined(Activator substrate, int maxNodes, int[] gridvalues) {

        Queue<Node> nodeQueue = new LinkedList<>();

        //Create position grid for modules/nodes
        ModuleGrid moduleGrid = new ModuleGrid(gridvalues[0], gridvalues[1], gridvalues[2], maxNodes);

        Node rootNode = new Node(0, 0, 0, 0, 0, 0, 0, null);
        moduleGrid.insertRootNode(rootNode);
        GridPosition rootPosition = moduleGrid.getPosition(rootNode);
        nodeQueue.offer(rootNode);
        int nodesAdded = 1;
        double[][] placedNodesStimuli = new double[gridvalues[0]][gridvalues[1]*gridvalues[2]];

        placedNodesStimuli[rootPosition.getZ()-1][(rootPosition.getX() -1) + ((rootPosition.getY() -1) * gridvalues[2])] = 1;


        while(moduleGrid.numberOfNodes() < maxNodes && nodesAdded != 0) {
            nodesAdded = 0;
            for(Node parent : nodeQueue) {

                GridPositionProperties posProps = moduleGrid.getPositionProperties(parent);
                int[] faceIndices = posProps.getFreeFaces();
                for (int i = 0; i < posProps.getNumberOfFreeFace(); i++) {
                    if (moduleGrid.numberOfNodes() >= maxNodes) break;
                    int faceIndex = faceIndices[i];

                    GridPosition facePos = posProps.getFacePosition(faceIndex);

                    placedNodesStimuli[rootPosition.getZ()-1][(rootPosition.getX() -1) + ((rootPosition.getY() -1) * gridvalues[2])] = 1;
                    double[][] response = substrate.next(placedNodesStimuli);
                    double orientation1response = response[0][0];
                    placedNodesStimuli[facePos.getZ()-1][(facePos.getX() -1) + ((facePos.getY() -1) * gridvalues[2])] = 0.5;
                    response = substrate.next(placedNodesStimuli);
                    double orientation2response = response[0][0];

                    if ((orientation1response > middleResponse || orientation2response > middleResponse) && moduleGrid.isAllowed(facePos)) {
                        int orientation = orientation1response > orientation2response ? 1 : 0;

                        Node newNode = createNodeWithController(parent, orientation, faceIndex);
                        moduleGrid.insertNode(facePos, newNode, parent);

                        placedNodesStimuli[facePos.getZ()-1][(facePos.getX() -1) + ((facePos.getY() -1) * gridvalues[2])] = orientation == 1 ? 1 : 0.5;
                        nodesAdded++;
                    } else {
                        placedNodesStimuli[facePos.getZ()-1][(facePos.getX() -1) + ((facePos.getY() -1) * gridvalues[2])] = 0;
                    }

                }
            }

            for(Node n : moduleGrid.getAllNodes()){
                nodeQueue.offer(n);
            }

        }

        return moduleGrid;
    }

    private static Node createNodeWithController(Node current, int orientation, int face) {
        double amplitude,phase,frequency;

        amplitude = 0.0;
        phase = 0.0;
        frequency = 0.0;


        Node node1 = new Node(1, 0, amplitude, frequency, phase, 0, 0, current);
        Connection conn = new Connection(current, node1, face, orientation);
        current.addChildren(node1, conn);
        return node1;
    }

}
