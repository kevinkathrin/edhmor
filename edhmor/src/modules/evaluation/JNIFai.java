/* 
 * EDHMOR - Evolutionary designer of heterogeneous modular robots
 * <https://bitbucket.org/afaina/edhmor>
 * Copyright (C) 2015 GII (UDC) and REAL (ITU)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package modules.evaluation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

/**
 *
 * @author fai
 */
public class JNIFai {

      private double calidad;

      public JNIFai(){

      }

    public double getCalidad() {
        return calidad;
    }

    public int evaluaMorfologia(int serverId, int nCiclosSim, double stepT, int nModulos, int[] tipoModulos, int[] paramControlModulos) {

        Process proc = null;
        try {
            proc = Runtime.getRuntime().exec("/bin/bash", null/*, wd*/);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (proc == null) {
            return -11;
        }

        BufferedReader brStdOut = new BufferedReader(new InputStreamReader(proc.getInputStream()));
        BufferedReader brStdErr = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
        PrintWriter pwOut = new PrintWriter(new BufferedWriter(new OutputStreamWriter(proc.getOutputStream())), true);
        String strErr, strOut;

        //Cargamos el script del gazebo
        pwOut.println("source /opt/scripts/gazebo.sh");
        String tipoStr="", paramStr="";
        for(int i=0; i<tipoModulos.length; i++)
            tipoStr=tipoStr + tipoModulos[i] + " ";

        for(int i=0; i<paramControlModulos.length; i++)
            paramStr=paramStr + paramControlModulos[i] + " ";
                
        //pwOut.println("/libgeneric_control64 "+ serverID+" "+nCiclosSim+" "+stepT+" "+nModulos+" "+tipoStr+" "+paramStr);

        int calidad = 0;

        return calidad;

    }
}
