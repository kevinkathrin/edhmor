/*
 * EDHMOR - Evolutionary designer of heterogeneous modular robots
 * <https://bitbucket.org/afaina/edhmor>
 * Copyright (C) 2016 GII (UDC) and REAL (ITU)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package modules.evaluation.fitness;

import coppelia.FloatWA;
import coppelia.IntW;
import coppelia.remoteApi;
import java.math.BigInteger;
import java.util.List;
import modules.evaluation.VrepCreateRobot;
import modules.util.SimulationConfiguration;

/**
 * DistanceTravelledAndBrokenConnPenaltyFitnessFunction.java
 * Created on 20/10/2016
 * @author Andres Faiña <anfv  at itu.dk>
 */
public class DistanceTravelledAndBrokenConnPenaltyFitnessFunction extends 
        DistanceTravelledFitnessFunction{

    public DistanceTravelledAndBrokenConnPenaltyFitnessFunction(remoteApi vrep, int clientID, VrepCreateRobot robot) {
        super(vrep, clientID, robot);
    }
    
    @Override
    public void end() {
        
        //Calculate the fitness
        super.end();
        
        //Apply the penalty based on the number of broken connections
        int nbc = getBrokenConnections();
        double penalty = SimulationConfiguration.getPenaltyFitness();
        this.fitness *= (Math.pow(penalty,nbc));
    }
    
    private int getBrokenConnections(){
        //int simxReadForceSensor(int clientID,int forceSensorHandle,IntWA state,FloatWA forceVector,FloatWA torqueVector,int operationMode)
        
        int nbc = 0;
        List<Integer> forceSensors = robot.getForceSensorHandlers();
        
        for (int forceSensorHandle : forceSensors) {
            FloatWA forceVector = new FloatWA(3);
            FloatWA torqueVector = new FloatWA(3);
            IntW state = new IntW(0);
            int ret = vrep.simxReadForceSensor(clientID, forceSensorHandle, state,  forceVector, torqueVector, remoteApi.simx_opmode_oneshot_wait);
            if (ret == remoteApi.simx_return_ok) {
                boolean isBroken = BigInteger.valueOf(state.getValue()).testBit(1);
                if(isBroken)
                    nbc++;
            } else {
                System.out.format("getBrokenConnections Function: Remote API function call returned with error code: %d\n", ret);
            }
        }
        //System.out.println(nbc + " connections broken.");
        return nbc;
    }
}
