/* 
 * EDHMOR - Evolutionary designer of heterogeneous modular robots
 * <https://bitbucket.org/afaina/edhmor>
 * Copyright (C) 2015 GII (UDC) and REAL (ITU)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package modules.evaluation;

import java.io.File;

import java.util.ArrayList;
import java.util.List;
import javax.vecmath.Matrix3d;
import javax.vecmath.Vector3d;
import modules.RobotFeatures;
import modules.ModuleSetFactory;
import modules.util.DepreciatedModuleRotation;
import modules.util.SimulationConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

/**
 * Funcion que crea un archivo de mundo XML (el robot y su mundo) a partir de un
 * cromosoma; ademas calcula varios parámetros característicos del robot como el
 * centro de masas, dimensiones máximas, momento de inercia, etc.
 *
 * @author fai
 */
public class GazeboCreateRobot {

    private int nModulesMax;
    private int nModules = 1;
    //CROMOSOMA A DECODIFICAR 
    private int cromosomaInt[];
    private double cromosomaDouble[];
    private int tipoModulo[];
    private int conexiones[];
    private int cara_padre[];
    private String pathBase = "/home/fai/svn/tesis/programacion/gazebo/tesis/";
    private String worldbase = "base.world";
    private int cara_hijo[];
    private double amplitudeControl[];
    private double angularFreqControl[];
    private int phaseControl[];
    private double amplitudeModulation[];
    private double frequencyModulation[];
    private int i_base = 0, i_slider = 0, i_telescope = 0, i_hinge_axial = 0, i_hinge = 0, i_effector = 0, i_snake = 0;
    private int indice_tipo_modulo[];
    private int nivel[];
    private int numero_modulos_nivel[];
    private int indice_modulos_nivel[];
    private int indice_hijo[];
    private int modulo_padre[];
    private int cara_padres_ocupadas[][];
    private double amplitudeControlOrgByTipo[];
    private double angularFreqControlOrgByTipo[];
    private int phaseControlOrgByTipo[];
    private double amplitudeModulationOrgByTipo[];
    private double frequencyModulationOrgByTipo[];
    private int treeMatrix[][];
    private Vector3d min_pos, max_pos, dimensiones;
    private Vector3D cdm;
    private List<Vector3D> carasApoyo;
    private double masaTotal = 0.0;
    private int hingeXY[];
    private String[][] prop_enlace;
    private double alturaInicial = 0.0;
    private boolean useSoporte = false;
    private double fitnessParameter = 0.0;

    private int nBase, nSlider, nTeles, nRotac, nHinge, nEffector;
    private double Ix = 0, Iy = 0, Iz = 0;
    private double mediaConexionesPorModulo;
    private double dispersionConexionesPorModulo;

    public GazeboCreateRobot(double[] cromos, String world, double fP) {
        this(cromos, world);
        this.fitnessParameter = fP;
    }

    public GazeboCreateRobot(double[] cromos, String world) {

        if (world.contains("ConSoporte")) {
            useSoporte = true;
            world = world.replaceAll("ConSoporte", "");
        }
        this.worldbase = world;

        //System.out.println("CreateRobot: Long cromo=" + cromos.length);
        cromosomaInt = new int[cromos.length];
        for (int i = 0; i < cromos.length; i++) {
            cromosomaInt[i] = (int) Math.floor(cromos[i]);
        }

        if ((cromos.length + 3) % 9 == 0) {
            this.nModulesMax = (cromos.length + 3) / 9;
        } else {

            if ((cromos.length + 3) % 6 == 0) {
                this.nModulesMax = (cromos.length + 3) / 6;

            } else {
                if ((cromos.length + 3) % 5 == 0) {
                    this.nModulesMax = (cromos.length + 3) / 5;
                    /*
                     this.tipoModulo = new int[nModulesMax];
                     this.conexiones = new int[nModulesMax - 1];
                     this.cara_padre = new int[nModulesMax - 1];
                     this.cara_hijo = new int[nModulesMax - 1];
                     this.control_modulo = new int[nModulesMax];
                     this.frequencyModulation = new double[nModulesMax];
                     for (int i = 0; i < nModulesMax; i++) {
                     this.frequencyModulation[i] = 0.0;
                     }
                     this.indice_tipo_modulo = new int[nModulesMax];
                     this.nivel = new int[nModulesMax];
                     this.numero_modulos_nivel = new int[nModulesMax];
                     this.indice_modulos_nivel = new int[nModulesMax];
                     this.indice_hijo = new int[nModulesMax];
                     this.modulo_padre = new int[nModulesMax];
                     this.cara_padres_ocupadas = new int[nModulesMax][14];
                     this.control_modulos_org_by_tipo = new int[nModulesMax];
                     this.frequencyModulationOrgByTipo = new double[nModulesMax];
                     for (int i = 0; i < nModulesMax; i++) {
                     this.frequencyModulationOrgByTipo[i] = 0.0;
                     }
                     this.treeMatrix = new int[nModulesMax][nModulesMax];
                     this.hingeXY = new int[nModulesMax];*/
                } else {
                    if (cromos.length % 5 == 0) {
                        //Depreciado
                        System.err.println("CreateRobot: Estas seguro de que esto va bien. Creo que esto esta depreciado. Long cromo: " + cromos.length + " cromosoma%5=0");

                        //Metamodulo base como unicio del arbol morfologico
                        this.nModulesMax = (cromos.length / 5) + 1;
                        /*
                         this.cromosomaInt = new int[nModulesMax * 5 - 3];
                         this.cromosomaInt[0] = 0;
                         for (int i = 0; i < (cromos.length - (nModulesMax - 1)); i++) {
                         this.cromosomaInt[1 + i] = (int) Math.floor(cromos[i]);
                         }
                         for (int i = (cromos.length - (nModulesMax - 1)); i < cromos.length; i++) {
                         this.cromosomaInt[i + 2] = (int) Math.floor(cromos[i]);
                         }

                         this.tipoModulo = new int[nModulesMax];
                         this.conexiones = new int[nModulesMax - 1];
                         this.cara_padre = new int[nModulesMax - 1];
                         this.cara_hijo = new int[nModulesMax - 1];
                         this.control_modulo = new int[nModulesMax];
                         this.frequencyModulation = new double[nModulesMax];
                         for (int i = 0; i < nModulesMax; i++) {
                         this.frequencyModulation[i] = 0.0;
                         }
                         this.indice_tipo_modulo = new int[nModulesMax];
                         this.nivel = new int[nModulesMax];
                         this.numero_modulos_nivel = new int[nModulesMax];
                         this.indice_modulos_nivel = new int[nModulesMax];
                         this.indice_hijo = new int[nModulesMax];
                         this.modulo_padre = new int[nModulesMax];
                         this.cara_padres_ocupadas = new int[nModulesMax][14];
                         this.control_modulos_org_by_tipo = new int[nModulesMax];
                         this.frequencyModulationOrgByTipo = new double[nModulesMax];
                         for (int i = 0; i < nModulesMax; i++) {
                         this.frequencyModulationOrgByTipo[i] = 0.0;
                         }
                         this.treeMatrix = new int[nModulesMax][nModulesMax];
                         this.hingeXY = new int[nModulesMax];
                         */

                    } else {
                        System.err.println("Create Robot: Error con la longitud del cromosoma; length: " + cromos.length);
                        for (int i = 0; i < cromos.length; i++) {
                            System.err.print(cromos[i] + ", ");
                        }
                        System.exit(-1);
                    }

                }
                if (SimulationConfiguration.isDebug()) {
                    for (int i = 0; i < this.cromosomaInt.length; i++) {
                        System.out.print(this.cromosomaInt[i] + ", ");
                    }
                }
            }
        }

        this.cromosomaDouble = cromos;
        this.tipoModulo = new int[nModulesMax];
        this.conexiones = new int[nModulesMax - 1];
        this.cara_padre = new int[nModulesMax - 1];
        this.cara_hijo = new int[nModulesMax - 1];
        this.amplitudeControl = new double[nModulesMax];
        this.angularFreqControl = new double[nModulesMax];
        this.phaseControl = new int[nModulesMax];
        this.amplitudeModulation = new double[nModulesMax];
        this.frequencyModulation = new double[nModulesMax];
        this.indice_tipo_modulo = new int[nModulesMax];
        this.nivel = new int[nModulesMax];
        this.numero_modulos_nivel = new int[nModulesMax];
        this.indice_modulos_nivel = new int[nModulesMax];
        this.indice_hijo = new int[nModulesMax];
        this.modulo_padre = new int[nModulesMax];
        this.cara_padres_ocupadas = new int[nModulesMax][14];
        this.amplitudeControlOrgByTipo = new double[nModulesMax];
        this.angularFreqControlOrgByTipo = new double[nModulesMax];
        this.phaseControlOrgByTipo = new int[nModulesMax];
        this.amplitudeModulationOrgByTipo = new double[nModulesMax];
        this.frequencyModulationOrgByTipo = new double[nModulesMax];
        this.treeMatrix = new int[nModulesMax][nModulesMax];
        this.hingeXY = new int[nModulesMax];

        if ((cromos.length + 3) % 9 != 0) {
            for (int i = 0; i < nModulesMax; i++) {
                this.amplitudeControl[i] = 0.0;
                this.amplitudeControlOrgByTipo[i] = 0.0;

                this.angularFreqControl[i] = 0.0;
                this.angularFreqControlOrgByTipo[i] = 0.0;

                this.amplitudeModulation[i] = 0.0;
                this.amplitudeModulationOrgByTipo[i] = 0.0;

                this.frequencyModulation[i] = 0.0;
                this.frequencyModulationOrgByTipo[i] = 0.0;
            }
        }

        this.prop_enlace = new String[nModules][4];

        this.max_pos = new Vector3d(Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY);
        this.min_pos = new Vector3d(Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE);
        this.dimensiones = new Vector3d(0, 0, 0);
        this.cdm = new Vector3D(0, 0, 0);
        this.carasApoyo = new ArrayList<Vector3D>();

        //Analizamos al individuo
        this.analisisCromosoma();
        int tipoMod = this.tipoModulo[0];
        this.masaTotal += ModuleSetFactory.getModulesSet().getModulesMass(tipoMod);
        //System.out.println("Masa Total al inicio: " + this.masaTotal);

        Vector3D posCara;
        for (int cara = 0; cara < ModuleSetFactory.getModulesSet().getModulesFacesNumber(tipoMod); cara++) {
            posCara = ModuleSetFactory.getModulesSet().getOriginFaceVector(tipoMod, cara);

            if (this.max_pos.x < posCara.getX()) {
                this.max_pos.x = posCara.getX();
            }
            if (this.max_pos.y < posCara.getY()) {
                this.max_pos.y = posCara.getY();
            }
            if (this.max_pos.z < posCara.getZ()) {
                this.max_pos.z = posCara.getZ();
            }

            if (this.min_pos.x > posCara.getX()) {
                this.min_pos.x = posCara.getX();
            }
            if (this.min_pos.y > posCara.getY()) {
                this.min_pos.y = posCara.getY();
            }
            if (this.min_pos.z > posCara.getZ()) {
                this.min_pos.z = posCara.getZ();
                carasApoyo.clear();
            }
            //Añadimos apoyos
            if (posCara.getZ() - this.min_pos.z < 0.15) {
                carasApoyo.add( posCara);
            }
        }

        //Calculamos el xyz y el rpy y los body de union para todas las uniones
        // y de paso calculamos las dimensiones del robot, el centro de masas, etc.
        prop_enlace = calcula_attach();
    }

    public int createWorldXML(String path, String name) {

//        for (int i = 0; i < this.cromosomaInt.length; i++) {
//            if (cromosomaInt[i] < 0) {
//                this.showDebugInformation();
//            }
//        }
        File file = new File(path + name);

        try {

            XMLConfiguration config;

            config = new XMLConfiguration(path + "worlds/" + worldbase);

            if (this.worldbase.contains("ConCarga")) {

                String modelName = "model:physical(1)";
                double alturaCarga = alturaInicial + 0.2;
                config.setProperty(modelName + ".xyz", "0 0 " + alturaCarga);
            }
            if (this.worldbase.contains("ConPared")) {
                String modelName = "model:physical(1)";
                double distanciaPared = this.fitnessParameter + 0.25 / 2 + 0.25;
                config.setProperty(modelName + ".xyz", distanciaPared + " 0 2");
            }

            //Bucle para cada modulo para añadir los xyz y el rpy
            for (int m = 0; m < nModules; m++) {

                //Creamos el model:physical
                String modelName = "model:physical";
                for (int i = 0; i < nivel[m]; i++) {
                    modelName += ".model:physical";
                    if ((nivel[m] - i) > 1) {
                        modelName += "(" + indice_hijo[treeMatrix[i + 1][m]] + ")";
                    }
                }
                config.addProperty(modelName + "(-1)[@name]", getNameModuloNumber(m));

                config.addProperty(modelName + ".xyz" + "(-1)", prop_enlace[m][0]);
                config.addProperty(modelName + ".rpy" + "(-1)", prop_enlace[m][1]);

                actualiza_indices_modulos(m);
            }

            if (SimulationConfiguration.isDebug()) {
                showDebugInformation();
            }
            reset_indice_modulos();
            for (int m = 0; m < nModules; m++) {
                String modelName = "model:physical";
                for (int i = 0; i < nivel[m]; i++) {
                    modelName += ".model:physical";
                    modelName += "(" + indice_hijo[treeMatrix[i + 1][m]] + ")";
                }
                if (m != 0) {
                    //El modulo cero no tiene padre y por tanto no se une a nadie
                    config.addProperty(modelName + ".attach(-1).parentBody", prop_enlace[m][2]);
                    config.addProperty(modelName + ".attach.myBody", prop_enlace[m][3]);
                }
                config.addProperty(modelName + ".include" + "(-1)[@embedded]", "true");
                String property = "models/" + getNameModulo(m) + "/" + getNameModuloNumber(m);
                if (m == 0 && this.useSoporte) {
                    property += "ConSoporte";
                }
                if (m == 0 && this.worldbase.contains("manipulator")) {
                    property += "Manipulator";
                }
                config.addProperty(modelName + ".include.xi:include" + "(-1)[@href]", property + ".model");
                actualiza_indices_modulos(m);
            }
            try {
                config.save(file);
            } catch (ConfigurationException cex) {
                System.err.println(cex);
            }
        } catch (ConfigurationException cex) {
            System.out.println(cex);
        }
        //System.out.println("WorldXML creado");
        return -1;
    }

    private String getNameModuloNumber(int numero_modulo) {
        String nameModel = "";
        switch (tipoModulo[numero_modulo]) {
            case 0:
                nameModel = "base" + i_base;
                break;
            case 1:
                nameModel = "slider" + i_slider;
                break;
            case 2:
                nameModel = "telescope" + i_telescope;
                break;
            case 3:
                nameModel = "hinge_axial" + i_hinge_axial;
                break;
            case 4:
                if (hingeXY[numero_modulo] == 0) {
                    nameModel = "hinge_x" + i_hinge;
                } else {
                    nameModel = "hinge_y" + i_hinge;
                }
                break;
            case 5:
                nameModel = "effector" + i_effector;
                break;
            case 6:
                nameModel = "snake" + i_snake;
                break;

            //case 5:nameModel="wheel"+i_wheel; break;
        }

//        System.out.println("El modulo " + numero_modulo + " es un " + nameModel);
        return nameModel;
    }

    private int getModuloNumber(int numero_modulo) {
        int number = 0;
        switch (tipoModulo[numero_modulo]) {
            case 0:
                number = i_base;
                break;
            case 1:
                number = i_slider;
                break;
            case 2:
                number = i_telescope;
                break;
            case 3:
                number = i_hinge_axial;
                break;
            case 4:
                number = i_hinge;
                break;
            case 5:
                number = i_effector;
                break;
            case 6:
                number = i_snake;
                break;
            //case 5:nameModel="wheel"+i_wheel; break;
        }
        return number;
    }

    private String getNameModulo(int numero_modulo) {
        String nameModel = "";
        switch (tipoModulo[numero_modulo]) {
            case 0:
                nameModel = "base";
                break;
            case 1:
                nameModel = "slider";
                break;
            case 2:
                nameModel = "telescope";
                break;
            case 3:
                nameModel = "hinge_axial";
                break;
            case 4:
                nameModel = "hinge";
                break;
            case 5:
                nameModel = "effector";
                break;
            case 6:
                nameModel = "snake";
                break;
            //case 5:nameModel="wheel"; break;
        }
        return nameModel;
    }

    private void actualiza_indices_modulos(int numero_modulo) {
        switch (tipoModulo[numero_modulo]) {
            case 0:
                i_base++;
                break;
            case 1:
                i_slider++;
                break;
            case 2:
                i_telescope++;
                break;
            case 3:
                i_hinge_axial++;
                break;
            case 4:
                i_hinge++;
                break;
            case 5:
                i_effector++;
                break;
            case 6:
                i_snake++;
                break;
            //case 5:i_wheel++; break;
        }
    }

    private void reset_indice_modulos() {
        i_base = 0;
        i_slider = 0;
        i_telescope = 0;
        i_hinge_axial = 0;
        i_hinge = 0;
        i_effector = 0;
        i_snake = 0;
        //i_wheeel=0;
    }

    public void analisisCromosoma() {
//        System.out.print("Analisis: Cromosoma= ");
//        for (int j = 0; j < cromosoma.length; j++) {
//            System.out.print(cromosoma[j] + " ");
//        }
        for (int i = 0; i < nModulesMax; i++) {
            tipoModulo[i] = cromosomaInt[i];
        }

        for (int i = 0; i < (nModulesMax - 1); i++) {
            conexiones[i] = cromosomaInt[nModulesMax + i];
        }

        for (int i = 0; i < (nModulesMax - 1); i++) {
            cara_padre[i] = cromosomaInt[2 * nModulesMax - 1 + i];
        }

        for (int i = 0; i < (nModulesMax - 1); i++) {
            cara_hijo[i] = cromosomaInt[3 * nModulesMax - 2 + i];
        }

        if ((cromosomaInt.length + 3) % 9 == 0) {

            //Parametros del control de la amplitud
            for (int i = 0; i < (nModulesMax); i++) {
                this.amplitudeControl[i] = cromosomaDouble[4 * nModulesMax - 3 + i];
            }

            //Parametros del control de la frecuencia angular
            for (int i = 0; i < (nModulesMax); i++) {
                this.angularFreqControl[i] = cromosomaDouble[5 * nModulesMax - 3 + i];
            }

            //Parametros del control de la fase
            for (int i = 0; i < (nModulesMax); i++) {
                this.phaseControl[i] = cromosomaInt[6 * nModulesMax - 3 + i];
            }

            //Parametros del control del modulador de la amplitud
            for (int i = 0; i < (nModulesMax); i++) {
                this.amplitudeModulation[i] = cromosomaDouble[7 * nModulesMax - 3 + i];
            }

            //Parametros del control del modulador de la amplitud
            for (int i = 0; i < (nModulesMax); i++) {
                this.frequencyModulation[i] = cromosomaDouble[8 * nModulesMax - 3 + i];
            }

        } else {
            for (int i = 0; i < (nModulesMax); i++) {
                phaseControl[i] = cromosomaInt[4 * nModulesMax - 3 + i];
            }

            if (cromosomaInt.length > 5 * nModulesMax - 3) {
                for (int i = 0; i < (nModulesMax); i++) {
                    frequencyModulation[i] = cromosomaDouble[5 * nModulesMax - 3 + i];
                }
            }

        }

        //Calculo del nivel de cada modulo
        int indice_nivel = 0;
        numero_modulos_nivel[0] = 1;
        int i = 0;

        for (int nivel_actual = 0; nivel_actual < nModulesMax; nivel_actual++) {
            for (int j = 0; j < numero_modulos_nivel[nivel_actual] && i < nModulesMax; j++) {
                nivel[i] = nivel_actual;
                indice_modulos_nivel[i] = indice_nivel++;
                if (i < nModulesMax - 1) //Para q no desborde el array
                {
                    numero_modulos_nivel[nivel_actual + 1] += conexiones[i];
                    nModules += conexiones[i];
                }
                i++;
            }
            indice_nivel = 0;
        }
        if (nModules > nModulesMax) {
            nModules = nModulesMax;
        }

        //calculamos el indice de hijo
        int k = 0;
        for (int ii = 0; ii < nModules - 1; ii++) {
            for (int j = 0; j < conexiones[ii] && k < (nModules - 1); j++) {
                modulo_padre[1 + k] = ii;
                indice_hijo[1 + k++] = j;
            }
        }

        //Rellenamos el indice de cada modulo segun el tipo
        reset_indice_modulos();
        for (int m = 0; m < nModules; m++) {
            indice_tipo_modulo[m] = getModuloNumber(m);
            this.actualiza_indices_modulos(m);
        }
        this.nBase = this.i_base;
        this.nSlider = this.i_slider;
        this.nTeles = this.i_telescope;
        this.nRotac = this.i_hinge_axial;
        this.nHinge = this.i_hinge;
        this.nEffector = this.i_effector;
        
        calculaCaracteristicasConexiones(conexiones);

        //Rellenamos la matriz del arbol (treeMatrix)
        for (int m = 0; m < nModules; m++) {
            int mAux = m;
            for (int n = nivel[m]; n > 0; n--) {
                treeMatrix[n][m] = mAux;
                mAux = modulo_padre[mAux];
            }
        }
//        for (int m = 0; m < nModules; m++) {
//            System.out.println("treeMatrix: " + treeMatrix[m][0] + " " + treeMatrix[m][1] + " " + treeMatrix[m][2] + " " + treeMatrix[m][3]);
//        }
        //Calculo del array de parametros de control
        for (int m = 0; m < nModules; m++) {
            if (tipoModulo[m] == 1) {
                this.amplitudeControlOrgByTipo[indice_tipo_modulo[m]] = this.amplitudeControl[m];
                this.angularFreqControlOrgByTipo[indice_tipo_modulo[m]] = this.angularFreqControl[m];
                phaseControlOrgByTipo[indice_tipo_modulo[m]] = phaseControl[m];
                this.amplitudeModulationOrgByTipo[indice_tipo_modulo[m]] = this.amplitudeModulation[m];
                this.frequencyModulationOrgByTipo[indice_tipo_modulo[m]] = this.frequencyModulation[m];
            }
            if (tipoModulo[m] == 2) {
                this.amplitudeControlOrgByTipo[indice_tipo_modulo[m] + i_slider] = this.amplitudeControl[m];
                this.angularFreqControlOrgByTipo[indice_tipo_modulo[m] + i_slider] = this.angularFreqControl[m];
                phaseControlOrgByTipo[indice_tipo_modulo[m] + i_slider] = phaseControl[m];
                this.amplitudeModulationOrgByTipo[indice_tipo_modulo[m] + i_slider] = this.amplitudeModulation[m];
                this.frequencyModulationOrgByTipo[indice_tipo_modulo[m] + i_slider] = this.frequencyModulation[m];
            }
            if (tipoModulo[m] == 3) {
                this.amplitudeControlOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope] = this.amplitudeControl[m];
                this.angularFreqControlOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope] = this.angularFreqControl[m];
                phaseControlOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope] = phaseControl[m];
                this.amplitudeModulationOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope] = this.amplitudeModulation[m];
                this.frequencyModulationOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope] = this.frequencyModulation[m];
            }
            if (tipoModulo[m] == 4) {
                this.amplitudeControlOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope + i_hinge_axial] = this.amplitudeControl[m];
                this.angularFreqControlOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope + i_hinge_axial] = this.angularFreqControl[m];
                phaseControlOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope + i_hinge_axial] = phaseControl[m];
                this.amplitudeModulationOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope + i_hinge_axial] = this.amplitudeModulation[m];
                this.frequencyModulationOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope + i_hinge_axial] = this.frequencyModulation[m];
            }
            if (tipoModulo[m] == 5) {
                this.amplitudeControlOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope + i_hinge_axial + i_hinge] = this.amplitudeControl[m];
                this.angularFreqControlOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope + i_hinge_axial + i_hinge] = this.angularFreqControl[m];
                phaseControlOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope + i_hinge_axial + i_hinge] = phaseControl[m];
                this.amplitudeModulationOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope + i_hinge_axial + i_hinge] = this.amplitudeModulation[m];
                this.frequencyModulationOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope + i_hinge_axial + i_hinge] = this.frequencyModulation[m];
            }
            if (tipoModulo[m] == 6) {
                this.amplitudeControlOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope + i_hinge_axial + i_hinge + i_effector] = this.amplitudeControl[m];
                this.angularFreqControlOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope + i_hinge_axial + i_hinge + i_effector] = this.angularFreqControl[m];
                phaseControlOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope + i_hinge_axial + i_hinge + i_effector] = phaseControl[m];
                this.amplitudeModulationOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope + i_hinge_axial + i_hinge + i_effector] = this.amplitudeModulation[m];
                this.frequencyModulationOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope + i_hinge_axial + i_hinge + i_effector] = this.frequencyModulation[m];
            }
        }

        this.reset_indice_modulos();

    }

    private String[][] calcula_attach() {

        String propiedades[][] = new String[nModules][4];
        Vector3D posicionModulo[] = new Vector3D[nModules];
        Matrix3d rotacionModulo[] = new Matrix3d[nModules];
        posicionModulo[0] = new Vector3D(0, 0, 0);
        rotacionModulo[0] = new Matrix3d(1, 0, 0, 0, 1, 0, 0, 0, 1);
        DepreciatedModuleRotation rotaciones = new DepreciatedModuleRotation();

        propiedades[0][0] = "0 0 0";                               //x,y,z
        propiedades[0][1] = "0 0 0";                               //r,p,y
        propiedades[0][2] = "ERROR, el modulo 0 no se une a nadie";  //parte del padre al que se une (base o actuador)
        propiedades[0][3] = "ERROR, el modulo 0 no se une a nadie";  //parte del modulo por la que se une al padre (base o acruador)

        actualiza_indices_modulos(0);

        for (int modulo = 1; modulo < nModules; modulo++) {

            int tipo_h = tipoModulo[modulo];
            int tipo_p = tipoModulo[modulo_padre[modulo]];
            //int cara_p = cara_padre[modulo - 1] % n_caras_modulos[tipo_p];+++++++++++++++
            int cara_p = cara_padre[modulo - 1] % ModuleSetFactory.getModulesSet().getModulesFacesNumber(tipo_p);
            //int cara_h = cara_hijo[(modulo - 1) % n_caras_reducidas_modulos[tipo_h]];+++++++++++++++
            int cara_h = cara_hijo[(modulo - 1)] % ModuleSetFactory.getModulesSet().getModuleOrientations(tipo_h);

            //Si es un modulo 
            if (tipo_h == 4) {
                if (cara_h == 0) {
                    hingeXY[modulo] = 0;
                } else {
                    if (cara_h == 1) {
                        hingeXY[modulo] = 1;
                    } else {
                        System.err.println("Error al elegir el tipo de hinge (eje en X o Y)");
                        System.exit(-1);
                    }
                }
                cara_h = 0;
            }

//
//            System.out.println("Tipo padre e hijo: " + tipo_p + "  " + tipo_h);
//            System.out.println("Cara padre e hijo: " + cara_p + "  " + cara_h);
            //Comprobaciuon de que la cara del padre donde vamos a unir el hijo esta libre
            while (cara_padres_ocupadas[modulo_padre[modulo]][cara_p] == 1) {
                //cara_p = ++cara_p % Test.n_caras_modulos[tipo_p];++++++++++++
                cara_p = ++cara_p % ModuleSetFactory.getModulesSet().getModulesFacesNumber(tipo_p);
            }
            cara_padres_ocupadas[modulo_padre[modulo]][cara_p] = 1;

            //Ponemos si esa cara del padre es base o actuador
            //if (cara_p < n_caras_base_modulos[tipo_p]) {++++++++++++
            if (cara_p < ModuleSetFactory.getModulesSet().getModulesBaseFacesNumber(tipo_p)) {
                propiedades[modulo][2] = getNameModulo(modulo_padre[modulo]) + "_base_body_" + indice_tipo_modulo[modulo_padre[modulo]];
            } else {
                propiedades[modulo][2] = getNameModulo(modulo_padre[modulo]) + "_actuador_body_" + indice_tipo_modulo[modulo_padre[modulo]];
            }

            //Ponemos si la cara del hijo es base o actuador (solo modulo slider)
            if (cara_h != 5 && cara_h != 6) {
                propiedades[modulo][3] = getNameModulo(modulo) + "_base_body_" + getModuloNumber(modulo);
            } else {
                propiedades[modulo][3] = getNameModulo(modulo) + "_actuador_body_" + getModuloNumber(modulo);
            }

            //Calculo de xyz y de rpy
            //Vector3d normalCaraPadre = vector_normal_cara[tipo_p][cara_p];
            Vector3D normalCaraPadreAux = ModuleSetFactory.getModulesSet().getNormalFaceVector(tipo_p, cara_p);
            if (normalCaraPadreAux == null) {
                System.err.println("normalCaraPadreAux == null!!!!!!!!!!!!");
                System.exit(-1);
            }

            Vector3D normalCaraPadre = new Vector3D(normalCaraPadreAux.getX() * (-1), normalCaraPadreAux.getY() * (-1), normalCaraPadreAux.getZ() * (-1));
            //Ya estamos negandola en la linea anterior
            //normalCaraPadre.negate();
//            System.out.println("VecNormalCaraPadre (negado): " + normalCaraPadre);

            //Calculo de rpy
            double rotacion[];
            rotacion = rotaciones.calculateRPY_Gazebo_oldMethod(cara_h, normalCaraPadre);

            propiedades[modulo][1] = rotacion[0] + " " + rotacion[1] + " " + rotacion[2];

            //Calculo del vector padre origen -> cara de union
            Vector3D ocp = null;
            //ocp = Test.vector_origen_cara[tipo_p][cara_p];+++++++++
            ocp = ModuleSetFactory.getModulesSet().getOriginFaceVector(tipo_p, cara_p);

            //Calculo del vector del modulo hijo de su origen -> cara de union
            Vector3D och = null;
            if (cara_h == 0) {
                //och = Test.vector_origen_cara[tipo_h][cara_h];++++++++
                och = ModuleSetFactory.getModulesSet().getOriginFaceVector(tipo_h, cara_h);
                cara_padres_ocupadas[modulo][cara_h] = 1;         //Ponemos esa cara ocupada
            } else {
                if (cara_h == 5 || cara_h == 6) {
                    //Caras del actuador del slider
                    //och = Test.vector_origen_cara[tipo_h][10];+++++++++
                    och = ModuleSetFactory.getModulesSet().getOriginFaceVector(tipo_h, 10);
                    cara_padres_ocupadas[modulo][10] = 1;         //Ponemos esa cara ocupada

                } else {
                    //och = Test.vector_origen_cara[tipo_h][1];+++++++++++
                    och = ModuleSetFactory.getModulesSet().getOriginFaceVector(tipo_h, 1);
                    cara_padres_ocupadas[modulo][1] = 1;         //Ponemos esa cara ocupada
                }
            }

//            System.out.println("Vector o->cara_h: " + och);
//            System.out.println("Vector o->cara_p: " + ocp);
            Vector3D och_rot = rotaciones.calculaRotacionRPY(och, rotacion);

            och_rot = och_rot.negate();
            och_rot = och_rot.add(ocp);

//            System.out.println("Vector o->origen_hijo: " + och_rot);
//            System.out.println("Cambio de union\n\n");
            propiedades[modulo][0] = och_rot.getX() + " " + och_rot.getY() + " " + och_rot.getZ();

            rotacionModulo[modulo] = rotaciones.calculaMatrizRotacionGlobal(propiedades[modulo][1], rotacionModulo[modulo_padre[modulo]]);

            posicionModulo[modulo] = rotaciones.calculaRotacion(new Vector3D(och_rot.getX(), och_rot.getY(), och_rot.getZ()), rotacionModulo[modulo_padre[modulo]]);

            posicionModulo[modulo] = posicionModulo[modulo].add(posicionModulo[modulo_padre[modulo]]);

            Vector3D aux = new Vector3D(posicionModulo[modulo].getX(), posicionModulo[modulo].getY(), posicionModulo[modulo].getZ());
            
            double s = ModuleSetFactory.getModulesSet().getModulesMass(tipo_h);
            this.masaTotal += s;
            aux = aux.scalarMultiply(s);
            this.cdm = this.cdm.add(aux);

            if (SimulationConfiguration.isDebug()) {
                System.out.println("Masa Total: " + this.masaTotal + "; cdm: " + this.cdm);
            }

            //Claculo de la cara mas baja para luego subir esa distancia el robot
            //es decir, le damos un apoyo al robot
            int nCarasBusqueda = 6;
            if (tipo_h == 4 || tipo_h == 6) {
                nCarasBusqueda = 2;
            }
            if (tipo_h == 5) {
                nCarasBusqueda = 1;
            }

            for (int cara = 0; cara < nCarasBusqueda; cara++) {
                och = ModuleSetFactory.getModulesSet().getOriginFaceVector(tipo_h, cara);
                och_rot = rotaciones.calculaRotacion(och, rotacionModulo[modulo]);
                och_rot = och_rot.add(posicionModulo[modulo]);

                if (this.max_pos.x < och_rot.getX()) {
                    this.max_pos.x = och_rot.getX();
                }
                if (this.max_pos.y < och_rot.getY()) {
                    this.max_pos.y = och_rot.getY();
                }
                if (this.max_pos.z < och_rot.getZ()) {
                    this.max_pos.z = och_rot.getZ();
                }

                if (this.min_pos.x > och_rot.getX()) {
                    this.min_pos.x = och_rot.getX();
                }
                if (this.min_pos.y > och_rot.getY()) {
                    this.min_pos.y = och_rot.getY();
                }
                if (this.min_pos.z > och_rot.getZ()) {
                    this.min_pos.z = och_rot.getZ();
                    carasApoyo.clear();
                }
                //Añadimos apoyos
                if (och_rot.getZ() - this.min_pos.z < 0.15) {
                    carasApoyo.add((Vector3D) och_rot);
                }
            }

            actualiza_indices_modulos(modulo);

        }

        this.dimensiones.add(this.max_pos);
        this.dimensiones.sub(this.min_pos);
        this.cdm = this.cdm.scalarMultiply(1 / this.masaTotal);

        //Calculo del momento de inercia
        for (int modulo = 1; modulo < nModules; modulo++) {
            Vector3D posModulo = new Vector3D(posicionModulo[modulo].getX(), posicionModulo[modulo].getY(), posicionModulo[modulo].getZ());
            
            double distanciaEjeZ = Math.pow(posModulo.getX() - this.cdm.getX(), 2) + Math.pow(posModulo.getY() - this.cdm.getY(), 2);
            distanciaEjeZ = Math.sqrt(distanciaEjeZ);
            double distanciaEjeX = Math.pow(posModulo.getY() - this.cdm.getY(), 2) + Math.pow(posModulo.getZ() - this.cdm.getZ(), 2);
            distanciaEjeX = Math.sqrt(distanciaEjeX);
            double distanciaEjeY = Math.pow(posModulo.getX() - this.cdm.getX(), 2) + Math.pow(posModulo.getZ() - this.cdm.getZ(), 2);
            distanciaEjeY = Math.sqrt(distanciaEjeY);

            Ix += distanciaEjeX * ModuleSetFactory.getModulesSet().getModulesMass(tipoModulo[modulo]);
            Iy += distanciaEjeY * ModuleSetFactory.getModulesSet().getModulesMass(tipoModulo[modulo]);
            Iz += distanciaEjeZ * ModuleSetFactory.getModulesSet().getModulesMass(tipoModulo[modulo]);

        }

        //TODO: Comprobar estabilidad
        //System.out.println("carasApoyo: "+ carasApoyo.size()+"uno: "+ carasApoyo.get(0)+"dos: "+ carasApoyo.get(1));
        //TODO: Comprobar que min_z y min_pos.z coinciden, eliminar la siguiente linea
        if (SimulationConfiguration.isDebug()) {
            System.out.println("min_pos: " + this.min_pos + "; max_pos: " + this.max_pos + "; dimensiones: " + this.dimensiones);
            System.out.println("Masa Total: " + this.masaTotal + "; cdm: " + this.cdm);
        }

        alturaInicial = (Math.abs(this.min_pos.z) + 0.05);
        if (this.worldbase.contains("baseEscalera")) {
            alturaInicial += 0.4;
            alturaInicial += 0.1 * Math.floor(this.max_pos.x);
        }
        if (this.worldbase.contains("sueloRugoso")) {
            alturaInicial += 0.15;
        }
        if (this.worldbase.contains("Guia")) {
            alturaInicial = alturaInicial + 0.5;//Math.max(alturaInicial, 0.5);
        }
        if (this.useSoporte) {
            alturaInicial = Math.max(alturaInicial, 0.6);
        }
        if (this.worldbase.contains("manipulator")) {
            alturaInicial = 1;
        }
        propiedades[0][0] = "0 0 " + alturaInicial;

        reset_indice_modulos();
        return propiedades;
    }

    public double[] getAmplitudeControlOrgByTipo() {
        if (nModules == nModulesMax) {
            return amplitudeControlOrgByTipo;
        }
        double aux[] = new double[nModules];
        for (int i = 0; i < nModules; i++) {
            aux[i] = amplitudeControlOrgByTipo[i];
        }
        return aux;
    }

    public double[] getAngularFreqControlOrgByTipo() {
        if (nModules == nModulesMax) {
            return angularFreqControlOrgByTipo;
        }
        double aux[] = new double[nModules];
        for (int i = 0; i < nModules; i++) {
            aux[i] = angularFreqControlOrgByTipo[i];
        }
        return aux;
    }

    public int[] getPhaseControlOrgByTipo() {
        if (nModules == nModulesMax) {
            return phaseControlOrgByTipo;
        }
        int aux[] = new int[nModules];
        for (int i = 0; i < nModules; i++) {
            aux[i] = phaseControlOrgByTipo[i];
        }
        return aux;
    }

    public double[] getAmpliModOrgByTipo() {
        if (nModules == nModulesMax) {
            return this.amplitudeModulationOrgByTipo;
        }
        double aux[] = new double[nModules];
        for (int i = 0; i < nModules; i++) {
            aux[i] = this.amplitudeModulationOrgByTipo[i];
        }
        return aux;
    }

    public double[] getFreqModOrgByTipo() {
        if (nModules == nModulesMax) {
            return this.frequencyModulationOrgByTipo;
        }
        double aux[] = new double[nModules];
        for (int i = 0; i < nModules; i++) {
            aux[i] = this.frequencyModulationOrgByTipo[i];
        }
        return aux;
    }

    public int getN_modulos() {
        return nModules;
    }

    public int[] getTipo_modulo() {
        if (nModules == nModulesMax) {
            return tipoModulo;
        }
        int aux[] = new int[nModules];
        for (int i = 0; i < nModules; i++) {
            aux[i] = tipoModulo[i];
            if (SimulationConfiguration.isDebug()) {
                System.out.print(aux[i] + " ");
            }
        }
        return aux;
    }

    public void setFitnessParameter(double fitnessParameter) {
        this.fitnessParameter = fitnessParameter;
    }

    private void showDebugInformation() {

        System.out.println("nModulosMax: " + this.nModulesMax);
        System.out.print("nModulos: " + this.nModules);
        System.out.print("\nTipo Modulo:");
        for (int i = 0; i < nModulesMax; i++) {
            System.out.print(tipoModulo[i] + ", ");
        }

        System.out.print("\nConexiones: ");
        for (int i = 0; i < (nModulesMax - 1); i++) {
            System.out.print(conexiones[i] + ", ");
        }

        System.out.print("\nCara padre: ");
        for (int i = 0; i < (nModulesMax - 1); i++) {
            System.out.print(this.cara_padre[i] + ", ");
        }

        System.out.print("\nCara hijo: ");
        for (int i = 0; i < (nModulesMax - 1); i++) {
            System.out.print(this.cara_hijo[i] + ", ");
        }

        System.out.print("\nAmplitud: ");
        for (int i = 0; i < nModulesMax; i++) {
            System.out.print(this.amplitudeControl[i] + ", ");
        }

        System.out.print("\nFrecuencia Angular: ");
        for (int i = 0; i < nModulesMax; i++) {
            System.out.print(this.angularFreqControl[i] + ", ");
        }

        System.out.print("\nPhase: ");
        for (int i = 0; i < nModulesMax; i++) {
            System.out.print(this.phaseControl[i] + ", ");
        }

        System.out.print("\nAmplitude Modulation: ");
        for (int i = 0; i < nModulesMax; i++) {
            System.out.print(this.amplitudeModulation[i] + ", ");
        }

        System.out.print("\nFrequency Modulation: ");
        for (int i = 0; i < nModulesMax; i++) {
            System.out.print(this.frequencyModulation[i] + ", ");
        }

        System.out.print("\nCromosoma (CreateXML): ");
        for (int i = 0; i < this.cromosomaInt.length; i++) {
            System.out.print(this.cromosomaInt[i] + ", ");

        }
        System.out.println("");
    }

    public int getnModules() {
        return nModules;
    }

    public int getnBase() {
        return nBase;
    }

    public int getnSlider() {
        return nSlider;
    }

    public int getnTeles() {
        return nTeles;
    }

    public int getnRotac() {
        return nRotac;
    }

    public int getnHinge() {
        return nHinge;
    }

    public int getnEffector() {
        return nEffector;
    }

    public Vector3d getDimensiones() {
        return dimensiones;
    }

    public Vector3D getCdm() {
        return cdm;
    }

    public double getMasaTotal() {
        return masaTotal;
    }

    public double getIx() {
        return Ix;
    }

    public double getIy() {
        return Iy;
    }

    public double getIz() {
        return Iz;
    }

    public double getMediaConexionesPorModulo() {
        return mediaConexionesPorModulo;
    }

    public double getDispersionConexionesPorModulo() {
        return dispersionConexionesPorModulo;
    }
    
    

    private void calculaCaracteristicasConexiones(int[] conexiones) {

        int[] conexionesModulo = new int[nModules];
        conexionesModulo[0] = conexiones[0];
        //Si hay mas de un modulo añadimos la conexion que falta a los demas modulos
        if (this.nModules > 1) {
            for (int i = 1; i < nModules - 1; i++) {
                conexionesModulo[i] = conexiones[i] + 1;
            }
            conexionesModulo[nModules - 1] = 1;
        }
        this.mediaConexionesPorModulo = this.media(conexionesModulo);
        this.dispersionConexionesPorModulo = this.dispersionTipica(conexionesModulo, this.mediaConexionesPorModulo);
    }

    private double media(int[] vector) {
        double media = 0;
        for (int i = 0; i < this.nModules; i++) {
            media += vector[i];
        }
        return media / this.nModules;
    }

    private double dispersionTipica(int[] vector, double media) {
        double dispersion = 0;
        for (int i = 0; i < this.nModules; i++) {
            dispersion += Math.pow(vector[i] - media,2);
        }
        return Math.sqrt(dispersion) / this.nModules;
    }

}
