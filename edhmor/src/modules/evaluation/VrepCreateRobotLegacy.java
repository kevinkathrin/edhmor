/*
 * EDHMOR - Evolutionary designer of heterogeneous modular robots
 * <https://bitbucket.org/afaina/edhmor>
 * Copyright (C) 2015 GII (UDC) and REAL (ITU)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package modules.evaluation;

import coppelia.FloatWA;
import coppelia.IntW;
import coppelia.IntWA;
import coppelia.remoteApi;
import java.util.ArrayList;
import java.util.List;
import javax.vecmath.Matrix3d;
import javax.vecmath.Vector3d;
import modules.ModuleSet;
import modules.ModuleSetFactory;
import modules.util.DepreciatedModuleRotation;
import modules.util.ModuleRotation;
import modules.util.SimulationConfiguration;
import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

/**
 * VrepCreateRobotLegacy.java
 * Created on 22/11/2015
 * @author Andres Faiña <anfv  at itu.dk>
 */



public class VrepCreateRobotLegacy {

    private int nModulesMax;
    private int nModules = 1;
    //CROMOSOMA A DECODIFICAR 
    private int cromosomaInt[];
    private double cromosomaDouble[];
    private int[] moduleType;
    private int conexiones[];
    private int cara_padre[];
    private String pathBase = "/home/fai/svn/tesis/programacion/gazebo/tesis/";
    private String worldbase = "base.world";
    private int cara_hijo[];
    private double amplitudeControl[];
    private double angularFreqControl[];
    private int phaseControl[];
    private double amplitudeModulation[];
    private double frequencyModulation[];
    private int i_base = 0, i_slider = 0, i_telescope = 0, i_hinge_axial = 0, i_hinge = 0, i_effector = 0, i_snake = 0;
    private int indice_tipo_modulo[];
    private int nivel[];
    private int numero_modulos_nivel[];
    private int indice_modulos_nivel[];
    private int indice_hijo[];
    private int modulo_padre[];
    private int cara_padres_ocupadas[][];
    private double amplitudeControlOrgByTipo[];
    private double angularFreqControlOrgByTipo[];
    private int phaseControlOrgByTipo[];
    private double amplitudeModulationOrgByTipo[];
    private double frequencyModulationOrgByTipo[];
    private int treeMatrix[][];
    private Vector3d min_pos, max_pos, dimensiones;
    private Vector3D cdm;
    private List<Vector3D> carasApoyo;
    private double robotMass = 0.0;
    private int hingeXY[];
    private String[][] prop_enlace;
    private double alturaInicial = 0.0;
    private boolean useSoporte = false;
    private double fitnessParameter = 0.0;

    private double Ix = 0, Iy = 0, Iz = 0;
    private double mediaConexionesPorModulo;
    private double dispersionConexionesPorModulo;

    private remoteApi vrep;
    private int clientID;
    private List<Integer> moduleHandlers;
    private List<Integer> forceSensorHandlers;
    private ModuleSet moduleSet;
            

    VrepCreateRobotLegacy(remoteApi vrep, int clientID, double[] cromosomaDouble, String worldBase, double fP) {
        this(clientID, cromosomaDouble, worldBase);
        this.fitnessParameter = fP;
        this.vrep = vrep;
        this.clientID = clientID;
    }

    VrepCreateRobotLegacy(int clientID, double[] cromos, String worldBase) {
        
        //Load the module set 
        moduleSet = ModuleSetFactory.getModulesSet();

        //TODO: Remove all the objects 
        //TODO: Create scene (obstacles, etc.)
        //System.out.println("CreateRobot: Long cromo=" + cromos.length);
        cromosomaInt = new int[cromos.length];
        for (int i = 0; i < cromos.length; i++) {
            cromosomaInt[i] = (int) Math.floor(cromos[i]);
        }

        if ((cromos.length + 3) % 9 == 0) {
            this.nModulesMax = (cromos.length + 3) / 9;
        } else {

            if ((cromos.length + 3) % 6 == 0) {
                this.nModulesMax = (cromos.length + 3) / 6;

            } else {
                if ((cromos.length + 3) % 5 == 0) {
                    this.nModulesMax = (cromos.length + 3) / 5;

                } else {
                    if (cromos.length % 5 == 0) {
                        //Depreciated
                        System.err.println("VrepCreateRobot: Are you sure? This lenght for the chromosome is deprecated. Chromosome lenght: " + cromos.length + ", in other words, chromosome%5=0");

                        //Metamodulo base como unicio del arbol morfologico
                        this.nModulesMax = (cromos.length / 5) + 1;

                    } else {
                        System.err.println("VrepCreateRobot: Error in the lenght of the chromosome; length: " + cromos.length);
                        for (int i = 0; i < cromos.length; i++) {
                            System.err.print(cromos[i] + ", ");
                        }
                        System.exit(-1);
                    }

                }
                if (SimulationConfiguration.isDebug()) {
                    for (int i = 0; i < this.cromosomaInt.length; i++) {
                        System.out.print(this.cromosomaInt[i] + ", ");
                    }
                }
            }
        }

        this.cromosomaDouble = cromos;
        this.moduleType = new int[nModulesMax];
        this.conexiones = new int[nModulesMax - 1];
        this.cara_padre = new int[nModulesMax - 1];
        this.cara_hijo = new int[nModulesMax - 1];
        this.amplitudeControl = new double[nModulesMax];
        this.angularFreqControl = new double[nModulesMax];
        this.phaseControl = new int[nModulesMax];
        this.amplitudeModulation = new double[nModulesMax];
        this.frequencyModulation = new double[nModulesMax];
        this.indice_tipo_modulo = new int[nModulesMax];
        this.nivel = new int[nModulesMax];
        this.numero_modulos_nivel = new int[nModulesMax];
        this.indice_modulos_nivel = new int[nModulesMax];
        this.indice_hijo = new int[nModulesMax];
        this.modulo_padre = new int[nModulesMax];
        this.cara_padres_ocupadas = new int[nModulesMax][14];
        this.amplitudeControlOrgByTipo = new double[nModulesMax];
        this.angularFreqControlOrgByTipo = new double[nModulesMax];
        this.phaseControlOrgByTipo = new int[nModulesMax];
        this.amplitudeModulationOrgByTipo = new double[nModulesMax];
        this.frequencyModulationOrgByTipo = new double[nModulesMax];
        this.treeMatrix = new int[nModulesMax][nModulesMax];
        this.hingeXY = new int[nModulesMax];

        if ((cromos.length + 3) % 9 != 0) {
            for (int i = 0; i < nModulesMax; i++) {
                this.amplitudeControl[i] = 0.0;
                this.amplitudeControlOrgByTipo[i] = 0.0;

                this.angularFreqControl[i] = 0.0;
                this.angularFreqControlOrgByTipo[i] = 0.0;

                this.amplitudeModulation[i] = 0.0;
                this.amplitudeModulationOrgByTipo[i] = 0.0;

                this.frequencyModulation[i] = 0.0;
                this.frequencyModulationOrgByTipo[i] = 0.0;
            }
        }

        this.prop_enlace = new String[nModules][4];

        this.max_pos = new Vector3d(Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY);
        this.min_pos = new Vector3d(Double.MAX_VALUE, Double.MAX_VALUE, Double.MAX_VALUE);
        this.dimensiones = new Vector3d(0, 0, 0);
        this.cdm = new Vector3D(0, 0, 0);
        this.carasApoyo = new ArrayList<Vector3D>();

        //Now, analysis the chromosome
        this.chromosomeAnalysis();
        int type = this.moduleType[0];
        this.robotMass += moduleSet.getModulesMass(type);

        Vector3D facePos;
        for (int face = 0; face < moduleSet.getModulesFacesNumber(type); face++) {
            facePos = moduleSet.getOriginFaceVector(type, face);

            if (this.max_pos.x < facePos.getX()) {
                this.max_pos.x = facePos.getX();
            }
            if (this.max_pos.y < facePos.getY()) {
                this.max_pos.y = facePos.getY();
            }
            if (this.max_pos.z < facePos.getZ()) {
                this.max_pos.z = facePos.getZ();
            }

            if (this.min_pos.x > facePos.getX()) {
                this.min_pos.x = facePos.getX();
            }
            if (this.min_pos.y > facePos.getY()) {
                this.min_pos.y = facePos.getY();
            }
            if (this.min_pos.z > facePos.getZ()) {
                this.min_pos.z = facePos.getZ();
                carasApoyo.clear();
            }
            //Añadimos apoyos
            if (facePos.getZ() - this.min_pos.z < 0.15) {
                carasApoyo.add(facePos);
            }
        }

    }

    public void createRobot() {

        //load a new scene
        loadScene();
        
        //Calculamos el xyz y el rpy y los body de union para todas las uniones
        // y de paso calculamos las dimensiones del robot, el centro de masas, etc.
        prop_enlace = calcula_attach();
    }
    
    public void loadScene(){
        //TODO: select the correct scene for the simulation (flat terrain, obstacles, paint wall, etc...)        
        String scenePath = "scenes/edhmor/default.ttt";

        int ret = vrep.simxLoadScene(clientID, scenePath, 0, remoteApi.simx_opmode_oneshot_wait );

        if (ret == remoteApi.simx_return_ok) {
            System.out.format("Scene loaded correctly: \n");
        } else {
            System.out.format("Remote API function call returned with error code: %d\n", ret);
            System.exit(-1);
        }
        
    }

    private String[][] calcula_attach() {

        String propiedades[][] = new String[nModules][4];
        Vector3D posicionModulo[] = new Vector3D[nModules];
        Matrix3d rotacionModulo[] = new Matrix3d[nModules];
        posicionModulo[0] = new Vector3D(0, 0, 0);
        rotacionModulo[0] = new Matrix3d(1, 0, 0, 0, 1, 0, 0, 0, 1);
        DepreciatedModuleRotation rotaciones = new DepreciatedModuleRotation();
        moduleHandlers = new ArrayList<Integer>();
        forceSensorHandlers = new ArrayList<Integer>();

        propiedades[0][0] = "0 0 0";                               //x,y,z
        propiedades[0][1] = "0 0 0";                               //r,p,y
        propiedades[0][2] = "ERROR, el modulo 0 no se une a nadie";  //parte del padre al que se une (base o actuador)
        propiedades[0][3] = "ERROR, el modulo 0 no se une a nadie";  //parte del modulo por la que se une al padre (base o acruador)

        int rootModuleHandler = addModule(0);
        moduleHandlers.add(rootModuleHandler);
        actualiza_indices_modulos(0);

        for (int modulo = 1; modulo < nModules; modulo++) {

            int moduleHandler = addModule(modulo);
            moduleHandlers.add(moduleHandler);

            int modType = moduleType[modulo];
            int parentModuleType = moduleType[modulo_padre[modulo]];
            int conectionFace = cara_padre[modulo - 1] % moduleSet.getModulesFacesNumber(parentModuleType);
            int orientation = cara_hijo[(modulo - 1)] % moduleSet.getModuleOrientations(modType);

            //We need two versions of the hinge model, one with the hinge axis
            //rotated 90 with respect to the other
            //If the module is a hinge module 
            if (modType == 4) {
                if (orientation == 0) {
                    hingeXY[modulo] = 0;
                } else {
                    if (orientation == 1) {
                        hingeXY[modulo] = 1;
                    } else {
                        System.err.println("Error al elegir el tipo de hinge (eje en X o Y)");
                        System.exit(-1);
                    }
                }
                orientation = 0;
            }


            
            
//            System.out.println("Tipo padre e hijo: " + tipo_p + "  " + tipo_h);
//            System.out.println("Cara padre e hijo: " + cara_p + "  " + cara_h);
            //Comprobaciuon de que la cara del padre donde vamos a unir el hijo esta libre
            while (cara_padres_ocupadas[modulo_padre[modulo]][conectionFace] == 1) {
                conectionFace = ++conectionFace % moduleSet.getModulesFacesNumber(parentModuleType);
            }
            cara_padres_ocupadas[modulo_padre[modulo]][conectionFace] = 1;


            //Calculo de xyz y de rpy
            Vector3D normalCaraPadre = moduleSet.getNormalFaceVector(parentModuleType, conectionFace);


            //Calculo de rpy
            double rotacion[];
            rotacion = rotaciones.calculateRPY_Gazebo_oldMethod(orientation, normalCaraPadre); 

            
            propiedades[modulo][1] = rotacion[0] + " " + rotacion[1] + " " + rotacion[2]; 

            //Calculo del vector padre origen -> cara de union
            Vector3D ocp = null;
            //ocp = Test.vector_origen_cara[tipo_p][cara_p];+++++++++
            ocp = moduleSet.getOriginFaceVector(parentModuleType, conectionFace);

            //Calculo del vector del modulo hijo de su origen -> cara de union
            Vector3D och = null;
            
            //Face of the child to attach the module
            int childFace = moduleSet.getConnectionFaceForEachOrientation(modType, orientation);
            och = moduleSet.getOriginFaceVector(modType, childFace);
            cara_padres_ocupadas[modulo][childFace] = 1;
            
            
            
            //TODO: REMOVE THIS CODE
//            if (orientation == 0) {
//                och = moduleSet.getOriginFaceVector(modType, orientation);
//                cara_padres_ocupadas[modulo][orientation] = 1;         //Ponemos esa cara ocupada
//            } else {
//                if (orientation == 5 || orientation == 6) {
//                    //Caras del actuador del slider
//                    och = moduleSet.getOriginFaceVector(modType, 10);
//                    cara_padres_ocupadas[modulo][10] = 1;         //Ponemos esa cara ocupada
//
//                } else {
//                    och = moduleSet.getOriginFaceVector(modType, 1);
//                    cara_padres_ocupadas[modulo][1] = 1;         //Ponemos esa cara ocupada
//                }
//            }

//            System.out.println("Vector o->cara_h: " + och);
//            System.out.println("Vector o->cara_p: " + ocp);
            Vector3D och_rot = rotaciones.calculaRotacionRPY(och, rotacion); //REMOVEME
            

            och_rot = och_rot.negate();
            och_rot = och_rot.add(ocp);

//            System.out.println("Vector o->origen_hijo: " + och_rot);
//            System.out.println("Cambio de union\n\n");
            propiedades[modulo][0] = och_rot.getX() + " " + och_rot.getY() + " " + och_rot.getZ();
            double pos[]=och_rot.toArray();

            rotacionModulo[modulo] = rotaciones.calculaMatrizRotacionGlobal(propiedades[modulo][1], rotacionModulo[modulo_padre[modulo]]); //REMOVE ME


            posicionModulo[modulo] = rotaciones.calculaRotacion(new Vector3D(och_rot.getX(), och_rot.getY(), och_rot.getZ()), rotacionModulo[modulo_padre[modulo]]); //REMOVE ME
            

            posicionModulo[modulo] = posicionModulo[modulo].add(posicionModulo[modulo_padre[modulo]]);

            
            //Rotate module to the correct orientation
            double [] vrepOrientation = rotaciones.calculateRPY_Vrep_oldMethod(modType, orientation, normalCaraPadre); //Orientation in the vrep system of refererence
            vrepOrientation[0]=vrepOrientation[0]/180*Math.PI;
            vrepOrientation[1]=vrepOrientation[1]/180*Math.PI;
            vrepOrientation[2]=vrepOrientation[2]/180*Math.PI;
            rotateModule(moduleHandler, moduleHandlers.get(modulo_padre[modulo]), vrepOrientation);

            //TODO: Move module to the correct position
            moveModule(moduleHandler, moduleHandlers.get(modulo_padre[modulo]), pos);
            
            //TODO: addForceSensor(dadHandler, dadFace);
            int forceSensor = addForceSensor();
            forceSensorHandlers.add(forceSensor);
            
            if(normalCaraPadre.getZ() == 0){
                double[] forceSensorOrientation={0, 0, 0};
                if(normalCaraPadre.getY() ==0)
                    forceSensorOrientation[1] = Math.PI/2;
                else
                    forceSensorOrientation[0] = Math.PI/2;
                
                rotateModule(forceSensor, moduleHandlers.get(modulo_padre[modulo]), forceSensorOrientation);
            }
            double posForceSensor[] = ocp.toArray();
            moveModule(forceSensor, moduleHandlers.get(modulo_padre[modulo]), posForceSensor);
            
            //TODO: Set the force sensor as the parent object
            int offset = 0;
            if ( !moduleSet.faceBelongsToBasePart(moduleType[modulo_padre[modulo]], conectionFace) )
                offset = 2;
            setObjectParent(forceSensor, moduleHandlers.get(modulo_padre[modulo]) + 1 + offset);
            
            offset = 0;
            if ( moduleSet.faceBelongsToBasePart(modType, childFace) )
                setObjectParent(moduleHandler + 1, forceSensor);
            else
            {
                setObjectParent(moduleHandler + 3, forceSensor);
                setObjectParent(moduleHandler + 2, moduleHandler + 3);
                setObjectParent(moduleHandler + 1, moduleHandler + 2);
                setObjectParent(moduleHandler, moduleHandler + 1);
            }
            
            
            Vector3D aux = new Vector3D(posicionModulo[modulo].getX(), posicionModulo[modulo].getY(), posicionModulo[modulo].getZ());
            
            double s = moduleSet.getModulesMass(modType);
            this.robotMass += s;
            aux = aux.scalarMultiply(s);
            this.cdm = this.cdm.add(aux);

            if (SimulationConfiguration.isDebug()) {
                System.out.println("Masa Total: " + this.robotMass + "; cdm: " + this.cdm);
            }

            //Claculo de la cara mas baja para luego subir esa distancia el robot
            //es decir, le damos un apoyo al robot
            int nCarasBusqueda = moduleSet.getModulesFacesNumber(modType);


            for (int cara = 0; cara < nCarasBusqueda; cara++) {
                och = moduleSet.getOriginFaceVector(modType, cara);
                och_rot = rotaciones.calculaRotacion(och, rotacionModulo[modulo]); 
        
                och_rot = och_rot.add(posicionModulo[modulo]);

                if (this.max_pos.x < och_rot.getX()) {
                    this.max_pos.x = och_rot.getX();
                }
                if (this.max_pos.y < och_rot.getY()) {
                    this.max_pos.y = och_rot.getY();
                }
                if (this.max_pos.z < och_rot.getZ()) {
                    this.max_pos.z = och_rot.getZ();
                }

                if (this.min_pos.x > och_rot.getX()) {
                    this.min_pos.x = och_rot.getX();
                }
                if (this.min_pos.y > och_rot.getY()) {
                    this.min_pos.y = och_rot.getY();
                }
                if (this.min_pos.z > och_rot.getZ()) {
                    this.min_pos.z = och_rot.getZ();
                    carasApoyo.clear();
                }
                //Añadimos apoyos
                if (och_rot.getZ() - this.min_pos.z < 0.15) {
                    carasApoyo.add(och_rot);
                }
            }

            actualiza_indices_modulos(modulo);

        }

        this.dimensiones.add(this.max_pos);
        this.dimensiones.sub(this.min_pos);
        this.cdm = this.cdm.scalarMultiply(1 / this.robotMass);

        //Calculo del momento de inercia
        for (int modulo = 1; modulo < nModules; modulo++) {
            Vector3D posModulo = new Vector3D(posicionModulo[modulo].getX(),posicionModulo[modulo].getY(),posicionModulo[modulo].getZ());
            
            double distanciaEjeZ = Math.pow(posModulo.getX() - this.cdm.getX(), 2) + Math.pow(posModulo.getY() - this.cdm.getY(), 2);
            distanciaEjeZ = Math.sqrt(distanciaEjeZ);
            double distanciaEjeX = Math.pow(posModulo.getY() - this.cdm.getY(), 2) + Math.pow(posModulo.getZ() - this.cdm.getZ(), 2);
            distanciaEjeX = Math.sqrt(distanciaEjeX);
            double distanciaEjeY = Math.pow(posModulo.getX() - this.cdm.getX(), 2) + Math.pow(posModulo.getZ() - this.cdm.getZ(), 2);
            distanciaEjeY = Math.sqrt(distanciaEjeY);

            Ix += distanciaEjeX * moduleSet.getModulesMass(moduleType[modulo]);
            Iy += distanciaEjeY * moduleSet.getModulesMass(moduleType[modulo]);
            Iz += distanciaEjeZ * moduleSet.getModulesMass(moduleType[modulo]);

        }

        //TODO: Comprobar estabilidad
        //System.out.println("carasApoyo: "+ carasApoyo.size()+"uno: "+ carasApoyo.get(0)+"dos: "+ carasApoyo.get(1));
        //TODO: Comprobar que min_z y min_pos.z coinciden, eliminar la siguiente linea
        if (SimulationConfiguration.isDebug()) {
            System.out.println("min_pos: " + this.min_pos + "; max_pos: " + this.max_pos + "; dimensiones: " + this.dimensiones);
            System.out.println("Masa Total: " + this.robotMass + "; cdm: " + this.cdm);
        }

        alturaInicial = (Math.abs(this.min_pos.z) + 0.01);
        if (this.worldbase.contains("baseEscalera")) {
            alturaInicial += 0.4;
            alturaInicial += 0.1 * Math.floor(this.max_pos.x);
        }
        if (this.worldbase.contains("sueloRugoso")) {
            alturaInicial += 0.15;
        }
        if (this.worldbase.contains("Guia")) {
            alturaInicial = alturaInicial + 0.5;//Math.max(alturaInicial, 0.5);
        }
        if (this.useSoporte) {
            alturaInicial = Math.max(alturaInicial, 0.6);
        }
        if (this.worldbase.contains("manipulator")) {
            alturaInicial = 1;
        }
        propiedades[0][0] = "0 0 " + alturaInicial;
        
        double[] posIni = {0, 0, alturaInicial};
        //: Move the robot up
        moveModule(moduleHandlers.get(0), -1, posIni);

        reset_indice_modulos();
        return propiedades;
    }

    private void chromosomeAnalysis() {
//        System.out.print("Analisis: Cromosoma= ");
//        for (int j = 0; j < cromosoma.length; j++) {
//            System.out.print(cromosoma[j] + " ");
//        }
        for (int i = 0; i < nModulesMax; i++) {
            moduleType[i] = cromosomaInt[i];
        }

        for (int i = 0; i < (nModulesMax - 1); i++) {
            conexiones[i] = cromosomaInt[nModulesMax + i];
        }

        for (int i = 0; i < (nModulesMax - 1); i++) {
            cara_padre[i] = cromosomaInt[2 * nModulesMax - 1 + i];
        }

        for (int i = 0; i < (nModulesMax - 1); i++) {
            cara_hijo[i] = cromosomaInt[3 * nModulesMax - 2 + i];
        }

        if ((cromosomaInt.length + 3) % 9 == 0) {

            //Parametros del control de la amplitud
            for (int i = 0; i < (nModulesMax); i++) {
                this.amplitudeControl[i] = cromosomaDouble[4 * nModulesMax - 3 + i];
            }

            //Parametros del control de la frecuencia angular
            for (int i = 0; i < (nModulesMax); i++) {
                this.angularFreqControl[i] = cromosomaDouble[5 * nModulesMax - 3 + i];
            }

            //Parametros del control de la fase
            for (int i = 0; i < (nModulesMax); i++) {
                this.phaseControl[i] = cromosomaInt[6 * nModulesMax - 3 + i];
            }

            //Parametros del control del modulador de la amplitud
            for (int i = 0; i < (nModulesMax); i++) {
                this.amplitudeModulation[i] = cromosomaDouble[7 * nModulesMax - 3 + i];
            }

            //Parametros del control del modulador de la amplitud
            for (int i = 0; i < (nModulesMax); i++) {
                this.frequencyModulation[i] = cromosomaDouble[8 * nModulesMax - 3 + i];
            }

        } else {
            for (int i = 0; i < (nModulesMax); i++) {
                phaseControl[i] = cromosomaInt[4 * nModulesMax - 3 + i];
            }

            if (cromosomaInt.length > 5 * nModulesMax - 3) {
                for (int i = 0; i < (nModulesMax); i++) {
                    frequencyModulation[i] = cromosomaDouble[5 * nModulesMax - 3 + i];
                }
            }

        }

        //Calculo del nivel de cada modulo
        int indice_nivel = 0;
        numero_modulos_nivel[0] = 1;
        int i = 0;

        for (int nivel_actual = 0; nivel_actual < nModulesMax; nivel_actual++) {
            for (int j = 0; j < numero_modulos_nivel[nivel_actual] && i < nModulesMax; j++) {
                nivel[i] = nivel_actual;
                indice_modulos_nivel[i] = indice_nivel++;
                if (i < nModulesMax - 1) //Para q no desborde el array
                {
                    numero_modulos_nivel[nivel_actual + 1] += conexiones[i];
                    nModules += conexiones[i];
                }
                i++;
            }
            indice_nivel = 0;
        }
        if (nModules > nModulesMax) {
            nModules = nModulesMax;
        }

        //calculamos el indice de hijo
        int k = 0;
        for (int ii = 0; ii < nModules - 1; ii++) {
            for (int j = 0; j < conexiones[ii] && k < (nModules - 1); j++) {
                modulo_padre[1 + k] = ii;
                indice_hijo[1 + k++] = j;
            }
        }

        //Rellenamos el indice de cada modulo segun el tipo
        reset_indice_modulos();
        for (int m = 0; m < nModules; m++) {
            indice_tipo_modulo[m] = getModuloNumber(m);
            this.actualiza_indices_modulos(m);
        }


        calculaCaracteristicasConexiones(conexiones);

        //Rellenamos la matriz del arbol (treeMatrix)
        for (int m = 0; m < nModules; m++) {
            int mAux = m;
            for (int n = nivel[m]; n > 0; n--) {
                treeMatrix[n][m] = mAux;
                mAux = modulo_padre[mAux];
            }
        }
//        for (int m = 0; m < nModules; m++) {
//            System.out.println("treeMatrix: " + treeMatrix[m][0] + " " + treeMatrix[m][1] + " " + treeMatrix[m][2] + " " + treeMatrix[m][3]);
//        }
        //Calculo del array de parametros de control
        for (int m = 0; m < nModules; m++) {
            if (moduleType[m] == 1) {
                this.amplitudeControlOrgByTipo[indice_tipo_modulo[m]] = this.amplitudeControl[m];
                this.angularFreqControlOrgByTipo[indice_tipo_modulo[m]] = this.angularFreqControl[m];
                phaseControlOrgByTipo[indice_tipo_modulo[m]] = phaseControl[m];
                this.amplitudeModulationOrgByTipo[indice_tipo_modulo[m]] = this.amplitudeModulation[m];
                this.frequencyModulationOrgByTipo[indice_tipo_modulo[m]] = this.frequencyModulation[m];
            }
            if (moduleType[m] == 2) {
                this.amplitudeControlOrgByTipo[indice_tipo_modulo[m] + i_slider] = this.amplitudeControl[m];
                this.angularFreqControlOrgByTipo[indice_tipo_modulo[m] + i_slider] = this.angularFreqControl[m];
                phaseControlOrgByTipo[indice_tipo_modulo[m] + i_slider] = phaseControl[m];
                this.amplitudeModulationOrgByTipo[indice_tipo_modulo[m] + i_slider] = this.amplitudeModulation[m];
                this.frequencyModulationOrgByTipo[indice_tipo_modulo[m] + i_slider] = this.frequencyModulation[m];
            }
            if (moduleType[m] == 3) {
                this.amplitudeControlOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope] = this.amplitudeControl[m];
                this.angularFreqControlOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope] = this.angularFreqControl[m];
                phaseControlOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope] = phaseControl[m];
                this.amplitudeModulationOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope] = this.amplitudeModulation[m];
                this.frequencyModulationOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope] = this.frequencyModulation[m];
            }
            if (moduleType[m] == 4) {
                this.amplitudeControlOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope + i_hinge_axial] = this.amplitudeControl[m];
                this.angularFreqControlOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope + i_hinge_axial] = this.angularFreqControl[m];
                phaseControlOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope + i_hinge_axial] = phaseControl[m];
                this.amplitudeModulationOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope + i_hinge_axial] = this.amplitudeModulation[m];
                this.frequencyModulationOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope + i_hinge_axial] = this.frequencyModulation[m];
            }
            if (moduleType[m] == 5) {
                this.amplitudeControlOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope + i_hinge_axial + i_hinge] = this.amplitudeControl[m];
                this.angularFreqControlOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope + i_hinge_axial + i_hinge] = this.angularFreqControl[m];
                phaseControlOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope + i_hinge_axial + i_hinge] = phaseControl[m];
                this.amplitudeModulationOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope + i_hinge_axial + i_hinge] = this.amplitudeModulation[m];
                this.frequencyModulationOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope + i_hinge_axial + i_hinge] = this.frequencyModulation[m];
            }
            if (moduleType[m] == 6) {
                this.amplitudeControlOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope + i_hinge_axial + i_hinge + i_effector] = this.amplitudeControl[m];
                this.angularFreqControlOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope + i_hinge_axial + i_hinge + i_effector] = this.angularFreqControl[m];
                phaseControlOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope + i_hinge_axial + i_hinge + i_effector] = phaseControl[m];
                this.amplitudeModulationOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope + i_hinge_axial + i_hinge + i_effector] = this.amplitudeModulation[m];
                this.frequencyModulationOrgByTipo[indice_tipo_modulo[m] + i_slider + i_telescope + i_hinge_axial + i_hinge + i_effector] = this.frequencyModulation[m];
            }
        }

        this.reset_indice_modulos();

    }

    //Aux functions
    private void reset_indice_modulos() {
        i_base = 0;
        i_slider = 0;
        i_telescope = 0;
        i_hinge_axial = 0;
        i_hinge = 0;
        i_effector = 0;
        i_snake = 0;
        //i_wheeel=0;
    }

    private int getModuloNumber(int numero_modulo) {
        int number = 0;
        switch (moduleType[numero_modulo]) {
            case 0:
                number = i_base;
                break;
            case 1:
                number = i_slider;
                break;
            case 2:
                number = i_telescope;
                break;
            case 3:
                number = i_hinge_axial;
                break;
            case 4:
                number = i_hinge;
                break;
            case 5:
                number = i_effector;
                break;
            case 6:
                number = i_snake;
                break;
            //case 5:nameModel="wheel"+i_wheel; break;
        }
        return number;
    }

    private void actualiza_indices_modulos(int numero_modulo) {
        switch (moduleType[numero_modulo]) {
            case 0:
                i_base++;
                break;
            case 1:
                i_slider++;
                break;
            case 2:
                i_telescope++;
                break;
            case 3:
                i_hinge_axial++;
                break;
            case 4:
                i_hinge++;
                break;
            case 5:
                i_effector++;
                break;
            case 6:
                i_snake++;
                break;
            //case 5:i_wheel++; break;
        }
    }

    private String getNameModulo(int numero_modulo) {
        String nameModel = "";
        switch (moduleType[numero_modulo]) {
            case 0:
                nameModel = "base";
                break;
            case 1:
                nameModel = "slider";
                break;
            case 2:
                nameModel = "telescope";
                break;
            case 3:
                nameModel = "hinge_axial";
                break;
            case 4:
                nameModel = "hinge";
                break;
            case 5:
                nameModel = "effector";
                break;
            case 6:
                nameModel = "snake";
                break;
            //case 5:nameModel="wheel"; break;
        }
        return nameModel;
    }

    private void calculaCaracteristicasConexiones(int[] conexiones) {

        int[] conexionesModulo = new int[nModules];
        conexionesModulo[0] = conexiones[0];
        //Si hay mas de un modulo añadimos la conexion que falta a los demas modulos
        if (this.nModules > 1) {
            for (int i = 1; i < nModules - 1; i++) {
                conexionesModulo[i] = conexiones[i] + 1;
            }
            conexionesModulo[nModules - 1] = 1;
        }
        this.mediaConexionesPorModulo = this.media(conexionesModulo);
        this.dispersionConexionesPorModulo = this.dispersionTipica(conexionesModulo, this.mediaConexionesPorModulo);
    }

    private double media(int[] vector) {
        double media = 0;
        for (int i = 0; i < this.nModules; i++) {
            media += vector[i];
        }
        return media / this.nModules;
    }

    private double dispersionTipica(int[] vector, double media) {
        double dispersion = 0;
        for (int i = 0; i < this.nModules; i++) {
            dispersion += Math.pow(vector[i] - media, 2);
        }
        return Math.sqrt(dispersion) / this.nModules;
    }

    private int addModule(int moduloNumber) {
        String modelPath = modelPath(moduleType[moduloNumber]);

        IntW parentModuleHandle = new IntW(0);
        //clientID,final String modelPathAndName, int options, IntW baseHandle, int operationMode
        int ret = vrep.simxLoadModel(clientID, modelPath, 0, parentModuleHandle, remoteApi.simx_opmode_oneshot_wait);

        if (ret == remoteApi.simx_return_ok) {
            //System.out.format("Model loaded correctly: %d\n", parentModuleHandle.getValue());
            return parentModuleHandle.getValue();
        } else {
            System.out.format("addModule Function: Remote API function call returned with error code: %d\n", ret);
            System.exit(-1);
            return -1;
        }

    }
    
    private int addForceSensor() {
        String modelPath = forceSensorPath();

        IntW parentModuleHandle = new IntW(0);
        //clientID,final String modelPathAndName, int options, IntW baseHandle, int operationMode
        int ret = vrep.simxLoadModel(clientID, modelPath, 0, parentModuleHandle, remoteApi.simx_opmode_oneshot_wait);

        if (ret == remoteApi.simx_return_ok) {
            //System.out.format("Model loaded correctly: %d\n", parentModuleHandle.getValue());
            return parentModuleHandle.getValue();
        } else {
            System.out.format("addForceSensor Function: Remote API function call returned with error code: %d\n", ret);
            System.exit(-1);
            return -1;
        }

    }

    private void rotateModule(int moduleHandler, int parentHandler, double[] rotation) {
        //int simxSetObjectPosition(int clientID,int objectHandle, int relativeToObjectHandle, final FloatWA position, int operationMode)  

        FloatWA rot = new FloatWA(3);
        rot.getArray()[0] = (float) (rotation[0]);
        rot.getArray()[1] = (float) (rotation[1]);
        rot.getArray()[2] = (float) (rotation[2]);

        int ret = vrep.simxSetObjectOrientation(clientID, moduleHandler, parentHandler, rot, remoteApi.simx_opmode_oneshot_wait);

//        if (ret == remoteApi.simx_return_ok) {
//            //System.out.println("Changed orientation correctly: " + rot.getArray()[0]/Math.PI*180 + " " + rot.getArray()[1]/Math.PI*180 + " " + rot.getArray()[2]/Math.PI*180 );
//        } else {
//            System.out.format("rotateModule Function: Remote API function call returned with error code: %d\n", ret);
//            System.exit(-1);
//        }
    }
    
    private void moveModule(int moduleHandler, int parentHandler, double[] position) {
        //int simxSetObjectPosition(int clientID,int objectHandle, int relativeToObjectHandle, final FloatWA position, int operationMode)
        FloatWA pos = new FloatWA(3);
        pos.getArray()[0] = (float) position[0];
        pos.getArray()[1] = (float) position[1];
        pos.getArray()[2] = (float) position[2];

        int ret = vrep.simxSetObjectPosition(clientID, moduleHandler, parentHandler, pos, remoteApi.simx_opmode_oneshot_wait);
//        if (ret == vrep.simx_return_ok) {
//            //System.out.format("Model moved correctly: %d\n", moduleHandler);
//        } else {
//            System.out.format("moveModule Function: Remote API function call returned with error code: %d\n", ret);
//        }
    }
    
    private void setObjectParent(int moduleHandler, int parentHandler){
        //int simxSetObjectParent(int clientID,int objectHandle,int parentObject,boolean keepInPlace,int operationMode);
    
        int ret = vrep.simxSetObjectParent(clientID, moduleHandler, parentHandler, true, remoteApi.simx_opmode_oneshot_wait);
//        if (ret == vrep.simx_return_ok) {
//            //System.out.format("Parent Object set correctly: %d\n", moduleHandler);
//        } else {
//            System.out.format("setObjectParent Function: Remote API function call returned with error code: %d\n", ret);
//        }
    }

    private String modelPath(int moduleType) {
        String path = "models/edhmor/";                         
        path += moduleSet.getModuleSetName() + "/";             //moduleSetName
        path += moduleSet.getModuleName(moduleType) + ".ttm";   //moduleName
        return path;
    }
    private String forceSensorPath() {
        String path = "models/edhmor/";                         
        path += moduleSet.getModuleSetName() + "/";             //moduleSetName
        path += "forceSensor.ttm";   //moduleName
        return path;
    }

    public List<Integer> getModuleHandlers() {
        return moduleHandlers;
    }

    public List<Integer> getForceSensorHandlers() {
        return forceSensorHandlers;
    }

    public double[] getAmplitudeControl() {
        return amplitudeControl;
    }

    public double[] getAngularFreqControl() {
        return angularFreqControl;
    }

    public int[] getPhaseControl() {
        return phaseControl;
    }

    public double[] getAmplitudeModulation() {
        return amplitudeModulation;
    }

    public double[] getFrequencyModulation() {
        return frequencyModulation;
    }

    public int[] getModuleType() {
        return moduleType;
    }
    
    
}
