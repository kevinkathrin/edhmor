/* 
 * EDHMOR - Evolutionary designer of heterogeneous modular robots
 * <https://bitbucket.org/afaina/edhmor>
 * Copyright (C) 2015 GII (UDC) and REAL (ITU)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package modules.evaluation;

import java.util.logging.Level;
import java.util.logging.Logger;






/**
 *
 */
public class JNIControlModulos {
    
    private double timeStopSimulation;
    private double calidad;
    
    private static String LINUX_NATIVELIB64 = "libgeneric_control64.so";
    private static String LINUX_NATIVELIB32 = "libgeneric_control32.so";
    
    private static String LIB_DIR = "lib/";
     
 
    static Logger LOGGER = Logger.getLogger("failogger"); 

    
    // Try to load the native Code library
    static {

      
        String nativeLibName = getNativelibName();
        String currentDir = System.getProperty("user.dir");
        String librender = currentDir + "/" + LIB_DIR + nativeLibName;
        
        try {
            
            System.load(librender);
            //LOGGER.info("Native rendering library loaded: " + librender);
            
        } catch( UnsatisfiedLinkError e ) {
            LOGGER.log(Level.SEVERE, "Could not load native code for render: " + librender, e);
            System.exit(-1);
        }
    }
   
    

    /** Returns the name of the native library for this operating system */
    private static String getNativelibName()
    {
            if (System.getProperty("os.arch").equalsIgnoreCase("i386")) 
                return LINUX_NATIVELIB32;
            else
                return LINUX_NATIVELIB64;
        }
    
        
    
   
    
    public JNIControlModulos() throws Exception {
        
    }

    
    
    public int evaluaMorfologia(int serverId, int nCiclosSim, double stepT, double poseUpdateRate, int nModulos, int[] tipoModulos,
            double[] amplitudeControl, double[] angFreqControl, int[] phaseControl,
            double[] amplitudeMod, double[] frequencyModulator, int fitnessFunction, double fitnessParameter,
            boolean useAmplitudeControl, boolean useAngFreqControl, boolean usePhaseControl,
            boolean useAmplitudeModulator, boolean useFrequencyModulator, boolean guiOn) throws Exception {
        try {
            double[] timeStopSimulationArray = {-1};
            double[] calidadArray= {-1};
            int status = evalua(serverId, nCiclosSim, stepT, poseUpdateRate, nModulos, tipoModulos,
                    amplitudeControl, angFreqControl, phaseControl,
                    amplitudeMod, frequencyModulator, fitnessFunction, fitnessParameter,
                    useAmplitudeControl, useAngFreqControl, usePhaseControl,
                    useAmplitudeModulator, useFrequencyModulator, guiOn, timeStopSimulationArray, calidadArray);
            timeStopSimulation = timeStopSimulationArray[0];
            calidad = calidadArray[0];
            
            return status;

        } catch (Exception e) {
            System.out.println("ERROR EN EL CONTROL (server "+ serverId+") Estamos en capturando la excepcion:");
            System.out.println(e);
            System.out.println("Intentamos seguir notificando el error...");
            return -6;
            //throw e;

        }
    }

    public double getCalidad() {
        return calidad;
    }

    public double getTimeStopSimulation() {
        return timeStopSimulation;
    }
    //Imported functions
    public native int evalua(int serverId, int nCiclosSim, double stepTime, double updatePoseRate, int n_modulos, int[] tipo_modulos,
            double[] ampControl, double[] angFreqControl, int[] phaseControl,
            double[] ampMod, double[] freqMod, int function, double functionParameter,
            boolean useAmpControl, boolean useAngFreqControl, boolean usePhaseControl, boolean useAmpMod, boolean useFreqMod, boolean guiOn, double[] timeStopSimulation, double[] calidad);
    
       
}




