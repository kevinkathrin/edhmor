/* 
 * EDHMOR - Evolutionary designer of heterogeneous modular robots
 * <https://bitbucket.org/afaina/edhmor>
 * Copyright (C) 2015 GII (UDC) and REAL (ITU)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package modules.evaluation;

import coppelia.BoolW;
import coppelia.FloatWA;
import coppelia.IntW;
import coppelia.IntWA;
import coppelia.StringWA;
import coppelia.remoteApi;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.vecmath.Vector3d;
import modules.ModuleSetFactory;
import modules.control.ControllerFactory;
import modules.control.RobotController;
import modules.control.SinusoidalController;
import modules.evaluation.fitness.FitnessFunction;
import modules.evaluation.fitness.FitnessFunctionFactory;
import modules.util.SimulationConfiguration;
import mpi.MPI;

/**
 *
 * @author fai
 */
public class VrepEvaluator {

    private double chromosomeDouble[];
    List<Double> fitnessList = new ArrayList<Double>();
    List<Double> simulationTimeList = new ArrayList<Double>();
    private int nAttempts = 10;
    private int nModules;
    private boolean useMPI = false;
    private boolean guiOn = true;
    static Logger LOGGER = Logger.getLogger("failogger");
    private int SERVER_ID = 0; // Numer of the Vrep server
    private double maxSimulationTime = 3;
    private double timeStartSim = 3;
    private int waitTimeLoadGazebo = 900;
    private int status = -1;
    private double timeStep = 0.05;
    private String scene = null;
    private String vrepComand;
    private VrepCreateRobot robot;
    private RobotController controller;
    private FitnessFunction fitnessFunction;
    private double fitnessParameter;

    private remoteApi vrepApi;
    private int clientID;
    private double time;
    private static int eval = 0;

    public void setGuiOn(boolean guiOn) {
        this.guiOn = guiOn;
    }

    public void setMaxSimulationTime(double number) {
        this.maxSimulationTime = number;
    }

    private void init(double[] cromo, String scene, double fP){
        this.fitnessParameter = fP;

        this.scene = scene;
        if (scene == null || scene.isEmpty() || scene.equals("")) {
            this.scene = SimulationConfiguration.getWorldsBase().get(0);
        }

        this.fitnessParameter = SimulationConfiguration.getFitnessParameters().get(0);

        this.chromosomeDouble = cromo;

        if ((cromo.length + 3) % 9 == 0) {
            nModules = (cromo.length + 3) / 9;
        } else {
            if ((cromo.length + 3) % 6 == 0) {
                nModules = (cromo.length + 3) / 6;
            } else {
                if ((cromo.length + 3) % 5 == 0) {
                    nModules = (cromo.length + 3) / 5;
                } else {
                    System.err.println("Vrep Evaluator");
                    System.err.println("Error in the number of modules nModules; cromo.length=" + cromo.length);
                    System.exit(-1);
                }
            }
        }
        this.getSimulationConfigurationParameters();

        if (useMPI) {
            int base = (SimulationConfiguration.getJobId() % 319) * 100;
//            if ((base + 100) > 65535)
//                base = (base + 100) % 65535;
            SERVER_ID = base + MPI.COMM_WORLD.Rank();
        }

        if (SimulationConfiguration.isDebug()) {
            System.out.print("\nSERVER: " + this.SERVER_ID + " Individual (Vrep Evaluator): ");
            for (int i = 0; i < this.chromosomeDouble.length; i++) {
                System.out.print(this.chromosomeDouble[i] + ", ");
            }
        }
        int rank = 0;
        if (SimulationConfiguration.isUseMPI()) {
            rank = MPI.COMM_WORLD.Rank();
        }

    }

    public VrepEvaluator(double[] cromo) {
        this(cromo, "");
    }

    public VrepEvaluator(double[] cromo, String scene) {
        this(cromo, scene, 0.0);
    }

    public VrepEvaluator(double[] cromo, String scene, double fP) {

        init(cromo,scene,fP);
        VrepSimulator vrepSimulator = SimulationConfiguration.getVrep();
        if(vrepSimulator == null){
            System.out.println("vrep simulator is null, connecting to vrep...");
            vrepSimulator = new VrepSimulator();
            vrepSimulator.connect2Vrep();
            vrepApi = vrepSimulator.getVrepApi();
            SimulationConfiguration.setVrep(vrepSimulator);
        }

        vrepApi = vrepSimulator.getVrepApi();
        clientID = vrepSimulator.getClientID();

        robot = new VrepCreateRobot(vrepApi, clientID, chromosomeDouble, this.scene, this.fitnessParameter);
        robot.createRobot();

        controller = (RobotController) new SinusoidalController(vrepApi, clientID, robot);
        fitnessFunction = FitnessFunctionFactory.getFitnessFunction(vrepApi, clientID, robot);



    }

    public VrepEvaluator(double[] cromo, String scene, double fP, VrepSimulator simulator){
        init(cromo,scene,fP);

        vrepApi = simulator.getVrepApi();
        clientID = simulator.getClientID();

        robot = new VrepCreateRobot(vrepApi, clientID, chromosomeDouble, this.scene, this.fitnessParameter);
        robot.createRobot();

        controller = ControllerFactory.getRobotController(vrepApi,clientID,robot);
        fitnessFunction = FitnessFunctionFactory.getFitnessFunction(vrepApi, clientID, robot);

    }


    public double evaluate() {

        //TODO: Check Vrep server
        //TODO: Set Vrep GUI OFF or ON (if it is possible)
        //TODO: handle error in the evaluation  
        // enable the synchronous mode on the client:

        vrepApi.simxSynchronous(clientID, true);
        // start the simulation:
        vrepApi.simxStartSimulation(clientID, remoteApi.simx_opmode_oneshot_wait);

        this.fitnessFunction.init();

        // Now step a few times:
        int maxIter = (int) (maxSimulationTime / timeStep);
        for (int i = 0; i < maxIter; i++) {
            //Update joint position goals
            controller.updateJoints(time);

            //Send trigger signal for the next step
            int ret = vrepApi.simxSynchronousTrigger(clientID);
            if (ret != remoteApi.simx_error_noerror &&ret != remoteApi.simx_error_novalue_flag)
                System.out.println("Error triggering the simulation; error=" + ret);
            time += timeStep;

            this.fitnessFunction.update(time);
        }

        this.fitnessFunction.end();


        // stop the simulation:
        vrepApi.simxStopSimulation(clientID, remoteApi.simx_opmode_oneshot_wait);

        // Before closing the connection to V-REP, make sure that the last command sent out had time to arrive. You can guarantee this with (for example):
        IntW pingTime = new IntW(0);
        vrepApi.simxGetPingTime(clientID, pingTime);

        //Close the scene
        int iter = 0;
        int ret = vrepApi.simxCloseScene(clientID, remoteApi.simx_opmode_oneshot_wait);
        while(ret != remoteApi.simx_return_ok && iter < 100){
            ret = vrepApi.simxCloseScene(clientID, remoteApi.simx_opmode_oneshot_wait);
            iter++;
        }
        if(ret != remoteApi.simx_return_ok)
            System.err.println("The scene has not been closed after 100 trials.");




        return this.fitnessFunction.getFitness();

    }





    private void getSimulationConfigurationParameters() {

        try {

            this.SERVER_ID = SimulationConfiguration.getSeverId();
            this.maxSimulationTime = SimulationConfiguration.getMaxSimulationTime();
            this.useMPI = SimulationConfiguration.isUseMPI();
            this.timeStartSim = SimulationConfiguration.getTimeStartSim();
            this.waitTimeLoadGazebo = SimulationConfiguration.getWaitTimeLoadSimulator();
            this.nAttempts = SimulationConfiguration.getAttempts();

        } catch (Exception e) {
            LOGGER.severe("Error loading the control parameters of the simulation.");
            System.out.println(e);
            System.exit(-1);
        }

    }

    private double getInitialStepTime() {
        //TODO 
        return 0;
    }

    private void setStepTime(double timeStep) {
        this.timeStep = timeStep;
    }

    public void setUpdateRate(double uRate) {
    }

    public void getVrepPing(){
    IntW pingTime = new IntW(0);
    vrepApi.simxGetPingTime(clientID, pingTime);
    }

}
