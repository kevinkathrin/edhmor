/* 
 * EDHMOR - Evolutionary designer of heterogeneous modular robots
 * <https://bitbucket.org/afaina/edhmor>
 * Copyright (C) 2015 GII (UDC) and REAL (ITU)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package modules.evaluation;

/**
 *
 * @author fai
 */
public class Evaluator {
    GazeboEvaluator gazeboEvaluator = null;
    VrepEvaluator vrepEvaluator = null;
    boolean useGazeboSimualtor = true;


    public Evaluator(double[] cromo) {
        this(cromo, "");
    }

    public Evaluator(double[] cromo, String worldBase) {
        this(cromo, worldBase, 0.0);
    }

    public Evaluator(double[] cromo, String worldBase, double fP) {
        //TODO: Check Simulation properties       
        if(useGazeboSimualtor)
            gazeboEvaluator = new GazeboEvaluator(cromo, worldBase, fP);
        else
            vrepEvaluator = new VrepEvaluator(cromo, worldBase, fP);
    }

    public Evaluator(double[] cromo, String worldBase, double fP, boolean useGazeboSimualtor) {
        this.useGazeboSimualtor = useGazeboSimualtor;
        if(useGazeboSimualtor)
            gazeboEvaluator = new GazeboEvaluator(cromo, worldBase, fP);
        else
            vrepEvaluator = new VrepEvaluator(cromo, worldBase, fP);
    }


    public double evaluate() {
        if(useGazeboSimualtor)
            return gazeboEvaluator.evaluate();
        else
            return vrepEvaluator.evaluate();
    }


    public void setMaxSimulationTime(double numero) {
        if(useGazeboSimualtor)
            gazeboEvaluator.setMaxSimulationTime(numero);
        else
            vrepEvaluator.setMaxSimulationTime(numero);
    }

    public void setGuiOn(boolean guiOn) {
        if(useGazeboSimualtor)
            gazeboEvaluator.setGuiOn(guiOn);
        else
            vrepEvaluator.setGuiOn(guiOn);
    }

    public void setUpdateRate(double uRate) {
        if(useGazeboSimualtor)
            gazeboEvaluator.setUpdateRate(uRate);
        else
            vrepEvaluator.setUpdateRate(uRate);
    }


}
