/* 
 * EDHMOR - Evolutionary designer of heterogeneous modular robots
 * <https://bitbucket.org/afaina/edhmor>
 * Copyright (C) 2015 GII (UDC) and REAL (ITU)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package modules.evaluation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import modules.util.SimulationConfiguration;
import mpi.MPI;
import org.apache.commons.configuration.XMLConfiguration;

/**
 *Clase que evalua el cromosoma 
 * @author fai
 */
public class GazeboEvaluator {

    private double cromosomaDouble[];
    private double calidad;
    List<Double> calidadList = new ArrayList<Double>();
    List<Double> simulatinTimeList = new ArrayList<Double>();
    private int nIntentos = 10;
    private int nModules;
    private boolean useMPI = false;
    private boolean guiOn = true;
    static Logger LOGGER = Logger.getLogger("failogger");
    private int SERVER_ID = 0; // Numero del servidor gazebo
    private int N_CICLOS_SIM = 3;
    private double timeStartSim = 3;
    private int waitTimeLoadGazebo = 900;
    private String odeError = "ODE Message 3: LCP internal error";
    private String odeError2 = "ODE INTERNAL ERROR 1";
    private int nOdeError = 0;
    private int nOdeErrorMax = 15;
    private int status = -1;
    private double stepTime;
    private String worldStr = "world.xml",  worldBase = null;
    private String gazeboComand;
    private GazeboCreateRobot robot;
    private double fitnessParameter;

    public void setGuiOn(boolean guiOn) {
        this.guiOn = guiOn;
    }

    public void setMaxSimulationTime(double number) {
        this.N_CICLOS_SIM = (int) (number%(2*Math.PI));
    }

    public GazeboEvaluator(double[] cromo) {
        this(cromo, "");
    }

    public GazeboEvaluator(double[] cromo, String worldBase) {
        this(cromo, worldBase, 0.0);
    }

    public GazeboEvaluator(double[] cromo, String worldBase, double fP) {

        this.fitnessParameter = fP;

        this.worldBase = worldBase;
        if (worldBase == null || worldBase.isEmpty() || worldBase.equals("")) {
            this.worldBase = SimulationConfiguration.getWorldsBase().get(0);
        }

        this.fitnessParameter = SimulationConfiguration.getFitnessParameters().get(0);

        this.cromosomaDouble = cromo;
        if ((cromo.length + 3) % 6 == 0) {
            nModules = (cromo.length + 3) / 6;
        } else {
            if ((cromo.length + 3) % 5 == 0) {
                nModules = (cromo.length + 3) / 5;
            } else {
                System.err.println("Evaluador");
                System.err.println("Errol al calcular nModules; cromo.length=" + cromo.length);
                System.exit(-1);
            }
        }

        this.getSimulationConfiguration();


        if (useMPI) {
            int base = (SimulationConfiguration.getJobId() % 319) * 100;
//            if ((base + 100) > 65535)
//                base = (base + 100) % 65535;
            SERVER_ID = base + MPI.COMM_WORLD.Rank();
        }
        worldStr = "world" + SERVER_ID + ".xml";




        this.stepTime = this.getInitialStepTime();
        if (SimulationConfiguration.isDebug()) {
            System.out.print("\nSERVER: " + this.SERVER_ID + " Cromosoma (Evaluador): ");
            for (int i = 0; i < this.cromosomaDouble.length; i++) {
                System.out.print(this.cromosomaDouble[i] + ", ");
            }
        }


        robot = new GazeboCreateRobot(cromosomaDouble, this.worldBase, this.fitnessParameter);
        robot.createWorldXML(System.getProperty("java.io.tmpdir") +"/", worldStr);
    //System.out.println("World creado");

    }

    public double evaluate() {

        if (guiOn) {
            gazeboComand = "/soft/gazebo/bin/gazebo -ufs" + SERVER_ID + " " + worldStr;
        } else {
            gazeboComand = "/soft/gazebo/bin/gazebo -ugrfs" + SERVER_ID + " " + worldStr;
        }

        try {

            FileOutputStream archivo;
            PrintStream printDebug = null;
            archivo = new FileOutputStream("log" + SERVER_ID, true);
            printDebug = new PrintStream(archivo);

            for (int intento = 0; intento < nIntentos; intento++) {

                this.eliminaArchivosTemporalesDelGazebo();

                nOdeError = 0;

                //Si en la evaluacion anterior se dieron errores en la simulacion
                if (status == -3) {
                    switch (intento % 3) {
                        case 0:
                            this.stepTime = this.getInitialStepTime();
                            break;
                        case 1:
                            this.stepTime = this.getInitialStepTime() / 2;
                            break;
                        case 2:
                            this.stepTime = this.getInitialStepTime() / 4;
                            break;
                        /*case 3:
                        this.stepTime = this.getInitialStepTime() / 8;
                        break;
                        case 4:
                        this.stepTime = this.getInitialStepTime() / 16;
                        break;*/
                    }
                    if (intento > 2) {
                        if (this.worldBase.contains("Rugoso")) {
                            setCFM(0.04, worldStr);
                        } else {
                            setCFM(0.02, worldStr);
                        }
                    }
                    if (intento > 5) {
                        if (this.worldBase.contains("Rugoso")) {
                            setERP(0.45, worldStr);
                        } else {
                            setERP(0.5, worldStr);
                        }
                    }
                    if (intento > 8) {
                        if (this.worldBase.contains("Rugoso")) {
                            setCFM(0.05, worldStr);
                        } else {
                            setCFM(0.04, worldStr);
                        }
                    }
                    if (intento > 11) {
                        if (this.worldBase.contains("Rugoso")) {
                            setERP(0.4, worldStr);
                        } else {
                            setERP(0.45, worldStr);
                        }
                    }


                    //System.out.println(stepTime);
                    setStepTime(stepTime, worldStr);
                    //System.out.println("Hemos reducido el stepTime a: " + stepTime);
                    printDebug.println("Hemos reducido el stepTime a: " + stepTime);
                }


                Runtime obj = Runtime.getRuntime();
                Process proc = obj.exec(gazeboComand, null, new File(System.getProperty("java.io.tmpdir")));

                BufferedReader brStdOut = new BufferedReader(new InputStreamReader(proc.getInputStream()));
                BufferedReader brStdErr = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
                String strErr, strOut;

                boolean succesIni = false;
                String gazeboError = "";
                int creatingCount = 0, nRead = 0, nReadMax = 10000;
                while (!succesIni && nRead < nReadMax) {
                    while (brStdOut.ready()) {
                        strOut = brStdOut.readLine();
                        if (SimulationConfiguration.isDebug()) {
                            printDebug.println("SERVER: " + this.SERVER_ID + " GAZEBO OUT: " + strOut);
                        //System.out.println("SERVER: " + this.SERVER_ID + " GAZEBO OUT: " + strOut);
                        }
                        if (strOut.contains("creating")) {
                            creatingCount++;
                        }
                        if (strOut.contains("Gazebo successfully initialized")) {
                            succesIni = true;
                        }
                        nRead = 0;
                    }
                    Thread.sleep((waitTimeLoadGazebo));
                    nRead++;
                //System.out.println("leemos std::out " + creatingCount);
                }


                nRead = 0;
                //Thread.sleep((waitTimeLoadGazebo));
                while (brStdErr.ready() && nRead < nReadMax) {
                    strErr = brStdErr.readLine();
                    gazeboError += strErr;
                    if (SimulationConfiguration.isDebug()) {
                        //System.out.println("SERVER: " + this.SERVER_ID + " GAZEBO ERROR: " + strErr);
                        printDebug.println("SERVER: " + this.SERVER_ID + " GAZEBO ERROR: " + strErr);
                    }
                    if (strErr.contentEquals(odeError)) {
                        nOdeError++;
                    } else {
                        if (!strErr.isEmpty()) {
                            //System.out.println("SERVER: " + this.SERVER_ID + " GAZEBO ERROR: " + strErr);
                        }
                    }
                    nRead++;
                }
                if (succesIni) {
                    if (SimulationConfiguration.isDebug()) {
                        //System.out.println("SERVER: " + this.SERVER_ID + " numero de errores en ode al inicio: " + nOdeError);
                        printDebug.println("SERVER: " + this.SERVER_ID + " numero de errores en ode al inicio: " + nOdeError);
                    }
                } else {
                    //System.out.println("SERVER: " + this.SERVER_ID + " Gazebo no se ha arrancado correctamente");
                    //System.out.println(gazeboError);
                    System.err.println(gazeboError);
                    printDebug.println("SERVER: " + this.SERVER_ID + " Gazebo no se ha arrancado correctamente");
                    printDebug.println(gazeboError);
                }
                if (nOdeError < nOdeErrorMax && succesIni) {

                    try {
                        JNIControlModulos controlModulos = null;


                        System.gc();
                        controlModulos = new JNIControlModulos();
                        //System.out.println("Vamos a evaluar " );
                        //System.out.println("(Evaluador) FitnessFunctionIndicator: " + SimulationConfiguration.getFitnessFunction());
                        status = controlModulos.evaluaMorfologia(this.SERVER_ID,//Servidor
                                this.N_CICLOS_SIM, //Numero ciclos a simular
                                this.stepTime, //Step time de la simulacion
                                SimulationConfiguration.getPoseUpdateRate(),//Pose update Rate de la simulacion
                                robot.getN_modulos(), //Numero de modulos que empleamos
                                robot.getTipo_modulo(), //Tipo de modulos que usamos
                                robot.getAmplitudeControlOrgByTipo(),//Amplitud de la señal de control
                                robot.getAngularFreqControlOrgByTipo(),//Frecuencia angular de la señal de control
                                robot.getPhaseControlOrgByTipo(), //Phase de la señal de control
                                robot.getAmpliModOrgByTipo(),//AmplitudeModulation de la señal de control
                                robot.getFreqModOrgByTipo(), //FrequencyModulation de la señal de control
                                SimulationConfiguration.getFitnessFunction(), //Tipo de funcion a usar
                                fitnessParameter,//Parametro para la funcion de fitness
                                SimulationConfiguration.isUseAmplitudeControl(),// Si se realiza o no control de la amplitud
                                SimulationConfiguration.isUseAngularFControl(),// Si se realiza o no control de la frecuencia angular
                                SimulationConfiguration.isUsePhaseControl(),// Si se realiza o no control de la fase
                                SimulationConfiguration.isUseAmplitudeModulator(),// Si se realiza o no modulacion de la amplitud
                                SimulationConfiguration.isUseFrequencyModulator(),// Si se realiza o no modulacion de la frecuencia
                                this.guiOn);                                    //Si estamos mostrando la simulacion

                        nOdeError = 0;
                        nRead = 0;
                        while (brStdErr.ready() && nRead < nReadMax) {
                            strErr = brStdErr.readLine();
                            if (strErr.contentEquals(odeError)) {
                                nOdeError++;
                            } else {
                                if (strErr.contains(odeError2)) {
                                    //System.out.println("SERVER: " + this.SERVER_ID + " GAZEBO ERROR: " + strErr);
                                    printDebug.println("SERVER: " + this.SERVER_ID + " GAZEBO ERROR: " + strErr);
                                    status = -3;
                                } else {
                                    if (!strErr.isEmpty()) {
                                        //System.out.println("SERVER: " + this.SERVER_ID + " GAZEBO ERROR: " + strErr);
                                        printDebug.println("SERVER: " + this.SERVER_ID + " GAZEBO ERROR: " + strErr);
                                    }
                                }
                            }
                            nRead++;
                        }
                        if (nOdeError > this.nOdeErrorMax) {
                            status = -3;
                        }

                        if (SimulationConfiguration.isDebug()) {
                            //System.out.println("SERVER: " + this.SERVER_ID + " numero de errores en ode en la evaluacion: " + nOdeError);
                            printDebug.println("SERVER: " + this.SERVER_ID + " numero de errores en ode en la evaluacion: " + nOdeError);
                        }

                        //solo se ejecuta si no hubo errores en la simulacion
                        nRead = 0;
                        while (creatingCount > 0 && nRead < nReadMax && status >= 0) {
                            while (brStdOut.ready()) {
                                strOut = brStdOut.readLine();
                                if (SimulationConfiguration.isDebug()) {
                                    //System.out.println("GAZEBO OUT: " + strOut);
                                    printDebug.println("GAZEBO OUT: " + strOut);
                                }
                                if (strOut.contains("deleting")) {
                                    creatingCount--;
                                }
                                nRead = 0;
                            }
                            Thread.sleep((waitTimeLoadGazebo));
                            nRead++;
                        //System.out.println("leemos al finalizar std::out " + creatingCount);
                        }
                        brStdOut.close();
                        brStdErr.close();
                        proc.destroy();
                        proc.getInputStream().close();
                        proc = null;
                        eliminaArchivosTemporalesDelGazebo();

                        calidad = controlModulos.getCalidad();
                        //TODO: Comprobamos que no se ha pasado del maximo para evitar que
                        //explosiones de la fisica nos fastidien evoluciones
                        //System.out.println("Calidad: " + calidad + ";  SimulationConfiguration.getMaxFitnessValue(): " + SimulationConfiguration.getMaxFitnessValue());
                        if (calidad < SimulationConfiguration.getMaxFitnessValue()) {



                            if (SimulationConfiguration.isShareEnergy()) {
                                calidad /= robot.getN_modulos() * 0.1;
                            }
                            if (SimulationConfiguration.isImprovedFitness()) {
                                //System.out.println("VAMOS A MEJORAR EL FITNESS " + SimulationConfiguration.getThreshold() + "   " + SimulationConfiguration.getFitnessIncrease() + "   "+SimulationConfiguration.getMaxModules());
                                if (calidad > SimulationConfiguration.getThreshold()) {
                                    calidad += SimulationConfiguration.getFitnessIncrease() / 16 * (16 - robot.getN_modulos());
                                }
                            }

                            if (status >= 0) {

                                if (status == 10) {
                                    printDebug.println("SERVER: " + this.SERVER_ID +
                                            ", Nos hemos caido por las escaleras o nos hemos salido del camino" +
                                            ", world: " + this.worldBase +
                                            ", fitnessParameter: " + this.fitnessParameter);
                                    System.out.println("SERVER: " + this.SERVER_ID +
                                            ", Nos hemos caido por las escaleras o nos hemos salido del camino" +
                                            ", world: " + this.worldBase +
                                            ", fitnessParameter: " + this.fitnessParameter);
                                }
                                if (status == 20) {
                                    printDebug.println("SERVER: " + this.SERVER_ID +
                                            ", Se ha caido la carga" +
                                            ", world: " + this.worldBase +
                                            ", fitnessParameter: " + this.fitnessParameter);
                                    System.out.println("SERVER: " + this.SERVER_ID +
                                            ", Se ha caido la carga" +
                                            ", world: " + this.worldBase +
                                            ", fitnessParameter: " + this.fitnessParameter);
                                }

                                if (!Double.isNaN(calidad)) {
                                    printDebug.println("SERVER: " + this.SERVER_ID +
                                            ", Evaluacion satisfactoria al intento: " + intento +
                                            ", world: " + this.worldBase +
                                            ", fitnessParameter: " + this.fitnessParameter +
                                            ", timeStopSimulation: " + controlModulos.getTimeStopSimulation() +
                                            ", Calidad: " + calidad);
                                    //System.out.println("SERVER: " + this.SERVER_ID + " Evaluacion satisfactoria al intento: " + intento + ", Calidad: " + calidad);
                                    printDebug.flush();
                                    printDebug.close();

                                    return calidad;
                                } else {
                                    status = -2;
                                }
                            }
                            //System.out.println("SERVER: " + this.SERVER_ID + " Evaluacion NO satisfactoria al intento: " + intento + " status: " + status + "calidad: " + calidad);
                            printDebug.println("SERVER: " + this.SERVER_ID +
                                    ", Evaluacion NO satisfactoria al intento: " + intento +
                                    ", world: " + this.worldBase +
                                    ", fitnessParameter" + this.fitnessParameter +
                                    ", status: " + status +
                                    ", timeStopSimulation: " + controlModulos.getTimeStopSimulation() +
                                    ", calidad: " + calidad);

                            //Metemos calidad infinitamente mala y tiempo de simulacion cero (no se ha simulado o demasiados errores en ODE)
                            this.calidadList.add(calidad);
                            this.simulatinTimeList.add(controlModulos.getTimeStopSimulation());

                        } else {
                            //Se ha superado la calidad maxima => la fisica ha explotado
                            System.out.println("Se pasado de la calidad maxima, resimulamos");
                            System.out.println("calidad: " + calidad + ", SimulationConfiguration.getMaxFitnessValue(): " + SimulationConfiguration.getMaxFitnessValue());
                            status = -3;
                            //Metemos calidad infinitamente mala y tiempo de simulacion cero (no se ha simulado o demasiados errores en ODE)
                            this.calidadList.add(Double.NEGATIVE_INFINITY);
                            this.simulatinTimeList.add(0.0);

                        }




                    } catch (Exception ex) {
                        Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
                    }

                //Fin de inicio satisfactorio del gazebo
                } else {
                    status = -100;
                    if (nOdeError >= nOdeErrorMax) {
                        //System.out.println("SERVER: " + this.SERVER_ID + " Individuo que tiene demasiados errores en ODE: ");
                        printDebug.println("SERVER: " + this.SERVER_ID + " Individuo que tiene demasiados errores en ODE: ");
                        for (int ii = 0; ii < (nModules * 6 - 3); ii++) {
                            //System.out.print(cromosoma[ii] + ", ");
                            printDebug.print(cromosomaDouble[ii] + ", ");
                        }
                        //System.out.println("");
                        printDebug.println("");
                        status = -3;
                    }
                    if (!succesIni) {
                        status = -5;
                    }

                    brStdOut.close();
                    brStdErr.close();
                    proc.destroy();
                    proc.getInputStream().close();
                    proc = null;
                    printDebug.println("SERVER: " + this.SERVER_ID +
                            ", Evaluacion NO satisfactoria al intento: " + intento +
                            ", world: " + this.worldBase +
                            ", fitnessParameter: " + this.fitnessParameter +
                            ", status: " + status +
                            ", calidad: " + calidad);
                    //System.out.println("SERVER: " + this.SERVER_ID + " Evaluacion NO satisfactoria al intento: " + intento + " status: " + status + "calidad: " + calidad);
                    printDebug.flush();
                    eliminaArchivosTemporalesDelGazebo();

                    //Metemos calidad infinitamente mala y tiempo de simulacion cero (no se ha simulado o demasiados errores en ODE)
                    this.calidadList.add(Double.NEGATIVE_INFINITY);
                    this.simulatinTimeList.add(0.0);
                }

            //Thread.sleep(500 * intento);


            }

            System.err.println("SERVER: " + this.SERVER_ID + " Evaluacion NO consegida despues de " + nIntentos + " intentos");
            printDebug.println("SERVER: " + this.SERVER_ID + " Evaluacion NO consegida despues de " + nIntentos + " intentos");
            System.err.println("World: " + this.worldBase + ", fitnessParameter: " + this.fitnessParameter);
            printDebug.println("World: " + this.worldBase + ", fitnessParameter: " + this.fitnessParameter);
            System.err.println("SERVER: " + this.SERVER_ID + " Individuo: ");
            printDebug.println("SERVER: " + this.SERVER_ID + " Individuo: ");
            for (int i = 0; i < (nModules * 6 - 3); i++) {
                System.err.print(cromosomaDouble[i] + ", ");
                printDebug.print(cromosomaDouble[i] + ", ");
            }


            double calidadAproximada=(-1)*SimulationConfiguration.getMaxFitnessValue();
            double maxTime = 0;
            int indiceMaximo = 0;
            for (int hh = 0; hh < nIntentos; hh++) {
                if (this.simulatinTimeList.get(hh) > maxTime) {
                    maxTime = this.simulatinTimeList.get(hh);
                    indiceMaximo = hh;
                }
                printDebug.println("Intento: " + hh + ", calidad: " + this.calidadList.get(hh) + ", timeStopSimulation: " + this.simulatinTimeList.get(hh));
                System.out.println("Intento: " + hh + ", calidad: " + this.calidadList.get(hh) + ", timeStopSimulation: " + this.simulatinTimeList.get(hh));
            }
            if (this.simulatinTimeList.get(indiceMaximo) > (3.1415 * 3)) {
                calidadAproximada = this.calidadList.get(indiceMaximo);
                System.err.println("SERVER: " + this.SERVER_ID + " Consideramos que la calidad es " + calidadAproximada + ", obtenida despues de simular " + this.simulatinTimeList.get(indiceMaximo) + "segundos en el intento: " + indiceMaximo);
                printDebug.println("SERVER: " + this.SERVER_ID + " Consideramos que la calidad es " + calidadAproximada + ", obtenida despues de simular " + this.simulatinTimeList.get(indiceMaximo) + "segundos en el intento: " + indiceMaximo);
            } else {

                //Finalizado con errores si llega hasta aqui
                System.err.println("SERVER: " + this.SERVER_ID + "No hemos podido obtener una calidad aproximada, calidad -MaxFitnessValue");
                printDebug.println("SERVER: " + this.SERVER_ID + "No hemos podido obtener una calidad aproximada, calidad -MaxFitnessValue");
            }


            printDebug.flush();
            printDebug.close();
            System.err.println("SERVER: " + this.SERVER_ID + " Continuamos evaluando...");
            printDebug.println("SERVER: " + this.SERVER_ID + " Continuamos evaluando...");
            return calidadAproximada;


        } catch (InterruptedException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }


        System.err.println("Excepciones en la evaluacion: revisar");
        System.exit(-1);
        return -1;

    }

    private void getSimulationConfiguration() {

        try {

            this.SERVER_ID = SimulationConfiguration.getSeverId();
            this.N_CICLOS_SIM = (int) (SimulationConfiguration.getMaxSimulationTime()%(2*Math.PI));
            this.useMPI = SimulationConfiguration.isUseMPI();
            this.timeStartSim = SimulationConfiguration.getTimeStartSim();
            this.waitTimeLoadGazebo = SimulationConfiguration.getWaitTimeLoadSimulator();
            this.nIntentos = SimulationConfiguration.getAttempts();

        } catch (Exception e) {
            //error cargando los parametros del control de la simulacion
            LOGGER.severe("Error cargando los parametros del control de la simulacion.");
            System.out.println(e);
            System.exit(-1);
        }


    }

    private double getInitialStepTime() {

        String worldBaseAux = worldBase, worldBaseAux2 = worldBase;
        if (worldBase.contains("ConSoporte")) {
            //System.out.println("WorldBaseAux: " + worldBaseAux + ", WorldBaseAux2: "  + worldBaseAux2);
            worldBaseAux2 = worldBaseAux.replaceAll("ConSoporte", "");
        }
        //System.out.println("WorldBaseAux: " + worldBaseAux + ", WorldBaseAux2: "  + worldBaseAux2);
        try {
            XMLConfiguration config = new XMLConfiguration(System.getProperty("java.io.tmpdir") + "/worlds/" + worldBaseAux2);
            double stepT = config.getDouble("physics:ode.stepTime");
            return stepT;
        } catch (Exception e) {
            //error cargando el stepTime de la simulacion
            LOGGER.severe("Error leyendo el stepTime de la simulacion.");
            System.out.println(e);
            System.exit(-1);
            return 0;
        }
    }

    private void setStepTime(double stepTime, String worldStr) {

        try {
            XMLConfiguration config = new XMLConfiguration(System.getProperty("java.io.tmpdir") + "/" + worldStr);
            config.setProperty("physics:ode.stepTime", stepTime);
            config.save();
        } catch (Exception e) {
            //error escribiendo el stepTime de la simulacion
            LOGGER.severe("Error escribiendo el stepTime de la simulacion.");
            System.out.println(e);
            System.exit(-1);
        }
    }

    private void setCFM(double cfm, String worldStr) {

        try {
            XMLConfiguration config = new XMLConfiguration(System.getProperty("java.io.tmpdir") + "/" + worldStr);
            config.setProperty("physics:ode.cfm", cfm);
            config.save();
            System.out.println("CFM cambiado a: " + cfm);
        } catch (Exception e) {
            //error escribiendo el stepTime de la simulacion
            LOGGER.severe("Error escribiendo el CFM de la simulacion.");
            System.out.println(e);
            System.exit(-1);
        }
    }

    private void setERP(double erp, String worldStr) {

        try {
            XMLConfiguration config = new XMLConfiguration(System.getProperty("java.io.tmpdir") + "/" + worldStr);
            config.setProperty("physics:ode.erp", erp);
            config.save();
            System.out.println("ERP cambiado a: " + erp);
        } catch (Exception e) {
            //error escribiendo el stepTime de la simulacion
            LOGGER.severe("Error escribiendo el ERP de la simulacion.");
            System.out.println(e);
            System.exit(-1);
        }
    }

    public void setUpdateRate(double uRate) {

        try {
            XMLConfiguration config = new XMLConfiguration(System.getProperty("java.io.tmpdir") + "/" + worldStr);
            config.setProperty("physics:ode.updateRate", uRate);
            config.save();
            System.out.println("Update Rate cambiado a: " + uRate);
        } catch (Exception e) {
            //error escribiendo el stepTime de la simulacion
            LOGGER.severe("Error escribiendo el Update Rate de la simulacion.");
            System.out.println(e);
            System.exit(-1);
        }
    }

    private void eliminaArchivosTemporalesDelGazebo() throws IOException, InterruptedException {

        //Process pp=Runtime.getRuntime().exec ("rm -rf gazebo.log");
        Process p = Runtime.getRuntime().exec("rm -rf " + System.getProperty("java.io.tmpdir") +"/gazebo-failiz-" + SERVER_ID);
        p.waitFor();
        BufferedReader errStr = new BufferedReader(new InputStreamReader(p.getErrorStream()));
        if (errStr.ready()) {
            System.out.println("Error al eliminar el archivo temporal del gazebo:" + errStr.readLine());
        }
        errStr.close();
        p.getInputStream().close();
        p.getOutputStream().close();
        p.destroy();
        p = null;
    }
}
