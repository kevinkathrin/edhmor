/* 
 * EDHMOR - Evolutionary designer of heterogeneous modular robots
 * <https://bitbucket.org/afaina/edhmor>
 * Copyright (C) 2015 GII (UDC) and REAL (ITU)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package modules.jeaf.operation.decrease;

import es.udc.gii.common.eaf.exception.ConfigurationException;
import modules.individual.Node;
import modules.individual.TreeIndividual;
import org.apache.commons.configuration.Configuration;

/**
 *
 * @author fai
 */
public class DeleteNode extends DecreaseMutationOperation {

    private static boolean useWorstNode = false;
    private static boolean useRandomNode = false;

    @Override
    public void run(TreeIndividual arbol) {

        //Elegimos el nodo a eliminar de entre aquellos que no tienen hijos
        Node toRemove = null;
        if (useWorstNode) {
            toRemove = arbol.getWorstNodeWithoutChildrens();
        } else {
            if (useWorstNode) {
                toRemove = arbol.getWorstNodeWithoutChildrens();
            }else{
                System.err.println("No tiene ningun metodo de seleccion del nodo a eliminar");
                System.exit(-1);
            }
        }

        //solo ejecutamos si el nodo tiene un padre (caso de que solo exista un módulo)
        if (toRemove.getDad() != null) {

            this.setIsWorking(true);

            toRemove.setIsOperationalActive(true);

            //Hay que poner el arbol como "padre" antes de modificarlo
            this.setFatherIndividual(arbol);

            //eliminar el nodo
            this.removeBranch(toRemove);

            //actualizamos al individuo
            arbol.modifyChromosome();
        } else {
            this.setIsWorking(false);
        }

    }

    @Override
    public void repair(TreeIndividual arbol) {
        double diferenciaFitness = arbol.getFitness() - arbol.getFatherFitness();
        //Eliminar el nodo aumento el fitness
        if (arbol.getFitness() >= (arbol.getFatherFitness() /* * (1 - this.incrementoSignificativo)*/ )) {
            //TODO: Llamar a ¿¿actualizar los valores de fitnessContribution de todos los modulos??
            arbol.getRootNode().addFitnessContribution(0);
        } else {

            //TODO: Llamar a actualizar los valores de fitnessContribution de los moduloseliminados, borrar los isActiveContribution

            //DEBUG:
            if (arbol.getFatherRootNode() == null) {
                System.err.print("Vamos a poner rootNode igual a null xq fatherRN es null");
                System.err.print("fitness: " + arbol.getFitness() + "FatherFitness: " + arbol.getFatherFitness());
                System.err.println("Arbol: " + arbol.toString());
            }
            arbol.setRootNode(arbol.getFatherRootNode());
            arbol.setFitness(arbol.getFatherFitness());

            //Aumentamos el fitnessContribution
            arbol.getRootNode().addFitnessContribution((-1) * diferenciaFitness);

            //actualizamos al individuo
            arbol.modifyChromosome();

        }
        this.removeFatherIndividual(arbol);
    }

    @Override
    public boolean isMandatory() {
        return false;
    }

    @Override
    public String toString() {
        String str = "DeleteNode";
        if(this.isWorking())
            str += "   (isWorking)";
        else
            str += "   (NOT Working)";
        return str;
    }

    public void configure(Configuration conf) {
        useWorstNode = conf.containsKey("UseWorstNode");
        useRandomNode = conf.containsKey("UseRandomNode");
        int count = 0;
        if(useWorstNode)
            count++;
        if(useRandomNode)
            count++;

        if(count != 1)
            throw new ConfigurationException("No se puede configurar Delete Node:\n" +
                    "useWorstNode: " + useWorstNode +
                    "\nuseRandomNode: " + useRandomNode);

    }
}
