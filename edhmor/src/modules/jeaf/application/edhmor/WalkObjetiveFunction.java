/* 
 * EDHMOR - Evolutionary designer of heterogeneous modular robots
 * <https://bitbucket.org/afaina/edhmor>
 * Copyright (C) 2015 GII (UDC) and REAL (ITU)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package modules.jeaf.application.edhmor;

//import es.udc.gii.common.eaf.algorithm.fitness.ObjectiveFunction;
import es.udc.gii.common.eaf.problem.objective.ObjectiveFunction;
import java.util.List;
import modules.evaluation.GazeboEvaluator;
import modules.evaluation.VrepEvaluator;
import modules.util.SimulationConfiguration;
import mpi.MPI;

/**
 *
 * @author fai
 */
public class WalkObjetiveFunction extends ObjectiveFunction {

    public WalkObjetiveFunction() {
    }
    private int nEval = 0;

    public double evaluate(double[] values) {


        double fitness = 0.0;
        double worldFitness = 0.0;
        List<String> worldsBase = SimulationConfiguration.getWorldsBase();
        List<Double> fitnessParameters = SimulationConfiguration.getFitnessParameters();
        double minFitness = Double.MAX_VALUE, meanFitness = 0;
        int evaluations = 0;
        for (String world : worldsBase) {
            for (Double fitnessParameter : fitnessParameters) {
                VrepEvaluator decodificador = new VrepEvaluator(values, world, fitnessParameter);
                decodificador.setGuiOn(false);
                worldFitness = decodificador.evaluate();
                System.out.println("Evaluation of the world " + world + ", with fitnessParameter: " + fitnessParameter);
                evaluations++;
                if (worldFitness < minFitness) {
                    minFitness = worldFitness;
                }
                meanFitness += worldFitness;
            }
        }
        this.nEval++;

        if (worldsBase.size() > 1 || fitnessParameters.size() > 1) {
            String functionToEvaluateWorlds = SimulationConfiguration.getFunctionToEvaluateWorlds();
            if (functionToEvaluateWorlds.contains("min")) {
                fitness = minFitness;
            } else {
                if (functionToEvaluateWorlds.contains("mean")) {
                    fitness = meanFitness / evaluations;
                } else {
                    String str = "WalkObjetiveFunction: Error, there are more "
                            + "than world or parameters but the function to "
                            + "calculate the final fitness is not defined "
                            + "(minimum or mean)." +
                            "\nfunctionToEvaluateWorlds:  " + functionToEvaluateWorlds;
                    System.out.println(str);
                    System.err.println(str);
                    System.exit(-1);
                }
            }
        } else {
            fitness = worldFitness;
        }

        return fitness;
    }

    public void reset() {
    }
}
