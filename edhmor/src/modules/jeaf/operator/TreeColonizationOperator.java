/* 
 * EDHMOR - Evolutionary designer of heterogeneous modular robots
 * <https://bitbucket.org/afaina/edhmor>
 * Copyright (C) 2015 GII (UDC) and REAL (ITU)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package modules.jeaf.operator;

import es.udc.gii.common.eaf.algorithm.EvolutionaryAlgorithm;
import es.udc.gii.common.eaf.algorithm.operator.reproduction.ColonizationOperator;
import es.udc.gii.common.eaf.algorithm.population.Individual;
import es.udc.gii.common.eaf.algorithm.population.MaIndividual;
import es.udc.gii.common.eaf.exception.OperatorException;
import es.udc.gii.common.eaf.util.EAFRandom;
import java.util.ArrayList;
import java.util.List;
import modules.individual.TreeIndividual;
import modules.util.SimulationConfiguration;

/**
 *
 * @author fai
 */
public class TreeColonizationOperator extends ColonizationOperator {

//    @Override
//    public List<Individual> operate(EvolutionaryAlgorithm algorithm,
//            List<Individual> individuals) throws OperatorException {
//        List<Individual> survivors = new ArrayList<Individual>(individuals.size() / 2);
//        List<Individual> extincts = new ArrayList<Individual>(individuals.size() / 2);
//        double[] survivorChrom, extinctChrom;
//        double tauValue = this.getTau().get(algorithm);
//        double rhoValue = this.getRho().get(algorithm);
//        double lambda;
//
//        for (Individual individual : individuals) {
//            if (((MaIndividual) individual).isSurvivor()) {
//                survivors.add(individual);
//            } else {
//                extincts.add(individual);
//            }
//        }
//        for (Individual extinct : extincts) {
//            if (EAFRandom.nextDouble() > tauValue) {
//                extinctChrom = extinct.getChromosomeAt(0);
//                survivorChrom = this.getChooser().get(algorithm, survivors, extinct).getChromosomeAt(0);
//
//                TreeIndividual extinctTree = (TreeIndividual) extinct;
//                TreeIndividual survivorTree = (TreeIndividual) this.getChooser().get(algorithm, survivors, extinct);
//
//                System.out.println("OPERADOR DE COLONIZACION");
//                System.out.println("survivor: " + survivorTree.toString());
//                System.out.println("extinct: " + extinctTree.toString());
//
//                lambda = EAFRandom.nextDouble();
//                int dadNodes = (int) (lambda * rhoValue * (extinctTree.getMaxModules() - 1));
//
//                TreeIndividual extinctPart = null;
//                boolean aumentamosArbol=false;
//
//                if (dadNodes > 0) {
//                    for (int i = dadNodes; i > 0; i--) {
//                        if (survivorTree.isSubTree(i) && extinctTree.isSubTree(i) && (survivorTree.getRootNode().getNModulesSubTree() > i)) {
//                            dadNodes = i;
//                            System.out.println("dadNodes: " + dadNodes + ". El arbol no aumenta.");
//                            break;
//                        } else {
//                            if (survivorTree.isSubTree(i - 1) && extinctTree.isSubTree(i) && (survivorTree.getRootNode().getNModulesSubTree() > (i - 1))) {
//                                dadNodes = i;
//                                aumentamosArbol=true;
//                                System.out.println("dadNodes: " + dadNodes + ". El arbol aumenta en uno.");
//                                break;
//                            }
//                        }
//                    }
//
//
//
//                    if (dadNodes > 0 && survivorTree.getRootNode().getNModulesSubTree() > 1) {
//                        extinctPart = extinctTree.generateLowSubTree(dadNodes).clone();
//                        System.out.println("lowSubTree: " + extinctPart.toString());
//                        //Si la parte del que cruzamos no es una base, cruzamos
//                        if (extinctPart.getRootNode().getType() > 0) {
//                            System.out.println("antes de unir y de producir el resultante");
//                            if(aumentamosArbol){
//                                extinctTree = survivorTree.jointTrees(dadNodes-1, extinctPart.getRootNode());
//                            }else{
//                                extinctTree = survivorTree.jointTrees(dadNodes, extinctPart.getRootNode());
//                            }
//                            System.out.println("resultante: " + extinctTree.toString());
//                        } else {
//                            System.out.println("antes de generar el individuo aleatorio");
//                            extinctTree.generate();
//                            System.out.println("No se cruza porque lowSubTree contiene una base, se regenera aleatoriamente");
//                        }
//                    }
//                }
//
//            /*
//            for (int j = 0; j < extinctChrom.length; j++)
//            {
//            lambda = EAFRandom.nextDouble() * 2 - 1;
//            extinctChrom[j] = checkBounds(algorithm,
//            survivorChrom[j] + rhoValue * lambda * (extinctChrom[j] - survivorChrom[j]));
//            }*/
//            } else {
//                extinct.generate();
//            }
//        }
//        return individuals;
//    }
}
