/* 
 * EDHMOR - Evolutionary designer of heterogeneous modular robots
 * <https://bitbucket.org/afaina/edhmor>
 * Copyright (C) 2015 GII (UDC) and REAL (ITU)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package modules.jeaf.operator;

import es.udc.gii.common.eaf.algorithm.EvolutionaryAlgorithm;
import es.udc.gii.common.eaf.algorithm.population.Individual;
import es.udc.gii.common.eaf.plugin.evaluation.IndividualImprover;
import java.util.ArrayList;
import java.util.List;
import modules.jeaf.operation.decrease.DeleteNode;
import modules.jeaf.operation.grow.AddNode;
import modules.jeaf.operation.morphological.ShakingModule;
import modules.individual.TreeIndividual;
import org.apache.commons.configuration.Configuration;

/**
 *
 * @author fai
 */
public class SpecialShakingOperator implements IndividualImprover {

    private int nEvalControl = 1;
    private int nEvalLocalFacesAndControl = 1;
    private double pMutation = 0.2;

    public Improvement improve(EvolutionaryAlgorithm alg, Individual seed) {
        List<Individual> variacionesIndividuo = new ArrayList();

        TreeIndividual a = (TreeIndividual) seed;
        if (a.getOperation() instanceof AddNode) {
            AddNode addNodeOperation = (AddNode) a.getOperation();
            if (addNodeOperation.isWorking()) {
                variacionesIndividuo = this.shakingLocalFacesAndControl(seed);
            } else {
                variacionesIndividuo = this.shakingControl(seed);
            }
        } else {
            if (a.getOperation() instanceof DeleteNode) {
                variacionesIndividuo = this.shakingControl(seed);
            } else {

                if (a.getOperation() instanceof ShakingModule) {
                    variacionesIndividuo = this.shakingLocalFacesAndControl(seed);
                } else {
                    if (a.getOperation() == null) {
                        variacionesIndividuo = this.shakingControl(seed);
                    }
                }

            }
        }

        return new Improvement(variacionesIndividuo, 0);
    }

    private List<Individual> shakingControl(Individual ind) {
        List<Individual> variaciones = new ArrayList();
        for (int i = 0; i < nEvalControl; i++) {
            TreeIndividual tree = (TreeIndividual) ind.clone();
            tree.shakeControl(pMutation);
            //TODO: Comprobar que no sean iguales los distintos cromosomas???
            variaciones.add(tree);
        }
        return variaciones;
    }

    private List<Individual> shakingLocalFacesAndControl(Individual ind) {
        List<Individual> variaciones = new ArrayList();
        for (int i = 0; i < nEvalLocalFacesAndControl; i++) {
            TreeIndividual tree = (TreeIndividual) ind.clone();
            tree.shakeDadFaceAndOrientation();
            //TODO: Comprobar que no sean iguales los distintos cromosomas???
            variaciones.add(tree);
        }
        return variaciones;
    }

    public void configure(Configuration conf) {
        this.nEvalControl = conf.getInt("NEvalControl");
        this.nEvalLocalFacesAndControl = conf.getInt("NEvalLocalFacesAndControl");
        this.pMutation = conf.getDouble("PMutation");

    }

    public boolean doesEvaluate() {
        return false;
    }
}
