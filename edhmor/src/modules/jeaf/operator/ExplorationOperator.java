/* 
 * EDHMOR - Evolutionary designer of heterogeneous modular robots
 * <https://bitbucket.org/afaina/edhmor>
 * Copyright (C) 2015 GII (UDC) and REAL (ITU)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package modules.jeaf.operator;

import es.udc.gii.common.eaf.algorithm.EvolutionaryAlgorithm;
import es.udc.gii.common.eaf.algorithm.population.Individual;
import es.udc.gii.common.eaf.plugin.evaluation.IndividualImprover;
import java.util.ArrayList;
import java.util.List;
import modules.evaluation.overlapping.CollisionDetector;
import modules.evaluation.overlapping.CollisionDetectorFactory;
import modules.jeaf.operation.decrease.DeleteAllNodes;
import modules.jeaf.operation.grow.AddNode;
import modules.jeaf.operation.morphological.ShakingControl;
import modules.jeaf.operation.morphological.ShakingModule;
import modules.individual.TreeIndividual;
import org.apache.commons.configuration.Configuration;

/**
 *
 * @author fai
 */
public class ExplorationOperator implements IndividualImprover{

    private int nEvalAddNode = 5;
    private int nEvalAdaptMorf = 5;
    private int nEvalAdaptControl = 5;


    public boolean doesEvaluate() {
        return false;
    }

    public Improvement improve(EvolutionaryAlgorithm alg, Individual seed) {

        List<Individual> variacionesIndividuo = new ArrayList();

        TreeIndividual a = (TreeIndividual) seed;
        if (a.getOperation() instanceof AddNode) {
            AddNode addNodeOperation = (AddNode) a.getOperation();
            if (addNodeOperation.isWorking()) {
                variacionesIndividuo = this.addNodeVariations(seed);
            }
        }

        if (a.getOperation() instanceof ShakingModule) {
            ShakingModule shakingModuleOperation = (ShakingModule) a.getOperation();
            if (shakingModuleOperation.isWorking()) {
                variacionesIndividuo = this.shakingModuleOperationVariations(seed);
            }
        }

        if (a.getOperation() instanceof ShakingControl) {
            ShakingControl shakingControlOperation = (ShakingControl) a.getOperation();
            if (shakingControlOperation.isWorking()) {
                variacionesIndividuo = this.shakingControlOperationVariations(seed, shakingControlOperation.getPMutationControl());
            }
        }

        if (a.getOperation() instanceof DeleteAllNodes) {
            DeleteAllNodes deleteAllNodesOperation = (DeleteAllNodes) a.getOperation();
            if (deleteAllNodesOperation.isWorking()) {
                variacionesIndividuo = deleteAllNodesOperation.generateCandidates(seed);
            }
        }


        return new Improvement(variacionesIndividuo, 0);
    }

        private List<Individual> shakingControlOperationVariations(Individual ind, double pMutationControl) {
        List<Individual> variaciones = new ArrayList();
        for (int i = 0; i < nEvalAdaptControl; i++) {
            TreeIndividual tree = (TreeIndividual) ind.clone();
            tree.shakeControl(pMutationControl);
            //TODO: Comprobar que no sean iguales los distintos cromosomas???
            variaciones.add(tree);
        }
        return variaciones;
    }

    private List<Individual> addNodeVariations(Individual ind) {
        List<Individual> variaciones = new ArrayList();
        for (int i = 0; i < nEvalAddNode; i++) {
            CollisionDetector collisionDetector = CollisionDetectorFactory.getCollisionDetector();
            TreeIndividual tree = (TreeIndividual) ind.clone();
            tree.shakeDadFaceAndOrientation();
            tree.modifyChromosome();
            collisionDetector.loadTree(tree);
            if (collisionDetector.isFeasible()) {
                
                 //TODO: Comprobar que no sean iguales los distintos cromosomas???
                variaciones.add(tree);
            }
        }
        return variaciones;
    }

    private List<Individual> shakingModuleOperationVariations(Individual ind) {
        List<Individual> variaciones = new ArrayList();
        
        for (int i = 0; i < nEvalAdaptMorf; i++) {
            CollisionDetector collisionDetector = CollisionDetectorFactory.getCollisionDetector();
            TreeIndividual tree = (TreeIndividual) ind.clone();
            tree.shakeDadFaceAndOrientation();
            tree.modifyChromosome();
            collisionDetector.loadTree(tree);
            if (collisionDetector.isFeasible()) {
                
                 //TODO: Comprobar que no sean iguales los distintos cromosomas???
                variaciones.add(tree);
                
            }
        }
        return variaciones;
    }

    public void configure(Configuration conf) {
        this.nEvalAddNode = conf.getInt("nEvalAddNode");
        this.nEvalAdaptMorf = conf.getInt("nEvalAdaptMorf");
        this.nEvalAdaptControl = conf.getInt("nEvalAdaptControl");
    }

}
