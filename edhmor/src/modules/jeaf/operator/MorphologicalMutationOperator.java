/* 
 * EDHMOR - Evolutionary designer of heterogeneous modular robots
 * <https://bitbucket.org/afaina/edhmor>
 * Copyright (C) 2015 GII (UDC) and REAL (ITU)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package modules.jeaf.operator;

import es.udc.gii.common.eaf.algorithm.EvolutionaryAlgorithm;
import es.udc.gii.common.eaf.algorithm.fitness.FitnessUtil;
import es.udc.gii.common.eaf.algorithm.operator.reproduction.mutation.MutationOperator;
import es.udc.gii.common.eaf.algorithm.population.Individual;
import es.udc.gii.common.eaf.exception.ConfigurationException;
import es.udc.gii.common.eaf.util.EAFRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import modules.jeaf.operation.MutationOperation;
import modules.jeaf.operation.decrease.DeleteAllNodes;
import modules.jeaf.operation.decrease.DeleteBranch;
import modules.jeaf.operation.decrease.DeleteNode;
import modules.jeaf.operation.decrease.DeleteNodesWithLowFitnessContribution;
import modules.jeaf.operation.grow.AddNode;
import modules.jeaf.operation.morphological.ChangeOrientation;
import modules.jeaf.operation.morphological.ChangePosition;
import modules.jeaf.operation.morphological.ShakingControl;
import modules.jeaf.operation.morphological.ShakingModule;
import modules.individual.TreeIndividual;
import modules.util.exceptions.InconsistentDataException;
import org.apache.commons.configuration.Configuration;

/**
 *
 * @author fai
 */
public class MorphologicalMutationOperator extends MutationOperator {

    private double[] probabilities;
    private int maxIterSinMejorar = 9;
    private int nAddNode = 3;
    private int nAdaptMorf = 2;
    private int nAdaptContr = 1;
    private int nPoda = 1;
    private int nCiclosExplor = nAddNode + nAdaptMorf + nAdaptContr + nPoda;
    private boolean resetToBest = false;


//    private MutationOperation addNodeOrShakingModule(TreeIndividual tree) {
//        MutationOperation op = null;
//        int nodosArbol = tree.getRootNode().getNumberModulesBranch();
//        int nodosMaximos = tree.getMaxModules();
//        int nivel = (int) Math.floor((tree.getIterSinMejorar() / 2) + 1);
//        if (nodosArbol == nodosMaximos) {
//            op = new ShakingModule(nivel);
//        } else {
//            double suerte = EAFRandom.nextFloat();
//            if (suerte < 0.4) {
//                op = new AddNode();
//            } else {
//                op = new ShakingModule(nivel);
//            }
//        }
//        return op;
//    }
    @Override
    protected List<Individual> mutation(EvolutionaryAlgorithm algorithm, Individual individual) {

        int generacion = algorithm.getGenerations();
        int iter = generacion % nCiclosExplor;


        //TODO: Implementar las distintas probabilidades de mutacion
        //Calcular probabilidades a partir de los datos anteriores

        TreeIndividual arbolInd = (TreeIndividual) individual;

        if (arbolInd.getBestFitness() < arbolInd.getFitness()) {
            arbolInd.setIterWithoutImprovement(0);
            try {
                arbolInd.setBestRootNode(arbolInd.getRootNode().clone());
            } catch (CloneNotSupportedException ex) {
                Logger.getLogger(MorphologicalMutationOperator.class.getName()).log(Level.SEVERE, null, ex);
            }
            arbolInd.setBestFitness(arbolInd.getFitness());
        } else {
            arbolInd.setIterWithoutImprovement(arbolInd.getIterWithoutImprovement() + 1);
        }

        if(iter == 0 && this.resetToBest){
            try {
                arbolInd.setRootNode(arbolInd.getBestRootNode().clone());
                arbolInd.setFitness(arbolInd.getBestFitness());
            } catch (CloneNotSupportedException ex) {
                Logger.getLogger(MorphologicalMutationOperator.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        //Crear la operacion a realizar segun toque
        MutationOperation operation = null;

        if (iter < nAddNode) {
            operation = new AddNode();
        } else {
            if (iter < (nAddNode + nAdaptMorf)) {
                operation = new ShakingModule();
            } else {
                if (iter < (nAddNode + nAdaptMorf + nAdaptContr)) {
                    operation = new ShakingControl();
                } else {
                    if (iter < (nAddNode + nAdaptMorf + nAdaptContr + nPoda)) {
                        operation = new DeleteAllNodes();
                    } else {
                        try {
                            throw new InconsistentDataException("Nos hemos pasado de los ciclos maximos de añadir, adaptar y podar");
                        } catch (InconsistentDataException ex) {
                            Logger.getLogger(MorphologicalMutationOperator.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }

            }
        }



//        if (arbolInd.getBestFitness() < arbolInd.getFitness()) {
//            if ((1 + MutationOperation.getIncrementoSignificativo()) * arbolInd.getBestFitness() < arbolInd.getFitness()) {
//                arbolInd.setIterSinMejorar(0);
//            }
//            arbolInd.setBestFitness(arbolInd.getFitness());
//        } else {
//            arbolInd.setIterSinMejorar(arbolInd.getIterSinMejorar() + 1);
//        }
//
//        if (arbolInd.getIterSinMejorar() < 3) {
//            operation = addNodeOrShakingModule(arbolInd);
//        } else {
//            if (arbolInd.getIterSinMejorar() < 4) {
//                operation = new DeleteNodesWithLowFitnessContribution();
//            } else {
//                if (arbolInd.getIterSinMejorar() < 7) {
//                    operation = addNodeOrShakingModule(arbolInd);
//                } else {
//                    if (arbolInd.getIterSinMejorar() < 8) {
//                        operation = new DeleteNodesWithLowFitnessContribution();
//                    } else {
//                        if (arbolInd.getIterSinMejorar() < 9) {
//                            operation = addNodeOrShakingModule(arbolInd);
//                        } else {
//                            double mediaPoblacion = FitnessUtil.meanFitnessValue(algorithm.getPopulation().getIndividuals());
//                            if (arbolInd.getFitness() < mediaPoblacion) {
//                                arbolInd.generate();
//                                arbolInd.setBestFitness(0);
//                                arbolInd.setIterSinMejorar(0);
//                                operation = null;
//                            } else {
//                                if ((arbolInd.getIterSinMejorar() - 9) % 4 == 0) {
//                                    //operation = new DeleteNodesWithLowFitnessContribution(); //v1.1
//                                    operation = new DeleteBranch();   //v1.2
//                                } else {
//                                    operation = addNodeOrShakingModule(arbolInd);
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }






        arbolInd.setOperation(operation);



        //Llamar a ejecutar la mutacion
        if (operation != null) {
            operation.run(arbolInd);
        }



        List<Individual> mutated_individual = new ArrayList<Individual>();
        mutated_individual.add(arbolInd);
        return mutated_individual;


    }

    @Override
    public void configure(Configuration conf) {
        super.configure(conf);

        this.nAddNode = conf.getInt("nAddNode");
        this.nAdaptMorf = conf.getInt("nAdaptMorf");
        this.nAdaptContr = conf.getInt("nAdaptContr");
        this.nPoda = conf.getInt("nPoda");
        nCiclosExplor = nAddNode + nAdaptMorf + nAdaptContr + nPoda;

        this.resetToBest = conf.containsKey("resetToBest");

        List ops = conf.getList("Operation.Class");
        for (int i = 0; i < ops.size(); i++) {
            try {
                MutationOperation mo = (MutationOperation) Class.forName((String) ops.get(i)).newInstance();
                mo.configure(conf.subset("Operation(" + i + ")"));
            } catch (Exception ex) {
                throw new ConfigurationException(
                        "Wrong reproduction operator configuration for " + (String) ops.get(i) + " ?" + " \n Thrown exception: \n" + ex);
            }

        }
    }
}
