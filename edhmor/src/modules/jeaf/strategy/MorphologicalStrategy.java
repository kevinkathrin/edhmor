/* 
 * EDHMOR - Evolutionary designer of heterogeneous modular robots
 * <https://bitbucket.org/afaina/edhmor>
 * Copyright (C) 2015 GII (UDC) and REAL (ITU)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package modules.jeaf.strategy;

import es.udc.gii.common.eaf.algorithm.evaluate.SerialEvaluationStrategy;
import es.udc.gii.common.eaf.algorithm.population.Individual;
import es.udc.gii.common.eaf.problem.constraint.Constraint;
import es.udc.gii.common.eaf.problem.objective.ObjectiveFunction;
import java.util.List;
import modules.jeaf.operation.MutationOperation;
import modules.individual.TreeIndividual;
import org.apache.commons.configuration.Configuration;

/**
 *
 * @author fai
 
public class MorphologicalStrategy extends SerialEvaluationStrategy{

    @Override
    public void configure(Configuration conf) {
        super.configure(conf);
    }

    @Override
    public void evaluate(List<Individual> individuals, List<ObjectiveFunction> functions, List<Constraint> constraints) {
        super.evaluate(individuals, functions, constraints);

//        //Reparar a los individuos
        for(int i=0; i<individuals.size(); i++){
            TreeIndividual arbol = (TreeIndividual) individuals.get(i);
            MutationOperation operation =arbol.getOperation();
            if(operation != null)
                arbol.getOperation().reparar(arbol);
        }
    }

    @Override
    public void evaluate(Individual individual, List<ObjectiveFunction> functions, List<Constraint> constraints) {
        super.evaluate(individual, functions, constraints);

        //Reparar el individuo
//        TreeIndividual arbol = (TreeIndividual) individual;
//        MutationOperation operation = arbol.getOperation();
//        if(operation != null)
//            operation.reparar(arbol);

    }



}
 *
 * */
