/* 
 * EDHMOR - Evolutionary designer of heterogeneous modular robots
 * <https://bitbucket.org/afaina/edhmor>
 * Copyright (C) 2015 GII (UDC) and REAL (ITU)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package modules.test.operations;

import es.udc.gii.common.eaf.util.EAFRandom;

/**
 *
 * @author fai
 */
public class NewClass {

    public static void main(String[] args) {
        EAFRandom.init();

        int nOperations = 3;
        double suerte = EAFRandom.nextDouble();
        System.out.println("Suerte: " + suerte);
        if (nOperations > 1) {
            int i = 0;
            double aux = ((1 / nOperations) * (i + 1));
            System.out.println("Suerte: " + suerte + " aux: " + aux);
            while (suerte > aux) {

                i++;
                aux = ( (1 / (double)nOperations) * (i + 1));
                System.out.println("Suerte: " + suerte + " aux: " + aux);
            }
            System.out.println("Salimos:");
            System.out.println("Suerte: " + suerte + " aux: " + aux);
            System.out.println("Operacion: " + i + " es la seleccionada");

        } else {
            System.out.println("Operacion: " + 0 + " es la seleccionada");

        }

    }
}
