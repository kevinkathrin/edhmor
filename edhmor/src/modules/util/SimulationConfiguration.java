/* 
 * EDHMOR - Evolutionary designer of heterogeneous modular robots
 * <https://bitbucket.org/afaina/edhmor>
 * Copyright (C) 2015 GII (UDC) and REAL (ITU)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package modules.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import modules.evaluation.VrepSimulator;
import org.apache.commons.configuration.XMLConfiguration;

/**
 * SimulationConfiguration stores all the parameters of the simulation. 
 * <p>
 * The parameters of the simulation are loaded, when the progam starts, from
 * simulationControl.xml. This file has to be placed in the same 
 * working directory as the main program. It throws an exception if a 
 * parameter is not well defined in the file.
 * <p> 
 *
 * SimulationConfiguration.java
 * Created on 
 * @author Andres FaiÃ±a <anfv  at itu.dk>
 */
public class SimulationConfiguration {



    //Parameters of the simulation
    private static int serverId;
    private static int waitTimeLoadSimulator;
    private static String simulator;
    private static double maxSimulationTime;
    private static double timeStartSim;
    private static int nAttempts;
    private static boolean useMPI;
    private static double poseUpdateRate;
    private static String vrepPath;
    private static int vrepStartingPort = 45000;
    private static VrepSimulator vrep = null;

    //Parametes of the modules 
    private static String moduleSet;

    //Parameters of the tree
    private static int nMaxModulesIni;
    private static boolean fistModulesBase;
    private static boolean manipulatorBase;
    private static int firstNumConnections = 1000;
    private static int minTypeModules;
    private static int maxTypeModules;
    private static int nMaxConnections;

    //Parameters of the control
    private static String robotControllerStr;
    private static double maxPhaseControl;
    private static double minPhaseControl;
    private static double maxAmplitudeControl;
    private static double minAmplitudeControl;
    private static double maxAngularFreqControl;
    private static double minAngularFreqControl;

    private static double maxFrequencyModulator;
    private static double minFrequencyModulator;
    private static double maxAmplitudeModulator;
    private static double minAmplitudeModulator;

    private static double maxWeighing;
    private static double minWeighing;

    private static boolean usePhaseControl;
    private static boolean useAmplitudeControl;
    private static boolean useAngularFControl;

    private static boolean useAmplitudeModulator;
    private static boolean useFrequencyModulator;
    private static boolean useBranchFrequencyControl;


    //various
    private static boolean debug;
    private static int jobId = 0;
    private static int maxModules = 1;

    //Parameters of the world/scene
    private static int numberOfWorldsBase = 1;
    private static List<String> worldsBase = new ArrayList<String>();
    private static String functionToEvaluateWorlds = "min";

    //Parameters of the fitness function
    private static double maxFitnessValue = 5;
    private static boolean energyShare = false;
    private static boolean improvedFitness = false;
    private static double threshold = 0.0;
    private static double fitnessIncrease = 0.0;
    private static String fitnessFunctionStr;
    private static int fitnessFunctionIndicator = 0;
    private static int numberOfFitnessParameters = 1;
    private static List<Double> fitnessParameters = new ArrayList<Double>();
    private static double timeIniFitness = 5, timeEndFitness = -1;
    private static double penaltyFitness = 0.8;
    private static String poseFitness = "BASE";

    static {
        try {

            XMLConfiguration config = new XMLConfiguration("edhmor/simulationControl.xml");



            //Parameters of the module Set
            SimulationConfiguration.moduleSet = config.getString("ModuleSet");

            //Parametros de la simulacion
            SimulationConfiguration.simulator = config.getString("Simulation.Simulator");
            SimulationConfiguration.serverId = config.getInt("Simulation.ServerId");
            SimulationConfiguration.waitTimeLoadSimulator = config.getInt("Simulation.WaitTimeLoadSimulator");
            SimulationConfiguration.maxSimulationTime = config.getDouble("Simulation.MaxSimulationTime");
            SimulationConfiguration.timeStartSim = config.getDouble("Simulation.TimeStartSimulation");
            SimulationConfiguration.nAttempts = config.getInt("Simulation.Attempts");
            SimulationConfiguration.useMPI = config.getBoolean("Simulation.UseMPI");
            SimulationConfiguration.poseUpdateRate = config.getDouble("Simulation.PoseUpdateRate");
            if(SimulationConfiguration.simulator.toLowerCase().contentEquals("vrep"))
                SimulationConfiguration.vrepPath = config.getString("Simulation.Vrep.VrepPath");

            //Parametros del mundo
            SimulationConfiguration.numberOfWorldsBase = config.getInt("Worlds.NumberOfWorldsBase");
            for(int i=0; i<numberOfWorldsBase; i++){
                worldsBase.add(config.getString("Worlds.WorldBase" + i));
                if(worldsBase.get(i).isEmpty() || worldsBase.get(i)==null || worldsBase.get(i).equals(""))
                    throw new Exception("Error loading the worlds/scenes: \n");
            }
            SimulationConfiguration.functionToEvaluateWorlds = config.getString("Worlds.FunctionToEvaluateWorlds");


            //Parametros del control
            SimulationConfiguration.robotControllerStr = config.getString("Control.RobotController");
            SimulationConfiguration.maxPhaseControl = config.getDouble("Control.PhaseControl.MaxValue");
            SimulationConfiguration.minPhaseControl = config.getDouble("Control.PhaseControl.MinValue");
            SimulationConfiguration.maxAmplitudeControl = config.getDouble("Control.AmplitudeControl.MaxValue");
            SimulationConfiguration.minAmplitudeControl = config.getDouble("Control.AmplitudeControl.MinValue");
            SimulationConfiguration.maxAngularFreqControl = config.getDouble("Control.AngularFreqControl.MaxValue");
            SimulationConfiguration.minAngularFreqControl = config.getDouble("Control.AngularFreqControl.MinValue");

            SimulationConfiguration.maxAmplitudeModulator = config.getDouble("Control.AmplitudeModulator.MaxValue");
            SimulationConfiguration.minAmplitudeModulator = config.getDouble("Control.AmplitudeModulator.MinValue");
            SimulationConfiguration.maxFrequencyModulator = config.getDouble("Control.FrequencyModulator.MaxValue");
            SimulationConfiguration.minFrequencyModulator = config.getDouble("Control.FrequencyModulator.MinValue");

            SimulationConfiguration.maxWeighing = config.getDouble("Control.Weighing.MaxValue");
            SimulationConfiguration.minWeighing = config.getDouble("Control.Weighing.MinValue");


            SimulationConfiguration.usePhaseControl = config.getBoolean("Control.UsePhaseControl");
            SimulationConfiguration.useAmplitudeControl = config.getBoolean("Control.UseAmplitudeControl");
            SimulationConfiguration.useAngularFControl = config.getBoolean("Control.UseAngularFreqControl");

            SimulationConfiguration.useAmplitudeModulator = config.getBoolean("Control.UseAmplitudeModulator");
            SimulationConfiguration.useFrequencyModulator = config.getBoolean("Control.UseFrequencyModulator");
            SimulationConfiguration.useBranchFrequencyControl = config.getBoolean("Control.BranchFrequencyModulator");
            if(useBranchFrequencyControl && !useFrequencyModulator){
                throw new Exception("Error in the parameters of frequency control: \n" +
                        "useFrequencyControl: " + useFrequencyModulator +
                        ";  useBranchFrequencyControl: " + useBranchFrequencyControl);
            }



            //Tree parameters
            SimulationConfiguration.nMaxModulesIni = config.getInt("Tree.NMaxModulesIni");
            SimulationConfiguration.fistModulesBase = config.getBoolean("Tree.FirstModuleBase");
            SimulationConfiguration.manipulatorBase = config.getBoolean("Tree.ManipulatorBase");
            SimulationConfiguration.firstNumConnections = config.getInt("Tree.FirstNumConnections");
            SimulationConfiguration.minTypeModules = config.getInt("Tree.TypeModules.MinValue");
            SimulationConfiguration.maxTypeModules = config.getInt("Tree.TypeModules.MaxValue");
            SimulationConfiguration.nMaxConnections = config.getInt("Tree.NMaxConnections");



            //Fitness parameters
            SimulationConfiguration.maxFitnessValue = config.getDouble("Fitness.MaxFitnessValue");
            SimulationConfiguration.energyShare = config.containsKey("Fitness.EnergyShare");
            SimulationConfiguration.improvedFitness = config.containsKey("Fitness.ImprovedFitness.Threshold") && config.containsKey("Fitness.ImprovedFitness.FitnessIncrease");
            if (SimulationConfiguration.improvedFitness) {
                SimulationConfiguration.threshold = config.getDouble("Fitness.ImprovedFitness.Threshold");
                SimulationConfiguration.fitnessIncrease = config.getDouble("Fitness.ImprovedFitness.FitnessIncrease");
            }

            fitnessFunctionStr = config.getString("Fitness.FitnessFunction.Name");
            SimulationConfiguration.setFitnessFunctionStr(fitnessFunctionStr);
            SimulationConfiguration.numberOfFitnessParameters = config.getInt("Fitness.NumberOfFitnessParameters");
            for(int i=0; i<numberOfFitnessParameters; i++){
                fitnessParameters.add(config.getDouble("Fitness.FitnessParameter" + i));
            }
            SimulationConfiguration.timeIniFitness = config.getDouble("Fitness.FitnessFunction.TimeIni");
            SimulationConfiguration.timeEndFitness = config.getDouble("Fitness.FitnessFunction.TimeEnd");
            SimulationConfiguration.penaltyFitness = config.getDouble("Fitness.FitnessFunction.Penalty");
            SimulationConfiguration.poseFitness = config.getString("Fitness.FitnessFunction.Pose");

            //Various
            SimulationConfiguration.debug = config.getBoolean("Debug");

            config = new XMLConfiguration("edhmor/walk_config.xml");
            SimulationConfiguration.maxModules = (config.getInt("Population.Individual.Chromosome[@size]") + 3) / 9;


        } catch (Exception e) {
            //Error loading the parameters of the simulation
            System.err.println("Error loading the parameters of the simulation.");
            System.out.println(e);
            System.exit(-1);
        }
    }

    public SimulationConfiguration() {
    }


    public static int getSeverId() {
        return serverId;
    }

    public static boolean isFistModulesBase() {
        return fistModulesBase;
    }

    public static int getMaxTypeModules() {
        return maxTypeModules;
    }

    public static int getMinTypeModules() {
        return minTypeModules;
    }

    public static int getAttempts() {
        return nAttempts;
    }

    public static int getNMaxConnections() {
        return nMaxConnections;
    }

    public static double getTimeStartSim() {
        return timeStartSim;
    }

    public static int getWaitTimeLoadSimulator() {
        return waitTimeLoadSimulator;
    }

    public static double getMaxPhaseControl() {
        return maxPhaseControl;
    }

    public static double getMaxAmplitudeControl() {
        return maxAmplitudeControl;
    }

    public static double getMaxAmplitudeModulator() {
        return maxAmplitudeModulator;
    }

    public static double getMaxAngularFreqControl() {
        return maxAngularFreqControl;
    }

    public static double getMinAmplitudeControl() {
        return minAmplitudeControl;
    }

    public static double getMinAmplitudeModulator() {
        return minAmplitudeModulator;
    }

    public static double getMinAngularFreqControl() {
        return minAngularFreqControl;
    }

    public static double getMaxWeighing() {
        return maxWeighing;
    }

    public static double getMinPhaseControl() {
        return minPhaseControl;
    }

    public static double getMaxFrequencyModulator() {
        return maxFrequencyModulator;
    }

    public static void setMaxFrequencyModulator(double maxFrequencyModulator) {
        SimulationConfiguration.maxFrequencyModulator = maxFrequencyModulator;
    }

    public static double getMinFrequencyModulator() {
        return minFrequencyModulator;
    }

    public static void setMinFrequencyModulator(double minFrequencyModulator) {
        SimulationConfiguration.minFrequencyModulator = minFrequencyModulator;
    }

    public static double getMinWeighing() {
        return minWeighing;
    }

    public static int getFirstNumConnections() {
        return firstNumConnections;
    }

    public static boolean isUseMPI() {
        return useMPI;
    }

    public static int getNMaxModulesIni() {
        return nMaxModulesIni;
    }

    public static boolean isDebug() {
        return debug;
    }

    public static int getJobId() {
        return jobId;
    }

    public static void setJobId(int jobId) {
        SimulationConfiguration.jobId = jobId;
    }

    public static int getMaxModules() {
        return maxModules;
    }

    public static void setMaxModules(int maxModules) {
        SimulationConfiguration.maxModules = maxModules;
    }

    public static int getFitnessFunction() {
        return fitnessFunctionIndicator;
    }

    public static String getFitnessFunctionStr() {
        return fitnessFunctionStr;
    }

    /*Depreciated. Legacy code for the gazebo simulator*/
    public static void setFitnessFunctionStr(String str) {
        fitnessFunctionStr = str;
        if (fitnessFunctionStr.contentEquals("useWalkDistance")) {
            fitnessFunctionIndicator = 0;
        } else if (fitnessFunctionStr.contentEquals("useWalkDistance45Degrees")) {
            fitnessFunctionIndicator = 1;
        } else if (fitnessFunctionStr.contentEquals("useWalkDistanceXMinusAbsY")) {
            fitnessFunctionIndicator = 2;
        } else if (fitnessFunctionStr.contentEquals("useWalkDistanceX")) {
            fitnessFunctionIndicator = 3;
        } else if (fitnessFunctionStr.contentEquals("useMaxHeight")) {
            fitnessFunctionIndicator = 10;
        } else if (fitnessFunctionStr.contentEquals("useMaxHeightWithLowTurns")) {
            fitnessFunctionIndicator = 11;
        } else if (fitnessFunctionStr.contentEquals("useMaxZMovement")) {
            fitnessFunctionIndicator = 20;
        } else if (fitnessFunctionStr.contentEquals("useMaxTurn")) {
            fitnessFunctionIndicator = 30;
        }else if (fitnessFunctionStr.contentEquals("useMaxTurnConCarga")) {
            fitnessFunctionIndicator = 31;
        }else if (fitnessFunctionStr.contentEquals("usePathZ")) {
            fitnessFunctionIndicator = 40;
        } else if (fitnessFunctionStr.contentEquals("useX-YWithoutFallStairs")) {
            fitnessFunctionIndicator = 50;
        }else if (fitnessFunctionStr.contentEquals("useCargaWithoutFall")) {
            fitnessFunctionIndicator = 60;
        }else if (fitnessFunctionStr.contentEquals("useCargaWithoutFallAndWithoutFallStairs")) {
            fitnessFunctionIndicator = 70;
        }else if (fitnessFunctionStr.contentEquals("usePaintWall")) {
            fitnessFunctionIndicator = 150;
        } else if (fitnessFunctionStr.contentEquals("useCleanFloor")) {
            fitnessFunctionIndicator = 200;
        }
//           else{
//            try {
//                throw new Exception("Wrong name for the fitness function!" +
//                        "fitnessFunctionStr: " + fitnessFunctionStr);
//            } catch (Exception ex) {
//                Logger.getLogger(SimulationConfiguration.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }

    }

    public static List<String> getWorldsBase() {
        return worldsBase;
    }

    public static void setWorldsBase(List<String> worldsBase) {
        SimulationConfiguration.worldsBase = worldsBase;
    }

    public static String getFunctionToEvaluateWorlds() {
        return functionToEvaluateWorlds;
    }

    public static void setFunctionToEvaluateWorlds(String functionToEvaluateWorlds) {
        SimulationConfiguration.functionToEvaluateWorlds = functionToEvaluateWorlds;
    }

    public static double getMaxFitnessValue() {
        return maxFitnessValue;
    }

    public static void setMaxFitnessValue(double maxFitnessValue) {
        SimulationConfiguration.maxFitnessValue = maxFitnessValue;
    }

    public static boolean isUseFrequencyModulator() {
        return useFrequencyModulator;
    }

    public static void setUseFrequencyModulator(boolean useFrequencyControl) {
        SimulationConfiguration.useFrequencyModulator = useFrequencyControl;
    }

    public static boolean isUseBranchFrequencyControl() {
        return useBranchFrequencyControl;
    }

    public static void setUseBranchFrequencyControl(boolean useBranchFrequencyControl) {
        SimulationConfiguration.useBranchFrequencyControl = useBranchFrequencyControl;
    }

    public static boolean isShareEnergy() {
        return energyShare;
    }

    public static void setEnergyShare(boolean energyShare) {
        SimulationConfiguration.energyShare = energyShare;
    }

    public static boolean isImprovedFitness() {
        return improvedFitness;
    }

    public static void setImprovedFitness(boolean improvedFitness) {
        SimulationConfiguration.improvedFitness = improvedFitness;
    }

    public static double getFitnessIncrease() {
        return fitnessIncrease;
    }

    public static void setFitnessIncrease(double fitnessIncrease) {
        SimulationConfiguration.fitnessIncrease = fitnessIncrease;
    }

    public static double getThreshold() {
        return threshold;
    }

    public static void setThreshold(double threshold) {
        SimulationConfiguration.threshold = threshold;
    }

    public static boolean isUseAmplitudeControl() {
        return useAmplitudeControl;
    }

    public static void setUseAmplitudeControl(boolean useAmplitudeControl) {
        SimulationConfiguration.useAmplitudeControl = useAmplitudeControl;
    }

    public static boolean isUseAmplitudeModulator() {
        return useAmplitudeModulator;
    }

    public static void setUseAmplitudeModulator(boolean useAmplitudeModulator) {
        SimulationConfiguration.useAmplitudeModulator = useAmplitudeModulator;
    }

    public static boolean isUseAngularFControl() {
        return useAngularFControl;
    }

    public static void setUseAngularFControl(boolean useAngularFControl) {
        SimulationConfiguration.useAngularFControl = useAngularFControl;
    }

    public static boolean isUsePhaseControl() {
        return usePhaseControl;
    }

    public static void setUsePhaseControl(boolean usePhaseControl) {
        SimulationConfiguration.usePhaseControl = usePhaseControl;
    }

    public static double getPoseUpdateRate() {
        return poseUpdateRate;
    }

    public static void setPoseUpdateRate(double poseUpdateRate) {
        SimulationConfiguration.poseUpdateRate = poseUpdateRate;
    }

    public static List<Double> getFitnessParameters() {
        return fitnessParameters;
    }

    public static void setFitnessParameters(List<Double> fitnessParameters) {
        SimulationConfiguration.fitnessParameters = fitnessParameters;
    }

    public static boolean isManipulatorBase() {
        return manipulatorBase;
    }

    public static void setManipulatorBase(boolean manipulatorBase) {
        SimulationConfiguration.manipulatorBase = manipulatorBase;
    }

    public static String getModuleSet() {
        return moduleSet;
    }

    public static void setModuleSet(String moduleSet) {
        SimulationConfiguration.moduleSet = moduleSet;
    }

    public static String getSimulator() {
        return simulator;
    }

    public static void setSimulator(String simulator) {
        SimulationConfiguration.simulator = simulator;
    }

    public static double getMaxSimulationTime() {
        return maxSimulationTime;
    }

    public static void setMaxSimulationTime(double maxSimulationTime) {
        SimulationConfiguration.maxSimulationTime = maxSimulationTime;
    }

    public static String getVrepPath() {
        return vrepPath;
    }

    public static int getVrepStartingPort() {
        return vrepStartingPort;
    }

    public static void setVrepStartingPort(int vrepStartingPort) {
        SimulationConfiguration.vrepStartingPort = vrepStartingPort;
    }

    public static double getTimeIniFitness() {
        return timeIniFitness;
    }

    public static void setTimeIniFitness(double timeIniFitness) {
        SimulationConfiguration.timeIniFitness = timeIniFitness;
    }

    public static double getTimeEndFitness() {
        return timeEndFitness;
    }

    public static void setTimeEndFitness(double timeEndFitness) {
        SimulationConfiguration.timeEndFitness = timeEndFitness;
    }

    public static double getPenaltyFitness() {
        return penaltyFitness;
    }

    public static void setPenaltyFitness(double penaltyFitness) {
        SimulationConfiguration.penaltyFitness = penaltyFitness;
    }

    public static String getPoseFitness() {
        return poseFitness;
    }

    public static void setPoseFitness(String poseFitness) {
        SimulationConfiguration.poseFitness = poseFitness;
    }

    public static VrepSimulator getVrep() {
        return vrep;
    }

    public static void setVrep(VrepSimulator vrep) {
        SimulationConfiguration.vrep = vrep;
    }

    public static String getRobotControllerStr() {
        return robotControllerStr;
    }

    public static void setRobotControllerStr(String s) {robotControllerStr = s;}



}
