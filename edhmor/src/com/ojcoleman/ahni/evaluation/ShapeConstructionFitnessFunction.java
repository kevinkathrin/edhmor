package com.ojcoleman.ahni.evaluation;

import com.anji.integration.Activator;
import com.anji.integration.AnjiActivator;
import com.anji.integration.AnjiNetTranscriber;
import com.anji.nn.AnjiNet;
import com.ojcoleman.ahni.evaluation.novelty.Behaviour;
import com.ojcoleman.ahni.hyperneat.Properties;
import modules.grid.MorphologyCreator;
import modules.grid.modulegrid.DrawStructure;
import modules.grid.modulegrid.ModuleGrid;
import modules.grid.ShapeFitnessCalculator;
import modules.util.SimulationConfiguration;
import org.jgapcustomised.Chromosome;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

/**
 * Created by i5-4670K on 18-04-2017.
 */
public class ShapeConstructionFitnessFunction extends HyperNEATFitnessFunction {

    private static final String MAX_NODES = "fitness.max_nodes";
    private static final String GRID_VALUES = "fitness.grid_values";
    private static final String MODULE_SET = "fitness.module_set";
    private static final String SHAPE_TYPE = "fitness.shape_type";
    private static final String VALIDATOR_ARGUMENTS = "fitness.validator_arguments";
    private int maxNodes;
    private int[] gridSize;
    private String testFolder;
    private String shape;
    private int[] args;

    @Override
    public void init(Properties props) {
        super.init(props);
        maxNodes = props.getIntProperty(MAX_NODES);
        gridSize = props.getIntArrayProperty(GRID_VALUES);
        SimulationConfiguration.setModuleSet(props.getProperty(MODULE_SET));

        shape = props.getProperty(SHAPE_TYPE);
        args = props.getIntArrayProperty(VALIDATOR_ARGUMENTS);


        Date d = new Date();
        testFolder = props.getOutputDirPath() + "/" + d.toString().replaceAll("\\s+", "").replaceAll(":","");
        boolean success = (new File(testFolder )).mkdirs();
        if(!success){
            System.out.println("HEY");
        }

        if(shape.equals("symmetry_1") || shape.equals("box_2") || shape.equals("box_3") ) {
            if (gridSize[0] != 10)
                throw new IllegalArgumentException("GRID SIZE MUST BE 10 ON ALL AXIS' FOR SYMMETRY SHAPE TESTS!");
        }
        args = gridSize;

    }

    @Override
    protected void evaluate(Chromosome genotype, Activator substrate, int evalThreadIndex, double[] fitnessValues, Behaviour[] behaviours) {
        super.evaluate(genotype, substrate, evalThreadIndex, fitnessValues, behaviours);
    }

    @Override
    protected double evaluate(Chromosome genotype, Activator substrate, int evalThreadIndex) {

        ModuleGrid grid = MorphologyCreator.constructShapeNEAT(substrate, maxNodes, gridSize);

        double fitness = ShapeFitnessCalculator.evaluate(grid, shape, args);

        genotype.setPerformanceValue(fitness);

        //Performance increase, save updated image and info
        if(genotype == lastBestChrom && bestPerformance < fitness || fitness == 1.0){
            bestPerformance = fitness;
            String id = genotype.getId().toString();
            String folderPath = testFolder + "/" + id;
            File folder = new File(folderPath);
            if(!folder.exists() || fitness == 1.0) {
                boolean success = folder.mkdirs();

                BufferedImage image = DrawStructure.getImages(
                        grid.getGridPositionMap().values(),
                        grid.getPosition(grid.getRootNode()));


                File f = new File(testFolder + "/" + id + "/" + "Image" + ".png");

                try {
                    ImageIO.write(image, "png", f);
                } catch (IOException e) {
                    e.printStackTrace();
                }


                try {
                    AnjiNetTranscriber cppnTranscriber = (AnjiNetTranscriber) props.singletonObjectProperty(AnjiNetTranscriber.class);
                    AnjiNet cppn = ((AnjiActivator) cppnTranscriber.transcribe(genotype)).getAnjiNet();
                    BufferedWriter cppnFile = new BufferedWriter(new FileWriter(testFolder + "/" + id + "/" + "cppn.xml"));
                    String cppnXml = cppn.toXml() + " \n PERFORMANCE VALUE:" + fitness;
                    cppnFile.write(cppnXml);
                    cppnFile.close();
                } catch (Exception e) {
                    System.err.println("Error transcribing CPPN for display:\n" + e.getMessage());
                }
            }
        }
        return fitness;
    }
}
