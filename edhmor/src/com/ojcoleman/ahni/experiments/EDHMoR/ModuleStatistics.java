package com.ojcoleman.ahni.experiments.EDHMoR;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by i5-4670K on 17-01-2018.
 */
public class ModuleStatistics {

    ArrayList<Integer> dataArray;
    int population, generations;


    public ModuleStatistics(int pop, int gen){
        population = pop;
        generations = gen;
        dataArray = new ArrayList<>();
    }


    public synchronized void addEntry(int numberOfModules){
        dataArray.add(numberOfModules);
    }



    public double[] computeGenerationData(){
        double sum = 0;
        for(int d : dataArray){
            sum += d;
        }
        dataArray.clear();
        double mean = sum / (double) population;
        return new double[]{mean};
    }
}
