package com.ojcoleman.ahni.experiments.EDHMoR;

import com.anji.integration.Activator;
import com.anji.integration.AnjiActivator;
import com.anji.integration.AnjiNetTranscriber;
import com.anji.nn.AnjiNet;
import com.ojcoleman.ahni.evaluation.BulkFitnessFunctionMT;
import com.ojcoleman.ahni.evaluation.novelty.Behaviour;
import com.ojcoleman.ahni.hyperneat.Properties;
import es.udc.gii.common.eaf.util.EAFRandom;
import modules.evaluation.overlapping.CollisionDetector;
import modules.evaluation.overlapping.CollisionDetectorFactory;
import modules.grid.MorphologyCreator;
import modules.grid.modulegrid.ModuleGrid;
import modules.evaluation.VrepEvaluator;
import modules.evaluation.VrepSimulator;
import modules.individual.TreeIndividual;
import modules.test.operations.TestSymmetry;
import modules.util.SimulationConfiguration;
import org.apache.log4j.Logger;
import org.jgapcustomised.Chromosome;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Stack;

/**
 * Created by Kevin on 23-Mar-17.
 */
public class NEATWalkingFitnessFunction extends BulkFitnessFunctionMT{
    private static final String MODULE_SET = "fitness.module_set" ;
    private static long serialVersionUID = 1L;
    private static final String START_PORT = "fitness.start_port";
    private static final String START_SIMULATIONS = "fitness.start_simulations";
    private static final String START_VREP = "fitness.start_vrep";
    private static final String GRID_VALUES = "fitness.grid_values";
    private static final String TEST_TYPE = "fitness.test_type";
    private static final String MAX_NODES = "fitness.max_nodes";
    private static final String PREDEFINED_CONTROLLER = "fitness.predefined_controller";
    private static final String FIXED_CONTROLLER_NAME = "fitness.fixed_controller";

    private static final Object lockGet = new Object(), lockPut = new Object();
    private static int startPort;
    private int maxNodes;
    private int[] gridvalues;
    private static Logger logger = Logger.getLogger(NEATWalkingFitnessFunction.class);
    private static Stack<VrepSimulator> simulators = new Stack<>();
    private int chromosomeLength;
    private ModuleStatistics statisticsGenerator;


    public NEATWalkingFitnessFunction(){

    }

    @Override
    protected void evaluate(Chromosome genotype, Activator substrate, int evalThreadIndex, double[] fitnessValues, Behaviour[] behaviours) {
        super.evaluate(genotype, substrate, evalThreadIndex, fitnessValues, behaviours);

    }

    @Override
    protected double evaluate(Chromosome genotype, Activator substrate, int evalThreadIndex) {




        VrepSimulator port;
        if(props.getBooleanProperty(START_SIMULATIONS)) {
            synchronized (lockGet) {    //To avoid race condition.
                while (simulators.size() == 0) {  //Only if a simulator is 'free'
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                port = simulators.pop();
            }
        }
        else{
            port = null;
        }
        double fitness,performance = 0.0;
        TreeIndividual tree = null;
        switch (props.getProperty(TEST_TYPE)){  //Test type should be set in props file
            case "DummyTest":
                fitness = TestSymmetry.fitnessTest(port);
                break;
            case "walkingTest":
                ModuleGrid mg = MorphologyCreator.constructShapeNEAT(substrate, maxNodes, gridvalues);
                tree = new TreeIndividual();
                tree.init(chromosomeLength);
                tree.setRootNode(mg.getRootNode());
                tree.modifyChromosome();
                CollisionDetector cs = CollisionDetectorFactory.getCollisionDetector();
                cs.loadTree(tree);
                statisticsGenerator.addEntry(mg.numberOfNodes());
                if(cs.isFeasible()) {
                    VrepEvaluator evaluator = new VrepEvaluator(tree.getChromosomeAt(0), "", 0, port);
                    evaluator.getVrepPing();
                    evaluator.setMaxSimulationTime(20);
                    fitness = evaluator.evaluate();
                } else
                    fitness = 0.0;

                performance = fitness / 5.0;
                break;
            default:
                System.out.println("given test type not found, printing out substrate setup");
                System.out.println("INPUT SIZE: " + substrate.getInputCount());
                System.out.println("OUTPUT SIZE " + substrate.getOutputCount());
                System.out.println("MAX OUTPUT: " + substrate.getMaxResponse());
                System.out.println("MIN OUTPUT: " + substrate.getMinResponse());
                System.out.println("NAME: " + substrate.getName());
                fitness = 0.0;
                if(port != null)
                    port.disconnect();
                System.exit(1);
        }

        //Write when significant increases in fitness happens
        if(bestPerformance * 1.2 < performance && tree != null ) {

            String testFolder = props.getOutputDirPath();
            try {
                AnjiNetTranscriber cppnTranscriber = (AnjiNetTranscriber) props.singletonObjectProperty(AnjiNetTranscriber.class);
                AnjiNet cppn = ((AnjiActivator) cppnTranscriber.transcribe(genotype)).getAnjiNet();
                BufferedWriter cppnFile = new BufferedWriter(new FileWriter(testFolder + "/" + "cppn" + fitness + ".xml"));
                String cppnXml = cppn.toXml() + " \n CHROMOSOME:" + tree.toString();
                cppnFile.write(cppnXml);
                cppnFile.close();
            } catch (Exception e) {
                System.err.println("Error transcribing CPPN for display:\n" + e.getStackTrace());
            }
            bestPerformance = performance;
        }

        genotype.setPerformanceValue(performance);              //Set value.
        if(props.getBooleanProperty(START_SIMULATIONS)) {
            synchronized (lockPut) {    //Again to avoid race
                simulators.push(port);
            }
        }
        //return fitness of evaluated genome.
        return performance;
    }

    public void init(Properties props){
        boolean initialKill = false;
        super.init(props);
        int max_threads = props.getIntProperty(MAX_THREADS_KEY);
        maxNodes = props.getIntProperty(MAX_NODES);
        startPort = props.getIntProperty(START_PORT);   //Initial port number...
        gridvalues = props.getIntArrayProperty(GRID_VALUES);
        chromosomeLength = maxNodes * 8 + 13;
        chromosomeLength = 141;
        SimulationConfiguration.setVrepStartingPort(startPort);
        String controller;
        if((controller = props.getProperty(FIXED_CONTROLLER_NAME, null)) != null && props.getBooleanProperty(PREDEFINED_CONTROLLER)){

            SimulationConfiguration.setRobotControllerStr(controller);
        }

        statisticsGenerator = new ModuleStatistics(props.getIntProperty("num.generations"), props.getIntProperty("popul.size"));

        //When starting new test, kill all current connections to VREP
        if(startPort == 19997)
            initialKill = true;

        //Create a new V-REP simulator for each thread with new port index.
        if(props.getBooleanProperty(START_SIMULATIONS)) {
            for (int i = 0; i < max_threads; i++) {
                System.out.println("Connecting to V-REP on port: " + (startPort + i));
                VrepSimulator sim = new VrepSimulator();
                sim.connect2Vrep(startPort + i, initialKill);
                initialKill = false;
                simulators.add(sim);
            }
            simulators.forEach((sim) -> System.out.println(sim.getClientID()));
        }
        EAFRandom.init();
        SimulationConfiguration.setModuleSet(props.getProperty(MODULE_SET));
        SimulationConfiguration.setFitnessFunctionStr("distanceTravelledAndBrokenConnPenalty");

    }

    @Override
    public void finaliseEvaluation() {
        super.finaliseEvaluation();

        super.finaliseEvaluation();
        double[] genData = statisticsGenerator.computeGenerationData();
        try {
            String testFolder = props.getOutputDirPath();
            BufferedWriter dataFile = new BufferedWriter(new FileWriter(testFolder + "/" + "ModuleData" + ".csv"));
            String dataString = "";
            for(double data : genData){
                dataString += data + ", ";
            }
            dataString += "\n";
            dataFile.write(dataString);
            dataFile.close();
        }catch (Exception e){
            System.err.print("FAILED TO SAVE MODULE DATA IN OUTPUT FOLDER");
        }

    }
}
