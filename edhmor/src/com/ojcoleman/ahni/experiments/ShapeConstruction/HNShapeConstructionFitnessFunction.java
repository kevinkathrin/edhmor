package com.ojcoleman.ahni.experiments.ShapeConstruction;

import com.anji.integration.Activator;
import com.ojcoleman.ahni.evaluation.HyperNEATFitnessFunction;
import com.ojcoleman.ahni.hyperneat.Properties;
import com.ojcoleman.ahni.transcriber.HyperNEATTranscriber;
import com.ojcoleman.ahni.util.Point;
import modules.util.SimulationConfiguration;
import org.jgapcustomised.Chromosome;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * Created by i5-4670K on 12-10-2017.
 */
public class HNShapeConstructionFitnessFunction extends HyperNEATFitnessFunction{
    //Setup props string values
    private static final String MAX_NODES = "fitness.max_nodes";
    private static final String GRID_SIZE = "fitness.grid_size";
    private static final String MODULE_SET = "fitness.module_set";
    private static final String DIMENSIONS = "fitness.dimensions";
    private int dim;
    private int size;
    private int maxNodes;
    private double[][] targetPattern2D;
    private double[][] startPattern2D;
    private static Logger logger = Logger.getLogger(HNShapeConstructionFitnessFunction.class);

    @Override
    public void init(Properties props) {
        super.init(props);

        SimulationConfiguration.setModuleSet(props.getProperty(MODULE_SET));
        maxNodes = props.getIntProperty(MAX_NODES);
        size = props.getIntProperty(GRID_SIZE);
        dim = props.getIntProperty(DIMENSIONS);
        int nodes = 7;
        //Generate test pattern
        switch (dim){
            case 2:
                targetPattern2D = generate2DPattern(size,nodes);
                break;
            case 3:

                break;
        }

    }

    private double[][] generate2DPattern(int size, int nodes){
        //TODO implement

        double[][] input = new double[size][size];
        startPattern2D = new double[size][size];

        //TODO make are more automated way of generating structure
        //Start solution hardcode structure
        int mid = size/2;
        input[mid][mid] = 1;
        input[mid+1][mid] = 1;
        input[mid+2][mid] = 1;
        input[mid+2][mid+1] = 1;
        input[mid+2][mid-1] = 1;
        input[mid-1][mid] = 1;
        input[mid-2][mid] = 1;

        for(double[] d : startPattern2D)
            Arrays.fill(d,0.0);
        startPattern2D[mid][mid] = 1;
        return input;
    }


    @Override
    protected double evaluate(Chromosome genotype, Activator substrate, int evalThreadIndex) {
        double[][] result = startPattern2D;
        double middleResponse = (substrate.getMinResponse() + substrate.getMaxResponse()) / 2.0;
        ArrayList<double[][]> inputs = new ArrayList<>();

        inputs.addAll(generatePossibleState(startPattern2D));

        int nodeCounter = 1;
        while(!inputs.isEmpty() && 7 > nodeCounter){
            //present possible states to substrate
            double[][][] outputs = substrate.nextSequence(inputs.toArray(new double[inputs.size()][size][size]));

            //Find highest valued state
            int favoriteState = -1;
            double currentMax = -1.0;
            for(int i = 0; i < outputs.length; i++){
                currentMax = outputs[i][0][0] > currentMax ? outputs[i][0][0] : currentMax;
                favoriteState = i;
            }

            //Check if favorite above threshold for using
            if(currentMax > middleResponse){
                double[][] selectedState = inputs.get(favoriteState);
                result = selectedState;
                inputs.clear();
                inputs.addAll(generatePossibleState(selectedState));
                nodeCounter++;

            }else{
                inputs.clear();
            }
        }

        //Calculate fitness value
        double fitness = evaluateShapeGrid(targetPattern2D, result, maxNodes, nodeCounter);

        genotype.setPerformanceValue(fitness);
        return fitness;
    }

    private static double evaluateShapeGrid(double[][] expected, double[][] actual, int maxNodes, int shape_nodes){
        if(shape_nodes == 1)
            return 0.0;
        int numberOfWrongs = 0, numberCorrect = 0;

        for(int i = 0; i < actual.length; i++){
            for(int j = 0; j < actual.length; j++){

                int ex = (int)expected[i][j];
                int ac = (int)actual[i][j];
                if(ac == 1 && ex == 1)
                    numberCorrect++;
                else if(ac == 1)
                    numberOfWrongs++;
                else if(ex == 1)
                    numberOfWrongs++;
            }
        }

        double maxErrors = shape_nodes + maxNodes;
        //double differenceNodes = Math.abs(shape_nodes - maxNodes);
        double propertionalFitness = 1 - ((double) (numberOfWrongs) / maxErrors);

        //Will not divide by 0 size the base cube is always correct.
        return propertionalFitness;
    }


    private Collection<? extends double[][]> generatePossibleState(double[][] state) {
        ArrayList<double[][]> possibleStates = new ArrayList<>();
        for(int i = 0; i < state.length; i++){
            for(int j = 0; j < state.length; j++){
                if(state[i][j] == 1){

                    if(i+1 < state.length && state[i+1][j] != 1){
                        double[][] pState = copyPattern(state);
                        pState[i+1][j] = 1;
                        possibleStates.add(pState);
                    }
                    if(i-1 < state.length && state[i-1][j] != 1){
                        double[][] pState = copyPattern(state);
                        pState[i-1][j] = 1;
                        possibleStates.add(pState);
                    }
                    if(j+1 < state.length && state[i][j+1] != 1){
                        double[][] pState = copyPattern(state);
                        pState[i][j+1] = 1;
                        possibleStates.add(pState);
                    }
                    if(j-1 < state.length && state[i][j-1] != 1){
                        double[][] pState = copyPattern(state);
                        pState[i][j-1] = 1;
                        possibleStates.add(pState);
                    }
                }

            }
        }
        return possibleStates;
    }

    private double[][] copyPattern(double[][] state) {

        return Arrays.stream(state)
                .map(double[]::clone)
                .toArray((int length) -> new double[length][]);
    }

    @Override
    protected void scale(int scaleCount, int scaleFactor, HyperNEATTranscriber transcriber) {
        super.scale(scaleCount, scaleFactor, transcriber);
    }

    /*
    @Override
    public int[] getLayerDimensions(int layer, int totalLayerCount) {
        if (layer == 0) // Input layer.
            // 3 range sensors plus reward.
            return new int[] { size, size };
        else if (layer == totalLayerCount - 1) { // Output layer.
            return new int[] { 1, 1 }; // Action to perform next (left, forward, right).
        }
        return null;
    }

    @Override
    public Point[] getNeuronPositions(int layer, int totalLayerCount) {
        // Coordinates are given in unit ranges and translated to whatever range is specified by the
        // experiment properties.
        Point[] positions = null;
        if (layer == 0) { // Input layer.

            positions = new Point[size*size];
            int posIndex = 0;
            // Current state.
            for (int x = 0; x < size; x++) {
                for (int y = 0; y < size; y++) {
                    positions[posIndex++] = new Point((double) x / (size - 1), (double) y / (size - 1), 0);
                }
            }

        } else if (layer == totalLayerCount - 1) { // Output layer.

            positions = new Point[1];
            // Action to perform next (left, forward, right).
            positions[0] = new Point(0.5,0.,1);

        }

        return positions;
    }
*/
}
