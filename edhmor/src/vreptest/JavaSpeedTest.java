/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package vreptest;

import java.io.File;
import modules.evaluation.VrepEvaluator;


/**
 *
 * @author fai
 */
public class JavaSpeedTest {
    
    private int n_modulos = 6;
    private double calidad = 0;

    public static void main(String[] args) {
        
        
        
        int nPruebas=10;
        
        double cromosoma[]={4, 4, 4, 4, 4, 4, 4, 4, 0, 0, 0, 0, 0,
                           1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0,
                           1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0,
                           1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0,
                           0,   180, 180, 180,   180, 180, 180,   180, 180, 180,   0, 0, 0

        };
        
        
        String vrepcommand = "vrep.exe";

		/* Initialize a v-rep simulator */
		try {
//
//			ProcessBuilder qq = new ProcessBuilder(vrepcommand, "-h",
//					"C:/Users/anfv/Documents/git/RODRIGO_EVOLUTION/VREPControl/Maze/MazeBuilder01.ttt");
//
//			qq.directory(new File("C:\\V-REP3\\V-REP_PRO_EDU\\"));
//
//			File log = new File("Simout/log");
//			qq.redirectErrorStream(true);
//			qq.redirectOutput(ProcessBuilder.Redirect.appendTo(log));
//			qq.start();
//			
                        
                        //Runtime.getRuntime().exec("C:\\V-REP3\\V-REP_PRO_EDU\\" + vrepcommand + " -h", null, new File("C:\\V-REP3\\V-REP_PRO_EDU\\"));
                        //Thread.sleep(10000);    
                } catch (Exception e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}
        
        
        double [] calidades = new double[nPruebas];
        long [] tiempos = new long[nPruebas];

        double calMin=Double.MAX_VALUE, calMax=Double.NEGATIVE_INFINITY, calMedia=0, desviacionTipicaCal=0;
        long tempMin=Long.MAX_VALUE, tempMax=Long.MIN_VALUE, tempMedia=0, desviacionTipicaTemp=0;
        for(int i =0; i<nPruebas; i++){
            System.out.println("EVALUACION CALIDAD "+i);

            long tempIni, tempFinal, tiempo;
            tempIni=System.currentTimeMillis();
            VrepEvaluator evaluador = new VrepEvaluator(cromosoma);
            double calidad = evaluador.evaluate();
            tempFinal=System.currentTimeMillis();
            tiempo=tempFinal-tempIni;

            if (calidad>calMax)
                calMax=calidad;
            if (calidad<calMin)
                calMin=calidad;
            calMedia+=calidad;
            calidades[i]=calidad;

            if (tiempo>tempMax)
                tempMax=tiempo;
            if (tiempo<tempMin)
                tempMin=tiempo;
            tempMedia+=tiempo;
            tiempos[i]=tiempo;
        }
        calMedia=calMedia/nPruebas;
        tempMedia=tempMedia/nPruebas;
        
        for(int i =0; i<nPruebas; i++){
            System.out.println("Calidad " + i + ": " + calidades[i]);
            desviacionTipicaCal+=((calidades[i]-calMedia)*(calidades[i]-calMedia));
        }

        System.out.println();

        for(int i =0; i<nPruebas; i++){
            System.out.println("Tiempo " + i + ": " + tiempos[i]);
            desviacionTipicaTemp+=((tiempos[i]-tempMedia)*(tiempos[i]-tempMedia));
        }
        
        desviacionTipicaCal=Math.sqrt(desviacionTipicaCal/nPruebas);
        desviacionTipicaTemp= (long) Math.sqrt(desviacionTipicaTemp/nPruebas);
        System.out.println();
        System.out.println();

        System.out.println("Calidad Media= " + calMedia + ", Desviacion Tipica: " + desviacionTipicaCal);
        System.out.println("Calidad Maxima: " + calMax +", Calidad Minima: " + calMin);

        System.out.println();

        System.out.println("Tiempo Medio= " + tempMedia + ", Desviacion Tipica: " + desviacionTipicaTemp);
        System.out.println("Tiempo Maximo: " + tempMax +", Tiempo Minimo: " + tempMin);
    }

    public double getCalidad() {
        return calidad;
    }

}
