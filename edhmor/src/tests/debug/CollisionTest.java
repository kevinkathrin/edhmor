/*
 * EDHMOR - Evolutionary designer of heterogeneous modular robots
 * <https://bitbucket.org/afaina/edhmor>
 * Copyright (C) 2015 GII (UDC) and REAL (ITU)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package tests.debug;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import modules.ModuleSet;
import modules.ModuleSetFactory;
import modules.evaluation.overlapping.BoundingBoxCollisionDetector;
import modules.evaluation.VrepEvaluator;
import modules.individual.Connection;
import modules.individual.Node;
import modules.individual.TreeIndividual;
import modules.util.SimulationConfiguration;

/**
 * CollisionTest.java Created on 09/02/2016
 *
 * @author Ceyue Liu  <celi at itu.dk>
 */
public class CollisionTest {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Object[] obj2 ={ "OldEdhmorModules", "RodrigoModules_1_2_3" };  
        String s = (String) JOptionPane.showInputDialog(null,"Please choose the module:\n", "collisiontest", JOptionPane.PLAIN_MESSAGE, new ImageIcon("icon.png"), obj2, "OldEdhmorModules");  
        SimulationConfiguration.setModuleSet(s);
        ModuleSetFactory.reloadModuleSet();
        ModuleSet moduleSet = ModuleSetFactory.getModulesSet();
        
//        int type = 1; //type of  the parent module
        int orientation = 0;
        Node rootNode = null;
        
        if(s=="OldEdhmorModules"){
//           rootNode = new Node(2, 0, 0, 0, 0, 0, 0, null);
//           Node node1 = new Node(1, 0, 0, 0, 0, 0, 0, rootNode);
//           Connection conn = new Connection(rootNode, node1, 4, orientation);
//           rootNode.addChildren(node1, conn);
//        
//           Node node2 = new Node(1, 0, 0, 0, 0, 0, 0, node1);
//           Connection conn2 = new Connection(rootNode, node2, 2, orientation);
//           rootNode.addChildren(node2, conn2);
//        
//           Node node3 = new Node(1, 0, 0, 0, 0, 0, 0, rootNode);
//           Connection conn3 = new Connection(rootNode, node3, 0, orientation);
//           rootNode.addChildren(node3, conn3);
//           
//           Node node4 = new Node(1, 0, 0, 0, 0, 0, 0, rootNode);
//           Connection conn4 = new Connection(rootNode, node4, 5, orientation);
//           rootNode.addChildren(node4, conn4);
           
           rootNode = new Node(2, 0, 0, 0, 0, 0, 0, null);
           Node node1 = new Node(1, 0, 0, 0, 0, 0, 0, rootNode);
           Connection conn = new Connection(rootNode, node1, 1, orientation);
           rootNode.addChildren(node1, conn);
        
           Node node2 = new Node(1, 0, 0, 0, 0, 0, 0, rootNode);
           Connection conn2 = new Connection(rootNode, node2, 6, orientation);
           rootNode.addChildren(node2, conn2);
        
           Node node3 = new Node(1, 0, 0, 0, 0, 0, 0, node1);
           Connection conn3 = new Connection(node1, node3, 12, orientation);
           node1.addChildren(node3, conn3);
           
        }
       
        if(s=="RodrigoModules_1_2_3"){

           rootNode= new Node(1, 0, 0, 0, 0, 0, 0, null);
           Node node1 = new Node(1, 0, 0, 0, 0, 0, 0, rootNode);
           Connection conn = new Connection(rootNode, node1, 1, 0);
           rootNode.addChildren(node1, conn);
        
           Node node2 = new Node(1, 0, 0, 0, 0, 0, 0, node1);
           Connection conn2 = new Connection(node1, node2, 2, 0);
           node1.addChildren(node2, conn2);
        
           Node node3 = new Node(1, 0, 0, 0, 0, 0, 0, node2);
           Connection conn3 = new Connection(node2, node3, 2, orientation);
           node2.addChildren(node3, conn3);
        
           Node node4 = new Node(1, 0, 0, 0, 0, 0, 0, node3);
           Connection conn4 = new Connection(node3, node4, 2, orientation);
            node3.addChildren(node4, conn4);
        }

        TreeIndividual tree = new TreeIndividual();
        tree.init(141);
        tree.setRootNode(rootNode);
        tree.modifyChromosome();

        VrepEvaluator evaluator = new VrepEvaluator(tree.getChromosomeAt(0), "", 0);
        
        BoundingBoxCollisionDetector collisionDetector = new BoundingBoxCollisionDetector();
        collisionDetector.loadTree(tree);
        collisionDetector.isFeasible();
        
        System.out.println("\n\n");

        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Logger.getLogger(TestDebugNewOrientationMethod.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
