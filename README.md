# Evolutionary Designer of Heterogeneous Modular Robots (EDHMOR) #

[![Alt text for your video](https://j.gifs.com/yD8wAG.gif)](http://www.youtube.com/watch?v=71596tvtp_M) [![Alt text for your video](https://j.gifs.com/KdLAze.gif)](http://www.youtube.com/watch?v=ekhvwQANzPY)

How is it possible to automatically design a robot for a task and build it less than one hour? 
 
To reach this goal, the EDHMOR system uses modular robots and evolutionary algorithms. Modular robots are simple and autonomous robotic devices that can be attached together. They provide simple building blocks with sensing, computational and actuation capabilities that allow us to deploy complex robots in minutes. Modular robots have been tested in a lot of different tasks, but usually the design of the robot (how the modules are assembled between them) is selected by an expert user. In contrast, the EDHMOR system employs evolutionary algorithms to find a suitable design, also called morphology or configuration, which can be built in a few minutes by assembling the modules. This repository contains all the required files to evolve the morphology of the modular robot and the parameters of the controller for a specific task.

The main features of EDHMOR are:

* Code written in Java, multiplatform
* Supports homogeneous and heterogeneous modules 
* Modules can only have one degree of freedom (but we are working to remove this limitation)
* Evolutionary algorithms are provided by JEAF library
* Evaluation of the robot in Vrep or Gazebo 0.8 simulators 
* Control based on sinusoidal controllers
* Distributed evaluation of the individuals


## Description ##

EDHMOR uses JAVA Evolutionary Algorithm Framework (JEAF) and a few other libraries. All the needed libraries are included in the edhmor\lib folder.

The main class, MainClass.java, sets up the parameters of the evolutionary algorithm by reading a configuration file, walk_config.xml. This file stores all the parameters related to the evolutionary algorithm and has to be properly configured. The parameters related to modules, task to perform, environmental conditions and evaluation of the candidates are set in simulationControl.xml. Both files should be placed in the same folder as the executable (*.jar) file. In addition, all the libraries should be placed in a folder called lib.  


## Simulators ##

EDHMOR uses a simulator to evaluate the morphology and the control parameters of the robots. Currently, two simulators are supported: gazebo-0.8 and Vrep. 

### [Gazebo 0.8](http://playerstage.sourceforge.net/doc/Gazebo-manual-0.8.0-pre1-html/) ###
**Deprecated, just for legacy purposes. It is strongly recommended that you use the Vrep simulator. This simulator only works in Linux and with EDHMOR modules.** 

This old version of the Gazebo simulator has some modifications in order to achieve synchronous simulations. So, the physics engine updates the controllers of the modules in every time step. 

In order to use this simulator, it is necessary to compile its source code. This can be found in the gazebo folder. The control of the robots during the simulation is achieved by calling a C library, which is called by using JNI. This library is already compiled and included in the lib folder, but if it is necessary, it can be compiled. Its C source code is located in gazebo_generic_control folder.  

The models of the different EDHMOR modules (slider, telescopic, rotational, hinge, base and end effector) are in edhmor\models\gazebo. These models define the properties of each module and they are used to build the morphology of the robot. Additionally, there are files that define the environment of the robots. They are found in the edhmor\scenes\gazebo\worlds folder. These models and worlds folders have to be placed in the same folder as the executable file. 

The morphologies are built by the GazeboCreateRobot class (edhmor/src/ modules/evol/morphology/GazeboCreateRobot.java), which generates a world file (as an example see: edhmor/world0.xml). This world file has the selected world (flat terrain, with obstacles, etc.) and the morphology of the robot with references to the predefined models of the modules.

In each evaluation, the GazeboEvaluator class initiates a new Gazebo simulator with the world file to evaluate. Then, it calls the controller, the C library. As previously stated, JNI is used to call this library with the control parameters. The library communicates with gazebo in every time step in order to provide the new goal position of the motor for all the modules. After some seconds of evaluation, it calculates the fitness and returns that value to GazeboEvaluator. Finally, GazeboEvaluator closes the Gazebo simulator and deletes all the temporal files that were generated.

### [Vrep](http://www.coppeliarobotics.com/) ###

The EDHMOR system currently uses Vrep as default simulator. It adds new features in the simulation as force sensors in all the connections and collisions between the modules of the robot. They increase the realism of the simulation and help us to reduce the reality gap between the simulations and the real robot.

The models of the modules for the Vrep simulator can be found in edhmor\models\vrep. In order to provide these models to Vrep, copy them into Vrep_installation_path\models. It is also necessary to copy the scenes from edhmor\scenes\vrep into Vrep_installation_path\scenes.

The communications between the Java code and the simulator is achieved through sockets. In order to be able to communicate, Vrep has to be configured. Check the file remoteApiConnections.txt in the Vrep folder, the three variables inside this file have to be set to: 

portIndex1_port 		= 19997

portIndex1_debug 		= false

portIndex1_syncSimTrigger = true

In addition, the java program has to be able to find the vrep api library. This can be done by adding the path of the library to the environmental variable, LD_LIBRARY_PATH in linux or PATH in Windows. Alternatively, edit the project properties in the run menu by writing this line in the VM options:

-Djava.library.path=PATH_TO_THE_LIBRARY

As an example:

-Djava.library.path="C:\V-REP3\V-REP_PRO_EDU\programming\remoteApiBindings\java\lib\64Bit"


The evaluation is performed using the class VrepEvaluator.java. It creates a VrepCreateRobot instance, which loads the modules and the force sensors in Vrep in the correct location. Then, VrepEvaluator starts a loop where it checks the limit of the force sensors, sets the goal position of the motor for all the modules and triggers a new run of the physics engine. This loop is executed until the simulation time is reached. Finally, the position of all the modules is acquired and a fitness value is generated.

Currently, it is necessary to start the Vrep simulator manually. Start it without its Graphical User interface for fast simulations or with it if you want to see the simulations while they are running. We are working to start automatically the Vrep simulation if it is not running and to allow running multiple instances of the simulator at the same time.

## Graphical User Interface ##

The TestGUI.java class is a graphical tool to see the solutions found by the evolutionary algorithm. These solutions are stored in log files and each solution is a string of numbers. First, run the TestGUI program and copy and paste the solution in text field called “Individual”. Then, select the right parameters for modules, simulators and other environments and press run. The evaluation of the morphology and the control parameters will be performed in the selected simulator and the fitness or score of the solution will be displayed when the evaluation finishes. In addition, this tool also allows removing or adding modules to one robot. Finally, it can also display the relations between the modules as it can plot a robot as a tree, where each module is a node of the tree, in order to analyze how the robot is built.     

## References ##

This system evolves morphologies and controllers for a specific task using a heterogeneous modular system. The specifications of the heterogeneous modular system and an ad-hoc algorithm to deal with the deceptive search space of this problem can be found in the following papers:


[An evolution friendly modular architecture to produce feasible robots](https://www.researchgate.net/publication/265052601_An_evolution_friendly_modular_architecture_to_produce_feasible_robots)

[EDHMoR: Evolutionary designer of heterogeneous modular robots](http://www.sciencedirect.com/science/article/pii/S0952197613001838)

[Towards feasible virtual creatures by using modular robots](https://www.researchgate.net/publication/267326645_Towards_feasible_virtual_creatures_by_using_modular_robots)

Finally, some of the results are shown in this video:

http://vimeo.com/afaina/feasible-virtual-creatures